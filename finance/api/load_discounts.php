<?php
include "../../_config/db.php";
$valueSelected = $_POST['valueSelected'];
$sql = "SELECT preregistration_info.id as student_id, preregistration_info.student_number, preregistration_info.first_name, preregistration_info.middle_name, preregistration_info.last_name, preregistration_info.reference_no, discounts.description, discounts.amount, discounts.fee_amount_type, enrollment_student.fees_summary FROM  `student_discounts` LEFT OUTER JOIN discounts ON student_discounts.discount_id = discounts.id LEFT OUTER JOIN enrollment_student ON enrollment_student.id = student_discounts.enrollment_student_id LEFT OUTER JOIN preregistration_info ON preregistration_info.id = enrollment_student.student_id where enrollment_student.setting_id = '$valueSelected'";
$result = $db->query($sql);
$rowcount=mysqli_num_rows($result);
if ($rowcount == 0){
  echo '1';
}else{
$TotalDiscount = 0;
  echo '<thead class="e-thead primary">';
  echo '<tr>';
  echo '<th>STUD NO.</th>';
  echo '<th>LRN</th>';
  echo '<th>NAME</th>';
  echo '<th>DISCOUNT NAME</th>';
  echo '<th>PERCENTAGE</th>';
  echo '<th>AMOUNT</th>';
  echo '</tr>';
  echo '</thead>';
  echo '<tbody>';
while($row = $result->fetch_assoc()){
  $discount_percent = 0;
  $discount_amount = 0;
  $TotalTuition = 0;
  $student_id = $row['student_id'];
  $student_number = $row['student_number'];
  $first_name = $row['first_name'];
  $middle_name = $row['middle_name'];
  $last_name = $row['last_name'];
  $reference_no  = $row['reference_no'];
  $discount_name  = $row['description'];
  $discount_amount  = $row['amount'];
  $fee_amount_type  = $row['fee_amount_type'];
  $fees_summary = $row['fees_summary'];

  $someJSON = '['.$fees_summary.']';
  $someArray = json_decode($someJSON, true);
  foreach ($someArray as $key => $value) {
     $TotalTuition = $value["tuition_fee"];
      }

  if ($fee_amount_type !== '1'){
  $discount_percent = $discount_amount * 100;
  $discount_amount_total = $discount_amount * $TotalTuition;
}else{
  $discount_amount_total = $discount_amount;
}



$TotalDiscount +=  $discount_amount_total;
echo  '<tr>';
echo  '<td><a href = "?student_info='.$student_id.'" class="text-primary">'.$student_number.'</a></td>';
echo  '<td style=\'text-transform: uppercase;\'>'.$reference_no.'</td>';
echo  '<td style=\'text-transform: uppercase;\'>'.$last_name.', '.$first_name.' '.$middle_name.'</td>';
echo  '<td style=\'text-transform: uppercase;\'>'.$discount_name.'</td>';
echo  '<td style=\'text-transform: uppercase;\'>'.$discount_percent.'%</td>';
if ($row['discount_name'] == "Cash Discount"){
echo '<td style=\'text-transform: uppercase;\'> ₱ '.number_format(($discount_amount_total), 2, ".", ",").' <a onclick= "deleteDiscount('.$row['discount_id'].')"><i class="fa fa-trash-alt text-danger"></i></a></td>';
}else{
echo  '<td style=\'text-transform: uppercase;\'> ₱ '.number_format(($discount_amount_total), 2, ".", ",").'</td>';
}
echo  '</tr>';
 }
 echo  '<tr>';
 echo  '<td></td>';
 echo  '<td></td>';
 echo  '<td></td>';
 echo  '<td></td>';
 echo  '<td>Total:</td>';
 echo  '<td>'.number_format(($TotalDiscount), 2, ".", ",").'</td>';
 echo  '</tr>';
 echo  '</tbody>';
 echo '</table>';
}

 ?>
