<?php
include "../../_config/db.php";
$sql = "SELECT fees.id as fees_id, fees.priority as priority, fees.code as code, fees.name as name, fee_type.name as fee_type FROM fees INNER JOIN fee_type on fees.fee_type_id = fee_type.id ORDER BY fee_type.id";
$result = $db ->query($sql);

while($row = $result->fetch_assoc()){
echo '<tr>';
echo '<td>'.$row['priority'].'</td>';
echo '<td style="text-transform: uppercase;">'.$row['code'].'</td>';
echo '<td style="text-transform: uppercase;">'.$row['name'].'</td>';
echo '<td style="text-transform: uppercase;">'.$row['fee_type'].'</td>';
echo '<td>';
echo '<div class="centered between">';
echo '<a onclick= "updateFee('.$row['fees_id'].')"><i class="fa fa-edit text-sky"></i></a>';
echo '<a onclick= "deleteFee('.$row['fees_id'].')"><i class="fa fa-trash-alt text-danger"></i></a>';
echo '</div>';
echo '</td>';
echo '</tr>';
 } ?>
