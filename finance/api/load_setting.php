<?php
include "../../_config/db.php";
$valueSelected = $_POST['valueSelected'];
$settingIDSelected = $_POST['settingIDSelected'];
$yearLevelSelected = $_POST['yearLevelSelected'];
$strandValueSelected = $_POST['strandValueSelected'];
$yearLevelText = " ";


if ($yearLevelSelected == "ALL"){
  $sqlall = "SELECT id as year_level_id, short_name FROM year_levels";
  $resultall = $db ->query($sqlall);

  while($rowall = $resultall->fetch_assoc()){
    $TotalLab = 0;
    $TotalFee = 0;
    $all_name = $rowall['short_name'];
    $all_name_id = $rowall['year_level_id'];

    $sqlfee = "SELECT DISTINCT fee_structure.id as fee_structure_id, fees.code as code, fees.name as fee_name, fee_structure.fee_amount as fee_amount, fee_structure.fee_amount_type as fee_amount_type FROM `fee_structure` INNER JOIN fees ON fees.id = fee_structure.fee_id WHERE fee_structure.strand_id = '$valueSelected' AND fee_structure.year_level_id = '$all_name_id' AND fee_structure.setting_id ='$settingIDSelected'";
    $resultfee = $db ->query($sqlfee);

    $sqllab= "SELECT DISTINCT subjects.code as subject_code, subjects.name as subject_name, fee_subjects.subject_amount as lab_amount FROM `fee_subjects` LEFT OUTER JOIN subjects ON fee_subjects.subject_id = subjects.id WHERE  fee_subjects.strand_id = '$valueSelected' AND fee_subjects.year_level_id = '$all_name_id' AND fee_subjects.setting_id = '$settingIDSelected'
    ORDER BY `lab_amount` DESC";

    $resultlab = $db ->query($sqllab);

    $rowcount=mysqli_num_rows($resultfee);
    if($rowcount == 0) {

    }else{
      echo  '<div class="e-col-6">';
      echo  '<div class="e-box">';
      echo  '<div class="between pl-3 pr-3"><h3>'.$all_name.'</h3>';
      echo  '<button class="e-btn primary small align-end" onclick="ShowCopyFeeModal(\''.$valueSelected.'\',\''.$all_name_id.'\',\''.$settingIDSelected.'\')"><i class="fa fa-copy"></i></button></div>';

      echo  '<div class="e-col-12">';
      echo  '<table class="e-table">';
      echo  '<tbody>';
      echo  '<tr class="bg-primary">';
      echo  '<th colspan="2" class="text-white">Tuition</th>';
      echo  '<input type="text" id="FeeStructureID" value="" hidden>';
      echo  '<th><a href="#" onclick= "ShowAddFeeModal(\''.$valueSelected.'\',\''.$all_name_id.'\',\''.$settingIDSelected.'\')"><i class="fa fa-plus text-white align-end pr-3"></i></a></th>';
      echo  '</tr>';

      while($rowfee = $resultfee->fetch_assoc()){
        $TotalFee += $rowfee['fee_amount'];
        echo  '<tr>';
        echo  '<td style=\'text-transform: uppercase;  word-break: break-all;\'><b>'.$rowfee['code'].'</b> - '.$rowfee['fee_name'].'</td>';
        echo  '<td><span> ₱ '.number_format(($rowfee['fee_amount']), 2).'</span></td>';
        echo  '<td>';
        echo  '<a href="#" onclick="ShowUpdateFeeModal(\''.$rowfee['fee_structure_id'].'\', \''.$rowfee['fee_name'].'\', \''.$rowfee['fee_amount'].'\', \''.$rowfee['fee_amount_type'].'\')"><span class="fa fa-edit text-sky" style="font-size:15px"></span></a>';
        echo  '<a href="#" onclick= "deleteFeeTuition('.$rowfee['fee_structure_id'].')"><span class="fa fa-trash-alt text-danger" style="font-size:15px"></span></a>';
        echo  '</td>';
        echo  '</tr>';
      }

      $rowcountlab=mysqli_num_rows($resultlab);
      if($rowcountlab == 0) {
      }else{
        echo  '<tr class="bg-primary">';
        echo  '<th colspan="2" class="text-white">LABS</th>';
        echo  '<th></th>';
        echo  '</tr>';
        while($rowlab = $resultlab->fetch_assoc()){
          $TotalLab += $rowlab['lab_amount'];
          echo  '<tr>';
          echo  '<td style="word-wrap: break-word;word-break: break-all;white-space: normal;"><b>'.$rowlab['subject_code'].'</b> - '.$rowlab['subject_name'].'</td>';
          echo  '<td><span style=\'text-transform: uppercase;\'> ₱ '.number_format(($rowlab['lab_amount']), 2).'</span></td>';
          echo  '<td></td>';
          echo  '</tr>';
        }
      }
      echo  '</tbody>';

      echo  '<tfoot>';
      echo  '<tr>';
      echo  '<td>Total:</td>';
      echo  '<th> <span> ₱ '.number_format(($TotalLab+$TotalFee), 2).'</span></th>';
      echo  '<th></th>';
      echo  '</tr>';
      echo  '</tfoot>';
      echo  '</table>';
      echo  '</div>';
      echo  '</div>';
      echo  '</div>';
    }
  }
}

else if ($yearLevelSelected == "Labs"){

  $sqllab= "SELECT DISTINCT subjects.id as subject_id, subjects.code as subject_code, subjects.name as subject_name, fee_subjects.subject_amount as lab_amount FROM `fee_subjects` LEFT OUTER JOIN subjects ON fee_subjects.subject_id = subjects.id WHERE  fee_subjects.strand_id = '$valueSelected' AND fee_subjects.setting_id = '$settingIDSelected'
  ORDER BY `lab_amount` DESC";

  $resultlab = $db ->query($sqllab);

  echo  '<div class="e-col-6">';
  echo  '<div class="e-form-box">';
  echo  '<div class="between pl-3 pr-3"><h3>'.$yearLevelText.'</h3>';
  echo  '<button class="e-btn primary small align-end" onClick="ShowCopyLabModal(\''.$valueSelected.'\',\''.$settingIDSelected.'\')"><i class="fa fa-copy"></i></button></div>';

  echo  '<div class="e-col-12">';
  echo  '<table class="e-table">';
  echo  '<tbody>';
  echo  '<tr class="bg-primary">';
  echo  '<th class="text-white">SUBJECT</th>';
  echo  '<th class="text-white">AMOUNT</th>';
  echo  '<th class="text-white"><a href="#" onclick= "ShowAddLabModal()"><i class="fa fa-plus text-white align-end pr-3"></i></a></th>';
  echo  '</tr>';
  while($rowlab = $resultlab->fetch_assoc()){
    echo  '<tr>';
    echo  '<td style="word-wrap: break-word;word-break: break-all;white-space: normal;"><b>'.$rowlab['subject_code'].'</b> - '.$rowlab['subject_name'].'</td>';
    echo  '<td><span style=\'text-transform: uppercase;\'> ₱ '.number_format(($rowlab['lab_amount']), 2).'</span></td>';
    echo  '<td>';
    echo  '<a href="#" onclick="ShowUpdateLabModal(\''.$rowlab['subject_id'].'\', \''.$rowlab['lab_amount'].'\', \''.$valueSelected.'\', \''.$settingIDSelected.'\')"><span class="fa fa-edit text-sky" style="font-size:15px"></span></a>';
    echo  '<a href="#" onclick= "deleteLabTuition(\''.$rowlab['subject_id'].'\')"><span class="fa fa-trash-alt text-danger" style="font-size:15px"></span></a>';
    echo  '</td>';
    echo  '</tr>';
  }
  echo  '</tbody>';
  echo  '</table>';
  echo  '</div>';
  echo  '</div>';
  echo  '</div>';
}
else{
  $yearLevelText = $_POST['yearLevelText'];
  $TotalFee = 0;
  $TotalLab = 0;
  $sqlfee = "SELECT DISTINCT fee_structure.id as fee_structure_id, fees.code as code, fees.name as fee_name, fee_structure.fee_amount as fee_amount, fee_structure.fee_amount_type as fee_amount_type FROM `fee_structure` INNER JOIN fees ON fees.id = fee_structure.fee_id WHERE fee_structure.strand_id = '$valueSelected' AND fee_structure.year_level_id = '$yearLevelSelected' AND fee_structure.setting_id ='$settingIDSelected'";
  $resultfee = $db ->query($sqlfee);

  $sqllab= "SELECT DISTINCT subjects.code as subject_code, subjects.name as subject_name, fee_subjects.subject_amount as lab_amount FROM `fee_subjects` LEFT OUTER JOIN subjects ON fee_subjects.subject_id = subjects.id WHERE  fee_subjects.strand_id = '$valueSelected' AND fee_subjects.year_level_id = '$yearLevelSelected' AND fee_subjects.setting_id = '$settingIDSelected'
  ORDER BY `lab_amount` DESC";

  $resultlab = $db ->query($sqllab);

  $rowcount=mysqli_num_rows($resultfee);
  if($rowcount == 0) {
    echo  '<div class="e-container">';
    echo  '<button class="e-btn primary fullwidth" onclick="ShowCreateStructureModal(\''.$strandValueSelected.'\',\''.$yearLevelText.'\',\''.$valueSelected.'\',\''.$settingIDSelected.'\',\''.$yearLevelSelected.'\')">Create Structure</button>';
    echo  '</div>';
  }else{
    echo  '<div class="e-col-6">';
    echo  '<div class="e-form-box">';
    echo  '<div class="between pl-3 pr-3"><h3>'.$yearLevelText.'</h3>';
    echo  '<button class="e-btn primary small align-end" onclick="ShowCopyFeeModal(\''.$valueSelected.'\',\''.$yearLevelSelected.'\',\''.$settingIDSelected.'\')"><i class="fa fa-copy"></i></button></div>';

    echo  '<div class="e-col-12">';
    echo  '<table class="e-table">';
    echo  '<tbody>';
    echo  '<tr class="bg-primary">';
    echo  '<th colspan="2" class="text-white">Tuition</th>';
    echo  '<input type="text" id="FeeStructureID" value="" hidden>';
    echo  '<th><a href="#" onclick= "ShowAddFeeModal(\''.$valueSelected.'\',\''.$yearLevelSelected.'\',\''.$settingIDSelected.'\')"><i class="fa fa-plus text-white align-end pr-3"></i></a></th>';
    echo  '</tr>';

    while($rowfee = $resultfee->fetch_assoc()){
      $TotalFee += $rowfee['fee_amount'];
      echo  '<tr>';
      echo  '<td style=\'text-transform: uppercase;\'><b>'.$rowfee['code'].'</b> - '.$rowfee['fee_name'].'</td>';
      echo  '<td><span> ₱ '.number_format(($rowfee['fee_amount']), 2).'</span></td>';
      echo  '<td>';
      echo  '<a href="#" onclick="ShowUpdateFeeModal(\''.$rowfee['fee_structure_id'].'\', \''.$rowfee['fee_name'].'\', \''.$rowfee['fee_amount'].'\', \''.$rowfee['fee_amount_type'].'\')"><span class="fa fa-edit text-sky" style="font-size:15px"></span></a>';
      echo  '<a href="#" onclick= "deleteFeeTuition('.$rowfee['fee_structure_id'].')"><span class="fa fa-trash-alt text-danger" style="font-size:15px"></span></a>';
      echo  '</td>';
      echo  '</tr>';
    }

    $rowcountlab=mysqli_num_rows($resultlab);
    if($rowcountlab == 0) {
    }else{
      echo  '<tr class="bg-primary">';
      echo  '<th colspan="2" class="text-white">LABS</th>';
      echo  '<th></th>';
      echo  '</tr>';
      while($rowlab = $resultlab->fetch_assoc()){
        $TotalLab += $rowlab['lab_amount'];
        echo  '<tr>';
        echo  '<td style="word-wrap: break-word;word-break: break-all;white-space: normal;"><b>'.$rowlab['subject_code'].'</b> - '.$rowlab['subject_name'].'</td>';
        echo  '<td><span style=\'text-transform: uppercase;\'> ₱ '.number_format(($rowlab['lab_amount']), 2).'</span></td>';
        echo  '<td></td>';
        echo  '</tr>';
      }
    }
    echo  '</tbody>';

    echo  '<tfoot>';
    echo  '<tr>';
    echo  '<td>Total:</td>';
    echo  '<th> <span> ₱ '.number_format(($TotalLab+$TotalFee), 2).'</span></th>';
    echo  '<th></th>';
    echo  '</tr>';
    echo  '</tfoot>';
    echo  '</table>';
    echo  '</div>';
    echo  '</div>';
    echo  '</div>';
  }
}
?>
