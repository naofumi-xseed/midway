<?php
include "../../_config/db.php";
$yearLevelID = $_POST['yearLevelValue'];
$strandValue = $_POST['strandValue'];
$settingValue = $_POST['settingValue'];
$TotalTotal = 0;
$sqlMain = "SELECT enrollment_student.id as enrollment_id, enrollment_student.fees_summary, preregistration_info.id as student_id, preregistration_info.student_number, preregistration_info.first_name, preregistration_info.middle_name, preregistration_info.last_name, strands_courses.name as strand_name FROM  `preregistration_info` LEFT OUTER JOIN enrollment_student ON preregistration_info.id = enrollment_student.student_id LEFT OUTER JOIN strands_courses ON strands_courses.id = enrollment_student.strand_id where enrollment_student.strand_id = '$strandValue' AND enrollment_student.year_level_id = '$yearLevelID' AND enrollment_student.setting_id = '$settingValue'";
$resultMain = $db->query($sqlMain);
while($rowSTUDdetails = $resultMain->fetch_assoc()){
  $TotalTuition = 0;
  $TotalPayment = 0;

  $enrollment_id = $rowSTUDdetails['enrollment_id'];
  $student_id = $rowSTUDdetails['student_id'];
  $student_number = $rowSTUDdetails['student_number'];
  $first_name = $rowSTUDdetails['first_name'];
  $middle_name = $rowSTUDdetails['middle_name'];
  $last_name = $rowSTUDdetails['last_name'];
  $strand_name  = $rowSTUDdetails['strand_name'];
  $fees_summary  = $rowSTUDdetails['fees_summary'];


  $sqlTransactionPayment =  "SELECT sum(amount) as amount FROM `student_payment_transactions` WHERE enrollment_student_id = '$enrollment_id'";
  $resultTransactionPayment = $db->query($sqlTransactionPayment);

  $TotalPayment = $resultTransactionPayment->fetch_assoc()['amount'];

  $someJSON = '['.$fees_summary.']';
  $someArray = json_decode($someJSON, true);
  foreach ($someArray as $key => $value) {
     $TotalTuition = $value["total_tuition_fee"];
      }

(int) $TotalBalance = $TotalTuition - $TotalPayment;

if($TotalBalance > 0){
    $is_active = 0;
    $sql = "SELECT t.is_active as is_active FROM student_blocking t INNER JOIN ( SELECT student_id, max(created_at) as MaxDate FROM student_blocking WHERE student_id = '$student_id' AND department_id = 7 GROUP BY student_id ) tm ON t.student_id = tm.student_id AND t.created_at = tm.MaxDate WHERE t.student_id = '$student_id' AND t.department_id = 7";
    $result = $db->query($sql);
    $rowcount=mysqli_num_rows($result);
    echo '<tr>';
    echo '<td><a href = "?student_info='.$student_id.'" class="text-primary">'.$student_number.'</a></td>';
    echo '<td style=\'text-transform: uppercase;\'>'.$last_name.', '.$first_name.' '.$middle_name.'</td>';
    echo '<td style=\'text-transform: uppercase;\'>'.$strand_name.'</td>';
    echo '<td style=\'text-transform: uppercase;\'>'.number_format(($TotalBalance), 2, ".", ",").'</td>';
    echo '<td style=\'text-transform: uppercase;\'>';
    echo '<div class="centered">';
    if($rowcount==0){
      echo '<a class="e-btn rounded danger width-60" onclick="ShowBlockModal('.$student_id.')">Block</a>';
    }else{
      while($row = $result->fetch_assoc()){
        $is_active = $row['is_active'];
        if ($is_active == 1){
          echo '<a onclick="unblockStudent('.$student_id.')" class="e-btn rounded sky width-60">Unblock</a>';
        }else if ($is_active == 0){
          echo '<a class="e-btn rounded danger width-60" onclick="ShowBlockModal('.$student_id.')">Block</a>';
        }
      }
    }
    echo '</div>';
    echo '</td>';
    echo '</tr>';
    $TotalTotal += $TotalBalance;
}
}
if ($TotalTotal <= 0){
  echo '1';
}else{
  echo  '<tr>';
  echo  '<td></td>';
  echo  '<td></td>';
  echo  '<td>Total:</td>';
  echo  '<td>'.number_format(($TotalTotal), 2, ".", ",").'</td>';
  echo  '<td></td>';
  echo  '</tr>';
}
?>
