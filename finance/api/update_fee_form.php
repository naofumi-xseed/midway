<!-- Update Fee -->
<?php
include "../../_config/db.php";
$fees_id = $_POST['fees_id'];
$sql = "SELECT fees.id as fees_id, fees.priority as priority, fees.code as code, fees.name as name, fee_type.name as fee_type, fees.fee_type_id as fee_id FROM fees INNER JOIN fee_type on fees.fee_type_id = fee_type.id where fees.id =$fees_id ";
$result = $db ->query($sql);
while($row = $result->fetch_assoc()){
echo  '<script>';
echo  '$(function() {';
echo  '$("div.e-form-group select").val("'.$row['fee_id'].'");';
echo  '});';
echo  '</script>';

echo  '<div id="update'.$row['fees_id'].'">';
echo  '<form class="e-form-box" id= "edit_fee" enctype="multipart/form-data">';
echo  '<h3 class="text-gray centered pb-1">Update Fee</h3>';
echo  '<div class="e-form-group unified">';
echo  '<div class="e-control-helper text-primary">';
echo  'Priority';
echo  '</div>';
echo  '<input  type="number" class="e-control text-uppercase" name="priority" id="priority" value="'.$row['priority'].'">';
echo  '</div>';

echo  '<div class="e-form-group unified">';
echo  '<div class="e-control-helper text-primary">';
echo  'Code';
echo  '</div>';
echo  '<input  type="text" class="e-control text-uppercase" name="code" id="code" value="'.$row['code'].'">';
echo  '</div>';

echo  '<div class="e-form-group unified">';
echo  '<div class="e-control-helper text-primary">';
echo  'Name';
echo  '</div>';
echo  '<input type="text" class="e-control text-uppercase" name="name" id="name" value="'.$row['name'].'">';
echo  '</div>';

echo  '<input type="text"  name="id" id="id" value="'.$row['fees_id'].'" hidden>';

echo  '<div class="e-form-group unified">';
echo  '<div class="e-control-helper text-primary">';
echo  'Type';
echo  '</div>';
echo  '<select class="e-select" name="fee_type" id="fee_type">';
echo  '<option label="TUITION FEE" value="1">TUITION FEE</option>';
echo  '<option label="MISCELLANEOUS FEE" value="2">MISCELLANEOUS FEE</option>';
echo  '<option label="BACK ACCOUNT" value="3">BACK ACCOUNT</option>';
echo  '</select>';
echo  '</div>';

echo  '<div class="between">';
echo  '<button type="button" class="e-btn inverted dark width-40" onClick="CreateNew();">Create New</button>';
echo  '<button type="button" class="e-btn primary width-40" onclick="editFee();">Update</button>';
echo  '</div>';

echo  '</form>';

echo  '</div>';
} ?>
