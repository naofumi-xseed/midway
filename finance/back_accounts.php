<div class="e-container">
  <p class="text-bold">Back Accounts</p>
  <div class="align-end">
      <select class="e-select" id="semester_option">
        <option value="" disabled="" class="" selected="selected">Select Semester</option>
        <?php
        $sqlsem = "SELECT settings.id as settings_id, settings.description as description FROM `settings` INNER JOIN school_years ON settings.school_year_id = school_years.id INNER JOIN terms ON settings.term_id = terms.id ORDER BY school_years.year DESC ,`terms`.`id` DESC";
        $resultsem = $db ->query($sqlsem);
        while($rowsem = $resultsem->fetch_assoc()){
          ?>
          <option value ="<?php echo $rowsem['settings_id'];?>"><?php echo $rowsem['description'];?></option>
        <?php } ?>
      </select>
  </div>

    <table class="e-table bordered hovered mt-3" id="indextable">
      <thead class="e-thead primary">
        <tr>
          <th><a href="javascript:SortTable(0,'N');">STUD NO.</a></th>
          <th><a href="javascript:SortTable(1,'N');">LRN</a></th>
          <th><a href="javascript:SortTable(2,'T');">NAME</a></th>
          <th><a href="javascript:SortTable(3,'N');">AMOUNT</a></th>
          <th><a href="javascript:SortTable(4,'T');">SETTING</a></th>
          <th><a href="javascript:SortTable(5,'T');">STRAND</a></th>
        </tr>
      </thead>
      <tbody id="tbody_back_accounts">

      </tbody>
    </table>

    <div id="NoResults" hidden="hidden">
    <br>
    <br>
    <center>
    <h3 id="NoResults1">No Results found <h3 id="NoResults2"></h3></h3>
    </center>
    </div>
<script>
$('#semester_option').on('change', function () {
    var optionSelected = $("option:selected", '#semester_option').text();
    var valueSelected = $('#semester_option').val();
    $.ajax({
      type:"POST",
      data: "valueSelected="+valueSelected+"&optionSelected="+optionSelected,
      url:"api/load_back_accounts.php",
      success:function(data){
        if (data==1){
          $('#tbody_back_accounts').hide();
          $('#NoResults1').html("No Results found in ");
          $('#NoResults2').text(optionSelected);
          $('#NoResults').show();
        }
        else{
        $('#NoResults').hide();
        $('#tbody_back_accounts').show();
        $('#tbody_back_accounts').html(data);
        }
      }
    });

});
</script>
