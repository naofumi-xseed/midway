<div class="e-container">
  <div id = "divMain">
    <p class="text-bold">Blocking/Unblocking</p>
    <div class="e-cols">
      <div class="e-col-5">
        <div class="e-buttons unified">
          <a class="e-btn inverted primary" onclick="ShowStudentOverpayment()">Students w/ Overpayment</a>
          <a class="e-btn inverted danger" onclick="ShowStudentBalance()">Students w/ Balance</a>
        </div>
      </div>
      <div class="e-col-3"></div>
      <div class="e-col-4">
        <form>
          <div class="e-form-group unified">
            <div class="e-control-helper">
              <i class="fas fa-search text-primary"></i>
            </div>
            <input class="e-control" id="search" type="text" placeholder="Search...">
            <button type="button" class="e-btn primary" onclick="loadSearch()">Search</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!--hidden Overpayment -->
  <div id="divOverpayment" hidden="hidden">
    <div class="e-cols">
      <div class="e-col-2">
        <p class="text-bold">Blocking/Unblocking</p>
      </div>
      <div class="e-col-2"></div>
      <div class="e-col">
        <div class="e-buttons unified align-end">
          <a class="e-btn inverted primary" onclick="ShowAllStudents()">All Students List</a>
          <a class="e-btn inverted danger" onclick="ShowStudentBalance()">Students w/ Balance</a>
        </div><br>
      </div>
    </div>
    <div class="e-cols">
      <div class="e-col-4">

        <h3>Students w/ Overpayment</h3>

      </div>
      <div class="e-col">

        <div class="e-form-group unified">
          <div class="e-control-helper marked">
            Select
          </div>
          <select class="e-control text-capitalize" id="strand_option_overpayment" name = "strand_option_overpayment">
            <option value="" disabled="" class="" selected="selected">Select Strand</option>
            <?php
            include "../_config/db.php";
            $sql = "SELECT DISTINCT id, name FROM strands_courses ORDER BY id ASC";
            $result = $db ->query($sql);
            while($row = $result->fetch_assoc()){
              ?>
              <option class="text-capitalize" value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
            <?php } ?>
          </select>
          <select class="e-select" id="semester_option_overpayment" name = "semester_option_overpayment">
            <option value="" disabled="" class="" selected="selected">Select Semester</option>
            <?php
            $sqlsem = "SELECT settings.id as settings_id, settings.description as description FROM `settings` INNER JOIN school_years ON settings.school_year_id = school_years.id INNER JOIN terms ON settings.term_id = terms.id ORDER BY school_years.year DESC ,`terms`.`id` DESC";
            $resultsem = $db ->query($sqlsem);
            while($rowsem = $resultsem->fetch_assoc()){
              ?>
              <option value ="<?php echo $rowsem['settings_id'];?>"><?php echo $rowsem['description'];?></option>
            <?php } ?>
          </select>
          <select class="e-select" id="yrlvl_option_overpayment" name = "yrlvl_option_overpayment">
            <option value="" disabled="" class="" selected="selected">Select Year</option>
            <?php
            $sqlyear = "SELECT id, short_name FROM year_levels";
            $resultyear = $db ->query($sqlyear);
            while($rowyear = $resultyear->fetch_assoc()){
              ?>
              <option value ="<?php echo $rowyear['id'];?>"><?php echo $rowyear['short_name'];?></option>
            <?php } ?>
          </select>
          <button type="button" class="e-btn primary" id="search_overpayment" onclick="tableOverpayment()">Go</button>
        </div>
      </div>
    </div>
  </div>
  <!--End hidden Overpayment -->

  <!--hidden Balance -->
  <div id="divBalance" hidden="hidden">
    <div class="e-cols">
      <div class="e-col-2">
        <p class="text-bold">Blocking/Unblocking</p>
      </div>
      <div class="e-col-2"></div>
      <div class="e-col">
        <div class="e-buttons unified align-end">
          <a class="e-btn inverted primary" onclick="ShowAllStudents()">All Students List</a>
          <a class="e-btn inverted danger" onclick="ShowStudentOverpayment()">Students w/ Overpayment</a>
        </div><br>
      </div>
    </div>
    <div class="e-cols">
      <div class="e-col-4">

        <h3>Students w/ Balance</h3>

      </div>
      <div class="e-col">
        <div class="e-form-group unified">
          <div class="e-control-helper marked">
            Select
          </div>
          <select class="e-control text-capitalize" id="strand_option_balance">
            <option value="" disabled="" class="" selected="selected">Select Strand</option>
            <?php
            $sql = "SELECT DISTINCT id, name FROM strands_courses ORDER BY id ASC";
            $result = $db ->query($sql);
            while($row = $result->fetch_assoc()){
              ?>
              <option class="text-capitalize" value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
            <?php } ?>
          </select>
          <select class="e-select" id="semester_option_balance">
            <option value="" disabled="" class="" selected="selected">Select Semester</option>
            <?php
            $sqlsem = "SELECT settings.id as settings_id, settings.description as description FROM `settings` INNER JOIN school_years ON settings.school_year_id = school_years.id INNER JOIN terms ON settings.term_id = terms.id ORDER BY school_years.year DESC ,`terms`.`id` DESC";
            $resultsem = $db ->query($sqlsem);
            while($rowsem = $resultsem->fetch_assoc()){
              ?>
              <option value ="<?php echo $rowsem['settings_id'];?>"><?php echo $rowsem['description'];?></option>
            <?php } ?>
          </select>
          <select class="e-select" id="yrlvl_option_balance">
            <option value="" disabled="" class="" selected="selected">Select Year</option>
            <?php
            $sqlyear = "SELECT id, short_name FROM year_levels";
            $resultyear = $db ->query($sqlyear);
            while($rowyear = $resultyear->fetch_assoc()){
              ?>
              <option value ="<?php echo $rowyear['id'];?>"><?php echo $rowyear['short_name'];?></option>
            <?php } ?>
          </select>
          <button type="button" class="e-btn primary" id="search_balance" onclick="tableBalance()">Go</button>
        </div>
      </div>
    </div>
  </div>
  <!--End hidden Balance -->

  <?php
  $totalrows =  $db->query('SELECT COUNT("*") FROM preregistration_info INNER JOIN enrollment_student ON preregistration_info.id = enrollment_student.student_id')->fetch_assoc()['COUNT("*")'];

  $limit = 20;

  $pages = ceil($totalrows / $limit);

  $page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
    'options' => array(
      'default'   => 1,
      'min_range' => 1,
    ),
  )));

  $offset = ($page - 1)  * $limit;

  $start = $offset + 1;
  $end = min(($offset + $limit), $totalrows);

  echo '<div id="tableOverpayment" hidden="hidden">';
  echo '<table class="e-table bordered hovered" id="indextableOverpayment">';
  echo '<thead>';
  echo '<tr>';
  echo '<th><a href="javascript:SortTable(0,\'N\');">Student No.</a></th>';
  echo '<th><a href="javascript:SortTable(1,\'T\');"><b>Name</b></a></th>';
  echo '<th><a href="javascript:SortTable(2,\'T\');"><b>Course/Strand</b></a></th>';
  echo '<th><a href="javascript:SortTable(2,\'N\');"><b>Balance</b></a></th>';
  echo '<th><a href="javascript:SortTable(3,\'T\');"><b>Block/Unblock</b></a></th>';
  echo '</tr>';
  echo '</thead>';
  echo '<tbody id="tbody_overpayment">';

  echo '</tbody>';
  echo '</table>';
  echo '<div id="NoResults" hidden="hidden">';
  echo '<br>';
  echo '<br>';
  echo '<center>';
  echo '<h3 id="NoResults1">No Results found <h3 id="NoResults2"></h3></h3>';
  echo '</center>';
  echo '</div>';
  echo '<div id="loading-image" hidden="hidden">';
  echo '<center>';
  echo '<img src="../_public/photos/loader.gif" style="width:600px;height:auto;">';
  echo '</center>';
  echo '</div>';
  echo '</div>';

  echo '<div id="tableBalance" hidden="hidden">';
  echo '<table class="e-table bordered hovered" id="indextable">';
  echo '<thead>';
  echo '<tr>';
  echo '<th><a href="javascript:SortTable(0,\'N\');">Student No.</a></th>';
  echo '<th><a href="javascript:SortTable(1,\'T\');"><b>Name</b></a></th>';
  echo '<th><a href="javascript:SortTable(2,\'T\');"><b>Course/Strand</b></a></th>';
  echo '<th><a href="javascript:SortTable(2,\'N\');"><b>Balance</b></a></th>';
  echo '<th><a href="javascript:SortTable(3,\'T\');"><b>Block/Unblock</b></a></th>';
  echo '</tr>';
  echo '</thead>';
  echo '<tbody id="tbody_balance">';
  echo '</tbody>';
  echo '</table>';
  echo '<div id="NoResultsBalance" hidden="hidden">';
  echo '<br>';
  echo '<br>';
  echo '<center>';
  echo '<h3 id="NoResults1Balance">No Results found <h3 id="NoResults2"></h3></h3>';
  echo '</center>';
  echo '</div>';
  echo '<div id="loading-imageBalance" hidden="hidden">';
  echo '<center>';
  echo '<img src="../_public/photos/loader.gif" style="width:600px;height:auto;">';
  echo '</center>';
  echo '</div>';
  echo '</div>';

  echo '<div id="tableMain">';
  echo '<table class="e-table bordered hovered" id="indextable">';
  echo '<thead>';
  echo '<tr>';
  echo '<th><a href="javascript:SortTable(0,\'N\');">Student No.</a></th>';
  echo '<th><a href="javascript:SortTable(1,\'N\');"><b>LRN</b></a></th>';
  echo '<th><a href="javascript:SortTable(2,\'T\');"><b>Name</b></a></th>';
  echo '<th><a href="javascript:SortTable(3,\'T\');"><b>Block/Unblock</b></a></th>';
  echo '</tr>';
  echo '</thead>';
  echo '<tbody id="tbody_blocking">';
  $data = "SELECT preregistration_info.id as id, preregistration_info.student_number as student_no, preregistration_info.first_name as first_name, preregistration_info.middle_name as middle_name, preregistration_info.last_name as last_name, preregistration_info.reference_no as reference_no, strands_courses.name as strand_name  FROM preregistration_info INNER JOIN enrollment_student ON preregistration_info.id = enrollment_student.student_id LEFT OUTER JOIN strands_courses ON enrollment_student.strand_id = strands_courses.id where preregistration_info.id > 0  LIMIT {$limit} OFFSET {$offset}";

  $connect = mysqli_query($db,$data);

  while($results=mysqli_fetch_array($connect)){
    $is_active = 0;
    $sql = "SELECT t.is_active as is_active FROM student_blocking t INNER JOIN ( SELECT student_id, max(created_at) as MaxDate FROM student_blocking WHERE student_id = {$results['id']} AND department_id = 7 GROUP BY student_id ) tm ON t.student_id = tm.student_id AND t.created_at = tm.MaxDate WHERE t.student_id = {$results['id']} AND t.department_id = 7";
    $result = $db ->query($sql);
    $rowcount=mysqli_num_rows($result);
    echo '<tr>';
    echo '<td><a href = "?student_info='.$results['id'].'" class="text-primary">'.$results['student_no'].'</a></td>';
    echo '<td>'.$results['reference_no'].'</td>';
    echo '<td style=\'text-transform: uppercase;\'>'.$results['last_name'].', '.$results['first_name'].' '.$results['middle_name'].'</td>';
    echo '<td style=\'text-transform: uppercase;\'>';
    echo '<div class="centered">';
    if($rowcount==0){
      echo '<a class="e-btn rounded danger width-60" onclick="ShowBlockModal('.$results['id'].')">Block</a>';
    }else{
      while($row = $result->fetch_assoc()){
        $is_active = $row['is_active'];
        if ($is_active == 1){
          echo '<a onclick="unblockStudent('.$results['id'].')" class="e-btn rounded sky width-60">Unblock</a>';
        }else if ($is_active == 0){
          echo '<a class="e-btn rounded danger width-60" onclick="ShowBlockModal('.$results['id'].')">Block</a>';
        }
      }
    }
    echo '</div>';
    echo '</td>';
    echo '</tr>';
  }


  echo '</tbody>';
  echo '</table>';

  echo '<nav class="e-pagination" aria-label="pagination">';
  echo '<div class="e-page-content">';

  $prevlink = ($page > 1) ? '<a href="?students_ledger&page=1" title="First page" class="e-page-item"><i class="fas fa-angle-double-left"></i> </a> <a href="?students_ledger&page=' . ($page - 1) . '" class="e-page-item" title="Previous page">previous</a>' : '<a class="e-page-item" disabled><i class="fas fa-angle-double-left"></i></a> <a class="e-page-item" disabled>previous</a>';

  $nextlink = ($page < $pages) ? '<a href="?students_ledger&page=' . ($page + 1) . '" class="e-page-item" title="Next page">Next</a> <a href="?students_ledger&page=' . $pages . '" class="e-page-item" title="Last page"><i class="fas fa-angle-double-right"></i></a>' : '<span class="e-page-item" disabled>Next</span> <span class="e-page-item" disabled><i class="fas fa-angle-double-right"></i></span>';

  echo  $prevlink, ' Page ', $page, ' of ', $pages, ' pages, displaying ', $start, '-', $end, ' of ', $totalrows, ' results ',     $nextlink;
  echo '</div> </nav>';

   echo '</div>';

  ?>

<!-- Block Modal -->
<div class="e-modal" id="blockModal">
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">Remarks for blocking</p>
      <button type="button" class="e-delete close">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    <div class="e-modal-body">
      <form class="e-cols no-gap">
        <div class="e-col e-form-group">
          <input class="e-control" type="text" id="remarks" placeholder="Remarks">
          <input class="e-control" type="text" id="studID" style="display:none;">
        </div>
      </form>
    </div>
    <footer class="e-modal-footer">
      <button class="e-btn danger fullwidth" type="button" onClick="blockStudent()">Block</button>
    </footer>
  </div>
</div>
<!-- End Block Modal -->

<!-- Refund Modal -->
<div class="e-modal" id="refundModal">
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">Refund</p>
      <button type="button" class="e-delete close">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    <div class="e-modal-body">
      <form class="e-cols no-gap">
        <div class="e-col-2 e-form-group">
          <label class="e-label">Refund Amount</label>
        </div>
        <div class="e-col e-form-group">
          <input class="e-control" type="text" id="refund" placeholder="Refund">
        </div>
        <i class="w-100"></i>
        <div class="e-col-2 e-form-group">
          <label class="e-label">Check Number</label>
        </div>
        <div class="e-col e-form-group">
          <input class="e-control" type="text" id="checkno" placeholder="Check Number">
        </div>
          <input class="e-control" type="text" id="enrollID" style="display:none;">
      </form>
    </div>
    <footer class="e-modal-footer">
      <button class="e-btn sky fullwidth" type="button" onClick="refundOverpayment()">Refund</button>
    </footer>
  </div>
</div>
<!-- End Refund Modal -->

<script>

function loadBlocking(){
  $.ajax({
    type: "GET",
    url:"api/load_table_blocking.php",
    success:function(data){
      $('#tbody_blocking').html(data);
    }
  });
}

function loadSearch(){
  var searchValue = $("#search").val();
  $.ajax({
    type: "POST",
    url:"api/search_blocking.php",
    data:"searchValue="+searchValue,
    success:function(data){
      $('#tbody_blocking').html(data);
    }
  });
}

$('.arrow').on('click',function()
{
  var valAttr = $(this).children('span').attr( "class" );
  if(valAttr == "arrow-up"){
    $(this).children('span').removeClass('arrow-up');
    $(this).children('span').addClass('arrow-down');
  }
  else if(valAttr == "arrow-down"){
    $(this).children('span').removeClass('arrow-down');
    $(this).children('span').addClass('arrow-up');
  }
});

$(".close").click(function(){
  $(".e-modal").removeClass("launch");
});

function ShowBlockModal(studID){
  $("#blockModal").addClass("launch");
  $("#studID").val(studID);

}

function ShowRefundModal(enrollID, overpayment){
  $("#refundModal").addClass("launch");
  $("#refund").val(overpayment * -1);
  $("#enrollID").val(enrollID);

}

function ShowStudentOverpayment(){
  $("#divMain").hide();
  $("#divBalance").hide();
  $('#tableMain').hide();
  $('#tableBalance').hide();
  $('#tableOverpayment').show();
  $("#divOverpayment").show();
}

function ShowStudentBalance(){
  $("#divMain").hide();
  $("#divOverpayment").hide();
  $('#tableMain').hide();
  $('#tableOverpayment').hide();
  $('#tableBalance').show();
  $("#divBalance").show();
}

function ShowAllStudents(){
  $("#divBalance").hide();
  $("#divOverpayment").hide();
  $('#tableBalance').hide();
  $('#tableOverpayment').hide();
  $('#tableMain').show();
  $("#divMain").show();
}

function tableOverpayment(){
  var yearLevelValue = $("#yrlvl_option_overpayment").val();
  var strandValue = $("#strand_option_overpayment").val();
  var settingValue = $("#semester_option_overpayment").val();
  $('#tbody_overpayment').hide();
  $('#NoResults').hide();
  $('#loading-image').show();
  $.ajax({
    type: "POST",
    url:  "api/load_table_overpayment.php",
    data: "yearLevelValue="+yearLevelValue+"&strandValue="+strandValue+"&settingValue="+settingValue,
    success:function(data){
      var isnum = /^\d+$/.test(data);
      if (isnum){
        $('#NoResults1').html("No Results found");
        $('#NoResults').show();
        $('#tableMain').hide();
        $('#tableBalance').hide();
        $('#tbody_overpayment').hide();
      }
      else{
      $('#NoResults').hide();
      $('#tableMain').hide();
      $('#tableBalance').hide();
      $('#tbody_overpayment').html(data);
      $('#tbody_overpayment').show();
      $('#tableOverpayment').show();
      }
    },
    complete: function(){
      $('#loading-image').hide();
    }
  });
}

function tableBalance(){
  var yearLevelValue = $("#yrlvl_option_balance").val();
  var strandValue = $("#strand_option_balance").val();
  var settingValue = $("#semester_option_balance").val();
  $('#tbody_balance').hide();
  $('#NoResultsBalance').hide();
  $('#loading-imageBalance').show();
  $.ajax({
    type: "POST",
    url:  "api/load_table_balance.php",
    data: "yearLevelValue="+yearLevelValue+"&strandValue="+strandValue+"&settingValue="+settingValue,
    success:function(data) {
      if (data==1){
        $('#NoResults1Balance').html("No Results found");
        $('#NoResultsBalance').show();
        $('#tableMain').hide();
        $('#tableOverpayment').hide();
        $('#tbody_balance').hide();
      }
      else{
      $('#NoResultsBalance').hide();
      $('#tableMain').hide();
      $('#tableOverpayment').hide();
      $('#tbody_balance').html(data);
      $('#tbody_balance').show();
      $('#tableBalance').show();
      }
    },
    complete: function(){
      $('#loading-imageBalance').hide();
    }
  });
}

function blockStudent(){
  var IDUser = $('#IDUser').val();
  var remarks = $("#remarks").val();
  var studID = $("#studID").val();
  $.ajax({
    type: "POST",
    url:  "api/block_student.php",
    data: "remarks="+remarks+"&studID="+studID+"&IDUser="+IDUser,
    success:function() {
      $(".close").click();
      alert('Blocked!');
      loadBlocking();
    }
});
}

function unblockStudent(studID){
  var IDUser = $('#IDUser').val();
  $.ajax({
    type: "POST",
    url:  "api/unblock_student.php",
    data: "studID="+studID+"&IDUser="+IDUser,
    success:function(data) {
      if (data==1){
          alert('Unblocked!');
          loadBlocking();
      }else if (data==2){
          alert('Need to be an Admin to unblock!');
      }
    }
});
}

function refundOverpayment(){
  var enrollID = $('#enrollID').val();
  var overpayment = $('#refund').val() * -1;
  var checkno = $('#checkno').val();
  var IDUser = $('#IDUser').val();
  $.ajax({
    type: "POST",
    url:  "api/refund_overpayment.php",
    data: "enrollID="+enrollID+"&IDUser="+IDUser+"&overpayment="+overpayment+"&checkno="+checkno,
    success:function(data) {
          alert("Refunded!");
          tableOverpayment();
        }
});
}
  </script>
