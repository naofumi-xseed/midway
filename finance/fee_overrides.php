<div class="e-container">
  <p class="text-bold">Fee Overrides</p>
  <div class="align-end">
      <select class="e-select" id="semester_option">
        <option value="" disabled="" class="" selected="selected">Select Semester</option>
        <?php
        include "../_config/db.php";
        $sql = "SELECT settings.description as description, settings.id as setting_id FROM `settings` INNER JOIN terms ON settings.term_id = terms.id INNER JOIN school_years ON settings.school_year_id = school_years.id ORDER BY school_years.year DESC";
        $result = $db ->query($sql);
        while($row = $result->fetch_assoc()){
          ?>
          <option value ="<?php echo $row['setting_id'];?>"><?php echo $row['description'];?></option>
        <?php } ?>
      </select>
  </div>

    <table class="e-table bordered hovered mt-3" id="indextable">
      <thead class="e-thead primary">
        <tr>
          <th>STUD NO.</th>
          <th>LRN</th>
          <th>NAME</th>
          <th>FEE NAME</th>
          <th>AMOUNT</th>
          <th></th>
        </tr>
      </thead>
      <tbody id="tbody_fee_overrides">

      </tbody>
    </table>

    <div id="NoResults" hidden="hidden">
    <br>
    <br>
    <center>
    <h3 id="NoResults1">No Results found <h3 id="NoResults2"></h3></h3>
    </center>
    </div>

<script>
$('#semester_option').on('change', function (e) {
    var optionSelected = $("option:selected", this).text();
    var valueSelected = this.value;
    $.ajax({
      type:"POST",
      data: "valueSelected="+valueSelected,
      url:"api/load_fee_overrides.php",
      success:function(data){
        if (data==1){
          $('#tbody_fee_overrides').hide();
          $('#NoResults1').html("No Results found in ");
          $('#NoResults2').text(optionSelected);
          $('#NoResults').show();
        }
        else{
        $('#NoResults').hide();
        $('#tbody_fee_overrides').show();
        $('#tbody_fee_overrides').html(data);
        }
      }
    });

});

function ajaxTable(){
  var optionSelected = $("option:selected", '#semester_option');
  var valueSelected = $('#semester_option').val();
  $.ajax({
    type:"POST",
    data: "valueSelected="+valueSelected,
    url:"api/load_fee_overrides.php",
    success:function(data){
      $('#tbody_fee_overrides').html(data);
    }
  });
}
function editAmount(obj){
  $(obj).parent().parent().parent().children('.group1_buttons').hide();
  $(obj).parent().parent().parent().children('.group2_buttons').show();
  var OrigValue =  $(obj).parent().parent().parent().children('.override_amount').html();
  $(obj).parent().parent().parent().children('.override_amount').hide();
  $(obj).parent().parent().parent().children('.override_edit').html('<input type="text" class="e-control" value="'+OrigValue+'">');
  $(obj).parent().parent().parent().children('.override_edit').show();

}

function cancelEdit(obj){
  $(obj).parent().parent().parent().children('.group1_buttons').show();
  $(obj).parent().parent().parent().children('.group2_buttons').hide();

  $(obj).parent().parent().parent().children('.override_amount').show();
  $(obj).parent().parent().parent().children('.override_edit').hide();

}

function saveEdit(obj){
  var overrideIDValue = $(obj).parent().parent().parent().children('.override_id').html();
  var editedValue = $(obj).parent().parent().parent().children('.override_edit').children('input').val();
  $.ajax({
    type:"POST",
    data: "editedValue="+editedValue+"&overrideIDValue="+overrideIDValue,
    url:"api/update_fee_override.php",
    success:function(data){
      alert('Updated!');
      $(obj).parent().parent().parent().children('.override_amount').html(editedValue);
    }
  });
}

function deleteOverride(id){
  var overrideIDValue = id;
  $.ajax({
    type:"GET",
    data:"overrideIDValue="+overrideIDValue,
    url:"api/delete_fee_override.php",
    success:function(data){
      alert('Deleted!');
      ajaxTable();
    }
  });
}
</script>
