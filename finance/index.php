<?php
include "../_config/db.php";
session_start();
?>

<?php
if(empty($_SESSION['username']))
{
  echo "
  <script>
  window.location = '../';
  </script>
  ";
}
else{
  //USERS
  $users="SELECT id, first_name , last_name , middle_name, email from users where employee_no = '{$_SESSION['username']}'";
  $connect_user = mysqli_query($db,$users);

  $results = mysqli_fetch_array($connect_user);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!-- Font awesome icons -->
  <link rel="stylesheet" href="../_public/css/all.css">
  <!-- Css -->
  <link rel="stylesheet" href="../_public/css/efrolic.min.css">
  <link rel="stylesheet" href="../_public/css/registrar.css">
  <link rel="stylesheet" href="../_public/css/finance.css">

  <script src="../_public/js/jquery-3.4.0.js"></script>

  <title>Finance</title>
</head>

<body ng-app="financeApp" ng-controller="mainController">
  <div class="e-container-fluid">
    <nav class="e-nav primary ">
      <img src="../_public/photos/logo.png" style="width:90px;height:auto;">

      <h6 class="text-white no-m">Midway Colleges
        <p class="text-white no-m">School Information Management Portal</p></h6>
        <i class="e-distribution"></i>
        <span class="px-3">
          <span class="text-center e-title no-p text-white">Finance</span>
          <br>

          <span class="no-p text-white size-13">
            Hi! <strong> <?php echo $results['first_name']?> </strong>

            <input class="e-control" id="emailUser" type="text" value="<?php echo $results['email']?>" style="display:none;">
            <input class="e-control" id="IDUser" type="text" value="<?php echo $results['id']?>" style="display:none;">
            <a href="?logout" class="pl-3 text-dark text-bold size-11"><i class="fas fa-sign-out-alt"></i>  Logout</a>
          </span>
        </span>

    </nav>

    <nav class="e-tabs ">
      <ul>
        <li><a href="?fees" class="size-11"><i class="fa fa-credit-card text-danger"></i> Fees</a></li>
        <li><a href="?refunds" class="size-11"><i class="fa fa-arrow-left text-primary"></i> Refunds</a></li>
        <li><a href="?discounts" class="size-11"><i class="fa fa-list text-warning"></i> Discounts</a></li>
        <li><a href="?fee_overrides" class="size-11"><i class="fa fa-users text-danger"></i> Fee Overrides</a></li>
        <li><a href="?back_accounts" class="size-11"><i class="fa fa-users text-primary"></i> Back Accounts </a></li>
        <li><a href="?settings" class="size-11"><i class="fa fa-cogs text-primary"></i> Settings</a></li>
        <li><a href="?blocking" class="size-11"><i class="fa fa-unlock-alt text-warning"></i> Blocking/Unblocking</a></li>
        <li><a href="?exam-list" class="size-11"><i class="fa fa-list text-danger"></i> Exam Master List</a></li>
        <li><a href="?students_ledger" class="size-11"><i class="fa fa-users text-primary"></i> Students Ledger</a></li>
        <li><a href="?reports" class="size-11"><i class="fa fa-chart-bar text-danger"></i> Reports</a></li>
        <li><a href="?subsidiary_ledger" class="size-11"><i class="fa fa-chart-bar text-warning"></i> Subsidiary Ledger</a></li>
        <li><a href="?collections" class="size-11"><i class="fa fa-chart-bar text-danger"></i> Collections</a></li>
        <li><a href="?balance" class="size-11"><i class="fa fa-chart-bar text-primary"></i> Balance</a></li>
        <li><a href="?sales" class="size-11"><i class="fa fa-chart-bar text-warning"></i> Sales</a></li>
      </ul>
    </nav>

    <?php
    if ($_SERVER['QUERY_STRING'] == ""){
      echo '<section class="mt-1 face-body">';
      echo '<div class="face-item">';
      echo '<h1 class="e-title">Welcome '. $results['first_name'] .'</h1>';
      echo '<h2 class="e-subtitle">Lorem enim laboris aliquip</h2>';
      echo '<div class="face-item">';
      echo '<img src="../_public/photos/finance.png" />';
      echo '</div>';
      echo '</section>';
      }
      ?>
    <div class="row">

      <?php
      if(isset($_GET['students_ledger']))
      {
        include "students_ledger.php";
      }
      else if(isset($_GET['subsidiary_ledger']))
      {
        include "subsidiary_ledger.php";
      }
      else if(isset($_GET['fees']))
      {
        include "fees.php";
      }
      else if(isset($_GET['settings']))
      {
        include "settings.php";
      }
      else if(isset($_GET['fee_overrides']))
      {
        include "fee_overrides.php";
      }
      else if(isset($_GET['show_setting']))
      {
        include "show_setting.php";
      }
      else if(isset($_GET['student_info']))
      {
        include "student_info.php";
      }
      else if(isset($_GET['back_accounts']))
      {
        include "back_accounts.php";
      }
      else if(isset($_GET['blocking']))
      {
        include "blocking.php";
      }
      else if(isset($_GET['discounts']))
      {
        include "discounts.php";
      }
      else if(isset($_GET['balance']))
      {
        include "balance.php";
      }
      else if(isset($_GET['refunds']))
      {
        include "refunds.php";
      }
      else if(isset($_GET['reports']))
      {
        include "reports.php";
      }
      else if(isset($_GET['collections']))
      {
        include "collections.php";
      }
      else if(isset($_GET['sales']))
      {
        include "sales.php";
      }
      else if(isset($_GET['exam-list']))
      {
        include "exam-list.php";
      }
      ?>


    </div>


    <?php
    if(isset($_GET['logout'])){
      $up_data = " UPDATE users SET remember_token='0',updated_at=''  where employee_no='{$_SESSION['username']}'";
      mysqli_query($db,$up_data);

      echo '<script>';
      echo 'window.location=\'../\';';
      echo '</script>';
      session_destroy();
    }

    ?>
    <div class="e-footer-bar primary">
      <p>©<a class="link">2019 Midway Colleges Inc.</a> | Created and Developed by IT Department | All rights reserved</p>
    </div>
  </div>


  <script src="../_public/js/angular.this.js"></script>
    <script src="controller/app.js"></script>



</body>

</html>
