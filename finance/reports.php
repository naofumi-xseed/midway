<div class="e-container">
  <div id = "divMain">
    <p class="text-bold">Reports</p>
    <div class="e-cols">
      <div class="e-col-6">
        <a class="e-btn inverted primary width_inherit" onclick="ShowSeniorHigh()">Posted Collection</a>
      </div>
      <div class="e-col-6">
        <a class="e-btn inverted danger width_inherit" onclick="ShowCollege()">Chart</a>
      </div>
    </div>
  </div>

  <!--hidden Overpayment -->
  <div id="divSeniorHigh" hidden="hidden">
    <div class="e-cols">
      <div class="e-col-2">
        <p class="text-bold">Reports</p>
      </div>
      <div class="e-col-2"></div>
      <div class="e-col">
        <div class="e-buttons unified align-end">
          <a class="e-btn inverted danger" onclick="ShowCollege()">Chart</a>
        </div><br>
      </div>
    </div>
    <div class="e-cols">
      <div class="e-col-4">

        <h3>Posted Collection</h3>

      </div>
      <div class="e-col">
        <div class="e-form-group unified">
          <div class="e-control-helper marked">
            Select
          </div>
          <select class="e-select" id="semester_option_seniorhigh" name = "semester_option_seniorhigh">
            <option value="" disabled="" class="" selected="selected">Select Semester</option>
            <?php
            $sqlsem = "SELECT settings.id as settings_id, settings.description as description FROM `settings` INNER JOIN school_years ON settings.school_year_id = school_years.id INNER JOIN terms ON settings.term_id = terms.id ORDER BY school_years.year DESC ,`terms`.`id` DESC";
            $resultsem = $db ->query($sqlsem);
            while($rowsem = $resultsem->fetch_assoc()){
              ?>
              <option value ="<?php echo $rowsem['settings_id'];?>"><?php echo $rowsem['description'];?></option>
            <?php } ?>
          </select>
          <select class="e-control text-capitalize" id="strand_option_seniorhigh" name = "strand_option_seniorhigh">
            <option value="" disabled="" class="" selected="selected">Select Strand</option>
            <?php
            include "../_config/db.php";
            $sql = "SELECT strands_courses.id, strands_courses.name FROM `strands_courses` ORDER BY strands_courses.id ASC";
            $result = $db ->query($sql);
            while($row = $result->fetch_assoc()){
              ?>
              <option class="text-capitalize" value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
            <?php } ?>
          </select>
          <select class="e-control text-capitalize" id="acadYr_option_PostCol" name = "acadYr_option_PostCol">
            <option value="" disabled="" class="" selected="selected">Select Academic Year</option>
            <?php
            $sql = "SELECT year, id FROM `school_years`  ORDER BY `school_years`.`year`  DESC";
            $result = $db ->query($sql);
            while($row = $result->fetch_assoc()){
              $num = (int)$row['year']+1;
              ?>
              <option value ="<?php echo $row['id'];?>"><?php echo 'A.Y. '.$row['year'].' - '.$num; ?></option>
            <?php } ?>
          </select>
        </div>
      </div>
    </div>
    <div class="e-cols">
      <div class="e-col-3">
        <div class="e-form-group unified">
          <div class="e-control-helper marked"><!-- Here marked -->
            FROM
          </div>
          <input class="e-control" type="date" >
        </div>
      </div>
      <div class="e-col-3">
        <div class="e-form-group unified">
          <div class="e-control-helper marked"><!-- Here marked -->
            TO
          </div>
          <input class="e-control" type="date" >
        </div>
      </div>
      <div class="e-col-3">
        <select class="e-select width_inherit" id="month_option_seniorhigh">
          <option value="" class="" disabled = "" selected="selected">Select Month</option>
            <option value="1">January</option>
            <option value="2">February</option>
            <option value="3">March</option>
            <option value="4">April</option>
            <option value="5">May</option>
            <option value="6">June</option>
            <option value="7">July</option>
            <option value="8">August</option>
            <option value="9">September</option>
            <option value="10">October</option>
            <option value="11">November</option>
            <option value="12">December</option>
        </select>
      </div>
      <div class="e-col-3" style="margin-top:-6px;">
          <button type="button" class="e-btn primary width_inherit" id="search_seniorhigh" onclick="tableSeniorHigh()">Go</button>
      </div>
      </div>
    </div>
  <!--End hidden Overpayment -->

  <!--hidden Balance -->
  <div id="divCollege" hidden="hidden">
    <div class="e-cols">
      <div class="e-col-2">
        <p class="text-bold">Reports</p>
      </div>
      <div class="e-col-2"></div>
      <div class="e-col">
        <div class="e-buttons unified align-end">
          <a class="e-btn inverted primary" onclick="ShowSeniorHigh()">Posted Collection</a>
        </div><br>
      </div>
    </div>
    <div class="e-cols">
      <div class="e-col-1">

        <h3>Chart</h3>

      </div>
      <div class="e-col">
        <div class="e-form-group unified">
          <div class="e-control-helper marked">
            Select
          </div>
          <select class="e-select" id="semester_option_college" name = "semester_option_seniorhigh">
            <option value="" disabled="" class="" selected="selected">Select Semester</option>
            <?php
            $sqlsem = "SELECT settings.id as settings_id, settings.description as description FROM `settings` INNER JOIN school_years ON settings.school_year_id = school_years.id INNER JOIN terms ON settings.term_id = terms.id ORDER BY school_years.year DESC ,`terms`.`id` DESC";
            $resultsem = $db ->query($sqlsem);
            while($rowsem = $resultsem->fetch_assoc()){
              ?>
              <option value ="<?php echo $rowsem['settings_id'];?>"><?php echo $rowsem['description'];?></option>
            <?php } ?>
          </select>
          <select class="e-control text-capitalize" id="strand_option_college" name = "strand_option_seniorhigh">
            <option value="" disabled="" class="" selected="selected">Select Academic Year</option>
            <?php
            $sql = "SELECT year, id FROM `school_years`  ORDER BY `school_years`.`year`  DESC";
            $result = $db ->query($sql);
            while($row = $result->fetch_assoc()){
              $num = (int)$row['year']+1;
              ?>
              <option value ="<?php echo $row['id'];?>"><?php echo 'A.Y. '.$row['year'].' - '.$num; ?></option>
            <?php } ?>
          </select>
          <select class="e-select" id="month_option_seniorhigh">
            <option value="" class="" disabled = "" selected="selected">Select Month</option>
              <option value="1">January</option>
              <option value="2">February</option>
              <option value="3">March</option>
              <option value="4">April</option>
              <option value="5">May</option>
              <option value="6">June</option>
              <option value="7">July</option>
              <option value="8">August</option>
              <option value="9">September</option>
              <option value="10">October</option>
              <option value="11">November</option>
              <option value="12">December</option>
          </select>
          <select class="e-select" id="quarter_option_seniorhigh">
            <option value="" class="" disabled = "" selected="selected">Select Quarter</option>
              <option value="1">First Quarter</option>
              <option value="2">Second Quarter</option>
              <option value="3">Third Quarter</option>
              <option value="4">Fourth Quarter</option>
          </select>
        </div>
      </div>
      <div class="e-col-1" style="margin-top:-6px;">
                <button type="button" class="e-btn primary width_inherit" id="search_college" onclick="tableCollege()">Go</button>
      </div>
    </div>
  </div>
  <!--End hidden Balance -->
  </div>
  <script>

  function ShowSeniorHigh(){
    $("#divMain").hide();
    $("#divCollege").hide();
    $('#tableCollege').hide();
    $('#tableSeniorHigh').show();
    $("#divSeniorHigh").show();
  }

  function ShowCollege(){
    $("#divMain").hide();
    $("#divSeniorHigh").hide();
    $('#tableSeniorHigh').hide();
    $('#tableCollege').show();
    $("#divCollege").show();
  }

  function tableSeniorHigh(){
    var yearLevelValue = $("#year_option_seniorhigh").val();
    var strandValue = $("#strand_option_seniorhigh").val();
    var settingValue = $("#semester_option_seniorhigh").val();
    $('#tbody_seniorhigh').hide();
    $('#NoResults').hide();
    $('#loading-image').show();
    $.ajax({
      type: "POST",
      url:  "api/load_seniorhigh_collection.php",
      data: "yearLevelValue="+yearLevelValue+"&strandValue="+strandValue+"&settingValue="+settingValue,
      success:function(data){
        var isnum = /^\d+$/.test(data);
        if (isnum){
          $('#NoResults1').html("No Results found");
          $('#NoResults').show();
          $('#tableMain').hide();
          $('#tableCollege').hide();
          $('#tbody_college').hide();
        }
        else{
          $('#NoResults').hide();
          $('#tableMain').hide();
          $('#tableBalance').hide();
          $('#tbody_overpayment').html(data);
          $('#tbody_overpayment').show();
          $('#tableSeniorHigh').show();
        }
      },
      complete: function(){
        $('#loading-image').hide();
      }
    });
  }

  function tableBalance(){
    var yearLevelValue = $("#yrlvl_option_balance").val();
    var strandValue = $("#strand_option_balance").val();
    var settingValue = $("#semester_option_balance").val();
    $('#tbody_balance').hide();
    $('#NoResultsBalance').hide();
    $('#loading-imageBalance').show();
    $.ajax({
      type: "POST",
      url:  "api/load_table_balance.php",
      data: "yearLevelValue="+yearLevelValue+"&strandValue="+strandValue+"&settingValue="+settingValue,
      success:function(data) {
        if (data==1){
          $('#NoResults1Balance').html("No Results found");
          $('#NoResultsBalance').show();
          $('#tableMain').hide();
          $('#tableSeniorHigh').hide();
          $('#tbody_balance').hide();
        }
        else{
          $('#NoResultsBalance').hide();
          $('#tableMain').hide();
          $('#tableSeniorHigh').hide();
          $('#tbody_balance').html(data);
          $('#tbody_balance').show();
          $('#tableBalance').show();
        }
      },
      complete: function(){
        $('#loading-imageBalance').hide();
      }
    });
  }

  </script>
