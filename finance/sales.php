<div class="e-container">
  <p class="text-bold">Sales</p>
  <div class="align-end">
    <div class="e-cols">
      <div class="e-col-8"></div>
      <div class="e-col-4">
      <select class="e-select width_inherit" id="year_option">
        <option value="" disabled="" class="" selected="selected">Select Year</option>
        <?php
        include "../_config/db.php";
        $sql = "SELECT year, id FROM `school_years`  ORDER BY `school_years`.`year`  DESC";
        $result = $db ->query($sql);
        while($row = $result->fetch_assoc()){
          ?>
          <option value ="<?php echo $row['id'];?>"><?php echo $row['year'];?></option>
        <?php } ?>
      </select>
      </div>
  </div>

    <table class="e-table bordered hovered mt-3" id="indextable">
      <thead class="e-thead primary">
        <tr>
          <th>DATE</th>
          <th>BEGINNING</th>
          <th>ENDING</th>
          <th>TOTAL</th>
        </tr>
      </thead>
      <tbody id="tbody_sales">

      </tbody>
    </table>

    <div id="NoResults" hidden="hidden">
    <br>
    <br>
    <center>
    <h3 id="NoResults1">No Results found <h3 id="NoResults2"></h3></h3>
    </center>
    </div>

<script>
$('#year_option').on('change', function (e) {
    var optionSelected = $("option:selected", this).text();
    var yearSelected = this.value;
    $.ajax({
      type:"POST",
      data: "yearSelected="+yearSelected,
      url:"api/load_sales.php",
      success:function(data){
        if (data==1){
          $('#tbody_sales').hide();
          $('#NoResults1').html("No Results found in ");
          $('#NoResults2').text(optionSelected);
          $('#NoResults').show();
        }
        else{
        $('#NoResults').hide();
        $('#tbody_sales').show();
        $('#tbody_sales').html(data);
        }
      }
    });

});

function ajaxTable(){
  var optionSelected = $("option:selected", '#year_option');
  var yearSelected = $('#year_option').val();
  $.ajax({
    type:"POST",
    data: "yearSelected="+yearSelected,
    url:"api/load_sales.php",
    success:function(data){
      $('#tbody_sales').html(data);
    }
  });
}

</script>
