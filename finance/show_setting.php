<div>
</div>
<br>
<div class="e-container">
  <div class="e-form-box">
    <div class = "e-cols">
      <div class="e-col-3">
        <div class="e-form-group">
          <select class="e-control text-capitalize" id="strand_option" onChange = "ajaxTable1(this)">
            <option value="" disabled="" class="" selected="selected">Select Strand</option>
            <?php
            include "../_config/db.php";
            $sql = "SELECT DISTINCT * FROM strands_courses ORDER BY id ASC";
            $result = $db ->query($sql);
            while($row = $result->fetch_assoc()){
              ?>
              <option class="text-capitalize" value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="e-col-7">
        <div class="e-buttons unified">
          <button type="button" class="e-btn rounded purple small" onClick="ajaxTable(this)" value="ALL">ALL</button>
          <button type="button" class="e-btn sky small" onClick="ajaxTable(this)" value="1">Grade 11</button>
          <button type="button" class="e-btn sky small" onClick="ajaxTable(this)" value="2">Grade 12</button>
          <button type="button" class="e-btn primary small" onClick="ajaxTable(this)" value="3">1st Year</button>
          <button type="button" class="e-btn primary small" onClick="ajaxTable(this)" value="4">2nd Year</button>
          <button type="button" class="e-btn primary small" onClick="ajaxTable(this)" value="5">3rd Year</button>
          <button type="button" class="e-btn primary small" onClick="ajaxTable(this)" value="6">4th Year</button>
          <button type="button" class="e-btn rounded purple small" onClick="ajaxTable(this)" value="Labs">Labs</button>
        </div>
      </div>

      <input type="number" class="e-control" value="<?php echo $_GET['show_setting'];?>" id= "settingID" style="
      display: none;">

      <div class="e-form-group e-col">
        <input class="e-control" type="number" value="1">
        <label class="e-label">Subjects</label>
      </div>

      <div class=" e-form-group e-col">
        <input class="e-control" type="number" value="1">
        <label class="e-label">Units</label>
      </div>
    </div>
  </div>


<br>

<div  id="showTabs" class="e-cols">

</div>
</div>
<!-- Add fee Modal -->
<div class="e-modal" id="addFeeModal">
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">Add</p>
      <button type="button" class="e-delete close">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    <div class="e-modal-body">
      <form class="e-cols no-gap">
        <div class="e-col-2 e-form-group">
          <label class="e-label">Fee</label>
        </div>
        <div class="e-col e-form-group">
          <select class="e-select text-uppercase width-100" id="feeSelected">
            <option value="" disabled="" class="" selected="selected">Select Fee</option>
            <?php

            $sql = "SELECT * FROM `fees` ORDER BY `name` ASC";
            $result = $db ->query($sql);
            while($row = $result->fetch_assoc()){
              ?>
              <option class="text-uppercase" value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
            <?php } ?>
          </select>
        </div>
        <i class="w-100"></i>
        <div class="e-col-2 e-form-group">
          <label class="e-label">Amount</label>
        </div>
        <div class="e-col e-form-group">
          <input class="e-control" type="text" id="feeAmount" placeholder="Amount">
        </div>
        <i class="w-100"></i>
        <div class="e-col-2 e-form-group">
          <label class="e-label">Rate</label>
        </div>
        <div class="e-col e-form-group">
          <input class="e-control" type="text" id="feeRate" placeholder="Rate">
        </div>
        <i class="w-100"></i>
        <div class="e-col-2 e-form-group">
          <label class="e-label">Per Subject</label>
        </div>
        <div class="e-col e-form-group">
          <input class="e-control" type="text" id="feePerSubject" placeholder="Per Subject">
        </div>
        <input class="e-control" type="text" id="strandID_add" style="display: none;">
        <input class="e-control" type="text" id="yearLevelID_add" style="display: none;">
        <input class="e-control" type="text" id="settingID_add" style="display: none;">
      </form>
    </div>
    <footer class="e-modal-footer">
      <button class="e-btn primary fullwidth" type="button" onClick="addFeeTuition()">Save changes</button>
    </footer>
  </div>
</div>
<!-- End Add fee Modal -->

<!-- Update fee Modal -->
<div class="e-modal" id="updateFeeModal">
<div class="e-modal-content eUp">
  <header class="e-modal-header">
    <p class="e-modal-title">Update</p>
    <button type="button" class="e-delete close">
      <i aria-hidden="true">&times;</i>
    </button>
  </header>
  <div class="e-modal-body">
    <form class="e-cols no-gap">
      <div class="e-col-2 e-form-group">
        <label class="e-label">Fee</label>
      </div>
      <div class="e-col e-form-group">
        <input class="e-control" type="text" id="feeNameUpdate" placeholder="Fee Name" disabled>
      </div>
      <i class="w-100"></i>
      <div class="e-col-2 e-form-group">
        <label class="e-label">Amount</label>
      </div>
      <div class="e-col e-form-group">
        <input class="e-control" type="text" id="feeAmountUpdate" placeholder="Amount">
      </div>
      <i class="w-100"></i>
      <div class="e-col-2 e-form-group">
        <label class="e-label">Rate / Per Unit</label>
      </div>
      <div class="e-col e-form-group">
        <input class="e-control" type="text" id="feeRateUpdate" placeholder="Rate">
      </div>
      <i class="w-100"></i>
      <div class="e-col-2 e-form-group">
        <label class="e-label">Per Subject</label>
      </div>
      <div class="e-col e-form-group">
        <input class="e-control" type="number" id="feePerSubjectUpdate" placeholder="Per Subject">
      </div>
      <input class="e-control" type="text" id="feeStructureID" style="display: none;">
    </div>
  </form>
  <footer class="e-modal-footer">
    <button class="e-btn primary fullwidth" type="button" id="updateFeeTuition">Update changes</button>
  </footer>
</div>
</div>
<!-- End Update fee Modal -->

<!-- Add Lab Modal -->
<div class="e-modal" id="addLabModal">
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">Add Lab</p>
      <button type="button" class="e-delete close">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    <div class="e-modal-body">
      <form class="e-cols no-gap">
        <div class="e-col-2 e-form-group">
          <label class="e-label">Subject</label>
        </div>
        <div class="e-col e-form-group">
          <select class="e-select text-uppercase width-100" id="SubjectSelected">
            <option value="" disabled="" class="" selected="selected">Select Subject</option>
            <?php
            $sql = "SELECT id, code, name FROM `subjects` ORDER BY `code` ASC";
            $result = $db ->query($sql);
            while($row = $result->fetch_assoc()){
              ?>
              <option class="text-uppercase" value="<?php echo $row['id'];?>"><?php echo $row['code'].' - '.$row['name'];?></option>
            <?php } ?>
          </select>
        </div>
        <i class="w-100"></i>
        <div class="e-col-2 e-form-group">
          <label class="e-label">Amount</label>
        </div>
        <div class="e-col e-form-group">
          <input class="e-control" type="number" id="labAmount" placeholder="Amount" value = "0">
        </div>
      </form>
    </div>
    <footer class="e-modal-footer">
      <button class="e-btn primary fullwidth" type="button" onClick="addLabTuition()">Save changes</button>
    </footer>
  </div>
</div>
<!-- End Add Lab Modal -->

<!-- Update Lab Modal -->
  <div class="e-modal" id="updateLabModal">
    <div class="e-modal-content eUp">
      <header class="e-modal-header">
        <p class="e-modal-title">Update</p>
        <button type="button" class="e-delete close">
          <i aria-hidden="true">&times;</i>
        </button>
      </header>
      <div class="e-modal-body">
        <form class="e-cols no-gap">
          <div class="e-col-2 e-form-group">
            <label class="e-label">Subject</label>
          </div>
          <div class="e-col e-form-group">
            <select class="e-select text-uppercase width-100" id="SubjectSelectedUpdate">
              <?php
              $sql = "SELECT id, code, name FROM `subjects` ORDER BY `code` ASC";
              $result = $db ->query($sql);
              while($row = $result->fetch_assoc()){
                ?>
                <option class="text-uppercase" value="<?php echo $row['id'];?>"><?php echo $row['code'].' - '.$row['name'];?></option>
              <?php } ?>
            </select>
          </div>
          <i class="w-100"></i>
          <div class="e-col-2 e-form-group">
            <label class="e-label">Amount</label>
          </div>
          <div class="e-col e-form-group">
            <input class="e-control" type="text" id="labAmountUpdate" placeholder="Amount" >
          </div>
          <input class="e-control" type="text" id="strandIDLabUpdate" style="display: none;">
          <input class="e-control" type="text" id="settingIDLabUpdate" style="display: none;">
        </div>
      </form>
      <footer class="e-modal-footer">
        <button class="e-btn primary fullwidth" type="button" id="updateLabBtn">Update changes</button>
      </footer>
    </div>
  </div>
<!-- End Update Lab Modal -->

<!-- Copy Fees Modal -->
<div class="e-modal" id="copyFeeModal">
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">Copy</p>
      <button type="button" class="e-delete close">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    <div class="e-modal-body">
      <form class="e-cols no-gap">
        <div class="e-col-2 e-form-group">
          <label class="e-label">Strand</label>
        </div>
        <div class="e-col e-form-group strandSelect">
          <select class="e-select text-uppercase width-100" id="strandSelected">
            <option value="" disabled="" class="" selected="selected">Select Strand</option>
            <?php
            $sql = "SELECT id, name FROM `strands_courses`";
            $result = $db ->query($sql);
            while($row = $result->fetch_assoc()){
              ?>
              <option class="text-uppercase" value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
            <?php } ?>
          </select>
        </div>
        <i class="w-100"></i>
        <div class="e-col-2 e-form-group">
          <label class="e-label">Year Level</label>
        </div>
        <div class="e-col e-form-group yearlevelSelect">
          <select class="e-select text-uppercase width-100" id="yearlevelSelected">
            <option value="" disabled="" class="" selected="selected">Select Year</option>
            <?php
            $sql = "SELECT id, short_name FROM `year_levels`";
            $result = $db ->query($sql);
            while($row = $result->fetch_assoc()){
              ?>
              <option class="text-uppercase" value="<?php echo $row['id'];?>"><?php echo $row['short_name'];?></option>
            <?php } ?>
          </select>
        </div>
        <i class="w-100"></i>
        <div class="e-col-2 e-form-group">
          <label class="e-label">Setting</label>
        </div>
        <div class="e-col e-form-group settingSelect">
          <select class="e-select text-uppercase width-100" id="settingSelected">
            <option value="" disabled="" class="" selected="selected">Select Setting</option>
            <?php
            $sql = "SELECT settings.id as settings_id, settings.description as description, settings.status as status,school_years.year, terms.id FROM `settings` INNER JOIN school_years ON settings.school_year_id = school_years.id INNER JOIN terms ON settings.term_id = terms.id ORDER BY school_years.year DESC ,`terms`.`id` DESC";
            $result = $db ->query($sql);
            while($row = $result->fetch_assoc()){
              ?>
              <option class="text-uppercase" value="<?php echo $row['settings_id'];?>"><?php echo $row['description'];?></option>
            <?php } ?>
          </select>
        </div>
        <input type="text" id="copySetting" style="display: none;">
        <input type="text" id="copyStrand" style="display: none;">
        <input type="text" id="copyYearLevel" style="display: none;">

      </form>
    </div>
    <footer class="e-modal-footer">
      <button class="e-btn primary fullwidth" type="button" onClick="copyStructure()">Copy Structure</button>
    </footer>
  </div>
</div>
<!-- End Copy Fees Modal -->

<!-- Copy Lab Modal -->
<div class="e-modal" id="copyLabModal">
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">Copy Lab</p>
      <button type="button" class="e-delete close">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    <div class="e-modal-body">
      <form class="e-cols no-gap">
        <div class="e-col-2 e-form-group">
          <label class="e-label">Strand</label>
        </div>
        <div class="e-col e-form-group strandLabSelect">
          <select class="e-select text-uppercase width-100" id="strandLabSelected">
            <option value="" disabled="" class="" selected="selected">Select Strand</option>
            <?php
            $sql = "SELECT id, name FROM `strands_courses`";
            $result = $db ->query($sql);
            while($row = $result->fetch_assoc()){
              ?>
              <option class="text-uppercase" value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
            <?php } ?>
          </select>
        </div>
        <i class="w-100"></i>
        <div class="e-col-2 e-form-group">
          <label class="e-label">Setting</label>
        </div>
        <div class="e-col e-form-group settingLabSelect">
          <select class="e-select text-uppercase width-100" id="settingLabSelected">
            <option value="" disabled="" class="" selected="selected">Select Setting</option>
            <?php
            $sql = "SELECT settings.id as settings_id, settings.description as description, settings.status as status,school_years.year, terms.id FROM `settings` INNER JOIN school_years ON settings.school_year_id = school_years.id INNER JOIN terms ON settings.term_id = terms.id ORDER BY school_years.year DESC ,`terms`.`id` DESC";
            $result = $db ->query($sql);
            while($row = $result->fetch_assoc()){
              ?>
            <option class="text-uppercase" value="<?php echo $row['settings_id'];?>"><?php echo $row['description'];?></option>
            <?php } ?>
          </select>
        </div>
        <input type="text" id="copyLabSetting" style="display: none;">
        <input type="text" id="copyLabStrand" style="display: none;">
      </form>
    </div>
    <footer class="e-modal-footer">
      <button class="e-btn primary fullwidth" type="button" onClick="copyLab()">Copy Lab</button>
    </footer>
  </div>
</div>
<!-- End Copy Lab Modal -->

<!-- Create Structure Modal -->
<div class="e-modal" id="createStructureModal">
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title text-uppercase createStructureTitle">Create Structure</p>
      <button type="button" class="e-delete close">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    <div class="e-modal-body">
      <form class="e-cols no-gap">
        <table class= "e-table ">
          <thead>
            <tr>
              <th>Particulars</th>
              <th></th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $sql = "SELECT id, name FROM `fee_type`";
            $result = $db ->query($sql);
            while($row = $result->fetch_assoc()){
              $feeTypeID=$row['id'];
              ?>
              <tr>
                <td><?php echo $row['name'];?></td>
                <td>
                  <table class= "e-table" style="margin-top: 0rem!important;margin-bottom: -0.5rem;">
                    <?php
                    $sqlfee = "SELECT id, name FROM `fees` WHERE fee_type_id= $feeTypeID";
                    $resultfee = $db ->query($sqlfee);
                    while($rowfee = $resultfee->fetch_assoc()){
                      ?>
                      <tr class="e-cols feeCreate">
                        <td class="feeCreateID" style="display: none;"><?php echo $rowfee['id'];?></td>
                        <td class="e-col-4 text-uppercase feeCreateName" style="word-wrap: break-word;word-break: break-all;white-space: normal; font-size:smaller;"><?php echo $rowfee['name'];?></td>
                        <td></td>
                        <td class="e-col">
                          <input class="e-control mb-3 amountCreate" type="text" id="feeCreateAmount" placeholder="Amount"><br>
                          <input class="e-control mb-3 amountCreate" type="text" id="feeCreateRate" placeholder="Rate / Per Unit"><br>
                          <input class="e-control mb-3 amountCreate" type="text" id="feeCreatePerSubject" placeholder="Per Subject">
                        </td>
                      </tr>
                    <?php } ?>
                    <input class="e-control" type="text" id="feeCreateStrandID" style="display: none;">
                    <input class="e-control" type="text" id="feeCreateSettingID" style="display: none;">
                    <input class="e-control" type="text" id="feeCreateYearLevelID" style="display: none;">
                  </table>
                </td>
                <td class="totalCreate">0.00</td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
             <tr>
               <th></th>
               <th class="align-end"> Total: </th>
              <th class="totalAmountCreate">0.00 </th>
            </tr>
           </tfoot>
        </table>
      </form>
    </div>
    <footer class="e-modal-footer">
      <button class="e-btn primary fullwidth" type="button" onClick="CreateStructure()">Generate</button>
    </footer>
  </div>
</div>
<!-- End Create Structure Modal -->

<script>
function ajaxTable(obj){
  var settingIDSelected = $("#settingID").val();
  var strandValueSelected =  $("option:selected", '#strand_option').text();
  var valueSelected = $('#strand_option').val();
  var yearLevelSelected = $(obj).val();
  var yearLevelText = $(obj).text();
  $.ajax({
    type:"POST",
    data: "valueSelected="+valueSelected+"&settingIDSelected="+settingIDSelected+"&yearLevelSelected="+yearLevelSelected+"&strandValueSelected="+strandValueSelected+"&yearLevelText="+yearLevelText,
    url:"api/load_setting.php",
    success:function(data){
      $('#showTabs').html(data);
    }
  });
}

function ajaxTable1(obj){
  var settingIDSelected = $("#settingID").val();
  var valueSelected = $('#strand_option').val();
  var strandValueSelected =  $("option:selected", '#strand_option').text();
  var yearLevelSelected = 'ALL';
  $.ajax({
    type:"POST",
    data: "valueSelected="+valueSelected+"&settingIDSelected="+settingIDSelected+"&yearLevelSelected="+yearLevelSelected+"&strandValueSelected="+strandValueSelected,
    url:"api/load_setting.php",
    success:function(data){
      $('#showTabs').html(data);
    }
  });
}

function ajaxTableLab(){
  var settingIDSelected = $("#settingID").val();
  var valueSelected = $('#strand_option').val();
  var strandValueSelected =  $("option:selected", '#strand_option').text();
  var yearLevelSelected = 'Labs';
  $.ajax({
    type:"POST",
    data: "valueSelected="+valueSelected+"&settingIDSelected="+settingIDSelected+"&yearLevelSelected="+yearLevelSelected+"&strandValueSelected="+strandValueSelected,
    url:"api/load_setting.php",
    success:function(data){
      $('#showTabs').html(data);
    }
  });
}

$(".close").click(function(){
  $(".e-modal").removeClass("launch");
});

function ShowAddFeeModal(strandID, yearLevelID, settingID){
  $("#addFeeModal").addClass("launch");
  $("#strandID_add").val(strandID);
  $("#yearLevelID_add").val(yearLevelID);
  $("#settingID_add").val(settingID);
}

function ShowUpdateFeeModal(feeStructureID, feeName, feeAmount, feeAmountType){
  $("#feeNameUpdate").val('');
  $("#feeAmountUpdate").val('');
  $("#feeRateUpdate").val('');
  $("#feePerSubjectUpdate").val('');
  $("#feeStructureID").val('');
  $("#updateFeeTuition").attr('onClick', '');

  $("#feeNameUpdate").val(feeName);
  if(feeAmountType == 1){
    $("#feeAmountUpdate").val(feeAmount);
  }
  else if(feeAmountType == 2){
      $("#feeRateUpdate").val(feeAmount);
  }
  else if(feeAmountType == 3){
      $("#feePerSubjectUpdate").val(feeAmount);
  }
  $("#feeStructureID").val(feeStructureID);
  $("#updateFeeTuition").attr('onClick', 'updateFeeTuition(\''+ feeStructureID+'\')');
  $("#updateFeeModal").addClass("launch");
}


function addFeeTuition(){
  var feeSelected = $("#feeSelected").val();
  var feeAmount = $("#feeAmount").val();
  var feeRate = $("#feeRate").val();
  var feePerSubject = $("#feePerSubject").val();
  var strandID = $("#strandID_add").val();
  var yearLevelID = $("#yearLevelID_add").val();
  var settingID = $("#settingID_add").val();

  $.ajax({
    type:"POST",
    data: "feeSelected="+feeSelected+"&feeAmount="+feeAmount+"&feeRate="+feeRate+"&feePerSubject="+feePerSubject+"&strandID="+strandID+"&yearLevelID="+yearLevelID+"&settingID="+settingID,
    url:"api/add_fee_tuition.php",
    success:function(data){
      ajaxTable1();
      alert("Added Successfully!");
    }
  });

}

function updateFeeTuition(id){
  var feeStructureID = $("#feeStructureID").val();
  var feeAmountUpdate = $("#feeAmountUpdate").val();
  var feeRateUpdate = $("#feeRateUpdate").val();
  var feePerSubjectUpdate = $("#feePerSubjectUpdate").val();
  $.ajax({
    type:"POST",
    data:"feeStructureID="+feeStructureID+"&feeAmountUpdate="+feeAmountUpdate+"&feeRateUpdate="+feeRateUpdate+"&feePerSubjectUpdate="+feePerSubjectUpdate,
    url:"api/update_fee_tuition.php",
    success:function(data){
      ajaxTable1();
      alert("Updated Successfully!");
    }
  });

}

function deleteFeeTuition(strID){
  var feeStructureID = strID;
  $.ajax({
    type:"GET",
    data:"feeStructureID="+feeStructureID,
    url:"api/delete_fee_tuition.php",
    success:function(data){
      ajaxTable1();
      alert("Deleted Successfully!");
    }
  });
}

function ShowAddLabModal(){
  $("#addLabModal").addClass("launch");
}

function ShowUpdateLabModal(subject_id, lab_amount, strandID, settingID){
  $("#SubjectSelectedUpdate").val(subject_id).change();
  $("#labAmountUpdate").val(lab_amount);
  $("#strandIDLabUpdate").val(strandID);
  $("#settingIDLabUpdate").val(settingID);
  $("#updateLabBtn").attr('onClick', 'updateLabTuition()');
  $("#updateLabModal").addClass("launch");
}

function addLabTuition(){
  var settingIDSelected = $("#settingID").val();
  var valueSelected = $('#strand_option').val();
  var SubjectSelected = $("#SubjectSelected").val();
  var labAmount = $("#labAmount").val();
  $.ajax({
    type:"POST",
    data: "&SubjectSelected="+SubjectSelected+"&labAmount="+labAmount+"&settingIDSelected="+settingIDSelected+"&valueSelected="+valueSelected,
    url:"api/add_lab_tuition.php",
    success:function(data){
      alert("Added Successfully!");
    }
  });

}

function updateLabTuition(){
  var settingIDSelected = $("#settingID").val();
  var valueSelected = $('#strand_option').val();
  var labAmountUpdate = $("#labAmountUpdate").val();
  var SubjectSelectedUpdate = $("#SubjectSelectedUpdate").val();
  $.ajax({
    type:"POST",
    data:"labAmountUpdate="+labAmountUpdate+"&SubjectSelectedUpdate="+SubjectSelectedUpdate+"&settingIDSelected="+settingIDSelected+"&valueSelected="+valueSelected,
    url:"api/update_lab_tuition.php",
    success:function(data){
      alert("Updated Successfully!");
    }
  });

}

function deleteLabTuition(subjectID){
  var subjectID = subjectID;
  var settingIDSelected = $("#settingID").val();
  var valueSelected = $('#strand_option').val();
  $.ajax({
    type:"GET",
    data:"subjectID="+subjectID+"&settingIDSelected="+settingIDSelected+"&valueSelected="+valueSelected,
    url:"api/delete_lab_tuition.php",
    success:function(data){
      ajaxTableLab();
      alert("Deleted Successfully!");
    }
  });
}

function ShowCopyFeeModal(strand,yearlevel,settingID){
  $("div.strandSelect select").val(strand);
  $("div.yearlevelSelect select").val(yearlevel);
  $("div.settingSelect select").val(settingID);

  $("#copyStrand").val(strand);
  $("#copyYearLevel").val(yearlevel);
  $("#copySetting").val(settingID);

  $("#copyFeeModal").addClass("launch");
}

function copyStructure(){
  var settingIDCopy = $("#copySetting").val();
  var strandCopy = $("#copyStrand").val();
  var yearlevelCopy = $("#copyYearLevel").val();
  var settingIDSelected =  $("div.settingSelect select").val();
  var strandSelected =   $("div.strandSelect select").val();
  var yearlevelSelected = $("div.yearlevelSelect select").val();
  $.ajax({
    type:"POST",
    data:"settingIDCopy="+settingIDCopy+"&strandCopy="+strandCopy+"&yearlevelCopy="+yearlevelCopy+"&settingIDSelected="+settingIDSelected+"&strandSelected="+strandSelected+"&yearlevelSelected="+yearlevelSelected,
    url:"api/copy_structure.php",
    success:function(data){
      if (data == '0'){
        alert("Existing!");
      }else{
        alert("Copied!");
      }
    }
  });
}

function ShowCreateStructureModal(strandName, yearLevel, strandID, settingID, yearLevelID){
  $(".createStructureTitle").html(strandName + "<br> " + yearLevel);
  $("#feeCreateSettingID").val(settingID);
  $("#feeCreateStrandID").val(strandID);
  $("#feeCreateYearLevelID").val(yearLevelID);
  $("#createStructureModal").addClass("launch");
}

function ShowCopyLabModal(strand,settingID){
  $("div.strandLabSelect select").val(strand);
  $("div.settingLabSelect select").val(settingID);

  $("#copyLabStrand").val(strand);
  $("#copyLabSetting").val(settingID);

  $("#copyLabModal").addClass("launch");
}

function copyLab(){
  var settingLabIDCopy = $("#copyLabSetting").val();
  var strandLabCopy = $("#copyLabStrand").val();
  var settingIDSelected =  $("div.settingLabSelect select").val();
  var strandSelected =   $("div.strandLabSelect select").val();
  $.ajax({
    type:"POST",
    data:"settingLabIDCopy="+settingLabIDCopy+"&strandLabCopy="+strandLabCopy+"&settingIDSelected="+settingIDSelected+"&strandSelected="+strandSelected,
    url:"api/copy_lab.php",
    success:function(data){
      console.log(data);
      alert("Copied");
    }
  });
}

function CreateStructure(){
  $(".feeCreate").each(function() {
    var feeCreateID = $(this).children(".feeCreateID").html();
    var feeCreateName = $(this).children(".feeCreateName").html();
    var feeCreateAmount = $(this).children().children("#feeCreateAmount").val();
    var feeCreateRate = $(this).children().children("#feeCreateRate").val();
    var feeCreatePerSubject = $(this).children().children("#feeCreatePerSubject").val();
    var feeCreateStrandID = $("#feeCreateStrandID").val();
    var feeCreateSettingID = $("#feeCreateSettingID").val();
    var feeCreateYearLevelID =$("#feeCreateYearLevelID").val();
    if(feeCreateAmount == '' && feeCreateRate == '' && feeCreatePerSubject == ''){
    //  alert("No Fees");
    }
    else {
      $.ajax({
        type:"POST",
        data:"feeCreateID="+feeCreateID+"&feeCreateName="+feeCreateName+"&feeCreateAmount="+feeCreateAmount+"&feeCreateRate="+feeCreateRate+"&feeCreatePerSubject="+feeCreatePerSubject+"&feeCreateStrandID="+feeCreateStrandID+"&feeCreateSettingID="+feeCreateSettingID+"&feeCreateYearLevelID="+feeCreateYearLevelID,
        url:"api/create_structure.php",
        success:function(data){
          alert("Done!");
        }
      });
    }
  });
}

$(".amountCreate").bind("keyup change", function() {
  var val = $(this).val();
  var sum = 0;
  var totalAmount = 0;
  var a = $(this).parent().parent().parent().parent().parent().parent().parent().find(".totalCreate");
  $($(this).parent().parent().parent().find(".amountCreate")).each(function() {
    sum += Number($(this).val());
  });

    $(this).parent().parent().parent().parent().parent().parent().children(".totalCreate").html(sum);

});

</script>
