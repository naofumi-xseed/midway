<div class="e-container" onload="ajaxLoadPage()">
  <div class="align-end pt-3 pb-3 pr-3">
    <select class="e-select semester_select" id="semester_option" onchange="ajaxLoadPage()">
      <?php
      include "../_config/db.php";
      $studentID = $_GET['student_info'];
      $sql = "SELECT settings.id as settings_id, settings.description as description, settings.status as status,school_years.year, terms.id FROM `settings` INNER JOIN school_years ON settings.school_year_id = school_years.id INNER JOIN terms ON settings.term_id = terms.id ORDER BY school_years.year DESC ,`terms`.`id` DESC";
      $result = $db ->query($sql);
      while($row = $result->fetch_assoc()){
        ?>
        <option value ="<?php echo $row['settings_id'];?>"><?php echo $row['description'];?></option>
      <?php } ?>
    </select>
    <input type="number" class="e-control" value="<?php echo $_GET['student_info'];?>" id= "studentID" style="
    display: none;">
  </div>
  <div id="loadPageOverride">
  </div>
  <div id="loading-image" hidden="hidden">
  <center>
  <img src="../_public/photos/loader.gif" style="width:600px;height:auto;">
  </center>
  </div>

</div>
<script>
function ajaxLoadPage(){
  var settingIDString = $("option:selected", '#semester_option').text();
  var settingIDSelected = $('#semester_option').val();
  var studentID = $("#studentID").val();
  $('#loadPageOverride').hide();
  $('#loading-image').show();
  $.ajax({
    type:"POST",
    data: "settingIDSelected="+settingIDSelected+"&studentID="+studentID+"&settingIDString="+settingIDString,
    url:"api/load_student_info.php",
    success:function(data){
      $('#loadPageOverride').html(data);
      $('#loadPageOverride').show();
    },
    complete: function(){
      $('#loading-image').hide();
    }
  });
}
$(function() {
  //$("div.e-form-group select").val("'.$row['fee_id'].'");
  ajaxLoadPage();
});
</script>
