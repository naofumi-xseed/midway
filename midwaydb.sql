-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 08, 2019 at 02:54 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `midway_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `buildings`
--

CREATE TABLE `buildings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `code`, `name`, `created_at`, `updated_at`) VALUES
(1, 'BSMarE', 'Bachelor of Science in Marine Engineering', '2017-03-24 09:56:08', '2017-03-30 06:35:53'),
(2, 'BSMT', 'Bachelor of Science in Marine Transportation', '2017-03-24 09:56:28', '2019-05-03 02:16:09'),
(3, 'BSIT', 'Bachelor of Science in Information Technology', '2017-03-24 09:56:53', '2017-03-24 09:56:53'),
(4, 'BSCE', 'Bachelor of Science in Civil Engineering', '2017-03-28 08:11:53', '2017-03-28 08:11:53'),
(5, 'BSES', 'Bachelor of Science in Environmental Science', '2017-03-30 02:56:26', '2017-03-30 02:56:26'),
(6, 'BSIS', 'Bachelor of Science in Information System', '2017-03-30 02:57:07', '2017-03-30 02:57:07'),
(7, 'BS Mathematics', 'Bachelor of Science in Mathematics', '2017-03-30 02:57:40', '2017-03-30 02:57:40'),
(8, 'BS Architecture', 'Bachelor of Science in Architecture', '2017-03-30 02:58:01', '2017-03-30 02:58:01'),
(9, 'BS Agriculture', 'Bachelor of Science in Agriculture', '2017-03-30 02:58:25', '2017-03-30 02:58:25'),
(10, 'AB History', 'Bachelor of Arts in History', '2017-03-30 03:00:45', '2017-03-30 03:00:45'),
(11, 'Philosophy AB', 'Bachelor of Arts in Philosophy', '2017-03-30 03:01:06', '2017-03-30 03:01:06'),
(12, 'BFA', 'Bachelor of Fine Arts Major in Industrial Design', '2017-03-30 03:01:26', '2017-03-30 03:01:26'),
(13, 'BFA', 'Bachelor of Fine Arts Major in Painting', '2017-03-30 03:01:48', '2017-03-30 03:01:48'),
(14, 'AB  Economics', 'Bachelor of Arts in Economics', '2017-03-30 03:02:22', '2017-03-30 03:02:22'),
(15, 'BS Economics', 'Bachelor of Science in Economics', '2017-03-30 03:03:47', '2017-03-30 03:03:47'),
(16, 'AB Psychology', 'Bachelor of Arts in Psychology', '2017-03-30 03:04:09', '2017-03-30 03:04:09'),
(17, 'BS Psychology', 'Bachelor of Science in Psychology', '2017-03-30 03:04:39', '2017-03-30 03:04:39'),
(18, 'BS Criminology', 'Bachelor of Science in Criminology', '2017-03-30 03:05:03', '2017-03-30 03:05:03'),
(19, 'AB Political Science', 'Bachelor of Arts in Political Science', '2017-03-30 03:05:29', '2017-03-30 03:05:29'),
(20, 'AB English', 'Bachelor of Arts in English', '2017-03-30 03:05:49', '2017-03-30 03:05:49'),
(21, 'AB Linguistics', 'Bachelor of Arts in Linguistics', '2017-03-30 03:06:08', '2017-03-30 03:06:08'),
(22, 'AB Literature', 'Bachelor of Arts in Literature', '2017-03-30 03:06:27', '2017-03-30 03:06:27'),
(23, 'AB Anthropology', 'Bachelor of Arts in Anthropology', '2017-03-30 03:06:47', '2017-03-30 03:06:47'),
(24, 'AB Sociology', 'Bachelor of Arts in Sociology', '2017-03-30 03:07:05', '2017-03-30 03:07:05'),
(25, 'AB Filipino', 'Bachelor of Arts in Filipino', '2017-03-30 03:07:23', '2017-03-30 03:07:23'),
(26, 'BSFS', 'Bachelor of Science in Forensic Science', '2017-03-30 03:07:43', '2017-03-30 03:07:43'),
(27, 'AB Islamic Studies', 'Bachelor of Arts in Islamic Studies', '2017-03-30 03:08:01', '2017-03-30 03:08:01'),
(28, 'BSES', 'Bachelor of Science in Environmental Science', '2017-03-30 03:08:25', '2017-03-30 03:08:25'),
(29, 'BS Forestry', 'Bachelor of Science in Forestry', '2017-03-30 03:08:50', '2017-03-30 03:08:50'),
(30, 'BSFi', 'Bachelor of Science in Fisheries', '2017-03-30 03:09:10', '2017-03-30 03:09:10'),
(31, 'BS Geology', 'Bachelor of Science in Geology', '2017-03-30 03:09:26', '2017-03-30 03:09:26'),
(32, 'BS Biology', 'Bachelor of Science in Biology', '2017-03-30 03:09:44', '2017-03-30 03:09:44'),
(33, 'BS Molecular Biology', 'Bachelor of Science in Molecular Biology', '2017-03-30 03:09:58', '2017-03-30 03:09:58'),
(34, 'BS Physics', 'Bachelor of Science in Physics', '2017-03-30 03:10:15', '2017-03-30 03:10:15'),
(35, 'BS Applied Physics', 'Bachelor of Science in Applied Physics', '2017-03-30 03:10:30', '2017-03-30 03:10:30'),
(36, 'BS Chemistry', 'Bachelor of Science in Chemistry', '2017-03-30 03:10:48', '2017-03-30 03:10:48'),
(37, 'BSCS', 'Bachelor of Science in Computer Science', '2017-03-30 03:11:17', '2017-03-30 03:11:17'),
(38, 'BS Mathematics', 'Bachelor of Science in Mathematics', '2017-03-30 03:13:20', '2017-03-30 03:13:20'),
(39, 'BS Applied Math', 'Bachelor of Science in Applied Mathematics', '2017-03-30 03:13:38', '2017-03-30 03:13:38'),
(40, 'BS Stat', 'Bachelor of Science in Statistics', '2017-03-30 03:14:00', '2017-03-30 03:14:00'),
(41, 'BSA', 'Bachelor of Science in Accountancy', '2017-03-30 03:14:38', '2017-03-30 03:14:38'),
(42, 'BSAcT', 'Bachelor of Science in Accounting Technology', '2017-03-30 03:14:53', '2017-03-30 03:14:53'),
(43, 'BSBA', 'Bachelor of Science in Business Administration', '2017-03-30 03:15:18', '2017-03-30 03:15:18'),
(44, 'BSBA', 'Bachelor of Science in Business Administration Major in Business Economics', '2017-03-30 03:15:39', '2017-03-30 03:15:39'),
(45, 'BSBA major in FM', 'Bachelor of Science in Business Administration Major in Financial Management', '2017-03-30 03:16:00', '2017-03-30 03:16:00'),
(46, 'BSBA major in HRDM', 'Bachelor of Science in Business Administration Major in Human Resource Development', '2017-03-30 03:16:20', '2017-03-30 03:16:20'),
(47, 'BSBA major in MM', 'Bachelor of Science in Business Administration Major in Marketing Management', '2017-03-30 03:16:43', '2017-03-30 03:16:43'),
(48, 'BSBA major in OM', 'Bachelor of Science in Business Administration Major in Operations Management', '2017-03-30 03:17:23', '2017-03-30 03:17:23'),
(49, 'BS HRM', 'Bachelor of Science in Bachelor of Science in Hotel and Restaurant Management', '2017-03-30 03:17:52', '2017-03-30 03:17:52'),
(50, 'BS Entrep', 'Bachelor of Science in Entrepreneurship', '2017-03-30 03:18:16', '2017-03-30 03:18:16'),
(51, 'BSOA', 'Bachelor of Science in Office Administration', '2017-03-30 03:18:35', '2017-03-30 03:18:35'),
(52, 'BS REM', 'Bachelor of Science in Real Estate Management', '2017-03-30 03:18:58', '2017-03-30 03:18:58'),
(53, 'BSTM', 'Bachelor of Science in Tourism Management', '2017-03-30 03:19:14', '2017-03-30 03:19:14'),
(54, 'BS Med Tech', 'Bachelor of Science in Medical Technology', '2017-03-30 03:19:41', '2017-03-30 03:19:41'),
(55, 'BS Midwifery', 'Bachelor of Science in Midwifery', '2017-03-30 03:20:03', '2017-03-30 03:20:03'),
(56, 'BSN', 'Bachelor of Science in Nursing', '2017-03-30 03:20:46', '2017-03-30 03:20:46'),
(57, 'BSOT', 'Bachelor of Science in Occupational Therapy', '2017-03-30 03:21:01', '2017-03-30 03:21:01'),
(58, 'BS Pharmacy', 'Bachelor of Science in in Pharmacy', '2017-03-30 03:21:20', '2017-03-30 03:21:20'),
(59, 'BSPT', 'Bachelor of Science in Physical Therapy', '2017-03-30 03:21:52', '2017-03-30 03:21:52'),
(60, 'BS Rad Tech', 'Bachelor of Science in Radiologic Technology', '2017-03-30 03:22:11', '2017-03-30 03:22:11'),
(61, 'BSRT', 'Bachelor of Science in Respiratory Therapy', '2017-03-30 03:22:42', '2017-03-30 03:22:42'),
(62, 'BSED', 'Bachelor in Secondary Education', '2017-03-30 03:23:31', '2017-03-30 03:23:31'),
(63, 'BEED', 'Bachelor in Elementary Education', '2017-03-30 03:23:51', '2017-03-30 03:23:51'),
(64, 'BSED', 'Bachelor in Secondary Education Major in Technology and Livelihood Education', '2017-03-30 03:24:16', '2017-03-30 03:24:16'),
(65, 'BSED', 'Bachelor in Secondary Education Major in Biological Sciences', '2017-03-30 03:24:41', '2017-03-30 03:24:41'),
(66, 'BSED', 'Bachelor in Secondary Education Major in English', '2017-03-30 03:25:02', '2017-03-30 03:25:02'),
(67, 'BSED', 'Bachelor in Secondary Education Major in Filipino', '2017-03-30 03:25:24', '2017-03-30 03:25:24'),
(68, 'BSED', 'Bachelor in Secondary Education Major in Mathematics', '2017-03-30 03:25:40', '2017-03-30 03:25:40'),
(69, 'BSED', 'Bachelor in Secondary Education Major in Islamic Studies', '2017-03-30 03:26:03', '2017-03-30 03:26:03'),
(70, 'BSED', 'Bachelor in Secondary Education Major in Music, Arts, Physical and Health Education', '2017-03-30 03:26:22', '2017-03-30 03:26:22'),
(71, 'BSED', 'Bachelor in Secondary Education Major in Physical Sciences', '2017-03-30 03:26:45', '2017-03-30 03:26:45'),
(72, 'BSED', 'Bachelor in Secondary Education Major in Social Studies', '2017-03-30 03:27:09', '2017-03-30 03:27:09'),
(73, 'BSED', 'Bachelor in Secondary Education Major in Values Education', '2017-03-30 03:27:35', '2017-03-30 03:27:35'),
(74, 'BEED', 'Bachelor in Elementary Education Major in Preschool Education', '2017-03-30 03:27:53', '2017-03-30 03:27:53'),
(75, 'BEED', 'Bachelor in Elementary Education Major in Special Education', '2017-03-30 03:28:30', '2017-03-30 03:28:30'),
(76, 'BLIS', 'Bachelor of Library and Information Science in the Philippines', '2017-03-30 03:28:44', '2017-03-30 03:28:44'),
(77, 'BPE', 'Bachelor of Physical Education', '2017-03-30 03:29:01', '2017-03-30 03:29:01'),
(78, 'BS AeroE', 'Bachelor of Science in Aeronautical Engineering', '2017-03-30 03:30:41', '2017-03-30 03:30:41'),
(79, 'BSCerE', 'Bachelor of Science in Ceramic Engineering', '2017-03-30 03:30:56', '2017-03-30 03:30:56'),
(80, 'BSChE', 'Bachelor of Science in Chemical Engineering', '2017-03-30 03:31:15', '2017-03-30 03:31:15'),
(81, 'BSCE', 'Bachelor of Science in Civil Engineering', '2017-03-30 03:31:33', '2017-03-30 03:31:33'),
(82, 'BSCpE', 'Bachelor of Science in Computer Engineering', '2017-03-30 03:31:57', '2017-03-30 03:31:57'),
(83, 'BSEE', 'Bachelor of Science in Electrical Engineering', '2017-03-30 03:32:18', '2017-03-30 03:32:18'),
(84, 'BSECE', 'Bachelor of Science in Electronics and Communications Engineering', '2017-03-30 03:32:43', '2017-03-30 03:32:43'),
(85, 'BSGE', 'Bachelor of Science in Geodetic Engineering', '2017-03-30 03:33:02', '2017-03-30 03:33:02'),
(86, 'BSGeoE', 'Bachelor of Science in Geological Engineering', '2017-03-30 03:33:21', '2017-03-30 03:33:21'),
(87, 'BSIE', 'Bachelor of Science in Industrial Engineering', '2017-03-30 03:33:38', '2017-03-30 03:33:38'),
(88, 'BSMarE', 'Bachelor of Science in Marine Engineering', '2017-03-30 03:34:04', '2017-03-30 03:34:04'),
(89, 'BSMatE', 'Bachelor of Science in Materials Engineering', '2017-03-30 03:34:24', '2017-03-30 03:34:24'),
(90, 'BSME', 'Bachelor of Science in Mechanical Engineering', '2017-03-30 03:34:49', '2017-03-30 03:34:49'),
(91, 'BSMetE', 'Bachelor of Science in Metallurgical Engineering', '2017-03-30 03:35:16', '2017-03-30 03:35:16'),
(92, 'BSEM', 'Bachelor of Science in Mining Engineering', '2017-03-30 03:35:36', '2017-03-30 03:35:36'),
(93, 'BSPetE', 'Bachelor of Science in Petroleum Engineering', '2017-03-30 03:36:06', '2017-03-30 03:36:06'),
(94, 'BSSE', 'Bachelor of Science in Sanitary Engineering', '2017-03-30 03:36:23', '2017-03-30 03:36:23'),
(95, 'AB Broadcasting', 'Bachelor of Arts in Broadcasting', '2017-03-30 03:37:10', '2017-03-30 03:37:10'),
(96, 'AB Communication', 'Bachelor of Arts in Communication', '2017-03-30 03:37:32', '2017-03-30 03:37:32'),
(97, 'BS DevComm', 'Bachelor of Science in in Development Communication', '2017-03-30 03:37:57', '2017-03-30 03:37:57'),
(98, 'AB Journalism', 'Bachelor of Arts in Journalism', '2017-03-30 03:38:15', '2017-03-30 03:38:15'),
(99, 'BS Community Development', 'Bachelor of Science in Community Development', '2017-03-30 03:39:25', '2017-03-30 03:39:25'),
(100, 'BSCA', 'Bachelor of Science in Customs Administration', '2017-03-30 03:39:39', '2017-03-30 03:39:39'),
(101, 'BS Foreign Service', 'Bachelor of Science in Foreign Service', '2017-03-30 03:39:57', '2017-03-30 03:39:57'),
(102, 'BSIS', 'Bachelor of Science in International Studies', '2017-03-30 03:40:12', '2017-03-30 03:40:12'),
(103, 'BPA', 'Bachelor of Public Administration', '2017-03-30 03:40:26', '2017-03-30 03:40:26'),
(104, 'BSPS', 'Bachelor of Science in Public Safety', '2017-03-30 03:40:43', '2017-03-30 03:40:43'),
(105, 'BS Social Work', 'Bachelor of Science in Social Work', '2017-03-30 03:41:04', '2017-03-30 03:41:04'),
(106, 'BSMT', 'Bachelor of Science in Marine Transportation', '2017-03-30 03:41:21', '2017-03-30 03:41:21'),
(107, 'BS Food Tech', 'Bachelor of Science in Food Technology', '2017-03-30 03:44:40', '2017-03-30 03:44:40'),
(108, 'BS Nutrition and Dietetics', 'Bachelor of Science in Nutrition and Dietetics', '2017-03-30 03:45:00', '2017-03-30 03:45:00'),
(109, 'HRM', 'Two-year Hotel and Restaurant Management', '2017-04-12 22:54:31', '2017-04-12 22:54:31'),
(110, 'BIT', 'Bachelor in Industrial Technology', '2017-04-28 01:21:40', '2017-04-28 01:21:40'),
(111, 'BSCOM', 'BACHELOR OF SCIENCE IN COMMERCE', '2017-07-14 07:17:01', '2017-07-14 07:18:40'),
(112, 'bsme', 'bachelor of science in marine engineering', '2017-07-19 03:40:31', '2017-07-19 03:40:31'),
(113, 'OM', 'OFFICE MANAGEMENT', '2017-07-21 02:57:01', '2017-07-21 02:57:01'),
(114, 'ACt', 'ASSOCIATE IN COMPUTER technology', '2017-07-21 03:48:45', '2018-02-09 03:23:26'),
(115, 'CEST', 'COMPUTER EQUIPMENT SERVICING TECH.', '2017-07-24 00:42:42', '2017-07-24 00:42:42'),
(116, 'CEST', 'COMPUTER EQUIPMENT SERVICING TECH.', '2017-07-24 00:44:43', '2017-07-24 00:44:43'),
(117, 'eimncii', 'electrical installation and maintenance nc ii', '2017-08-01 08:11:28', '2017-08-01 08:11:28'),
(118, 'cpnciv', 'computer programming nc iv', '2017-08-08 02:11:48', '2017-08-08 02:11:48'),
(119, 'AGT', 'ASSOCIATE IN GARMENT TECHNOLOGY', '2017-08-10 06:21:16', '2017-08-10 06:21:16'),
(120, 'ESLPMD', 'ENHANCED SUPPORT LEVEL PROGRAM In MARINE DECK', '2017-08-10 07:45:28', '2017-11-08 03:28:17'),
(121, 'BEED', 'BACHELOR OF SCIENCE IN ELEMENTARY EDUCATION', '2017-08-11 07:02:47', '2017-08-11 07:02:47'),
(122, 'ite', 'information technology education', '2017-08-17 06:37:09', '2017-08-17 06:37:09'),
(123, 'CFYC', 'COMMON FIRST YEAR CURRICULUM', '2017-08-17 07:12:30', '2017-08-17 07:12:30'),
(124, 'ncii', 'building wiring installation nc ii', '2017-08-25 02:29:34', '2017-08-25 02:30:33'),
(125, 'BSECE', 'Bachelor of Science in Electronics Engineering', '2017-10-04 07:00:07', '2017-10-04 07:00:07'),
(126, 'BSECE', 'bachelor of science in electronics and communications engineering', '2017-10-04 07:08:41', '2017-10-04 07:08:41'),
(127, 'bshm', 'bachelor of science in HOSPITALITY management', '2017-10-26 08:29:28', '2017-10-26 08:29:28'),
(128, 'act', 'two-year associate in computer technology', '2017-10-26 08:54:09', '2017-10-26 08:54:09'),
(129, 'bshrm', 'bachelor of science in hotel and restaurant management', '2017-10-27 03:23:19', '2017-10-27 03:24:11'),
(130, 'CFYC', 'COMMON FIRST YEAR CURRICULUM', '2017-10-30 00:50:32', '2017-10-30 00:50:32'),
(131, 'BSAE', 'BACHELOR OF SCIENCE IN Agricultural Education', '2017-10-30 01:48:52', '2017-10-30 01:48:52'),
(132, 'it', 'bachelor of science in industrial technology', '2017-10-30 06:22:25', '2017-10-30 06:22:25'),
(133, 'gas', 'general academic strand', '2017-11-02 03:55:03', '2017-11-02 03:55:03'),
(134, 'tvl', 'tvl-he-hrm', '2017-11-03 00:12:22', '2017-11-03 00:12:22'),
(135, 'humss', 'humanities and social sciences', '2017-11-03 02:44:37', '2017-11-03 02:44:37'),
(136, 'PBM', 'PRE-BACCALAUREATE MARITIME', '2017-11-03 04:45:48', '2017-11-03 04:45:48'),
(137, 'gas', 'academic/gas', '2017-11-03 06:18:19', '2017-11-03 06:18:19'),
(138, 'stem', 'science technology engineering and mathematics', '2017-11-03 07:49:22', '2017-11-03 07:49:22'),
(139, 'stem', 'science technology engineering and mathematics', '2017-11-03 07:50:13', '2017-11-03 07:50:13'),
(140, 'stem', 'science technology engineering and mathematics', '2017-11-03 07:51:26', '2017-11-03 07:51:26'),
(141, 'academic', 'academic/stem', '2017-11-03 07:52:46', '2017-11-03 07:52:46'),
(142, 'smaw', 'smaw', '2017-11-03 08:35:33', '2017-11-03 08:35:33'),
(143, 'smaw', 'smaw', '2017-11-03 08:40:17', '2017-11-03 08:40:17'),
(144, 'abm', 'accountancy, business and management', '2017-11-06 07:09:11', '2017-11-06 07:09:11'),
(145, 'tvl-ict', 'tvl-information and communications technology', '2017-11-06 09:29:28', '2017-11-06 09:29:28'),
(146, 'tvl-c', 'tvl-cookery', '2017-11-07 00:45:01', '2017-11-07 00:45:01'),
(147, 'mt', 'manufacturing technology', '2017-11-07 02:11:28', '2017-11-07 02:11:28'),
(148, 'bsae', 'bachelor of science in agricultural engineering', '2017-11-08 03:10:34', '2017-11-08 03:10:34'),
(149, 'stem', 'stem', '2017-11-09 23:48:19', '2017-11-09 23:48:19'),
(150, 'gas', 'gas', '2017-11-10 00:27:12', '2017-11-10 00:27:12'),
(151, 'ABM', 'ABM', '2017-11-10 05:34:34', '2017-11-10 05:34:34'),
(152, 'tvl-ia', 'tvl-ia', '2017-11-10 06:12:26', '2017-11-10 06:12:26'),
(153, 'tvl-ict', 'tvl-ict', '2017-11-13 06:47:15', '2017-11-13 06:47:15'),
(154, 'CHNC II', 'COMPUTER HARDWARE SERVICING NC II', '2017-11-28 02:52:33', '2017-11-28 02:52:33'),
(155, 'bsima', 'bachelor of science in management accountancy', '2017-12-01 03:12:51', '2017-12-01 03:12:51'),
(156, 'cgncii', 'caregiving nc ii', '2017-12-04 02:12:08', '2017-12-04 02:12:08'),
(157, 'bsa', 'bachelor of science in agribusiness', '2017-12-07 05:26:36', '2017-12-07 05:26:36'),
(158, 'tycs', 'two-year computer science', '2017-12-12 07:27:38', '2017-12-12 07:27:38'),
(159, 'NTT', 'NETWORKING AND TELECOMMUNICATIONS TECHNOLOGY', '2017-12-14 06:53:37', '2017-12-14 06:53:37'),
(160, 'tba', 'tba', '2017-12-20 02:12:45', '2017-12-20 02:12:45'),
(161, 'bim', 'business & information management', '2017-12-20 07:54:20', '2017-12-20 07:54:20'),
(162, 'bass', 'bachelor of arts in social sciences', '2017-12-21 01:22:48', '2017-12-21 01:22:48'),
(163, 'TVL-HE', 'TVL-HOME ECONOMICS', '2018-01-12 02:20:43', '2018-01-12 02:20:43'),
(164, 'bas', 'bachelor of animal science', '2018-01-18 00:17:06', '2018-01-18 00:17:06'),
(165, 'cp', 'caregiver program', '2018-01-22 00:02:57', '2018-01-22 00:02:57'),
(166, 'src', 'seafarers rating course', '2018-01-25 01:50:40', '2018-01-25 01:50:40'),
(167, 'sd', 'software development', '2018-01-25 02:42:26', '2018-01-25 02:42:26'),
(168, '2yit', '2 year information technology', '2018-01-29 02:36:33', '2018-01-29 02:36:33'),
(169, '2yit', '2 year information technology', '2018-01-29 02:36:34', '2018-01-29 02:36:34'),
(170, 'FDSH', 'AUTOMOTIVE SERVICING NC II', '2018-01-31 02:44:56', '2018-01-31 02:44:56'),
(171, 'jj', 'bachelor of science in education', '2018-02-01 01:33:15', '2018-02-01 01:33:15'),
(172, 'ecet', 'ecet: diploma in electronics and communications engineering technology', '2018-02-05 01:40:14', '2018-02-05 01:40:14'),
(173, 'grav', 'computer hardware technology', '2018-02-05 03:05:31', '2018-02-05 03:05:31'),
(174, 'fg', 'THREE-YEAR ELECTRONICS COMM. ENG\'G TECH.', '2018-02-06 05:26:02', '2018-02-06 05:26:02'),
(175, 'BSHTM', 'BACHELOR OF SCIENCE IN HOSPITALITY AND TOURISM MANAGEMENT(TOURS AND TRAVELS MANAGEMENT)', '2018-02-13 07:44:01', '2018-02-13 07:44:01'),
(176, 'maa', 'management and accountancy', '2018-02-14 05:22:52', '2018-02-14 05:22:52'),
(177, 'CT', '2 YEAR COMPUTER TECHNICIAN', '2018-02-22 07:11:03', '2018-02-22 07:11:03'),
(178, 'fyy', 'bs in ENGINEERING AND TECHNOLOGY', '2018-03-06 01:39:28', '2018-03-06 01:39:28'),
(179, 'btte', 'bachelor of technical teacher education', '2018-04-18 03:52:45', '2018-04-18 03:52:45'),
(180, 'adfcafas', 'industrial technology-automotive technology', '2018-04-24 06:49:40', '2018-04-24 06:49:40'),
(181, 'HBS', 'BACHELOR OF SCIENCE IN ENGINEERING', '2018-08-09 02:43:29', '2018-08-09 02:43:29'),
(182, 'JDJF', 'ASSOCIATE IN INFORMATION TECHNOLOGY', '2018-08-29 01:45:07', '2018-08-29 01:45:07'),
(183, '2YHRS', 'TWO- YEAR HOSPITALITY AND RESTAURANT SERVICES', '2018-09-03 01:31:29', '2018-09-03 01:31:29'),
(184, 'ASD', 'ABLE SEAFARER DECK', '2018-10-26 01:50:57', '2018-10-26 01:50:57'),
(185, '2YCT', '2 YEAR COMPUTER TECHNOLOGY', '2018-10-26 03:15:48', '2018-10-26 03:15:48'),
(186, 'TYCST', 'TWO- YEAR COMPUTER SCIENCE TECHNOLOGY', '2018-11-15 01:55:43', '2018-11-15 01:55:43'),
(187, 'NC', 'COMPUTER SERVICING NC II,EVENTS MANAGEMENT SERVICES NC II, BOOKKEEOING NCIII', '2018-11-15 06:40:20', '2018-11-15 06:40:20'),
(188, 'NONE', 'NOT AVAILABLE', '2018-11-19 01:56:47', '2018-11-19 01:56:47'),
(189, 'ite', 'information technology and engineering', '2018-12-11 01:25:32', '2018-12-11 01:25:32'),
(190, 'CSNT', 'COMPUTER SYSTEM & NETWORKING TECHNOLOGY', '2019-01-17 06:42:07', '2019-01-17 06:42:07'),
(191, 'BSP', 'BACHELOR OF SCIENCE IN PHARMACY', '2019-02-22 03:14:20', '2019-02-22 03:14:20'),
(192, 'FAE', 'BACHELOR OF SCIENCE IN HOSPITALITY MANAGEMENT', '2019-03-06 02:11:53', '2019-03-06 02:11:53'),
(193, 'VEG', 'BACHELOR OF SCIENCE IN HOSPITALITY AND TOURISM MANAGEMENT', '2019-03-06 02:13:06', '2019-03-06 02:13:06'),
(194, 'BSET', 'BACHELOR OF SCIENCE IN ELECTRONIC TECHNOLOGY', '2019-03-07 01:32:24', '2019-03-07 01:32:24'),
(195, 'CC', 'COMMUNICATIONS', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(196, 'dhss', 'domican high school', '0000-00-00 00:00:00', '2019-04-30 06:28:26');

-- --------------------------------------------------------

--
-- Table structure for table `curricula`
--

CREATE TABLE `curricula` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_default` tinyint(1) DEFAULT '0',
  `strand_courses_id` varchar(10) NOT NULL,
  `curricula_details` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `rate` varchar(20) NOT NULL,
  `amount` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `enrollment_student`
--

CREATE TABLE `enrollment_student` (
  `id` int(11) NOT NULL,
  `student_no` varchar(30) NOT NULL,
  `student_year_level` varchar(10) NOT NULL,
  `discounts` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `fees_summary` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `setting_id` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `strand` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `enrollment_student_required_subjects`
--

CREATE TABLE `enrollment_student_required_subjects` (
  `id` int(11) NOT NULL,
  `enrollment_student_id` int(11) NOT NULL,
  `assigned_subjects` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fees`
--

CREATE TABLE `fees` (
  `id` int(11) NOT NULL,
  `priority` int(4) NOT NULL,
  `code` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `type` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fee_structure`
--

CREATE TABLE `fee_structure` (
  `id` int(11) NOT NULL,
  `fee_code` varchar(200) NOT NULL,
  `fee_name` varchar(200) NOT NULL,
  `fee_amount` decimal(8,2) NOT NULL,
  `fee_per_subject` decimal(8,2) NOT NULL,
  `strand` varchar(200) NOT NULL,
  `year_level` varchar(200) NOT NULL,
  `setting` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `labs_subjects`
--

CREATE TABLE `labs_subjects` (
  `id` int(11) NOT NULL,
  `subject_code` varchar(200) NOT NULL,
  `subject_name` varchar(200) NOT NULL,
  `lab_amount` varchar(200) NOT NULL,
  `strand` varchar(200) NOT NULL,
  `year_level` varchar(200) NOT NULL,
  `setting` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payco_receipts`
--

CREATE TABLE `payco_receipts` (
  `id` int(11) NOT NULL,
  `receipt_no` varchar(30) NOT NULL,
  `payco_user_id` varchar(30) NOT NULL,
  `status` varchar(30) NOT NULL,
  `remarks` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payco_transactions`
--

CREATE TABLE `payco_transactions` (
  `id` int(11) NOT NULL,
  `subtotal` varchar(20) NOT NULL,
  `cash` varchar(20) NOT NULL,
  `cash_change` varchar(20) NOT NULL,
  `transaction_details` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `receipt_no` varchar(50) NOT NULL,
  `enrollement_student_id` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `preregistration_documents_checklist`
--

CREATE TABLE `preregistration_documents_checklist` (
  `id` int(11) NOT NULL,
  `student_no` varchar(100) NOT NULL,
  `form_138` tinyint(1) DEFAULT NULL,
  `form_137a` tinyint(1) DEFAULT NULL,
  `certificate_of_junior_high_school_completion` tinyint(1) DEFAULT NULL,
  `good_moral_character_certificate` tinyint(1) DEFAULT NULL,
  `birth_certificate` tinyint(1) DEFAULT NULL,
  `honorable_dismissal` tinyint(1) DEFAULT NULL,
  `transcript_of_records` tinyint(1) DEFAULT NULL,
  `qualified_voucher_discrepancy` tinyint(1) DEFAULT NULL,
  `affidavit_of_discrepancy` tinyint(1) DEFAULT NULL,
  `affidavit_of_lost_form_138` tinyint(1) DEFAULT NULL,
  `als_certificate` tinyint(1) DEFAULT NULL,
  `picture_with_nametag` tinyint(1) DEFAULT NULL,
  `education_service_contracting` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `preregistration_info`
--

CREATE TABLE `preregistration_info` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `suffix` varchar(10) NOT NULL,
  `email` int(50) NOT NULL,
  `contact_no` varchar(30) NOT NULL,
  `sex` varchar(20) NOT NULL,
  `birthday` date NOT NULL,
  `birth_place` varchar(200) NOT NULL,
  `region` varchar(120) NOT NULL,
  `province` varchar(120) NOT NULL,
  `city` varchar(120) NOT NULL,
  `barangay` varchar(120) NOT NULL,
  `street_info` varchar(120) NOT NULL,
  `strand_course` varchar(100) NOT NULL,
  `student_number` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `student_type` varchar(45) NOT NULL,
  `enrollment_status` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `preregistration_siblings`
--

CREATE TABLE `preregistration_siblings` (
  `id` int(11) NOT NULL,
  `student_id` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `grade_level` varchar(100) NOT NULL,
  `school` varchar(100) NOT NULL,
  `contact_no` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `preregistration_supporting_info`
--

CREATE TABLE `preregistration_supporting_info` (
  `id` int(11) NOT NULL,
  `student_no` varchar(100) NOT NULL,
  `mother_tongue` varchar(100) NOT NULL,
  `civil_status` varchar(100) NOT NULL,
  `fathers_name` varchar(100) NOT NULL,
  `father_occupation` varchar(100) NOT NULL,
  `mothers_name` varchar(100) NOT NULL,
  `mothers_occupation` varchar(100) NOT NULL,
  `guardian_name` varchar(100) NOT NULL,
  `guardian_contact` varchar(100) NOT NULL,
  `guardian_relationship` varchar(100) NOT NULL,
  `religion` varchar(100) NOT NULL,
  `previous_school` varchar(100) NOT NULL,
  `previous_school_address` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(255) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Teacher', 'Teacher', '2019-07-02 08:26:56', '0000-00-00 00:00:00'),
(2, 'Registrar', 'Registrar', '2019-07-02 08:26:56', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `building_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `strand_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `year_level_id` int(11) DEFAULT NULL,
  `teacher_id` int(11) NOT NULL,
  `setting_id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `schedule_details`
--

CREATE TABLE `schedule_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `schedule_id` int(10) UNSIGNED NOT NULL,
  `subject_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `quota` int(11) NOT NULL DEFAULT '40'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `schedule_subject_details`
--

CREATE TABLE `schedule_subject_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `day_id` int(10) UNSIGNED NOT NULL,
  `room_id` int(10) UNSIGNED NOT NULL,
  `schedule_detail_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `teacher_id` int(10) UNSIGNED NOT NULL,
  `alternating_day_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

CREATE TABLE `schools` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `schools`
--

INSERT INTO `schools` (`id`, `code`, `name`, `created_at`, `updated_at`) VALUES
(1, 'CC', 'COMMUNICATIONSss', '0000-00-00 00:00:00', '2019-05-15 03:13:12'),
(2, 'NEUST', 'Nueva Ecija University of Science and Technology', '2017-03-24 09:53:52', '2017-03-24 09:53:52'),
(3, 'CIC', 'College of the Immaculate Conception', '2017-03-24 09:54:13', '2017-03-24 09:54:13'),
(4, 'AU', 'Araullo University', '2017-03-24 09:54:27', '2017-03-24 09:54:27'),
(5, 'WUP', 'Wesleyan University - Philippines', '2017-03-24 09:54:45', '2017-03-24 09:54:45'),
(6, 'CLSU', 'Central Luzon State University', '2017-03-24 09:55:12', '2017-03-24 09:55:12'),
(7, 'PMI', 'Philippine Maritime Institute', '2017-03-24 09:55:36', '2017-03-24 09:55:36'),
(8, 'LCUP', 'La Consolacion University Philippines', '2017-03-30 04:05:48', '2017-03-30 04:05:48'),
(9, 'DHVTSU', 'Don Honorio Ventura Technological State University', '2017-03-30 04:06:19', '2017-03-30 04:06:19'),
(10, 'MVGFC', 'Manuel V. Gallego Foundation Colleges', '2017-03-30 05:13:08', '2017-03-30 05:13:08'),
(11, 'CGC', 'Core Gateway College', '2017-03-30 05:13:39', '2017-03-30 05:13:39'),
(12, 'LFC', 'La Fortuna College', '2017-03-30 05:14:05', '2017-03-30 05:14:05'),
(13, 'ELJMC', 'Eduardo L. Joson Memorial College', '2017-03-30 05:14:30', '2017-03-30 05:14:30'),
(14, 'WCC', 'World Citi College', '2017-03-30 05:15:08', '2017-03-30 05:15:08'),
(15, 'GDJC', 'General De Jesus College', '2017-03-30 05:15:41', '2017-03-30 05:15:41'),
(16, 'SJCC', 'San Jose Christian Colleges', '2017-03-30 05:15:58', '2017-03-30 05:15:58'),
(17, 'IES', 'Institute of Enterprise Solutions', '2017-03-30 05:16:23', '2017-03-30 05:16:23'),
(18, 'PUp', 'Polytechnic University of the Philippines', '2017-03-30 05:16:46', '2019-05-03 02:15:15'),
(19, 'FEU', 'Far Eastern University', '2017-03-30 05:17:10', '2017-04-03 22:24:38'),
(20, 'MC', 'Meycauayan College', '2017-03-30 05:19:05', '2017-03-30 05:19:05'),
(21, 'BU', 'Baliuag University', '2017-03-30 05:19:31', '2017-03-30 05:19:31'),
(22, 'BPC', 'Bulacan Polytechnic College', '2017-03-30 05:19:48', '2017-03-30 05:19:48'),
(23, 'PDM', 'Pambayang Dalubhasaan ng Marilao', '2017-03-30 05:20:19', '2017-03-30 05:20:19'),
(24, 'BSU', 'Bulacan State University', '2017-03-30 05:20:40', '2017-03-30 05:23:57'),
(25, 'SDSGA', 'Colegio de San Gabriel Arcangel', '2017-03-30 05:21:13', '2017-03-30 05:21:13'),
(26, 'BASC', 'Bulacan Agricultural State College', '2017-03-30 05:21:40', '2017-03-30 05:21:40'),
(27, 'BPC', 'Baliwag Polytechnic College', '2017-03-30 05:22:04', '2017-03-30 05:22:04'),
(28, 'CEU', 'Centro Escolar University', '2017-03-30 05:22:27', '2017-03-30 05:22:27'),
(29, 'SMCM', 'St. Mary\'s College of Meycauayan', '2017-03-30 05:23:02', '2017-03-30 05:23:02'),
(30, 'MQAPC', 'Mariano Quinto Alarilla Polytechnic College', '2017-03-30 05:25:04', '2017-03-30 05:25:04'),
(31, 'CMI', 'College of Mary Immaculate', '2017-03-30 05:25:21', '2017-03-30 05:25:21'),
(32, 'FCPC', 'First City Providential College', '2017-03-30 05:25:53', '2017-03-30 05:25:53'),
(33, 'CMC', 'College of Mt. Carmel', '2017-03-30 05:26:17', '2017-03-30 05:26:17'),
(34, 'PUTP', 'Polytechnic University of The Philippines', '2017-03-30 05:26:57', '2017-03-30 05:26:57'),
(35, 'CLDH.EI', 'Central Luzon Doctors\' Hospital Educational Institution', '2017-03-30 05:27:56', '2017-03-30 05:27:56'),
(36, 'TSU', 'Tarlac State University', '2017-03-30 05:28:12', '2017-03-30 05:28:12'),
(37, 'TCA', 'Tarlac College of Agriculture', '2017-03-30 05:28:29', '2017-03-30 05:28:29'),
(38, 'DCT', 'Dominican College of Tarlac', '2017-03-30 05:28:52', '2017-03-30 05:28:52'),
(39, 'CC', 'Columban College', '2017-03-30 05:29:29', '2017-03-30 05:29:29'),
(40, 'RMTU', 'Ramon Magsaysay Technological University', '2017-03-30 05:29:44', '2017-03-30 05:29:44'),
(41, 'GC', 'Gordon College', '2017-03-30 05:29:59', '2017-03-30 05:29:59'),
(42, 'LSB', 'Lyceum of Subic Bay', '2017-03-30 05:30:21', '2017-03-30 05:30:21'),
(43, 'KS', 'Kolehiyo ng Subic', '2017-03-30 05:30:38', '2017-03-30 05:30:38'),
(44, 'CCBC', 'Comteq Computer and Business College', '2017-03-30 05:31:00', '2017-03-30 05:31:00'),
(45, 'MMC', 'Magsaysay Memorial College', '2017-03-30 05:31:32', '2017-03-30 05:31:32'),
(46, 'PMMA', 'Philippine Merchant Marine Academy', '2017-03-30 05:32:25', '2017-03-30 05:32:25'),
(47, 'UP', 'University of the Philippines', '2017-03-30 05:33:00', '2017-03-30 05:33:00'),
(48, 'DHVTSU', 'Don Honorio Ventura Technological State University', '2017-03-30 05:33:41', '2017-03-30 05:33:41'),
(49, 'PSAU', 'Pampanga State Agricultural University', '2017-03-30 05:33:57', '2017-03-30 05:33:57'),
(50, 'PSCA', 'Philippine State College of Aeronautics', '2017-03-30 05:34:56', '2017-03-30 05:34:56'),
(51, 'HAU', 'Holy Angel University', '2017-03-30 05:35:14', '2017-04-03 22:25:53'),
(52, 'GNU', 'Guagua National Colleges', '2017-03-30 05:35:33', '2017-03-30 05:35:33'),
(53, 'UA', 'University of the Assumption', '2017-03-30 05:36:08', '2017-03-30 05:36:08'),
(54, 'JCFC', 'Jose C. Feliciano College', '2017-03-30 05:36:23', '2017-03-30 05:36:23'),
(55, 'RCC', 'Republic Central Colleges', '2017-03-30 05:36:37', '2017-03-30 05:36:37'),
(56, 'CCSFP', 'City College Of San Fernando Pampanga', '2017-03-30 05:37:03', '2017-03-30 05:37:03'),
(57, 'AUF', 'Angeles University Foundation', '2017-03-30 05:37:46', '2017-03-30 05:37:46'),
(58, 'BPSU', 'Bataan Peninsula State University', '2017-03-30 05:38:22', '2017-03-30 05:38:22'),
(59, 'LPC', 'Limay Polytechnic College', '2017-03-30 05:38:49', '2017-03-30 05:38:49'),
(60, 'SPUP', 'St. Paul University Philippines', '2017-03-30 05:43:57', '2017-03-30 05:43:57'),
(61, 'SMU', 'Saint Mary\'s University', '2017-03-30 05:44:22', '2017-03-30 05:44:22'),
(62, 'NVSU', 'Nueva Vizcaya State University', '2017-03-30 05:44:43', '2017-03-30 05:44:43'),
(63, 'CSU', 'Cagayan State University', '2017-03-30 05:45:23', '2017-03-30 05:45:23'),
(64, 'ULSS', 'University of La Salette Santiago', '2017-03-30 05:45:46', '2017-03-30 05:45:46'),
(65, 'AC', 'Aldersgate College', '2017-03-30 05:46:09', '2017-03-30 05:46:09'),
(66, 'USL', 'University of Saint Louis', '2017-03-30 05:46:24', '2017-03-30 05:46:24'),
(67, 'SFC', 'St. Ferdinand College', '2017-03-30 05:46:43', '2017-03-30 05:46:43'),
(68, 'ISU', 'Isabela State University', '2017-03-30 05:46:56', '2017-03-30 05:46:56'),
(69, 'PSCC', 'Patria Sable Corpus College', '2017-03-30 05:48:01', '2017-03-30 05:48:01'),
(70, 'NC', 'Northeastern College', '2017-03-30 05:48:13', '2017-03-30 05:48:13'),
(71, 'ISU', 'Isabela State University', '2017-03-30 05:48:31', '2017-03-30 05:48:31'),
(72, 'BSC', 'Batanes State College', '2017-03-30 05:49:00', '2017-03-30 05:49:00'),
(73, 'QSC', 'Quirino State College', '2017-03-30 05:49:32', '2017-03-30 05:49:32'),
(74, 'LOA', 'Lyceum of Aparri', '2017-03-30 05:50:04', '2017-03-30 05:50:04'),
(75, 'PNU', 'Philippine Normal University', '2017-03-30 05:50:26', '2017-03-30 05:50:26'),
(76, 'CVCITCI', 'Cagayan Valley Computer and Information Technology College, Inc.', '2017-03-30 05:50:54', '2017-03-30 05:50:54'),
(77, 'ISAP', 'International School of Asia and the Pacific', '2017-03-30 05:51:29', '2017-03-30 05:51:29'),
(78, 'UPHSJ', 'University of Perpetual Help System JONELTA', '2017-03-30 05:52:22', '2017-03-30 05:52:22'),
(79, 'MCNP', 'Medical Colleges of Northern Philippines', '2017-03-30 05:52:50', '2017-03-30 05:52:50'),
(80, 'CEU', 'Centro Escolar University', '2017-04-03 22:23:18', '2017-04-03 22:23:18'),
(81, 'CCPF', 'Community College of Philippines Foundation', '2017-04-12 22:52:50', '2017-04-12 22:52:50'),
(82, 'MCL', 'Malayan Colleges Laguna', '2017-05-15 03:24:29', '2017-05-15 03:24:29'),
(83, 'DPC', 'DIVINA PASTORA COLLEGE', '2017-06-29 02:30:26', '2017-06-29 02:30:26'),
(84, 'hpsmshs', 'honorato c. perez senior memorial science high school', '2017-07-13 01:14:47', '2017-07-13 01:14:47'),
(85, 'RNHS', 'RIZAL NATIONAL HIGH SCHOOL', '2017-07-13 01:45:47', '2017-07-14 05:58:31'),
(86, 'SHALI', 'SACRED HEART ACADEMY OF LUPAO, INC', '2017-07-13 02:21:34', '2017-07-13 02:21:34'),
(87, 'JNHS', 'JAEN NATIONAL HIGH SCHOOL', '2017-07-13 02:43:51', '2017-07-13 02:43:51'),
(88, 'JNHS', 'JAEN NATIONAL HIGH SCHOOL', '2017-07-14 03:53:15', '2017-07-14 03:53:15'),
(89, 'OLSHC', 'OUR LADY OF SACRED HEART COLLEGE OF GUIMBA, INC.', '2017-07-14 06:09:44', '2017-07-14 06:09:44'),
(90, 'PRSM', 'PARK RIDGE SCHOOL OF MONTESSORI', '2017-07-14 07:29:34', '2017-07-14 07:29:34'),
(91, 'MNHS', 'MUñOZ NATIONAL HIGH SCHOOL', '2017-07-18 07:49:19', '2017-07-18 07:49:19'),
(92, 'JRL', 'JUAN R. LIWAG MEMORIAL HIGH SCHOOL', '2017-07-19 00:16:37', '2017-07-19 00:16:37'),
(93, 'mmfi', 'midway maritime foundation inc.', '2017-07-19 03:39:41', '2017-07-19 03:39:41'),
(94, 'srnhs', 'san ricardo national high school', '2017-07-19 05:15:30', '2017-07-19 05:15:30'),
(95, 'DRDSNHS', 'DR. RAMON DE SANTOS NATIONAL HIGH SCHOOL', '2017-07-19 06:24:11', '2017-07-19 06:24:11'),
(96, 'MNHS', 'MARIVELES NATIONAL HIGH SCHOOL', '2017-07-19 08:00:21', '2017-07-19 08:00:21'),
(97, 'PMIC', 'pmi COLLEGES', '2017-07-20 06:21:19', '2017-09-28 03:25:27'),
(98, 'BNHS', 'BURGOS NATIONAL HIGH SCHOOL', '2017-07-20 07:07:07', '2017-07-20 07:07:07'),
(99, 'bnhs', 'bongabon national high school', '2017-07-21 00:26:57', '2017-07-21 00:26:57'),
(100, 'chs', 'cabucbucan high school', '2017-07-21 01:29:26', '2017-07-21 01:29:26'),
(101, 'WCCGI', 'WORLD CITI COLLEGES GUIMBA INC', '2017-07-21 01:50:48', '2017-07-21 01:50:48'),
(102, 'gtnhs', 'general tinio national high school', '2017-07-21 01:58:19', '2017-07-21 01:58:19'),
(103, 'geai', 'gabaldon essential academy inc.', '2017-07-21 02:09:45', '2017-07-21 02:09:45'),
(104, 'scnhs', 'sta. cruz national high school', '2017-07-21 02:16:39', '2017-07-21 02:16:39'),
(105, 'SDNTS', 'STO. DOMINGO NATIONAL TRADE SCHOOL', '2017-07-21 02:24:04', '2017-07-21 02:24:04'),
(106, 'SDNTS', 'STO. DOMINGO NATIONAL TRADE SCHOOL', '2017-07-21 02:24:47', '2017-07-21 02:24:47'),
(107, 'ahs', 'aritao  high school', '2017-07-21 02:29:04', '2017-07-21 02:29:04'),
(108, 'MNHS', 'MAYAPYAP NATIONAL HIGH SCHOOL', '2017-07-21 02:39:09', '2017-07-21 02:39:09'),
(109, 'LVCC', 'LA VERDAD CHRISTIAN COLLEGE, INC.', '2017-07-21 02:56:26', '2017-07-21 02:56:26'),
(110, 'DNHS', 'DIPACULAO NATIONAL HIGH SCHOOL', '2017-07-21 03:46:11', '2017-07-21 03:46:11'),
(111, 'ZCAHS', 'ZACARIAS C. AQUILIZAN HIGH SCHOOL', '2017-07-21 08:42:55', '2017-07-21 08:42:55'),
(112, 'citu', 'cebu institute of technology university', '2017-07-25 00:11:27', '2017-07-25 00:11:27'),
(113, 'NEHS', 'NUEVA ECIJA HIGH SCHOOL', '2017-07-26 05:29:43', '2017-07-26 05:29:43'),
(114, 'BATITANG, ZARAGOZA, NUEVA ECIJA', 'BATITANG NATIONAL HIGH SCHOOL', '2017-07-26 05:53:09', '2017-07-26 05:53:09'),
(115, 'CHS', 'CABUCBUCAN HIGH SCHOOL', '2017-07-26 07:02:34', '2017-07-26 07:02:34'),
(116, 'ANHS', 'ALIAGA NATIONAL HIGH SCHOOL', '2017-07-26 07:10:55', '2017-07-26 07:10:55'),
(117, 'SRNHS', 'STA. RITA NATIONAL HIGH SCHOOL', '2017-07-26 07:28:18', '2017-07-26 07:28:18'),
(118, 'lnhs', 'ligaya national high school', '2017-07-26 07:39:16', '2017-07-26 07:39:16'),
(119, 'lnhs', 'lamo national high school', '2017-07-26 07:46:09', '2017-07-26 07:46:09'),
(120, 'vrbsrnh', 'vicente r. bumanlag sr. national high school', '2017-07-31 07:21:18', '2017-07-31 07:21:18'),
(121, 'SPA', 'SAINT PATRICK\'S ACADEMY', '2017-07-31 08:27:49', '2017-07-31 08:27:49'),
(122, 'pnhs', 'peÑaranda national high school', '2017-07-31 08:38:30', '2017-07-31 08:38:30'),
(123, 'PHS', 'PULO HIGH SCHOOL', '2017-07-31 08:43:24', '2017-07-31 08:43:24'),
(124, 'STA', 'SANTA HIGH SCHOOL INC.', '2017-07-31 08:43:38', '2017-07-31 08:43:38'),
(125, 'AHS', 'CABUCBUCAN HIGH SCHOOL', '2017-07-31 08:48:15', '2017-07-31 08:48:15'),
(126, 'SMNHS', 'SAN MIGUEL NATIONAL HIGH SCHOOL', '2017-08-01 03:38:23', '2017-08-01 03:38:23'),
(127, 'SMNHS', 'SAN MIGUEL NATIONAL HIGH SCHOOL', '2017-08-01 03:39:29', '2017-08-01 03:39:29'),
(128, 'GCEI', 'GRANARY CO EDUCATIONAL INSTITUTION', '2017-08-01 03:39:36', '2017-08-01 03:39:36'),
(129, 'BNHS', 'BINTAWAN NATIONAL HIGH SCHOOL', '2017-08-01 07:23:18', '2017-08-01 07:23:18'),
(130, 'srnhs', 'san roque national high school', '2017-08-01 07:47:18', '2017-08-01 07:47:18'),
(131, 'JOLNHS', 'JULIA ORTIZ LUIS NATIONAL HIGH SCHOOL', '2017-08-01 07:50:23', '2017-08-01 07:50:23'),
(132, 'dbc', 'don bosco college', '2017-08-01 08:06:00', '2017-08-01 08:06:00'),
(133, 'ACNHS', 'ALFONSO CASTAÑEDA NATIONAL HIGH SCHOOL', '2017-08-02 07:03:39', '2017-08-02 07:03:39'),
(134, 'MDRMNHS', 'MARCIANO DEL ROSARIO MEMORIAL NATIONAL HIGH SCHOOL', '2017-08-02 07:20:35', '2017-08-02 07:20:35'),
(135, 'CNHS', 'CALABA NATIONAL HIGH SCHOOL', '2017-08-02 07:36:31', '2017-08-02 07:36:31'),
(136, 'SNHS', 'Salapungan National High School', '2017-08-02 08:33:06', '2017-08-02 08:33:06'),
(137, 'CDJC', 'GENERAL DE JESUS COLLEGE', '2017-08-03 06:22:58', '2017-08-03 06:22:58'),
(138, 'CNHS', 'CABIAO NATIONAL HIGH SCHOOL', '2017-08-03 06:36:14', '2017-08-03 06:36:14'),
(139, 'SSA', 'ST. STEPHEN\'S ACADEMY', '2017-08-03 06:39:14', '2017-08-03 06:39:14'),
(140, 'DNHS', 'DIPACULAO NATIONAL HIGH SCHOOL', '2017-08-03 06:42:16', '2017-08-03 06:42:16'),
(141, 'GTNHS', 'GENERAL TINIO NATIONAL HIGH SCHOOL', '2017-08-03 06:45:53', '2017-08-03 06:45:53'),
(142, 'VVHS', 'VACA VALLEY HIGH SCHOOL', '2017-08-03 06:53:26', '2017-08-03 06:53:26'),
(143, 'PNHS', 'PEÑARANDA NATIONAL HIGH SCHOOL', '2017-08-03 07:05:49', '2017-08-03 07:05:49'),
(144, 'DHS', 'DOMINICAN HIGH SCHOOL', '2017-08-03 07:15:37', '2017-08-03 07:15:37'),
(145, 'JJRMHS', 'JOHN J. RUSSELL MEMORIAL HIGH SCHOOL', '2017-08-03 07:22:32', '2017-08-03 07:22:32'),
(146, 'TNHS', 'TALAVERA NATIONAL HIGH SCHOOL', '2017-08-03 07:36:54', '2017-08-03 07:36:54'),
(147, 'AJS', 'ST. JOSEPH SCHOOL', '2017-08-03 08:09:45', '2017-08-03 08:09:45'),
(148, 'SJS', 'ST. JOSEPH SCHOOL', '2017-08-03 08:11:22', '2017-08-03 08:11:22'),
(149, 'CFGHS', 'CARLOS F. GONZALES HIGH SCHOOL', '2017-08-03 08:14:57', '2017-08-03 08:14:57'),
(150, 'SRNHS', 'STO. ROSARIO NATIONAL HIGH SCHOOL', '2017-08-03 08:20:31', '2017-08-03 08:20:31'),
(151, 'PCNHS', 'PALAYAN CITY NATIONAL HIGH SCHOOL', '2017-08-04 08:09:33', '2017-08-04 08:09:33'),
(152, 'shs', 'salapungan high school', '2017-08-04 08:38:12', '2017-08-04 08:38:12'),
(153, 'qghs', 'quirino general high school', '2017-08-04 08:43:02', '2017-08-04 08:43:02'),
(154, 'STNHS', 'STA RITA NATIONAL HIGH SCHOOL', '2017-08-07 06:43:42', '2017-08-07 06:43:42'),
(155, 'AHS', 'ARITAO HIGH SCHOOL', '2017-08-08 01:12:22', '2017-08-08 01:12:22'),
(156, 'VCA', 'VINCENTIAN CATHOLIC ACADEMY', '2017-08-08 01:18:08', '2017-08-08 01:18:08'),
(157, 'PMMA', 'PANGASINAN MERCHANT MARINE ACADEMY', '2017-08-08 01:23:16', '2017-08-08 01:23:16'),
(158, 'sti', 'sti college dagupan', '2017-08-08 02:10:15', '2017-08-08 02:10:15'),
(159, 'rbphs', 'restituto b. peria national high school', '2017-08-08 07:07:45', '2017-08-08 07:07:45'),
(160, 'GVAHS', 'GABALDON VOCATIONAL AGRICULTURE HIGH SCHOOL', '2017-08-08 07:19:57', '2017-08-08 07:19:57'),
(161, 'SJCNHS', 'SAN JOSE CITY NATIONAL HIGH SCHOOL', '2017-08-09 07:29:14', '2017-08-09 07:29:14'),
(162, 'SNDS', 'STO. NIÑO DIOCESAN SCHOOL', '2017-08-09 07:49:03', '2017-08-09 07:49:03'),
(163, 'SJNHS', 'SAN JOSEF NATIONAL HIGH SCHOOL', '2017-08-09 07:54:42', '2017-08-09 07:54:42'),
(164, 'TADNHS', 'T.A. DIONISIO NATIONAL HIGH SCHOOL', '2017-08-09 07:59:44', '2017-08-22 01:32:14'),
(165, 'BNHS', 'BAGABAG NATIONAL HIGH SCHOOL', '2017-08-09 08:24:12', '2017-08-09 08:24:12'),
(166, 'RLHS', 'ROXY LEFFORGE FOUNDATION COLLEGES', '2017-08-10 02:26:35', '2017-08-10 02:26:35'),
(167, 'SFNHS', 'SAN FRANCISCO NATIONAL HIGH SCHOOL', '2017-08-10 07:23:18', '2017-08-10 07:23:18'),
(168, 'MCSMA', 'MOUNT CARMEL SCHOOL OF MARIA AURORA', '2017-08-10 07:29:11', '2017-08-10 07:29:11'),
(169, 'RNHS', 'RAMADA NATIONAL HIGH SCHOOL', '2017-08-10 07:34:27', '2017-08-10 07:34:27'),
(170, 'FCAT', 'FERNANDEZ COLLEGE OF ARTS & TECHNOLOGY', '2017-08-10 07:44:53', '2017-08-10 07:44:53'),
(171, 'SRLCS', 'ST. ROSE OF LIMA CATHOLIC SCHOOL', '2017-08-10 08:22:08', '2017-08-10 08:22:08'),
(172, 'PHS', 'PORAIS HIGH SCHOOL', '2017-08-10 08:28:45', '2017-08-10 08:28:45'),
(173, 'TNHS', 'TABACAO NATIONAL HIGH SCHOOL', '2017-08-10 08:35:14', '2017-08-10 08:35:14'),
(174, 'GEI', 'GREAT EASTERN INSTITUTE', '2017-08-10 08:45:15', '2017-08-10 08:45:15'),
(175, 'SNHS', 'SAMPAGUITA NATIONAL HIGH SCHOOL', '2017-08-11 00:24:50', '2017-08-11 00:24:50'),
(176, 'SPXI', 'ST. PIUS X INSTITUTE', '2017-08-11 08:00:37', '2017-08-11 08:00:37'),
(177, 'BHCS', 'BLESSED HOPE CHRISTIAN SCHOOL', '2017-08-11 08:14:31', '2017-08-11 08:14:31'),
(178, 'LSAI', 'LA SALETTE OF AURORA, INC.', '2017-08-11 08:21:37', '2017-08-11 08:21:37'),
(179, 'bnhs', 'batitang national high school', '2017-08-14 00:47:20', '2017-08-14 00:47:20'),
(180, 'ahs', 'amucao high school', '2017-08-14 01:25:10', '2017-08-14 01:25:10'),
(181, 'SPSHS', 'ST. PAUL SCHOOL, GEN. NATIVIDAD', '2017-08-14 01:59:55', '2017-08-14 01:59:55'),
(182, 'NEHS', 'NUEVA ECIJA HIGH SCHOOL', '2017-08-14 02:03:35', '2017-08-14 02:03:35'),
(183, 'SOLA', 'SCHOOL OF OUR LADY OF ATOCHA', '2017-08-14 02:10:00', '2017-08-14 02:10:00'),
(184, 'olfa', 'our lady of fatima academy', '2017-08-14 02:33:35', '2017-08-14 02:33:35'),
(185, 'TFBC', 'TARLAC FIRST BAPTIST CHURCH SCHOOL INC.', '2017-08-14 02:40:35', '2017-08-14 02:40:35'),
(186, 'PNHS', 'PANTABANGAN NATIONAL HIGH SCHOOL', '2017-08-14 02:45:04', '2017-08-14 02:45:04'),
(187, 'MNHS', 'MUÑOZ NATIONAL HIGH SCHOOL', '2017-08-14 02:53:37', '2017-08-14 02:53:37'),
(188, 'CTNHS', 'CAMP TINIO NATIONAL HIGH SCHOOL', '2017-08-14 07:03:11', '2017-08-14 07:03:11'),
(189, 'PSJNHS', 'PUTLOD-SAN JOSE NATIONAL HIGH SCHOOL', '2017-08-14 07:17:46', '2017-08-14 07:17:46'),
(190, 'VRSHS', 'VEDASTO R. SANTIAGO HIGH SCHOOL', '2017-08-15 00:56:23', '2017-08-15 00:56:23'),
(191, 'MNHS', 'MANGGITAHAN NATIONAL HIGH SCHOOL', '2017-08-15 02:25:46', '2017-08-15 02:25:46'),
(192, 'SPSSA', 'ST.PAUL SCHOOL OF SAN ANTONIO', '2017-08-15 03:00:49', '2017-08-15 03:00:49'),
(193, 'CNHS', 'CUYAPO NATIONAL HIGH SCHOOL', '2017-08-15 03:04:40', '2017-08-15 03:04:40'),
(194, 'RMHS', 'RAMON MAGSAYSAY (CUBAO) HIGH SCHOOL', '2017-08-15 05:58:35', '2017-08-15 05:58:35'),
(195, 'SLA', 'SAN LEONARDO ACADEMY', '2017-08-15 06:34:40', '2017-08-15 06:34:40'),
(196, 'dhs', 'domican high school', '2017-08-15 07:01:44', '2017-08-15 07:01:44'),
(197, 'SPATSM', 'ST.PAUL UNIVERSITY AT SAN MIGUEL', '2017-08-15 07:07:42', '2017-08-15 07:07:42'),
(198, 'GSS', 'GOOD SAMARITAN COLLEGES', '2017-08-15 07:25:00', '2017-08-15 07:25:00'),
(199, 'KA', 'KALAHAN ACADEMY', '2017-08-15 07:30:59', '2017-08-15 07:30:59'),
(200, 'SMHS', 'SAN MATEO VOCATIONAL AND INDUSTRIAL HIGH SCHOOL', '2017-08-15 07:39:30', '2017-08-15 07:39:30'),
(201, 'CLMS', 'CLEVER LANE MONTESSORI SCHOOL', '2017-08-15 08:53:09', '2017-08-15 08:53:09'),
(202, 'srlcs', 'st. rose of lima catholic school', '2017-08-16 05:23:33', '2017-08-16 05:23:33'),
(203, 'SJCNHS', 'SAN JOSE CITY NATIONAL HIGH SCHOOL', '2017-08-16 05:39:18', '2017-08-16 05:39:18'),
(204, 'SJNPS', 'SAINT JOHN NEPOMUCENE PAROCHIAL SCHOOL', '2017-08-16 06:58:07', '2017-08-16 06:58:07'),
(205, 'ECC', 'EVELAND CHRISTIAN COLLEGE', '2017-08-16 07:46:57', '2017-08-16 07:46:57'),
(206, 'LNHS', 'LAMBAKIN NATIONAL HIGH SCHOOL', '2017-08-16 08:10:51', '2017-08-16 08:10:51'),
(207, 'ELJMHS', 'EDUARDO L JOSON MEMORIAL HIGH SCHOOL', '2017-08-16 08:20:55', '2017-08-16 08:20:55'),
(208, 'ECNHS', 'EASTERN CABU NATIONAL HIGH SCHOOL', '2017-08-16 08:25:25', '2017-08-16 08:25:25'),
(209, 'sams', 'san antonio montessori school', '2017-08-17 00:08:42', '2017-08-17 00:08:42'),
(210, 'TRYCEI', 'T.R. YANGCO CATHOLIC EDUCATION INSTITUTE', '2017-08-17 00:14:48', '2017-08-17 00:14:48'),
(211, 'mhs', 'mambangnan high school', '2017-08-18 00:19:27', '2017-08-18 00:19:27'),
(212, 'MNHS', 'MALIGAYA NATIONAL HIGH SCHOOL', '2017-08-18 06:41:35', '2017-08-18 06:41:35'),
(213, 'INHS', 'IBONA NATIONAL HIGH SCHOOL', '2017-08-22 01:18:30', '2017-08-22 01:18:30'),
(214, 'MCPNHS', 'MAYOR CESARIO A PIMENTEL NAT\'L, HIGH SCHOOL', '2017-08-22 01:27:09', '2017-08-22 01:27:09'),
(215, 'ZNHS', 'ZARAGOZA NATIONAL HIGH SCHOOL', '2017-08-22 02:05:52', '2017-08-22 02:05:52'),
(216, 'DNHS', 'DINADIAWAN NATIONAL HIGH SCHOOL', '2017-08-22 02:20:49', '2017-08-22 02:20:49'),
(217, 'ULSI', 'UNIVERSITY OF LA SALETTE INC.', '2017-08-22 02:46:35', '2017-08-22 02:46:35'),
(218, 'MMHS', 'MAKABULOS MEMORIAL HIGH SCHOOL', '2017-08-22 05:29:06', '2017-08-22 05:29:06'),
(219, 'dnhs', 'diarabasin national high school', '2017-08-22 07:11:06', '2017-08-22 07:11:06'),
(220, 'mcc', 'mount carmel college', '2017-08-22 07:33:08', '2017-08-22 07:33:08'),
(221, 'tnhs', 'talugtog national high school', '2017-08-22 08:32:51', '2017-08-22 08:32:51'),
(222, 'HCC', 'HOLY CROSS COLLEGE', '2017-08-23 06:28:42', '2017-08-23 06:28:42'),
(223, 'BSNHS', 'BARTOLOME SANGALANG NATIONAL HIGH SCHOOL', '2017-08-23 06:32:24', '2017-08-23 06:32:24'),
(224, 'CNHS', 'CARRANGLAN NATIONAL HIGH SCHOOL', '2017-08-23 06:42:13', '2017-08-23 06:42:13'),
(225, 'DSH', 'DOÑA JUANA CHIOCO NATIONAL HIGH SCHOOL', '2017-08-24 01:17:09', '2017-08-24 01:17:23'),
(226, 'GNHS', 'GANDUZ NATIONAL HIGH SCHOOL', '2017-08-24 01:31:57', '2017-08-24 01:31:57'),
(227, 'SMNHS', 'SAN MARIANO NATIONAL HIGH SCHOOL', '2017-08-24 01:48:39', '2017-08-24 01:48:39'),
(228, 'UMHS', 'UMANGAN NATIONAL HIGH SCHOOL', '2017-08-24 01:52:15', '2017-08-24 01:52:15'),
(229, 'SBHS', 'STA BARBARA HIGH SCHOOL', '2017-08-24 02:06:04', '2017-08-24 02:06:04'),
(230, 'DHS', 'DIGDIG HIGH SCHOOL', '2017-08-25 01:04:23', '2017-08-25 01:04:23'),
(231, 'CNHSAT', 'CABARROGUIS NATIONAL SCHOOL OF ARTS & TRADES', '2017-08-25 01:09:18', '2017-08-25 01:09:18'),
(232, 'bhs', 'BETTBIEN HIGH SCHOOL', '2017-08-25 01:33:58', '2017-09-11 03:06:04'),
(233, 'eci', 'ednor colleges incorporated', '2017-08-25 02:28:20', '2017-08-25 02:28:20'),
(234, 'mnhs', 'maliwalo national high school', '2017-08-25 03:12:40', '2017-08-25 03:12:40'),
(235, 'SCHS', 'SAN CARLOS HIGH SCHOOL', '2017-08-29 03:47:27', '2017-08-29 03:47:27'),
(236, 'SCHS', 'SAN CARLOS HIGH SCHOOL', '2017-08-29 03:48:40', '2017-08-29 03:48:40'),
(237, 'RMHS', 'REH MONTESORRI COLLEGE', '2017-08-29 05:45:25', '2017-08-29 05:45:25'),
(238, 'ecrhs', 'eliseo c. ronquillo sr. memorial national high school', '2017-08-29 06:32:30', '2017-08-29 06:32:30'),
(239, 'srnhs', 'santa rosa national high school', '2017-08-29 06:46:48', '2017-08-29 06:46:48'),
(240, 'DSH', 'DIFFUN HIGH SCHOOL', '2017-08-30 02:43:29', '2017-08-30 02:43:29'),
(241, 'MNHS', 'MOLLARCA NATIONAL HIGH SCHOOL', '2017-08-30 03:00:17', '2017-08-30 03:00:17'),
(242, 'OSOA', 'ODIZEE SCHOOL OF ACHIEVERS', '2017-08-31 02:06:51', '2017-08-31 02:06:51'),
(243, 'dnhs', 'dingalan national high school', '2017-08-31 02:44:19', '2017-08-31 02:44:19'),
(244, 'CHS', 'CABULAY HIGH SCHOOL', '2017-08-31 03:29:40', '2017-08-31 03:29:40'),
(245, 'NEDCI', 'NUEVA ECIJA DOCTOR\'S COLLEGES, INC.', '2017-08-31 06:31:01', '2017-08-31 06:31:01'),
(246, 'snhs', 'salinungan NATIONAL high school', '2017-09-04 01:38:25', '2017-09-04 01:38:25'),
(247, 'MNHS', 'MACABACLAY NATIONAL HIGH SCHOOL', '2017-09-04 07:07:27', '2017-09-04 07:07:27'),
(248, 'aghs', 'san agustin diocesan academy inc.', '2017-09-04 08:11:12', '2017-09-04 08:11:12'),
(249, 'CNHS', 'CARMEN NATIONAL HIGH SCHOOL', '2017-09-04 08:16:35', '2017-09-04 08:16:35'),
(250, 'mnhs', 'marawa national high school', '2017-09-05 02:37:15', '2017-09-05 02:37:15'),
(251, 'ERLHS', 'EXEQUIEL R. LINA HIGH SCHOOL', '2017-09-05 02:55:54', '2017-09-05 02:55:54'),
(252, 'als a&e', 'alternative learning system accreditation and equivalency', '2017-09-05 06:36:50', '2017-09-05 06:36:50'),
(253, 'PHS', 'PAMBUAN HIGH SCHOOL', '2017-09-05 08:40:07', '2017-09-05 08:40:07'),
(254, 'neda', 'n.e. dominican academy', '2017-09-06 05:55:00', '2017-09-06 05:55:00'),
(255, 'SCNHS', 'SANTIAGO CITY NATIONAL HIGH SCHOOL', '2017-09-06 06:11:46', '2017-09-06 06:11:46'),
(256, 'bnhs', 'bulac national high school', '2017-09-07 01:30:02', '2017-09-07 01:30:02'),
(257, 'BNHS', 'BALER NATIONAL HIGH SCHOOL', '2017-09-07 01:46:58', '2017-09-07 01:46:58'),
(258, 'PHS', 'PACAC HIGH SCHOOL', '2017-09-07 02:36:43', '2017-09-07 02:36:43'),
(259, 'sja', 'st. john\'s academy', '2017-09-07 02:58:49', '2017-09-07 02:58:49'),
(260, 'olshcg', 'our lady of the sacred heart college of guimba, inc.', '2017-09-08 00:48:50', '2017-09-08 00:48:50'),
(261, 'spsa', 'st. paul school of san antonio', '2017-09-08 01:23:10', '2017-09-08 01:23:10'),
(262, 'GLNHS', 'GENERAL LUNA NATIONAL HIGH SCHOOL', '2017-09-08 02:40:37', '2017-09-08 02:40:37'),
(263, 'SFNHS', 'STA FE NATIONAL HIGH SCHOOL', '2017-09-08 03:38:53', '2017-09-08 03:38:53'),
(264, 'DIS', 'DUMABATO INTEGRATED SCHOOL', '2017-09-11 00:03:10', '2017-09-11 00:03:10'),
(265, 'THS', 'TONDOD HIGH SCHOOL', '2017-09-11 00:07:52', '2017-09-11 00:07:52'),
(266, 'PNHS', 'PULO NATIONAL HIGH SCHOOL', '2017-09-11 00:35:51', '2017-09-11 00:35:51'),
(267, 'BES', 'BONGABON ESSENTIAL SCHOOL', '2017-09-11 00:39:15', '2017-09-11 00:39:15'),
(268, 'MCS', 'MADDELA COMPREHENSIVE SCHOOL', '2017-09-11 02:24:16', '2017-09-11 02:24:16'),
(269, 'VNHS', 'VICTORIA NATIONAL HIGH SCHOOL', '2017-09-11 02:28:17', '2017-09-11 02:28:17'),
(270, 'SCNHS', 'STA. CRUZ NATIONAL HIGH SCHOOL', '2017-09-11 02:42:52', '2017-09-11 02:42:52'),
(271, 'SNHS', 'SAN ANTON NATIONAL HIGH SCHOOL', '2017-09-11 03:21:59', '2017-09-11 03:21:59'),
(272, 'mmnhs', 'macario molina national high school', '2017-09-11 03:30:53', '2017-09-11 03:30:53'),
(273, 'SPUSM', 'ST. PAUL UNIVERSITY AT SAN MIGUEL', '2017-09-15 03:57:54', '2017-09-15 03:57:54'),
(274, 'dvs', 'divisoria high school', '2017-09-19 00:15:35', '2017-09-19 00:15:35'),
(275, 'SECSJ', 'STI EDUCATION CENTER SAN JOSE', '2017-09-19 04:41:42', '2017-09-19 04:41:42'),
(276, 'CVMSH', 'CESAR E. VERGARA MEMORIAL HIGh SCHOOL', '2017-09-21 01:16:44', '2017-11-03 02:43:10'),
(277, 'PSC', 'PHILIPPINE STATESMAN COLLEGE', '2017-09-21 02:15:16', '2017-09-21 02:15:16'),
(278, 'HGNHS', 'HERMINIO G. NICOLAS  HIGH SCHOOL', '2017-09-21 05:58:07', '2017-09-21 05:58:07'),
(279, 'KK', 'KK', '2017-09-21 06:46:33', '2017-09-21 06:46:33'),
(280, 'LNHS', 'LUNA NATIONAL HIGH SCHOOL-ANNEX', '2017-09-21 06:49:46', '2017-09-21 06:50:49'),
(281, 'DDNNHS', 'DUPAX DEL NORTE NATIONAL HIGH SCHOOL', '2017-09-21 06:55:37', '2017-09-21 06:55:37'),
(282, 'LACAI', 'LPI AGAPE CHRISTIAN ACADEMY INC.', '2017-09-21 06:59:52', '2017-09-21 06:59:52'),
(283, 'MPGCS', 'MOUNTAIN PROVINCE GENERAL COMPREHENSIVE SCHOOL', '2017-09-21 07:12:49', '2017-09-21 07:12:49'),
(284, 'ESVE', 'ELIM SCHOOL FOR VALUES AND EXCELLENCE', '2017-09-21 07:30:08', '2017-09-21 07:30:08'),
(285, 'DJLSHS', 'DR. JOSE LAPUZ SALONGA HIGH SCHOOL', '2017-09-21 08:02:26', '2017-09-21 08:02:26'),
(286, 'SHS', 'SOLANO HIGH SCHOOL', '2017-09-21 08:20:38', '2017-09-21 08:20:38'),
(287, 'UE', 'UNIVERSITY OF THE EAST', '2017-09-26 03:31:55', '2017-09-26 03:31:55'),
(288, 'SLRDAI', 'san lorenzo ruiz diocesan academy, inc.', '2017-09-27 07:50:13', '2017-09-27 07:50:13'),
(289, 'CDSCDJ', 'COLEGIO DEL SAGRADO CORAZON DE JESUS', '2017-09-29 05:37:10', '2017-09-29 05:37:10'),
(290, 'CDSCDJ', 'COLEGIO DEL SAGRADO CORAZON DE JESUS', '2017-09-29 05:37:10', '2017-09-29 05:37:10'),
(291, 'bsanhs', 'benigno s. aquino national high school', '2017-09-29 06:56:40', '2017-09-29 06:56:40'),
(292, 'PSHN', 'PSHN-UMIRAY', '2017-10-04 03:17:34', '2017-10-04 03:17:34'),
(293, 'SMU', 'SAINT MARY\'S UNIVERSITY', '2017-10-04 06:57:18', '2017-10-04 06:57:18'),
(294, 'fhhs', 'fort magsaysay national high school', '2017-10-05 07:10:58', '2017-10-05 07:10:58'),
(295, 'ghs', 'GUEVARA HIGH SCHOOL', '2017-10-06 03:48:43', '2017-10-06 03:48:43'),
(296, 'aclc', 'ama computer learning center', '2017-10-11 01:37:32', '2017-10-11 02:12:05'),
(297, 'APCI', 'AURORA POLYTECHNIC COLLEGE, INC.', '2017-10-11 02:46:23', '2017-10-11 02:46:23'),
(298, 'MHS', 'MALIGAYA HIGH SCHOOL', '2017-10-11 03:30:10', '2017-10-11 03:30:10'),
(299, 'sjnhs', 'SAN JOSEF NATIONAL HIGH SCHOOL', '2017-10-11 05:52:25', '2017-10-11 05:52:25'),
(300, 'JLSCCF', 'JESUS LORD & SAVIOR CHRISTIAN COLLEGE FOUNDATION, INC.', '2017-10-13 03:43:13', '2017-10-13 03:43:13'),
(301, 'MNHS', 'MALLORCA NATIONAL HIGH SCHOOL', '2017-10-16 07:19:04', '2017-10-16 07:19:04'),
(302, 'SMNHS', 'STA. MARIA NATIONAL HIGH SCHOOL', '2017-10-16 07:29:06', '2017-10-16 07:29:06'),
(303, 'SPHS', 'STA. PEREGRINA HIGH SCHOOL', '2017-10-16 07:56:51', '2017-10-16 07:56:51'),
(304, 'LNHS', 'LIBERTAD NATIONAL HIGH SCHOOL', '2017-10-16 08:12:45', '2017-10-16 08:12:45'),
(305, 'SNHS', 'SAN NICOLAS HIGH SCHOOL', '2017-10-17 07:01:07', '2017-10-17 07:01:07'),
(306, 'CHS', 'CADACLAN HIGH SCHOOL', '2017-10-17 07:13:13', '2017-10-17 07:13:13'),
(307, 'NNHS', 'NAGTIPUNAN NATIONAL HIGH SCHOOL', '2017-10-17 07:25:14', '2017-10-17 07:25:14'),
(308, 'STA', 'SAINT TERESITA\'S ACADEMY', '2017-10-17 08:00:11', '2017-10-17 08:00:11'),
(309, 'RNHS', 'ROXAS NATIONAL HIGH SCHOOL', '2017-10-17 08:02:36', '2017-10-17 08:02:36'),
(310, 'mnhs', 'MAGPAPALAYOK NATIONAL HIGH SCHOOL', '2017-10-24 02:56:03', '2017-10-24 02:56:03'),
(311, 'PSU', 'pangasinan state university', '2017-10-26 08:28:21', '2018-02-15 01:26:27'),
(312, 'aclc', 'aclc college', '2017-10-27 01:20:09', '2017-10-27 01:20:09'),
(313, 'cdscdj', 'colegio del sagrado corazon de jesus', '2017-10-27 02:55:46', '2017-10-27 02:55:46'),
(314, 'lnhs', 'llanera national high school', '2017-10-30 00:43:22', '2017-10-30 00:43:22'),
(315, 'trac', 'tawi tawi regional agricultural college', '2017-10-30 01:47:08', '2017-10-30 01:47:08'),
(316, 'earst', 'eulogio \"amang\" rodriguez institute of science and technology', '2017-10-30 06:21:46', '2017-10-30 06:21:46'),
(317, 'clsu-ctc', 'clsu-ctc foundation', '2017-10-30 07:33:47', '2017-10-30 07:33:47'),
(318, 'tvl-he-hrm', 'skills power institute', '2017-11-03 00:11:07', '2017-11-03 00:11:07'),
(319, 'gei', 'great eastern institute', '2017-11-03 06:18:01', '2017-11-03 06:18:01'),
(320, 'shsbishs', 'doÑa hortencia salas benedicto integrated senior high school', '2017-11-03 08:35:21', '2017-11-03 08:35:21'),
(321, 'jblfmui', 'john b. lacson foundation maritime university (arevalo), inc.', '2017-11-07 00:00:15', '2017-11-07 00:00:15'),
(322, 'crt', 'colllege for research & technology of cabanatuan', '2017-11-07 01:40:28', '2017-11-07 01:40:28'),
(323, 'IIRT', 'NCST INSTITUTE OF INDUSTRIAL RESEARCH & TRAINING', '2017-11-07 02:07:38', '2017-11-07 02:07:38'),
(324, 'aclc cg', 'aclc college of gapan', '2017-11-07 09:29:21', '2017-11-07 09:29:21'),
(325, 'sjasjci', 'st. john\'s academy of san jose city, inc.', '2017-11-07 10:24:53', '2017-11-07 10:25:36'),
(326, 'isat', 'isabela college of arts & technology', '2017-11-08 03:27:36', '2017-11-08 03:27:36'),
(327, 'dhsbishs', 'doÑa hortencia salas benedicto integrated shs', '2017-11-10 06:12:00', '2017-11-10 06:12:00'),
(328, 'SCANE', 'ST. CHRISTOPHER ACADEMY OF NUEVA ECIJA, INC.', '2017-11-13 10:03:11', '2017-11-13 10:03:11'),
(329, 'sqnhs', 'SAN QUINTIN NATIONAL HIGH SCHOOL', '2017-11-15 01:29:49', '2017-11-15 01:29:49'),
(330, 'CHS', 'CADACLAN HIGH SCHOOL', '2017-11-21 03:17:36', '2017-11-21 03:17:36'),
(331, 'GMNNHS', 'general mamerto natividad NATIONAL HIGH SCHOOL', '2017-11-21 05:44:57', '2017-11-21 05:44:57'),
(332, 'manhs', 'maria aurora national high school', '2017-11-21 05:51:23', '2017-11-21 05:51:23'),
(333, 'JMPHS', 'JORGE M. PADILLA HIGH SCHOOL', '2017-11-21 06:23:46', '2017-11-21 06:23:46'),
(334, 'LNHS', 'LINGA NATIONAL HIGH SCHOOL', '2017-11-21 07:09:00', '2017-11-21 07:09:00'),
(335, 'FAISC', 'FIRST ASIAN INTERNATIONAL SYSTEMS COLLEGE', '2017-11-28 02:51:56', '2017-11-28 02:51:56'),
(336, 'PSHS', 'PAITAN SUR HIGH SCHOOL', '2017-11-28 07:26:17', '2017-11-28 07:26:17'),
(337, 'npcfc', 'northern philippines college of maritime science and technology inc.', '2017-11-29 05:48:22', '2017-11-29 05:48:22'),
(338, 'spi', 'skill-power institute', '2017-12-04 02:17:36', '2017-12-04 02:17:36'),
(339, 'pue', 'PSHN Umiray Extension', '2017-12-04 04:01:29', '2017-12-04 04:01:29'),
(340, 'CNHS', 'CABATUAN NATIONAL HIGH SCHOOL', '2017-12-04 05:40:22', '2017-12-04 05:40:22'),
(341, 'MCSSL', 'MOUNT CARMEL SCHOOL OF SAN LUIS', '2017-12-04 07:43:09', '2017-12-04 07:43:09'),
(342, 'mkhs', 'mataas na kahoy high school', '2017-12-05 00:03:41', '2017-12-05 00:03:41'),
(343, 'PNTC', 'PNTC COLLEGES', '2017-12-05 00:46:42', '2017-12-05 00:46:42'),
(344, 'PNHS', 'PAMBUAN NATIONAL HIGH SCHOOL', '2017-12-05 06:34:32', '2017-12-05 06:34:32'),
(345, 'LNHS', 'LA PAZ  NATIONAL HIGH SCHOOL', '2017-12-06 02:11:38', '2017-12-06 02:11:38'),
(346, 'LPNHS', 'LA PAZ NATIONAL HIG SCHOOL', '2017-12-06 02:14:21', '2017-12-06 02:14:21'),
(347, 'SHA', 'SACRED HEART ACADEMY', '2017-12-11 03:44:16', '2017-12-11 03:44:16'),
(348, 'actc', 'access computer & technical colleges', '2017-12-12 07:26:36', '2017-12-12 07:26:36'),
(349, 'crt', 'college for research & technology of cabanatuan', '2017-12-14 05:11:11', '2017-12-14 05:11:11'),
(350, 'SNHS', 'SALUPUNGAN NATIONAL HIGH SCHOOL', '2017-12-15 02:00:16', '2017-12-15 02:00:16'),
(351, 'DHHS', 'DISALAG NATIONAL HIGH SCHOOL', '2017-12-15 03:29:35', '2017-12-15 03:29:35'),
(352, 'MCCC', 'MOUNT CARMEL COLLEGE OF CASIGURAN', '2017-12-18 03:44:43', '2017-12-18 03:44:43'),
(353, 'asct', 'aurora state college of technology', '2017-12-20 00:49:15', '2017-12-20 00:49:15'),
(354, 'bma', 'baliwag maritime academy', '2017-12-20 01:39:00', '2017-12-20 01:39:00'),
(355, 'bnhs', 'bicos national high school', '2017-12-20 02:40:30', '2017-12-20 02:40:30'),
(356, 'acc', 'aie college cabanatuan', '2017-12-20 07:34:30', '2017-12-20 07:34:30'),
(357, 'sls', 'saint louis school', '2018-01-04 08:17:17', '2018-01-04 08:18:05'),
(358, 'hit', 'harvard institute of technology', '2018-01-05 00:30:29', '2018-01-05 00:30:29'),
(359, 'TIP', 'TECHNOLOGICAL INSTITUTE OF THE PHILIPPINES', '2018-01-10 06:07:58', '2018-01-10 06:07:58'),
(360, 'VGCTC', 'VMA GLOBAL COLLEGE & TRAINING CENTERS, INC.', '2018-01-11 07:15:05', '2018-01-11 07:15:05'),
(361, 'MKNHS', 'MATAAS NA KAHOY NATIONAL HIGH SCHOOL', '2018-01-12 02:17:33', '2018-01-12 02:17:33'),
(362, 'QSU', 'QUIRINO STATE UNIVERSITY', '2018-01-12 07:13:21', '2018-01-12 07:13:21'),
(363, 'mpspc', 'mountain province state polytechnic college', '2018-01-12 07:18:46', '2018-01-12 07:18:46'),
(364, 'NU', 'NORTHWESTERN UNIVERSITY', '2018-01-12 07:54:38', '2018-01-12 07:54:38'),
(365, 'UPHSL', 'UNIVERSITY OF PERPETUAL HELP SYSTEM LAGUNA', '2018-01-12 07:58:42', '2018-01-12 07:58:42'),
(366, 'NPI', 'NAMEI POLYTECHNIC INSTITUTE', '2018-01-15 08:04:49', '2018-01-15 08:04:49'),
(367, 'dglfc', 'dr. gloria lacson foundation colleges', '2018-01-18 00:55:15', '2018-01-18 00:55:15'),
(368, 'SAEC', 'SAN LEONARDO ACADEMIC EXTENSION CAMPUS', '2018-01-18 01:37:04', '2018-01-18 01:37:04'),
(369, 'DD', 'OUR LADY OF FATIMA UNIVERSITY', '2018-01-18 01:46:12', '2018-01-18 01:46:12'),
(370, 'jlcf-bi', 'john b. lacson colleges foundation-bacolod, inc.', '2018-01-18 02:03:22', '2018-01-18 02:03:22'),
(371, 'lpu', 'lyceum of the philippines university', '2018-01-18 02:26:48', '2018-01-18 02:26:48'),
(372, 'unhs', 'umiray national high school', '2018-01-18 02:44:02', '2018-01-18 02:44:02'),
(373, 'knhs', 'kalumpang national high school', '2018-01-18 02:58:17', '2018-01-18 02:58:17'),
(374, 'pcnhs', 'pines city national high school', '2018-01-18 03:12:14', '2018-01-18 03:12:14'),
(375, 'sacsti', 'st. anthony center of science and technology inc.', '2018-01-18 03:16:29', '2018-01-18 03:16:29'),
(376, 'lmbnhs', 'leonor m. bautista national high school', '2018-01-18 03:26:44', '2018-01-18 03:26:44'),
(377, 'lmbnhs', 'leonor m. bautista national high school', '2018-01-18 03:26:49', '2018-01-18 03:26:49'),
(378, 'lmbnhs', 'leonor m. bautista national high school', '2018-01-18 03:26:49', '2018-01-18 03:26:49'),
(379, 'lmbnhs', 'leonor m. bautista national high school', '2018-01-18 03:26:49', '2018-01-18 03:26:49'),
(380, 'lmbnhs', 'leonor m. bautista national high school', '2018-01-18 03:26:49', '2018-01-18 03:26:49'),
(381, 'lmbnhs', 'leonor m. bautista national high school', '2018-01-18 03:26:50', '2018-01-18 03:26:50'),
(382, 'ccpfi', 'cit college of paniqui foundation, inc.', '2018-01-18 05:10:03', '2018-01-18 05:10:03'),
(383, 'sac', 'st. anthony\'s college', '2018-01-18 05:16:25', '2018-01-18 05:16:25'),
(384, 'sac', 'st. anthony\'s college', '2018-01-18 05:16:30', '2018-01-18 05:16:30'),
(385, 'RSCS', 'ROSE OF SHARON CHRISTIAN SCHOOL', '2018-01-19 07:33:49', '2018-01-19 07:33:49'),
(386, 'cp', 'caregiver program', '2018-01-22 00:01:11', '2018-01-22 00:01:11'),
(387, 'cp', 'caregiver program', '2018-01-22 00:01:55', '2018-01-22 00:01:55'),
(388, 'ANHS', 'ARITAO NATIONAL HIGH SCHOOL', '2018-01-22 03:18:49', '2018-01-22 03:18:49'),
(389, 'sp', 'st. patrick\'s school of novaliches, inc.', '2018-01-22 08:01:52', '2018-01-22 08:01:52'),
(390, 'chs', 'calaba high school', '2018-01-22 08:40:00', '2018-01-22 08:40:00'),
(391, 'zdsmit', 'zamboanga del sur maritime institute of technology', '2018-01-24 07:46:43', '2018-01-24 07:46:43'),
(392, 'acsi', 'apo computer school inc.', '2018-01-25 01:46:55', '2018-01-25 01:46:55'),
(393, 'ba in tourism management', 'abe international business college', '2018-01-25 02:34:15', '2018-01-25 02:34:15'),
(394, 'safc', 'st. agustine foundation colleges', '2018-01-25 02:38:35', '2018-01-25 02:38:35'),
(395, 'safc', 'st. agustine foundation colleges', '2018-01-25 02:38:46', '2018-01-25 02:38:46'),
(396, 'ispsc', 'ilocos sur POLYTECHNIC state college', '2018-01-25 05:39:08', '2018-01-25 05:39:08'),
(397, 'ff', 'dalubhasa ng lunsod ng san pablo', '2018-01-26 01:05:05', '2018-01-26 01:05:05'),
(398, 'ait', 'allen institute of technology', '2018-01-26 02:39:59', '2018-01-26 02:39:59'),
(399, 'wnec', 'western nueva ecija college', '2018-01-29 02:32:47', '2018-01-29 02:32:47'),
(400, 'ht', 'ricardo dizon canlas agricultural school', '2018-01-30 07:12:01', '2018-01-30 07:12:01'),
(401, 'CVH', 'PANIKI HIGH SCHOOL', '2018-01-30 07:23:42', '2018-01-30 07:23:42'),
(402, 'DSDS', 'FIRST TARLAC TECHNOLOGICAL INSTITUTE', '2018-01-31 02:40:30', '2018-01-31 02:40:30'),
(403, 'hyh', 'saint louis university', '2018-02-01 01:27:41', '2018-02-01 01:27:41'),
(404, 'ge', 'constancio padilla national high school', '2018-02-01 01:42:53', '2018-02-01 01:42:53'),
(405, 'afc', 'diagyan national high school', '2018-02-01 03:11:56', '2018-02-01 03:11:56'),
(406, 'tnhs', 'talugtug national high school', '2018-02-01 03:19:28', '2018-02-01 03:19:28'),
(407, 'ucu', 'urdaneta city university', '2018-02-01 05:49:41', '2018-02-01 05:49:41'),
(408, 'sacli', 'st. anne college lucena, inc.', '2018-02-01 06:04:59', '2018-02-01 06:04:59'),
(409, 'rtu', 'rizal technological university', '2018-02-05 01:21:40', '2018-02-05 01:25:56'),
(410, 'LNHS', 'LUAL NATIONAL HIGH SCHOOL', '2018-02-05 02:43:19', '2018-02-05 02:43:19'),
(411, 'anhs', 'agno national high school', '2018-02-05 07:37:56', '2018-02-05 07:37:56'),
(412, 'geh', 'caanawan high school', '2018-02-07 03:30:02', '2018-02-07 03:30:02'),
(413, 'SVNHS', 'SAMPAGUITA VILLAGE NATIONAL HIGH SCHOOL', '2018-02-07 07:19:45', '2018-02-07 07:19:45'),
(414, 'FZ B', 'RIO CHICO HIGH SCHOOL', '2018-02-08 02:24:58', '2018-02-08 02:24:58'),
(415, 'snhs', 'sibul national high school', '2018-02-08 04:20:46', '2018-02-08 04:20:46'),
(416, 'RCNHS', 'RIO CHICO NATIONAL HIGH SCHOOL', '2018-02-08 04:32:20', '2018-02-08 04:32:20'),
(417, 'RCNHS', 'RIO CHICO NATIONAL HIGH SCHOOL', '2018-02-08 04:34:08', '2018-02-08 04:34:08'),
(418, 'smnhs', 'san mariano national high school', '2018-02-08 06:24:40', '2018-02-08 06:24:40'),
(419, 'PSNHS', 'PAITAN SUR NATIONAL HIGH SCHOOL', '2018-02-09 01:55:10', '2018-02-09 01:55:10'),
(420, 'lnu', 'lyceum-northwestern university', '2018-02-09 01:55:34', '2018-02-09 01:55:34'),
(421, 'aims', 'asian institute of the maritime studeis', '2018-02-09 05:32:30', '2018-02-09 05:32:30'),
(422, 'ANHS', 'AURORA NATIONAL HIGH SCHOOL', '2018-02-09 05:43:29', '2018-02-09 05:43:29'),
(423, 'CNHS', 'CASIGURAN NATIONAL HIGH SCHOOL', '2018-02-13 00:21:22', '2018-02-13 00:21:22'),
(424, 'MNHS-A', 'MUÑOZ NATIONAL HIGH SCHOOL-ANNEX', '2018-02-13 00:47:23', '2018-02-13 00:47:23'),
(425, 'anhs', 'alicia national high school', '2018-02-13 01:15:01', '2018-02-13 01:15:01'),
(426, 'MNHS-M', 'MUÑOZ NATIONAL HIGH SCHOOL-MAIN', '2018-02-13 02:58:53', '2018-02-13 02:58:53'),
(427, 'DNHS', 'DINAPIGUE NATIONAL HIGH SCHOOL', '2018-02-13 03:13:21', '2018-02-13 03:13:21'),
(428, 'LDCK', 'LICEO DE CHRIST THE KING, INC.', '2018-02-13 04:28:05', '2018-02-13 04:28:05'),
(429, 'JAMHS', 'JUAN C. ANGARA MEMORIAL HIGH SCHOOL', '2018-02-13 07:04:43', '2018-02-13 07:04:43'),
(430, 'rssths', 'raja soliman science and technology high school', '2018-02-14 05:30:58', '2018-02-14 05:30:58'),
(431, 'bpnhs', 'buklod palad national high school', '2018-02-20 03:11:15', '2018-02-20 03:11:15'),
(432, 'NEU', 'NEW ERA UNIVERSITY', '2018-02-20 05:28:38', '2018-02-20 05:28:38'),
(433, 'MDHS', 'MADDELA COMPREHENSIVE HIGH SCHOOL', '2018-02-20 06:52:13', '2018-02-20 06:52:13'),
(434, 'ANHS', 'ANGADANAN NATIONAL HIGH SCHOOL', '2018-02-22 02:55:27', '2018-02-22 02:55:27'),
(435, 'BMNHS', 'BARANGAY MILITAR NATIONAL HIGH SCHOOL', '2018-02-22 03:25:58', '2018-02-22 03:25:58'),
(436, 'dhs', 'dapdap high school', '2018-02-23 00:09:44', '2018-02-23 00:09:44'),
(437, 'mchs', 'mount carmel high school', '2018-03-01 05:02:38', '2018-03-01 05:02:38'),
(438, 'NVCHS', 'NUEVA VIZCAYA GENERAL COMPREHENSIVE HIGH SCHOOL', '2018-03-02 03:30:57', '2018-03-02 03:30:57'),
(439, 'VAHS', 'VILLA AGLIPAY HIGH SCHOOL', '2018-03-05 06:01:15', '2018-03-05 06:01:15'),
(440, 'FHK', 'TARLAC CHRISTIAN COLLEGE INC.', '2018-03-05 07:09:57', '2018-03-05 07:09:57'),
(441, 'VNHS', 'VEGA NATIONAL HIGH SCHOOL', '2018-03-08 00:59:45', '2018-03-08 00:59:45'),
(442, 'PNHS', 'PINARIPAD NATIONAL HIGH SCHOOL', '2018-03-13 06:33:26', '2018-03-13 06:33:26'),
(443, 'CENHS', 'CANAAN EAST NATIONAL HIGH SCHOOL', '2018-03-21 08:37:13', '2018-03-21 08:37:13'),
(444, 'mdrnhs', 'marciano del rosario national high school', '2018-03-21 08:44:18', '2018-03-21 08:44:18'),
(445, 'SMCT', 'ST. MARY\'S COLLEGE OF TECHNOLOGY', '2018-03-22 05:38:00', '2018-03-22 05:38:00'),
(446, 'TNNHS', 'TALABUTAB NORTE NATIONAL HIGH SCHOOL', '2018-03-28 07:00:07', '2018-03-28 07:00:07'),
(447, 'MCSD', 'MOUNT CARMEL SCHOOL OF DINALUNGAN', '2018-04-10 04:17:40', '2018-04-10 04:17:40'),
(448, 'rwcoaati', 'resource world college of arts and trade inc', '2018-04-18 06:37:03', '2018-04-18 06:37:03'),
(449, 'xaf', 'university of cagayan valley', '2018-04-26 00:24:43', '2018-04-26 00:24:43'),
(450, 'LSQ', 'LA SALETE OF QUEZON', '2018-04-26 02:23:15', '2018-04-26 02:23:15'),
(451, '----', 'LA SALETE OF QUEZON', '2018-04-26 02:24:10', '2018-04-26 02:24:10'),
(452, '-', 'WESTMEAD INTERNATIONAL SCHOOL,  Batangas city', '2018-04-26 07:42:41', '2018-04-26 07:44:11'),
(453, '-', 'holy family academy', '2018-04-30 00:59:01', '2018-04-30 00:59:01'),
(454, '-', 'JUAN C. AURORA MEMORIAL SCHOOL', '2018-05-03 00:52:52', '2018-05-03 00:52:52'),
(455, '..', 'ROOSEVELT COLLEGE MARIKINA', '2018-05-03 01:24:38', '2018-05-03 01:24:38'),
(456, 'DAXFADF', 'KING\'S COLLEGE OF THE PHILIPPINES', '2018-05-03 08:09:19', '2018-05-03 08:09:19'),
(457, 'vsgsd', 'florita herrera high school', '2018-05-03 08:11:21', '2018-05-03 08:11:21'),
(458, '...', 'STA. FE NATIONAL HIGH SCHOOL', '2018-05-03 08:11:43', '2018-05-03 08:11:43'),
(459, 'jb', 'scala integrated school', '2018-05-03 08:18:47', '2018-05-03 08:18:47'),
(460, 'njh', 'nieves  center for EDUCATION inc.', '2018-05-03 08:23:12', '2018-05-03 08:23:12'),
(461, 'czzs', 'vicentian catholic academy', '2018-05-03 09:01:16', '2018-05-03 09:01:16'),
(462, 'VS', 'COLEGIO DE SANTA TERESA DE AVILA', '2018-05-03 09:08:41', '2018-05-03 09:08:41'),
(463, 'ZFDAS', 'DEOKSU JUNIOR HIGH SCHOOL', '2018-05-03 09:11:57', '2018-05-03 09:11:57'),
(464, 'XAFQ', 'VILLAGE MONTESSORI SCHOOL', '2018-05-03 09:15:26', '2018-05-03 09:15:26'),
(465, 'HJJH', 'GOV. F. T SAN LUIS INTEGRATED SCHOOL', '2018-05-03 09:22:16', '2018-05-03 09:22:16'),
(466, 'CE', 'LAUR HIGH SCHOOL', '2018-05-03 09:37:00', '2018-05-03 09:37:00'),
(467, 'fxea', 'san sebastians school', '2018-05-03 09:40:24', '2018-05-03 09:40:24'),
(468, 'wsxcv', 'ilocos norte national high school', '2018-05-03 09:44:31', '2018-05-03 09:44:31'),
(469, 'a g', 'sangbay integrated school', '2018-05-03 09:46:36', '2018-05-03 09:46:36'),
(470, 'a', 'agupalo  weste high school', '2018-05-03 09:47:27', '2018-05-03 09:47:27'),
(471, '..', 'sti. academic center novaliches', '2018-05-03 10:03:39', '2018-05-03 10:03:39'),
(472, '..', 'sti. academic center novaliches', '2018-05-03 10:03:39', '2018-05-03 10:03:39'),
(473, '....', 'ST. NICOLAS SENIOR HIGH SCHOOL', '2018-05-04 03:34:21', '2018-05-04 03:34:21'),
(474, 'PATTS', 'COLLEGE OF AERONAUTICS', '2018-05-04 06:03:16', '2018-05-04 06:03:16'),
(475, 'H', 'ICCT COLLEGE', '2018-05-04 06:06:38', '2018-05-04 06:06:38'),
(476, 'angs', 'agbannawag national high school', '2018-05-04 07:39:32', '2018-05-04 07:39:32'),
(477, '..', 'sta. isabel montessori inc', '2018-05-07 05:51:00', '2018-05-07 05:51:00'),
(478, '....', 'canili area national high school', '2018-05-07 08:55:56', '2018-05-07 08:55:56'),
(479, 'A V', 'CAB CABIN NATIONAL HIGH SCHOOL', '2018-05-08 00:43:49', '2018-05-08 00:43:49'),
(480, 'SGV', 'CABCABEN NATIONAL HIGH SCHOOL MARIVELEZ', '2018-05-08 00:45:26', '2018-05-08 00:45:26'),
(481, '..', 'bongabon senior high school', '2018-05-08 03:41:37', '2018-05-08 03:41:37'),
(482, '....', 'philippine statesman college', '2018-05-08 04:13:12', '2018-05-08 04:13:12'),
(483, '...', 'J. VICTORIA MONTESSORI SCHOOL', '2018-05-08 07:22:14', '2018-05-08 07:22:14'),
(484, 'JFJFJ', 'PARTIDA HIGH SCHOOL', '2018-05-09 01:09:11', '2018-05-09 01:09:11'),
(485, '..', 'emilia belmonte acosta national high school', '2018-05-10 08:12:30', '2018-05-10 08:12:30'),
(486, '..', 'pudoc west integrated school', '2018-05-11 04:13:09', '2018-05-11 04:13:09'),
(487, 'chcc', 'concepcion holy cross college', '2018-05-11 08:18:00', '2018-05-11 08:18:00'),
(488, 'AXCF', 'UNIVERSITY OF LABORATORY HIGH SCHOOL', '2018-05-15 00:26:41', '2018-05-15 00:26:41'),
(489, 'STI', 'STI COLLEGE SAN JOSE', '2018-05-15 06:55:02', '2018-05-15 06:55:02'),
(490, '..', 'BALUCUS HIGH SCHOOL', '2018-05-15 07:35:26', '2018-05-15 07:35:26'),
(491, 'BCP', 'BESTLINK COLLEGE OF THE PHILIPPINES', '2018-05-16 01:21:39', '2018-05-16 01:21:39'),
(492, 'BCP', 'BESTLINK COLLEGE OF THE PHILIPPINES', '2018-05-16 01:22:45', '2018-05-16 01:22:45'),
(493, 'HEHMS', 'HILARIO E. HERMOSA MEMORIAL SCHOOL', '2018-05-16 03:04:05', '2018-05-16 03:04:05'),
(494, 'BNHS', 'BARUCBOC NATIONAL HIGH SCHOOL', '2018-05-16 03:58:47', '2018-05-16 03:58:47'),
(495, 'bx', 'lino p. bernardo national high school', '2018-05-17 01:05:08', '2018-05-17 01:05:08'),
(496, 'BSU', 'BATANGAS STATE UNIVERSITY, BATANGAS CITY', '2018-05-17 02:59:03', '2018-05-17 02:59:03'),
(497, 'OLF', 'OUR LADY OF FATIMA OF BOCAUE', '2018-05-18 04:33:52', '2018-05-18 04:33:52'),
(498, '.....', 'panpacific university north philippines', '2018-05-18 05:49:46', '2018-05-18 05:49:46'),
(499, '..', 'MALACAÑANG NATIONAL HIGH SCHOOL', '2018-05-18 08:47:42', '2018-05-18 08:47:42'),
(500, '...', 'CHARLES ANGEL MONTESSORI SCHOOL', '2018-05-21 06:49:29', '2018-05-21 06:49:29'),
(501, '...', 'san andres national high school', '2018-05-21 07:14:36', '2018-05-21 07:14:36'),
(502, 'czhs', 'cielito zamora high school i', '2018-05-21 08:27:43', '2018-05-21 08:27:43'),
(503, 'JDJD', 'TIBAG HIGH SCHOOL', '2018-05-22 07:46:48', '2018-05-22 07:46:48'),
(504, 'GTA', 'GRAND TOURING ACADEMY', '2018-05-23 02:49:24', '2018-05-23 02:49:24'),
(505, 'MNHS', 'MARAGUSAN NATIONAL HIGH SCHOOL', '2018-05-24 00:49:43', '2018-05-24 00:49:43'),
(506, 'CNHS', 'CALABANGA NATIONAL HIGH SCHOOL', '2018-05-24 08:14:15', '2018-05-24 08:14:15'),
(507, 'mvvs', 'mountain view village school', '2018-05-25 00:26:05', '2018-05-25 00:26:05'),
(508, 'dvgsh d', 'f. bustamante natonal high school, davao city', '2018-05-25 03:15:31', '2018-05-25 03:15:31'),
(509, 'FF', 'STO. NIÑO DIOCESAN SCHOOL', '2018-05-28 00:31:41', '2018-05-28 00:31:41'),
(510, 'JSJDJ', 'ST. JOHN BERCHMANS HIGH SCHOOL', '2018-05-28 00:38:52', '2018-05-28 00:38:52'),
(511, 'vh', 'casay national high school', '2018-05-28 02:41:35', '2018-05-28 02:41:35'),
(512, 'cfs', 'st. francis of assisi diocesan school', '2018-05-28 05:55:32', '2018-05-28 05:55:32'),
(513, 'gjm', 'st, luis anne montesorri high school & colleges', '2018-05-29 05:52:02', '2018-05-29 05:52:02'),
(514, 'vga', 'capintalan high school', '2018-05-29 07:24:37', '2018-05-29 07:24:37'),
(515, 'd', 'kinect academy, inc', '2018-05-29 08:07:41', '2018-05-29 08:07:41'),
(516, 'cd', 'bunga integrated school', '2018-05-29 08:34:04', '2018-05-29 08:34:04'),
(517, 'xd', 'sti west negros university', '2018-05-29 08:43:37', '2018-05-29 08:43:37'),
(518, 'ASKI', 'ASKI SKILLS AND KNOWLEDGE INSTITUTE INCORPORATED', '2018-05-31 08:13:32', '2018-05-31 08:13:32'),
(519, 'MHDPNS', 'MARCELO H. DEL PILAR NATIONAL HIGH SCHOOL', '2018-06-01 01:46:18', '2018-06-01 01:46:18'),
(520, 'J', 'GUELEW INTEGRATED SCHOOL, PANGASINAN', '2018-06-01 03:25:48', '2018-06-01 03:25:48'),
(521, 'YCAI', 'YOBREL CHRISTIAN ACADEMY INC', '2018-06-01 07:50:24', '2018-06-01 07:50:24'),
(522, 'hjdhd', 'san leonardo national high school', '2018-06-04 02:08:22', '2018-06-04 02:08:22'),
(523, '..', 'INTERWORLD ASIAN INSTITUTE, INC', '2018-06-04 03:13:11', '2018-06-04 03:13:11'),
(524, 'fbn', 'bulacan virginia institute of technology, inc.', '2018-06-04 03:58:35', '2018-06-04 03:58:35'),
(525, 'fbn', 'bulacan virginia institute of technology, inc.', '2018-06-04 03:58:35', '2018-06-04 03:58:35'),
(526, 'xjk', 'IMURONG NATIONAL HIGH SCHOOL', '2018-06-05 01:52:39', '2018-06-05 01:52:39'),
(527, 'PSC', 'PHILLIPINES STATEMAN COLLEGES', '2018-06-05 08:06:32', '2018-06-05 08:06:32'),
(528, 'SMSM', 'SCHOOL OF MOUNT ST. MARY', '2018-06-06 08:00:23', '2018-06-06 08:00:23'),
(529, 'RHS', 'RANIAG HIGH SCHOOL', '2018-06-07 03:03:41', '2018-06-07 03:03:41'),
(530, 'DWHS', 'DIVINE WORD SCHOOL', '2018-06-07 07:30:41', '2018-06-07 07:30:41'),
(531, 'sfv', 'banaue national high school', '2018-06-07 07:33:13', '2018-06-07 07:33:13'),
(532, 'CIS', 'CABOG INTEGRATED SCHOOL', '2018-06-08 04:02:20', '2018-06-08 04:02:20'),
(533, '..', 'romeo acuÑa santos memorial high school', '2018-06-08 05:47:56', '2018-06-08 05:47:56'),
(534, 'ST. ANDREW CHRISTIAN ACADEMY PEÑARANDA INC.', 'ST. ANDREW CHRISTIAN ACADEMY PEÑARANDA INC.', '2018-06-11 08:05:11', '2018-06-11 08:05:11'),
(535, 'JTR', 'DIADI NATIONAL HIGH SCHOOL', '2018-06-13 08:45:10', '2018-06-13 08:45:10'),
(536, 'sv', 'sain vincent school', '2018-06-13 08:52:03', '2018-06-13 08:52:03');
INSERT INTO `schools` (`id`, `code`, `name`, `created_at`, `updated_at`) VALUES
(537, 'b n', 'paaralang sekundarya ng heneral nakar-umiray extension', '2018-06-13 08:59:18', '2018-06-13 08:59:18'),
(538, 'h', 'ELIZABETH GLOBAL SKILLS INSTITUTE, INC.', '2018-06-13 09:08:35', '2018-06-13 09:08:35'),
(539, 'GHS', 'GALVAN HIGH SCHOOL', '2018-06-14 03:12:12', '2018-06-14 03:12:12'),
(540, '-', 'palayan city national senior high school', '2018-06-14 08:25:41', '2018-06-14 08:25:41'),
(541, '-', 'palayan city national senior high school', '2018-06-14 08:27:13', '2018-06-14 08:27:13'),
(542, 'MSC', 'MONCADA CATHOLIC SCHOOL', '2018-06-18 04:55:18', '2018-06-18 04:55:18'),
(543, 'ASDSA', 'NORTHERN MINDANAO COLLEGE, INC', '2018-06-18 05:40:58', '2018-06-18 05:40:58'),
(544, 'AICS', 'ASIAN INSTITUTE OF COMPUTER STUDIES', '2018-06-18 06:10:04', '2018-06-18 06:10:04'),
(545, 'vsh', 'montesorri school of san jose', '2018-06-20 05:03:11', '2018-06-20 05:03:11'),
(546, 'jfjd', 'university of batangas', '2018-06-20 07:29:24', '2018-06-20 07:29:24'),
(547, 'hgf', 'gerona junior college', '2018-06-20 08:22:07', '2018-06-20 08:22:07'),
(548, 'BSCGH', 'ST JOHN BERCHMANS HIGH SCHOOL', '2018-06-21 05:50:48', '2018-06-21 05:50:48'),
(549, 'NHDFHM', 'DITUMABO NATIONAL HIGH SCHOOL', '2018-06-21 08:38:04', '2018-06-21 08:38:04'),
(550, 'VF', 'JUSTINO SEVILLA HIGH SCHOOL', '2018-06-22 03:08:14', '2018-06-22 03:08:14'),
(551, 'hvs', 'tomas del rosario college', '2018-06-25 04:18:28', '2018-06-25 04:18:28'),
(552, 'sjc', 'st. john school', '2018-06-25 05:04:49', '2018-06-25 05:04:49'),
(553, 'SA', 'ST. ANDREW\'S SCHOOL OF PANTABANAGN, INC', '2018-06-25 07:45:30', '2018-06-25 07:45:30'),
(554, 'bfk', 'eto school of science and technology, incorporated', '2018-06-25 08:21:07', '2018-06-25 08:21:07'),
(555, 'JET', 'JET MONTESSORI OF RAMON, INC', '2018-06-26 07:07:52', '2018-06-26 07:07:52'),
(556, 'DICT', 'DATAMEX INSTITUTE OF COMPUTER TECHNOLOGY', '2018-06-27 07:15:48', '2018-06-27 07:15:48'),
(557, 'isabela', 'sistech college of santiago city, inc', '2018-06-27 07:26:03', '2018-06-27 07:26:03'),
(558, 'jfj', 'our lady og grace school of caloocan', '2018-06-28 07:40:03', '2018-06-28 07:40:03'),
(559, 'sja', 'st.jerome academy', '2018-07-02 02:59:39', '2018-07-02 02:59:39'),
(560, 'qwwq', 'bonifacio luz natividad education foundation,inc', '2018-07-02 03:11:43', '2018-07-02 03:11:43'),
(561, 'ZD', 'ST. NICHOLE\'S TECHNICAL SCHOOL, INC.', '2018-07-02 08:49:03', '2018-07-02 08:49:03'),
(562, 'FU', 'FEATI UNIVERSITY', '2018-07-04 01:35:54', '2018-07-04 01:35:54'),
(563, '.', 'DILASAG NATIONAL HIGH SCHOOL', '2018-07-16 02:42:04', '2018-07-16 02:42:04'),
(564, 'BAGONTAAS, VALENCIA CITY', 'PARAMOUNT SCHOOL OF ARTS, LANGUAGES, MANAGEMENT AND SCIENCE, INC.', '2018-07-16 07:30:26', '2018-07-16 07:30:26'),
(565, 'UCNHS', 'UMINGAN CENTRAL NATIONAL HIGH SCHOOL', '2018-07-17 07:50:01', '2018-07-17 07:50:01'),
(566, 'UUJHH', 'SAINT NICOLAS ACADEMY OF CARRANGLAN, INC.', '2018-07-25 06:38:07', '2018-07-25 06:38:07'),
(567, 'DFDS', 'ST. CHRISTOPHER MONTESSORI SCHOOL OF SANTA ROSA', '2018-07-26 03:06:34', '2018-07-26 03:06:34'),
(568, 'DHS', 'DINIOG  HIGH SCHOOL', '2018-07-26 07:09:09', '2018-07-26 07:09:09'),
(569, 'SBIS', 'STA. BARBARA INTEGRATED SCHOOL', '2018-07-31 07:25:29', '2018-07-31 07:25:29'),
(570, 'HSHS', 'PATUL NATIONAL HIGH SCHOOL', '2018-08-01 06:09:57', '2018-08-01 06:09:57'),
(571, 'SLAM', 'ST. LOUIS ANNE MONTESSORII SCHOOL & COLLEGES', '2018-08-07 01:25:35', '2018-08-07 01:25:35'),
(572, 'cZV V', 'the national teacher college', '2018-08-07 02:32:15', '2018-08-07 02:32:15'),
(573, 'VSGD', 'TRIALA HIGH SCHOOL', '2018-08-07 06:15:37', '2018-08-07 06:15:37'),
(574, 'XAF', 'MATAAS NA PAARALANG JUAN C. LAYA', '2018-08-07 07:36:08', '2018-08-07 07:36:08'),
(575, 'cfas', 'nueva vizcaya institute', '2018-08-07 08:09:24', '2018-08-07 08:09:24'),
(576, 'VDH', 'SCHOLA CHRISTI, INC.', '2018-08-10 05:26:45', '2018-08-10 05:26:45'),
(577, 'C D', 'father john karash memorial high school', '2018-08-10 06:03:01', '2018-08-10 06:03:01'),
(578, 'CSDG', 'SAN ISIDRO NATIONAL HIGH SCHOOL', '2018-08-13 03:23:23', '2018-08-13 03:23:23'),
(579, 'DDD', 'STO. NIÑO ACADEMY OF SAN RAFAEL, INC', '2018-08-13 03:30:51', '2018-08-13 03:30:51'),
(580, 'V', 'QUEZON NATIONAL HIGH SCHOOL', '2018-08-13 03:34:34', '2018-08-13 03:34:34'),
(581, 'JMSMS', 'JOHN MARCO SPECIAL MONTESSORI SCHOOL', '2018-08-13 03:49:17', '2018-08-13 03:49:17'),
(582, 'yof', 'st. yanga\'s college, inc', '2018-08-13 05:23:59', '2018-08-13 05:23:59'),
(583, 'CAFS', 'KAPALANGAN NATIONAL HIGH SCHOOL', '2018-08-14 02:53:55', '2018-08-14 02:53:55'),
(584, 'AXDS', 'SAINT MARK\'S SCHOOL', '2018-08-14 07:01:42', '2018-08-14 07:01:42'),
(585, 'SBHS', 'STA. BARBARA HIGH SCHOOL', '2018-08-14 07:13:29', '2018-08-14 07:13:29'),
(586, 'HDCHS', 'HORACIO DELA COSTA SENIOR HIGH SCHOOLL', '2018-08-18 01:49:42', '2018-08-18 01:54:07'),
(587, 'GH', 'GETHSEMANE CHRISTIAN SCHOOL', '2018-08-18 07:31:43', '2018-08-18 07:31:43'),
(588, 'SHSHS', 'ST. JOSEPH SCHOOL', '2018-08-29 00:19:09', '2018-08-29 00:19:09'),
(589, 'CNBC', 'CITIZEN POLYTECHNIC INSTITUTE', '2018-08-29 00:57:33', '2018-08-29 00:57:33'),
(590, 'anhs', 'apalangan national high school', '2018-09-03 01:58:38', '2018-09-03 01:58:38'),
(591, 'FHINHS', 'FLORITA HERRERA-IRIZARI NATIONAL HIGH SCHOOL', '2018-09-05 02:47:02', '2018-09-05 02:47:02'),
(592, 'AIMS', 'ASIAN INSTITUTE OF MARITIME STUDIES', '2018-09-05 07:02:38', '2018-09-05 07:02:38'),
(593, 'TSHS', 'TALAVERA SENIOR HIGH SCHOOL', '2018-09-05 08:37:01', '2018-09-05 08:37:01'),
(594, 'CNSHS', 'CABIAO NATIONAL SENIOR HIGH SCHOOL', '2018-09-06 08:16:41', '2018-09-06 08:16:41'),
(595, 'F', 'SALAGUSOG NATIONAL HIGH SCHOOL', '2018-09-13 01:10:15', '2018-09-13 01:10:15'),
(596, 'FGC', 'F.G CALDERON INTEGRATED SCHOOL', '2018-10-01 01:16:02', '2018-10-01 01:16:02'),
(597, 'IOE', 'INSTITUTE OF EDUCATIONAL SOLUTIONS-NUEVA ECIJA', '2018-10-02 03:24:19', '2018-10-02 03:24:19'),
(598, 'erd', 'eulogio r. dizon college of nueva ecija', '2018-10-05 06:53:02', '2018-10-05 06:53:02'),
(599, 'MDC', 'MIDWAY COLLEGES, INC. CABANATUAN CITY', '2018-10-08 02:20:55', '2018-10-08 02:22:15'),
(600, 'chs', 'calibungan high school', '2018-10-15 01:39:46', '2018-10-15 01:39:46'),
(601, 'ub', 'university of baguio', '2018-10-15 04:52:11', '2018-10-15 04:52:11'),
(602, 'ERHS', 'ERNESTO RONDOM HIGH SCHOOL', '2018-10-17 05:27:36', '2018-10-17 05:27:36'),
(603, 'bi', 'baler institute', '2018-10-18 06:33:30', '2018-10-18 06:33:30'),
(604, 'DYC', 'DR. YANGA\'S COLLEGES,INC.', '2018-10-23 06:15:06', '2018-10-23 06:15:06'),
(605, 'SCC', 'SAINT COLUMBIAN COLLEGE', '2018-10-24 03:15:12', '2018-10-24 03:15:12'),
(606, 'AMECI', 'ADVANCED MONTESSORI EDUCATION CENTER OF ISABELA', '2018-11-07 06:20:03', '2018-11-07 06:20:03'),
(607, 'MDRMNHS', 'MARCIANO DEL ROSARIO MEMORIAL NATIONAL HIGH SCHOOL', '2018-11-09 02:01:11', '2018-11-09 02:01:11'),
(608, 'SACS', 'ST. ANNE\'S CATHOLIC SCHOOL', '2018-11-13 01:15:10', '2018-11-13 01:15:10'),
(609, 'XZVSC', 'SCHOOL OF ST. JOSEPH THE WORKERS', '2018-11-13 02:14:12', '2018-11-13 02:14:12'),
(610, 'DBGF', 'SAN PABLO NATIONAL HIGH SCHOOL', '2018-11-13 02:55:42', '2018-11-13 02:55:42'),
(611, 'MNPH', 'MATAAS NA PAARALAN NG JOH J. RUSSEL', '2018-11-13 06:03:45', '2018-11-13 06:03:45'),
(612, 'CF', 'BULINAO SCHOOL OF FISHERIES', '2018-11-14 03:16:02', '2018-11-14 03:16:02'),
(613, 'SMHS', 'SAN MANUEL HIGH SCHOOL', '2018-11-15 01:18:39', '2018-11-15 01:18:39'),
(614, 'ACST', 'ASIAN COLLEGE OF SCIENCE AND TECHNOLOGY', '2018-11-15 01:53:12', '2018-11-15 01:53:12'),
(615, 'NCII', 'COMPUTER HARDWARE SERVICES NCII, EVENTS MANAGEMENT SERVICES NC II, BOOKKEEPING NC II', '2018-11-15 06:32:49', '2018-11-15 06:32:49'),
(616, 'USA', 'UNIVERSITY OF SAN AGUSTIN', '2018-11-20 05:30:48', '2018-11-20 05:30:48'),
(617, 'FG', 'HOLY TRINITY CHRISTIAN ACADEMY', '2018-11-22 06:11:01', '2018-11-22 06:11:01'),
(618, 'pc', 'pimsat colleges', '2018-11-23 08:01:21', '2018-11-23 08:01:21'),
(619, 'CLCST', 'CENTRAL LUZON COLLEGE OF SCIENCE & TECHNOLOGY', '2018-11-26 02:44:22', '2018-11-26 02:44:22'),
(620, 'segc', 'st. elizabeth global college', '2018-11-26 06:39:07', '2018-11-26 06:39:07'),
(621, 'lote', 'lyceum of the east', '2018-11-27 01:45:34', '2018-11-27 01:45:34'),
(622, 'msnhs', 'maseil seil national high school', '2018-11-27 05:31:09', '2018-11-27 05:31:09'),
(623, 'RRTNHS', 'ROSAURO R. TANGSON NATIONAL HIGH SCHOOL', '2018-11-27 08:04:14', '2018-11-27 08:04:14'),
(624, 'cpfhs', 'center for positive futures high school', '2018-11-28 03:15:20', '2018-11-28 03:15:20'),
(625, 'ibmc', 'inats bulagao main campus', '2018-11-29 01:36:19', '2018-11-29 01:36:19'),
(626, 'arsf', 'alvarez - ramales school foundation', '2018-12-07 01:20:15', '2018-12-07 01:20:15'),
(627, 'PNHS', 'PUANGI NATIONAL HIGH SCHOOL', '2018-12-19 05:19:19', '2018-12-19 05:19:19'),
(628, 'mdmhs', 'mariano d. marquez high school', '2018-12-21 07:23:34', '2018-12-21 07:23:34'),
(629, 'AAA', 'STA. RITA NATIONAL HIGH SCHOOL', '2019-01-07 06:16:40', '2019-01-07 06:16:40'),
(630, 'tsoms', 'the sister\'s of mary school', '2019-01-11 00:48:14', '2019-01-11 00:48:14'),
(631, 'msoj', 'montessori school of jaen', '2019-01-15 01:58:16', '2019-01-15 01:58:16'),
(632, 'DBX', 'MARAGON NATIONAL HIGH SCHOOL', '2019-01-16 07:23:56', '2019-01-16 07:23:56'),
(633, 'ac', 'pamantasan ng araullo', '2019-01-23 01:45:27', '2019-01-23 01:45:27'),
(634, 'sma', 'san miguel academy', '2019-01-23 06:10:11', '2019-01-23 06:10:11'),
(635, 'SCPHS', 'SANTA CRUZ PINGKIAN HIGH SCHOOL', '2019-01-23 07:55:13', '2019-01-23 07:55:13'),
(636, 'snnhs', 'san nicolas national high school', '2019-02-08 00:40:51', '2019-02-08 00:40:51'),
(637, 'SHS', 'CADALORIA HIGH SCHOOL', '2019-02-18 02:40:06', '2019-02-18 02:40:06'),
(638, 'SIHS', 'STA. IGNACIA HIGH SCHOOL', '2019-02-18 07:02:01', '2019-02-18 07:02:01'),
(639, 'NHS', 'NAGPANDAYAN HIGH SCHOOL', '2019-02-21 01:08:19', '2019-02-21 01:08:19'),
(640, 'PMIC', 'PHILIPPINE MARITIME INSTITUTE-COLLEGES, MANILA', '2019-03-07 01:15:23', '2019-03-07 01:15:23'),
(641, 'xca', 'st. john\'s academy of san jose  city, inc.', '2019-03-11 00:03:00', '2019-03-11 00:03:00'),
(642, 'erthvreh', 'manacsac guimba nueva ecija', '2019-03-11 05:02:28', '2019-03-11 05:02:28'),
(643, 'FAWR', 'BORLONGAN NATIONAL HIGH SCHOOL', '2019-03-12 08:05:36', '2019-03-12 08:05:36'),
(644, 'RHAM', 'ROMINA\'S HEAVENLY ANGLES MONTESSORI', '2019-03-12 08:51:40', '2019-03-12 08:51:40'),
(645, 'FCC', 'FEU-FERN COLLEGE', '2019-03-19 02:59:18', '2019-03-19 02:59:18'),
(646, 'JCJMHS', 'JOSE CHONA JO MEMORIAL HIGH SCHOOL', '2019-03-22 02:16:00', '2019-03-22 02:16:00'),
(647, 'EPAC', 'EASTERN PANGASINAN agriculture college', '2019-03-25 01:59:54', '2019-03-25 01:59:54'),
(648, 'JN', 'BALOY HIGH SCHOOL', '2019-03-27 03:47:28', '2019-03-27 03:47:28'),
(649, 'AVELURS TECHNOLOGIES', 'AVELURS TECHNOLOGIES', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(650, 'ATTa', 'AVELURS TECHNOLOGIES', '0000-00-00 00:00:00', '2019-04-30 06:28:41'),
(651, 'ATTaa', 'COMMUNICATIONS', '2019-05-07 07:57:22', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `description` varchar(200) NOT NULL,
  `code` varchar(30) NOT NULL,
  `status` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sickbay_doctors`
--

CREATE TABLE `sickbay_doctors` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `suffix` varchar(10) NOT NULL,
  `postion` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sickbay_inventory`
--

CREATE TABLE `sickbay_inventory` (
  `id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `stock_quantity` int(7) NOT NULL,
  `stock_used` int(10) NOT NULL,
  `stock_available` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sickbay_ishihara_plates`
--

CREATE TABLE `sickbay_ishihara_plates` (
  `id` int(11) NOT NULL,
  `image_path` varchar(255) NOT NULL,
  `type` varchar(30) NOT NULL,
  `answer` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sickbay_preregistration_transaction`
--

CREATE TABLE `sickbay_preregistration_transaction` (
  `id` int(11) NOT NULL,
  `student_no` varchar(100) NOT NULL,
  `ishihara_test` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `basic_test` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `family_medical_history` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `setting_id` varchar(6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sickbay_transactions`
--

CREATE TABLE `sickbay_transactions` (
  `id` int(11) NOT NULL,
  `setting_id` varchar(6) NOT NULL,
  `student_no` varchar(30) NOT NULL,
  `task_performed` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `inventory_consumed` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `strands_courses`
--

CREATE TABLE `strands_courses` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `code` varchar(30) NOT NULL,
  `track_description` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_assigned_subjects`
--

CREATE TABLE `student_assigned_subjects` (
  `id` int(11) NOT NULL,
  `enrollment_student_id` varchar(20) NOT NULL,
  `subject_load` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_back_accounts`
--

CREATE TABLE `student_back_accounts` (
  `id` int(11) NOT NULL,
  `enrollment_student_id` varchar(10) NOT NULL,
  `back_accounts_details` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_blocking`
--

CREATE TABLE `student_blocking` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_info_fees`
--

CREATE TABLE `student_info_fees` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `miscellaneous` decimal(8,2) NOT NULL,
  `tuition_fee` decimal(8,2) NOT NULL,
  `sub_total` decimal(8,2) NOT NULL,
  `total_fees` decimal(8,2) NOT NULL,
  `year_level` varchar(200) NOT NULL,
  `setting` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_payment_transactions`
--

CREATE TABLE `student_payment_transactions` (
  `id` int(11) NOT NULL,
  `enrollment_student_id` varchar(10) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `receipt_no` varchar(50) NOT NULL,
  `transaction_details` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `cashier` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_subjects`
--

CREATE TABLE `student_subjects` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `schedule_detail_id` int(11) NOT NULL,
  `term1` decimal(8,2) NOT NULL,
  `term2` decimal(8,2) NOT NULL,
  `term3` decimal(8,2) NOT NULL,
  `term4` decimal(8,2) NOT NULL,
  `final` decimal(8,2) NOT NULL,
  `student_subject_remark_id` int(11) NOT NULL,
  `enrollment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `old_student_id` int(11) NOT NULL,
  `credit_subject_id` int(11) NOT NULL,
  `strand_id` int(11) NOT NULL,
  `year_level_id` int(11) NOT NULL,
  `setting_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_transcript`
--

CREATE TABLE `student_transcript` (
  `id` int(11) NOT NULL,
  `student_no` varchar(20) NOT NULL,
  `tor_details` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `course` varchar(50) NOT NULL,
  `tor_status` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_transferee_record`
--

CREATE TABLE `student_transferee_record` (
  `id` int(11) NOT NULL,
  `student_no` varchar(20) NOT NULL,
  `subject_details` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `school` varchar(70) NOT NULL,
  `course` varchar(70) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `subject_code` varchar(200) NOT NULL,
  `subject_name` varchar(200) NOT NULL,
  `subject_amount` varchar(200) NOT NULL,
  `has_lab` int(11) NOT NULL,
  `lab_hours` int(11) NOT NULL,
  `subject_type_id` int(11) NOT NULL,
  `subject_parent_id` int(11) NOT NULL,
  `lec_hours` int(11) NOT NULL,
  `units` int(11) NOT NULL,
  `is_academic` tinyint(1) NOT NULL,
  `strand_id` int(11) NOT NULL,
  `year_level_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subject_parent`
--

CREATE TABLE `subject_parent` (
  `id` int(11) NOT NULL,
  `subject_code` varchar(200) NOT NULL,
  `subject_name` varchar(200) NOT NULL,
  `subject_amount` varchar(200) NOT NULL,
  `has_lab` int(11) NOT NULL,
  `lab_hours` int(11) NOT NULL,
  `subject_type_id` int(11) NOT NULL,
  `subject_parent_id` int(11) NOT NULL,
  `lec_hours` int(11) NOT NULL,
  `units` int(11) NOT NULL,
  `is_academic` tinyint(1) NOT NULL,
  `strand_id` int(11) NOT NULL,
  `year_level_id` int(11) NOT NULL,
  `setting_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subject_type`
--

CREATE TABLE `subject_type` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `removed` tinyint(1) NOT NULL DEFAULT '0',
  `complete_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `region_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `barangay_id` int(11) NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `suffix` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `employee_no`, `first_name`, `middle_name`, `last_name`, `removed`, `complete_address`, `region_id`, `province_id`, `city_id`, `barangay_id`, `street`, `suffix`, `created_at`, `updated_at`, `password`, `active`) VALUES
(1, '123-45-67', 'be', 'confirmed', 'to', 0, '', 0, 0, 0, 0, '', '', '2016-05-30 09:34:46', '2017-05-19 03:08:25', '0a5d1a00675c3966b67f0f426c7f5c2b', 0),
(2, '254-09-01', 'EMMANUEL', 'CARLOS', 'NAVARRO', 0, '', 0, 0, 0, 0, '', '', '2016-05-31 06:50:41', '2016-09-14 07:32:11', '$2y$10$ny5yl0lLwN6PcqxfJ4QLA.rC5Sfx/RRM6dmAVXKsqoFKtE.IvNYk2', 0),
(3, '556-06-16', 'pauline kay', 'sunglao', 'gaya', 0, '', 0, 0, 0, 0, '', '', '2016-06-09 01:53:32', '2018-07-05 02:30:07', '$2y$10$up0E.a/rOCDq5Byth2chWO.EmejNroKqnWM2DpuBJ37/NIXCvq5t2', 1),
(4, '555-06-16', 'juan carlo', 'm', 'termulo', 0, '', 0, 0, 0, 0, '', '', '2016-06-09 01:54:06', '2018-10-26 08:13:43', '$2y$10$Ny4bRMBAg71rJhDUrid3WuF8SAFakSsFbleJc6pgx2vtYYQfkd/Ye', 0),
(5, '523-05-15', 'edward', '', 'rodriguez', 0, '', 0, 0, 0, 0, '', '', '2016-06-09 01:55:58', '2018-02-09 00:19:25', '$2y$10$HG7zxiSY/9WFaf60DcBbQegONMxdIkcw6kySvIGbEffgj1tXopxpS', 0),
(6, '530-06-15', 'sarah joy', 'Gabriel', 'rivera', 0, '', 0, 0, 0, 0, '', '', '2016-06-09 01:56:20', '2017-01-26 08:58:24', '$2y$10$TfJxEqHw9t2H9Oy2Htt7euSEKaY/AUvgKFWN37pbhmfgd/ONaJ2uK', 0),
(7, '303-06-05', 'florence', 'alvarez', 'flaviano', 0, '', 0, 0, 0, 0, '', '', '2016-06-09 01:56:49', '2017-04-19 05:34:41', 'fbd1049b57707f65d22ad5782ee4bc30', 1),
(8, '500-08-14', 'danica', 'evangelista', 'paraton', 0, '', 0, 0, 0, 0, '', '', '2016-06-09 01:57:11', '2017-11-09 09:39:19', '$2y$10$.PcXrxhkqS2i2/wbAL7xROdr35L2tHK1ewgK6GKiuKjL6dW6cASxO', 0),
(9, '547-05-16', 'edrei son', 'c', 'victoria', 0, '', 0, 0, 0, 0, '', '', '2016-06-09 04:27:23', '2018-05-22 03:55:47', '$2y$10$GMRn0JtIkBC3MkRf0Ow9lOFZc1BamXQ96VBtbeKetIbPByx.8lFqa', 0),
(10, '572-06-16', 'jassen ka eunice', 'c', 'manalili', 0, '', 0, 0, 0, 0, '', '', '2016-06-09 04:27:41', '2018-11-16 06:27:28', '$2y$10$Fa/lK7KwC8CGo9I8sDirwOaaaw1/ZZX0ossyrlctAIdLE34H9mSaW', 0),
(11, '551-05-16', 'george kevin', 'n', 'tomas', 0, '', 0, 0, 0, 0, '', '', '2016-06-09 04:27:59', '2018-05-08 07:18:57', '$2y$10$aK.tMEgVKXh5ZBwtfLpa6OuaUSHTsTR8ucHPXh48I4Ss7.dmSvwhe', 1),
(12, '549-05-16', 'zyron', 'g', 'tabajonda', 0, '', 0, 0, 0, 0, '', '', '2016-06-09 04:28:16', '2017-05-18 00:49:21', '$2y$10$HjMlUZ4ZXDdeELtVUziVE.Beo4/.VXn3wC7jxQpnyWmvVusutToO6', 0),
(13, '436-07-13', 'cecile', '', 'alday', 0, '', 0, 0, 0, 0, '', '', '2016-06-09 08:12:43', '2018-07-03 06:39:09', '$2y$10$rEIJDOx21IuRWA1heJDWtOlr1KRL5SR9z3A3rg1qO34ciXkKIT81G', 0),
(14, '532-06-15', 'magie', '', 'rebicoy', 0, '', 0, 0, 0, 0, '', '', '2016-06-09 08:12:56', '2016-09-15 07:09:03', '$2y$10$SdsM3k5RJfJsQ4u3uZt9BeqFS0B0f1fyzp9yXU974RWz6HHHzhtY.', 0),
(15, '570-06-16', 'melvin', 'laluna', 'lacson', 0, '', 0, 0, 0, 0, '', '', '2016-11-25 08:49:11', '2018-07-05 02:30:17', '$2y$10$oV3Bufw35d68ymrhJvfvMOFxqxOW1opvAN7PKLY3WAEp2RkXw4uLC', 1),
(16, '439-07-13', 'maurine', 'reguyal', 'lazatin', 0, '', 0, 0, 0, 0, '', '', '2016-11-25 08:51:07', '2018-10-11 03:39:17', '$2y$10$JqAnxvYQBRJSm.cN6Kdk4.FkuFYx.zDh/3TACpwhz1s07asEpQSfq', 1),
(17, '566-06-16', 'christine joy', 'Marinas', 'velicaria', 0, '', 0, 0, 0, 0, '', '', '2016-11-25 08:52:15', '2018-07-05 02:57:22', '$2y$10$WhVj2Ov7vKqD7RNsHk0Wo.bqZjsMYGSNJXldN7Yv.xHatW/U9qDlC', 0),
(18, '563-06-16', 'jamin', 'r', 'asuncion', 0, '', 0, 0, 0, 0, '', '', '2016-11-25 08:53:08', '2017-06-13 03:29:43', '$2y$10$z5gYMbf6RM6hQvimZewNKencsiJT2KHkpuuA79Scu6/s6lqMikU9u', 0),
(19, '565-06-16', 'jaynelle', 'Gomez', 'domingo', 0, '', 0, 0, 0, 0, '', '', '2016-11-25 08:53:51', '2018-06-06 07:41:40', '$2y$10$wgM8yl8TWg82QNK/WFuiOOuBrtqmweVhtv87EThpZqJTYUuKFy4NO', 1),
(20, '595-11-16', 'ciara mae princes', 'Resilva', 'caba', 0, '', 0, 0, 0, 0, '', '', '2016-11-25 08:54:50', '2017-01-26 09:02:14', '$2y$10$/7WYMJHiwYWhEtJviPBA4.tcRIErlsC8AwCpzzr6VnjEEF1.plq6G', 0),
(22, '000-00-00', 'nrotc', 'nrotc', 'nrotc', 0, '', 0, 0, 0, 0, '', '', '2016-11-25 08:56:13', '2018-08-24 06:37:39', '$2y$10$geCx9uIaXkwD9sqkoCLegenL/apBlSyLDfXNXJ/IyEzp1vq71AIbe', 1),
(23, '531-06-15', 'myko roEve', 'e', 'lagman', 0, '', 0, 0, 0, 0, '', '', '2016-12-02 06:59:10', '2018-11-15 03:23:28', '$2y$10$FClERKBMfhLSFymZuFA07u7bi8JNdye7KsFDeNwM/.mUSBBG6GYey', 0),
(24, '498-07-14', 'gladys', 'Dueñas', 'dela cruz', 0, '', 0, 0, 0, 0, '', '', '2016-12-02 07:36:46', '2017-11-09 09:37:49', '$2y$10$QWA7d9S8ng4Ry.maK2A9/uyWP6dy3vnIQ9/L/vSH4cm3M2C2sM5jC', 0),
(25, '541-06-15', 'jenelyn', 'D.', 'abrera', 0, '', 0, 0, 0, 0, '', '', '2016-12-02 08:31:48', '2018-06-26 05:31:14', '$2y$10$u31y1zHDcwwv/HugyCVqauYyD2v48CL0dE3e2YcWUGVK5k0y123Gu', 1),
(26, '321-06-07', 'pedro', 'Catalon', 'DE VERA', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:37:51', '2018-07-05 02:29:40', '$2y$10$jAMzY2vpn7EHfa.pjWJYH.3EfzloaaNGy8UmpSXhZklhEj1IdDiXy', 1),
(27, '255-11-01', 'FEDERICO', 'navarro', 'DELIZO', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:38:43', '2016-12-04 23:38:43', '$2y$10$jP49pgIFeAXqDXNidnpu/.7ks632oOLEcQCvAw/XdYE5.DXWsRKf6', 0),
(28, '592-09-16', 'ALFRED', 's', 'DOMA', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:39:29', '2016-12-04 23:39:29', '$2y$10$m6kJAAYP1aQyZ4vteatA0ut.oYOwIS52DPwZF3tJyW4tyPA6PDPUG', 0),
(29, '570-09-15', 'ABRAHAM', 'Bero', 'DUQUE', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:40:11', '2018-08-02 01:02:38', '$2y$10$fImSQwSTYe.ks6SmLDOOTO6weccVLL4BKpHGgDDOUL7sI/JXSJuDC', 1),
(30, '597-11-16', 'rony', 'Ismael', 'DUQUE', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:40:33', '2017-11-09 09:40:32', '$2y$10$ONURNZVIsio.CisTX/DV3OKpGPrFXl9jDoQuOFSbk4/qjb4Nfrk.e', 0),
(31, '347-06-10', 'NICANOR', 'Wenceslao', 'ESTRERA', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:41:21', '2017-11-09 09:40:19', '$2y$10$mtkAdBQBmFm7EFxDfhfCAuQZiNulza.pHzFYuLrI0UuWJSi5Am5Du', 0),
(32, '603-11-19', 'manuel', 'Cruz', 'FERNANDO', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:41:49', '2017-01-26 08:58:54', '$2y$10$a6E4M9zznLqp9wNKGP/u8ubU.LwzPhThGsOUwoll8PR0yjrzUpvOm', 0),
(33, '170-06-06', 'VIRGILIO', 'Serrano', 'FERRER', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:48:04', '2017-06-05 01:31:22', '$2y$10$P6SIGlQRpedA4O2zKO82OuZtuqUpgbdaSgGzHdOCLcp8lrTPmEG2q', 1),
(34, '382-06-12', 'REYNALDO', 'Alvarez', 'FRANCISCO', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:48:33', '2017-06-13 03:30:56', '$2y$10$P8lMnzBhuG9t4JCljR40.eeZe6EJay3Nk0APJR343ZLztBgy9F9nC', 0),
(35, '506-11-14', 'ROMMEL', 'Paganagan', 'GARCIA', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:49:03', '2017-01-27 02:41:14', '$2y$10$Qso7euFblrlC062HynsUCuJJa5biNKaM2F2cPFzYq0ihCRlLLmF8W', 0),
(36, '572-09-15', 'IRENEO', 'c', 'GERONIMO', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:49:24', '2016-12-04 23:49:24', '$2y$10$zSA../oaewnUsBTKLBzBJOdvHgrLIJsTJfS2ByRStpY4O5/7ySrHu', 0),
(37, '601-11-16', 'NEIL PATRICK', 'Ricardo', 'GERONIMO', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:49:43', '2017-01-26 08:59:38', '$2y$10$c7I2oAIsJlfb02xqgEJFpO2quoS0rD/Kc2kmlfUedOHDyWxYASVIq', 0),
(38, '201-01-97', 'edgardo', 'Ilumin', 'MANUCOT', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:51:36', '2017-01-26 08:59:10', '$2y$10$CkomxC1yZY9L08048Y8CBO./MAOlpwwmsZ6c9SzwhXHpndsCL/kI6', 0),
(39, '423-06-13', 'evelyn', 'abalos', 'NAVALES', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:51:59', '2016-12-04 23:51:59', '$2y$10$cGfP88pKCsMlhSV4/ksj2.GTkeDh.WqDYfKPVHFCiDJlAIVg2mYlK', 0),
(40, '540-01-16', 'JEFFREY', 'd', 'NIRO', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:52:58', '2017-07-24 09:46:25', '$2y$10$NaWGWE08AgxcMuHbWAEcm.B9BH1sXTXZfG7AsVyZo8jOKuN9CMxB2', 1),
(41, '469-06-14', 'MICHEIL', 'l', 'NOLASCO', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:53:47', '2018-07-02 09:14:11', '$2y$10$RFA81YWi0aODLeejg4fdpOpaZxfJ5hmEIXEYCTPq46kpr2uLmgy0.', 1),
(42, '392-11-14', 'arnold', 'p', 'nool', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:54:08', '2016-12-04 23:54:08', '$2y$10$nyNYfKj5tJ6XQGlixY3SNO8Ag6FA8ANIHcmNQahqwaitfd7TWpVnm', 0),
(43, '542-06-15', 'celso flor', 'Cayabyab', 'PANTE', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:54:31', '2017-01-27 08:02:54', '$2y$10$qT3TrveZrc/JEhf40gcYteRHmKaUC8b9HQyrQUoEEw2Sdlo3hO/y2', 0),
(44, '596-11-16', 'joyce ann', 'del rosario', 'PARATON', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:55:07', '2017-01-26 09:04:45', '$2y$10$4qp4fdKiMvn.UMOlb1dsxuau80MK8gXTY60Yf3UKAdpZif1DAZg8K', 0),
(45, '561-06-16', 'ALONA JANE', 'Cabamungan', 'PERALTA', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:55:40', '2018-07-26 06:36:17', '$2y$10$hwVnDcA7gL0oWQf6DstVMuvo.VpWfDq91vP8zVPXwCq.PRqLvW.5G', 1),
(46, '569-06-16', 'RONALD', 'Banzuela', 'polong', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:56:05', '2018-08-17 07:39:18', '$2y$10$SjGvy6d6c9qbG1Ll5pctv.610YcxO9A0hGRy9SfSyV.bAcYHpaP0i', 1),
(47, '547-06-15', 'ISIDRO ARVIN', 'Santiago', 'ROBERTO', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:56:37', '2018-07-10 01:03:52', '$2y$10$CNwyo/hpGQ24fWIrXRNcYOnfH7ftghDpEGGdUmqKLBKgR8zr6.TJS', 1),
(48, '373-06-11', 'MANOLITO', 'Cayabyab', 'SOLIS', 0, '', 0, 0, 0, 0, '', '', '2016-12-04 23:57:26', '2018-06-04 03:58:19', '$2y$10$/Ixr4GlTEwxifwVhD4jTruXpyGmXJRT7CKdXs3dv7l2R4IZHdsL1G', 1),
(50, '599-11-16', 'EDUARDO', 'Natividad', 'VERGARA', 0, '', 0, 0, 0, 0, '', '', '2016-12-05 00:04:49', '2018-08-13 07:06:08', '$2y$10$2nr/bey2K9Bmyqz3NhcJBus2eu6njAryFkxjq8PrUs.GEZ6O0oG9i', 1),
(51, '147-06-96', 'CRISPULO', 'Rabida', 'VILLAMAR', 0, '', 0, 0, 0, 0, '', '', '2016-12-05 00:05:07', '2017-01-27 08:02:35', '$2y$10$.HCTbcO/LAmiu.pTomfdZ.H.qQURm3wkMKNnJgXRtpEZFIZjZW57S', 0),
(52, '467-06-14', 'JEROME', 'a', 'ABANILLA', 0, '', 0, 0, 0, 0, '', '', '2016-12-05 00:05:59', '2018-06-22 00:30:58', '$2y$10$NGM8Sy0srvkij3DFLR0nuukXNxddrSecQ67Ev2rP3D0HzmGDKrZDa', 1),
(53, '164-11-94', 'RICHARD', 'sawit', 'ANTOLIN', 0, '', 0, 0, 0, 0, '', '', '2016-12-05 00:06:46', '2018-08-07 05:28:38', '$2y$10$7qDhaO5nQ.IVr0JgMGpjheHcTOPfEq4ojyjkNPUM8J3BdrFDwHZA2', 1),
(54, '340-06-09', 'ALEJANDRO', 'Gonzales', 'asana', 0, '', 0, 0, 0, 0, '', '', '2016-12-05 00:07:25', '2018-06-09 06:22:45', '$2y$10$KWaPuNOfwi7r2HVaVEq/n.bArxtjTQzlew3Z9nyFyq2..Ht8.cMD2', 1),
(55, '567-06-16', 'CHANDRA-JHO', 'Lanuza', 'BALAONG', 0, '', 0, 0, 0, 0, '', '', '2016-12-05 00:08:08', '2018-05-08 07:16:49', '$2y$10$BYvw964Yme7m80wK.9Gu5.4IAeW/wg.x3XnFrDyOMVfCoMTvtTq6G', 0),
(56, '364-06-11', 'domingo', 'g', 'CABADING', 0, '', 0, 0, 0, 0, '', 'jr', '2016-12-05 00:08:35', '2018-03-13 03:32:07', '$2y$10$FhemSHuZFwIBEc0F1PP.r.l/xDuzhTteFRuNCmOIb.h.oaaeaCmwS', 1),
(57, '571-06-16', 'CRISTIAN', 'Cortez', 'CABALLERO', 0, '', 0, 0, 0, 0, '', '', '2016-12-05 00:08:52', '2019-03-19 02:09:27', '$2y$10$6kp5UBhyZ3wy9t.Tae/jmOE4DHrKtJuhlUr3uIE2iT6ABSxIAAXNe', 0),
(58, '337-04-09', 'DANIEL', 'v', 'CAJUCOM', 0, '', 0, 0, 0, 0, '', '', '2016-12-05 00:09:26', '2018-06-06 07:18:09', '$2y$10$M78IjuD1PRNcZYoE2O5a7O724siTLtBZwGZaHnzSr/Z5xr8Pw9JZK', 0),
(59, '513-01-15', 'LOUIE JANE', 'Agabin', 'CARBONEL', 0, '', 0, 0, 0, 0, '', '', '2016-12-05 00:10:00', '2017-01-27 02:42:06', '$2y$10$EfdoMJ3CxnaQUjv7Uy68x./k1epBh5TjzixLJgZ8vPpRapn6hi4jm', 0),
(60, '557-08-15', 'JAYSON', 'Alvarez', 'CARINGAL', 0, '', 0, 0, 0, 0, '', '', '2016-12-05 00:10:22', '2018-08-18 08:01:28', '$2y$10$mZNVGENFWvhJa50/e8OgtO/Blkbr5GmJE0z8lRo93DFXYT/aMto/K', 1),
(61, '562-06-16', 'ROBIE MAR', 'Aquino', 'DAYTO', 0, '', 0, 0, 0, 0, '', '', '2016-12-05 00:15:58', '2018-10-17 12:14:29', '$2y$10$I6AypjFO/luhTDmJZrNf5OdrlbVsFc1XpittomhmmjqVAE.QGsIqa', 1),
(62, '533-06-15', 'PRINCESS JOYCE', 'b', 'CALDERON', 0, '', 0, 0, 0, 0, '', '', '2016-12-05 00:17:19', '2018-06-22 00:31:29', '$2y$10$AayTbuEBLp5KzQ/mmFaDEO5M8vW0/Pl/04B66GiFeBWkCvA84u4v2', 0),
(63, '554-06-15', 'ANJOE MICHAEL', 'm', 'MALLARI', 0, '', 0, 0, 0, 0, '', '', '2016-12-05 00:18:42', '2019-03-19 02:10:53', '$2y$10$ufOkmhFUw7mJk/xblw4tsemVdrVxUqUQXR/5wPhWg8ne/JNgi4gqa', 0),
(64, '554-07-15', 'JESUS', 'b', 'DAYRIT', 0, '', 0, 0, 0, 0, '', '', '2016-12-05 01:47:13', '2018-10-16 02:08:56', '$2y$10$PmmFd7p3WaYd/61N0E/5DeyK3E26DdROFPa.gsPCrRc2aQhO2NMgS', 1),
(65, '577-06-16', 'JAN LOUIS VINCENT', 'f', 'MIRANDA', 0, '', 0, 0, 0, 0, '', '', '2016-12-05 01:49:12', '2019-01-09 06:24:41', '$2y$10$QmTUOILIPutQMGxsuyLXh.L5J/5SzOnlwivbcLpYmb7AGnuFs8iJm', 1),
(66, '516-04-15', 'richard', 'l', 'OANDASAN', 0, '', 0, 0, 0, 0, '', '', '2016-12-05 01:49:37', '2018-06-01 02:19:20', '$2y$10$VEBUJeFIyYyCzmlVjMoeyuUOmziKZU99qWOYdwS8P2mOl.lLzHRa2', 1),
(67, '318-11-06', 'citadel', 'b', 'PUNZAL', 0, '', 0, 0, 0, 0, '', '', '2016-12-05 01:50:12', '2018-07-05 02:31:02', '$2y$10$DZUJhq6ymLfJbGtH99CqpOgzs12YwjOEMujwcd/aXdRG8FsjT3Kq.', 1),
(68, '590-08-16', 'jessa amor', 'Guzman', 'necesito', 0, '', 0, 0, 0, 0, '', '', '2016-12-09 23:31:33', '2018-07-05 02:30:40', '$2y$10$rHa4AuoM8zQUz5FdIFQgYO1nkWeXUUu/pgN4vU6Vk7GxpTuaEddPG', 1),
(69, '598-11-16', 'pauline kate', 'Fermin', 'catahan', 0, '', 0, 0, 0, 0, '', '', '2016-12-10 05:37:25', '2017-01-27 02:42:39', '$2y$10$zayKnXWjkhQdefUlM09uQehpsm0oAtxXnHrKoftufP3zB5YqI0wCW', 0),
(70, '602-11-16', 'venancio simon', 'Sobredilla', 'Sulit', 0, '', 0, 0, 0, 0, '', '', '2017-01-26 07:31:54', '2017-01-26 07:31:54', '$2y$10$jCXjk33yaXsSKv8KT66BaOCf.aHtSzxn29fbr2yer4fJbq6sb1Dk.', 0),
(71, '564-06-16', 'BABY JANE', 'Mirasol', 'CONCEPCION', 0, '', 0, 0, 0, 0, '', '', '2017-01-26 07:58:16', '2018-06-22 00:31:37', '$2y$10$S3rwiFm/Mm0EtttdFRZIdeyZVeTJaUlIrKRZVNpZ8cycbU7Hn2X3a', 1),
(72, '405-08-14', 'Ariel', 'Canoza', 'Mag Isa', 0, '', 0, 0, 0, 0, '', '', '2017-01-26 07:59:44', '2017-01-26 07:59:44', '$2y$10$2JeuUKCZntH/EG1kQ7wKSejjS/0Wf5we4qrIMU38ZC7i.oqZEPW5C', 0),
(73, '376-11-11', 'Jaxpee', 'Lopez', 'Poblete', 0, '', 0, 0, 0, 0, '', '', '2017-01-27 02:38:18', '2018-07-26 06:54:37', '$2y$10$JI0ZqijLERgiGzEf8zITAOHdRGvhLTE3lsnIBgMPtns70mSoxRTO.', 1),
(74, '605-02-17', 'IAN CARLO', 'De Luna', 'VillANueva', 0, '', 0, 0, 0, 0, '', '', '2017-04-19 04:10:41', '2018-05-08 07:19:05', '$2y$10$9qAhMf.ikr9fg5.BrYeNROSptNiEb5Rio2YcfzpBYmqq06zPmmf2a', 0),
(75, '578-07-16', 'RODRIGO', 'JULIAN', 'MARTIN', 0, '', 0, 0, 0, 0, '', '', '2017-04-25 08:52:03', '2017-09-14 09:25:50', '$2y$10$liz53yX6Dkk88w/2j1kXfesGlZhLvSwi7YNhQp/7Y7ne8Mwr3fF9C', 1),
(76, '389-06-12', 'NOWEL', 'URBANO', 'RAMOS', 0, '', 0, 0, 0, 0, '', '', '2017-05-04 05:33:29', '2017-11-09 09:38:43', '$2y$10$i4wyBQXQRezYLTNshfz8v.yAjRFfEIUJbg0Ee42SZDnxbUudRANkm', 0),
(77, '608-05-17', 'daniel', 'f', 'aguilar', 0, '', 0, 0, 0, 0, '', '', '2017-06-07 05:26:19', '2018-07-26 06:37:27', '$2y$10$2yJqPflEDjy5r3gSvnPSp.pcNDB/zH/91EwS/zW7ErsvTleP0nCuS', 1),
(78, '609-05-17', 'gerald', 'dg', 'mabuti', 0, '', 0, 0, 0, 0, '', '', '2017-06-07 05:27:04', '2018-05-08 07:17:58', '$2y$10$ZEnwxgkFt8/S96YXLCZ0KuFN07tmuzGmOolfcOYAGqAFh05gxoYi.', 0),
(79, '610-05-17', 'joana mary', 'c', 'salazar', 0, '', 0, 0, 0, 0, '', '', '2017-06-07 05:27:48', '2018-05-08 07:18:48', '$2y$10$M2UhHWQPjYIlgSyGHmINouTT4IEATIIwhxwbufjxE5foYcpgBBPne', 1),
(80, '611-05-17A', 'janice', 'c', 'ignacio', 0, '', 0, 0, 0, 0, '', '', '2017-06-07 05:28:31', '2018-05-08 07:17:45', '$2y$10$bXBP1qdtoE27eVD6Phs51uFrZTeiB62CnX3dfg3MJbii8FrfdP4R2', 1),
(81, '612-05-17', 'esther angela', 't', 'romero', 0, '', 0, 0, 0, 0, '', '', '2017-06-07 05:30:21', '2018-05-08 07:18:46', '$2y$10$8Y7Jc39Iyp1q.ZAIFk76UO8ZVPxzPfnzjR7enTQ1dt/jERK7g6jCu', 1),
(82, '613-05-17', 'alvin jay', 's', 'ramos', 0, '', 0, 0, 0, 0, '', '', '2017-06-07 05:31:07', '2018-05-08 07:18:40', '$2y$10$BMw6KYWmhvhHcK0W3VnOzO90HJjU1SPgcPOOMtk6ixUtRHLCPRn5q', 0),
(83, '614-05-17', 'michelle joie', 'm', 'pascual', 0, '', 0, 0, 0, 0, '', '', '2017-06-07 05:32:16', '2018-08-22 02:51:18', '$2y$10$RZ3GxBSJO37odT4zrQtW9.m8L7umQGXrzP3UCgh2xqoHwV12CFdzu', 1),
(84, '615-05-17', 'junior', 'm', 'pacol', 0, '', 0, 0, 0, 0, '', '', '2017-06-07 05:33:13', '2018-05-08 07:18:22', '$2y$10$0vLh1p.P6KQY2fgofhYFSefiI5j5lNf/hlOjG6UIOw5.1aTcB41SC', 0),
(86, '617-06-17', 'bryan jay', 'e', 'dela cruz', 0, '', 0, 0, 0, 0, '', '', '2017-06-07 05:48:11', '2018-05-11 08:29:57', '$2y$10$ZCWhwDEQliBp8c3F/lt6Y.AIlfh77DqxnecesswAoyDxOZN1ONViO', 0),
(87, '618-06-17', 'james michael', 'a', 'dalde', 0, '', 0, 0, 0, 0, '', '', '2017-06-07 05:48:57', '2018-07-26 07:50:59', '$2y$10$Z2biH/KO4iygof1vNYrujO.VZn1cr/gVjUlYKiKHfgowzkjt.kfqi', 1),
(88, '619-06-17', 'nicole', 'dg', 'pascua', 0, '', 0, 0, 0, 0, '', '', '2017-06-07 05:49:34', '2018-06-22 00:33:10', '$2y$10$TlEjEVEbaf6gr27jDnAJQexpeP1.GbtTgGXTVOX3juXvo0Ao.0vhW', 0),
(89, '444-08-13', 'FRANZ RUSSEL', '', 'MURALLA', 0, '', 0, 0, 0, 0, '', '', '2017-06-14 05:20:14', '2017-09-13 02:32:28', '$2y$10$JLjKe.x6lsIJ3jVRk4o97OXJMCx6U9cOWJs2dZLoUtV8eqdSuWSCC', 0),
(90, '574-06-16', 'GLYFORD', '', 'SELENDRE', 0, '', 0, 0, 0, 0, '', '', '2017-06-14 05:20:48', '2018-07-03 06:21:33', '$2y$10$mOe8J0KUZKYHti2u/3pTjOdcq/e2a6uruSt33IB/SFj8uPjmv6QnC', 1),
(91, '545-04-16', 'ricardo', '', 'maines', 0, '', 0, 0, 0, 0, '', 'jr', '2017-06-15 00:02:21', '2019-03-19 02:10:50', '$2y$10$lHO6mLm9yqm2z8sZDneroecc6UIjxrf8OvNw/CCuZqBo4MQiDFHQe', 0),
(92, '620-06-17', 'jessie', 'd', 'pincil', 0, '', 0, 0, 0, 0, '', '', '2017-06-21 03:12:42', '2018-07-05 02:30:56', '$2y$10$icLpbMWXtETNQUa5H/7YV.dYgU5n077W/TArNUAxYAsLduC/dMjpK', 1),
(93, '616-05-17', 'michel jame', '', 'grospe', 0, '', 0, 0, 0, 0, '', '', '2017-07-05 06:25:30', '2018-05-08 07:17:41', '$2y$10$/tBB9xyZglrU7by/DxEQxuhrUee6uXT6pRhg.jyNJDvhwRW6ParMa', 0),
(94, '579-07-16', 'winnie', '', 'matic', 0, '', 0, 0, 0, 0, '', '', '2017-07-06 00:14:02', '2018-05-08 07:18:09', '$2y$10$MSEsUlncH6JyLaZw4Tlj5eALDsvQhbVqALOvUsV8nqutgTfWCNrtC', 0),
(95, '455-11-13', 'sabas', '', 'mercado', 0, '', 0, 0, 0, 0, '', '', '2017-07-06 00:17:26', '2017-11-09 09:39:40', '$2y$10$8yBOwwXogMwbHS0LvJC/N.7aOd9W5FyDnP4iy3hjLSx3jwjxwbvLa', 0),
(96, '550-08-15', 'kathleen kay', '', 'antonio', 0, '', 0, 0, 0, 0, '', '', '2017-07-06 00:58:52', '2018-06-06 07:17:37', '$2y$10$EEXAMu3MA2PoaGgGr/jC7.Fy5zOz83D49RwlUCuRflSXsfcTHTWmS', 0),
(97, '565-08-15', 'ervhin Bryan', '', 'de luna', 0, '', 0, 0, 0, 0, '', '', '2017-07-06 01:02:23', '2018-06-06 07:18:26', '$2y$10$FlulwdGNLKUr7sIAH/LrG.CwKHHFKOwTjcbWLt8SCXoExJQJIz6YW', 0),
(98, '552-05-16', 'bryan', '', 'dela cruz', 0, '', 0, 0, 0, 0, '', '', '2017-07-06 01:04:13', '2018-12-06 03:03:55', '$2y$10$PdXnWuXd5u3H0AI4KpNMYuwZkC1kPoPlDs3xkXOdbx6LrhpkYhFVS', 1),
(99, '519-04-15', 'edmar', 'REYES', 'dizon', 0, '', 0, 0, 0, 0, '', '', '2017-07-06 01:08:09', '2019-03-19 08:49:28', '$2y$10$wo7SB6DwQtNS319xyb5fuudgSD9UcbnyPXmXvq/EbiQeXOK/ZBV/S', 1),
(100, '543-03-16', 'lyn', '', 'gonzales', 0, '', 0, 0, 0, 0, '', '', '2017-07-06 01:12:26', '2017-08-25 01:36:21', '$2y$10$uu36sQfPkqLErHt/L77EKOc7l/1bnJbOUjfh4PUrqGSzKSZ7zJb86', 0),
(101, '747-153', 'nstp', '', 'nrotc', 0, '', 0, 0, 0, 0, '', '', '2017-07-17 03:47:15', '2018-07-09 08:39:06', '$2y$10$FUdGUCtj5Rt8vEe4DcBnI.llrP.0B49K5CumIVde.cUHzfmZcSVLm', 1),
(102, '489-06-14', 'PHI OMEGA', '', 'MANUBAY', 0, '', 0, 0, 0, 0, '', '', '2017-08-15 05:50:43', '2017-11-09 09:39:49', '$2y$10$shknt6Sm0j7y1m6w3snjnOgOWQgcwj6JHAu9FwmwAeNbEnQjBftea', 0),
(103, '526-05-15', 'KEVIN CHRISTIAN', '', 'JUAN', 0, '', 0, 0, 0, 0, '', '', '2017-08-15 05:51:28', '2018-05-20 14:47:48', '$2y$10$IPyBbjn/rd3Ukax1q4cfWOBQjR3DkA2nJlzi7Jkwx.UH5rX59Bs6S', 0),
(104, '622-08-17', 'chino conrado', 'manalastas', 'pingue', 0, '', 0, 0, 0, 0, '', '', '2017-09-13 02:27:16', '2018-05-08 07:18:34', '$2y$10$F.6hWA50BePj2c196sS/IOyGWBqvlFIacl.BrBvuy/Wx/ou8/i2.2', 0),
(105, '548-06-15', 'ARIES', 'V', 'DAYAO', 0, '', 0, 0, 0, 0, '', '', '2017-09-14 08:36:47', '2018-05-08 07:17:06', '$2y$10$/1egBlxec0k1UX5UMexGV.bsYWmb4NBgqGIaXaPwocBITLxttqsZG', 0),
(106, '550-05-15', 'alexis', 's', 'villanueva', 0, '', 0, 0, 0, 0, '', '', '2017-10-16 00:36:29', '2018-10-17 07:34:46', '$2y$10$o9K8n2pcqvr7f5CJRPAQLO/rWvSNTzo11Tcbgtcy/D7xW.GkVTlze', 1),
(107, '623-11-17', 'c/e jerry', 'a', 'esperon', 0, '', 0, 0, 0, 0, '', '', '2017-11-09 09:30:29', '2018-05-08 07:17:33', '$2y$10$5Cjv1Xn3Vt6y8qUhgego4eqDsEb8zyP1J8H4h/hoobnHtP28Bo2zm', 0),
(108, '624-11-17', 'lexter', 's.', 'rodriguez', 0, '', 0, 0, 0, 0, '', '', '2017-11-09 09:31:23', '2018-06-06 07:41:48', '$2y$10$kWU25PbY7P9LGURpz9wS1uFX9M4TU9oeeFYt3Y0GCqtm1.RIze5RG', 1),
(109, '625-11-17', 'orlando', 'b', 'andrada', 0, '', 0, 0, 0, 0, '', '', '2017-11-09 09:32:34', '2018-07-16 06:47:14', '$2y$10$sYf7noqIOhsRIfRz.0st0.61C/pmQ8uAej0pBVxQU/XyXJeRh1Xnu', 1),
(110, '626-11-17', 'kenneth', '', 'esguerra', 0, '', 0, 0, 0, 0, '', '', '2017-11-09 09:33:12', '2018-05-08 07:17:28', '$2y$10$BHdMxH0I2tUyOdZiOB9IW.kXC8lJNJWqW4ePlf54XUrz653//NF/C', 1),
(111, '627-11-17', 'john ephraim', '', 'anday', 0, '', 0, 0, 0, 0, '', '', '2017-11-09 09:34:01', '2018-04-23 03:25:57', '$2y$10$5FQ9R2BQys5qj/Z.HjiocecACAl/yQSILqPzqjCas/Z/Z7Pm0n1Eq', 0),
(112, '628-11-17', 'christian', '', 'erana', 0, '', 0, 0, 0, 0, '', '', '2017-11-09 09:34:31', '2018-03-05 06:12:08', '$2y$10$uOthzpQCEarFQIPWv4.pPOKPcRB3mrhx0s0dE82xo32pIe0oeImbW', 0),
(113, '558-06-16', 'eunice gem', '', 'deocares', 0, '', 0, 0, 0, 0, '', '', '2017-11-09 09:35:09', '2019-03-19 02:06:02', '$2y$10$dZnGg2MuR9rw5VWe5OQM/urBxMDtdAp6rHbFeq14Cr77PLj5u8VOS', 0),
(114, '192-07-96', 'maricel', 's', 'esguerra', 0, '', 0, 0, 0, 0, '', '', '2017-11-15 00:36:28', '2018-06-08 00:23:06', '$2y$10$610tiP66Vx2qnn1g97QwUeisqEEj60AVO8xbqxVUH/ncHk7NbiS26', 1),
(115, '591-08-16', 'sean tristan', 'd', 'sebastian', 0, '', 0, 0, 0, 0, '', '', '2018-01-25 01:59:43', '2018-04-23 03:26:40', '$2y$10$ztqi9eu5vVFIzVu5U/CmJOqty03OJLvndSJ3ofH52cHk/PO/D98E2', 0),
(116, '635-01-18', 'Ronolfo', 'a', 'almacen', 0, '', 0, 0, 0, 0, '', '', '2018-01-25 02:01:20', '2018-09-17 03:26:55', '$2y$10$QKGEY1v7ISfNXO3GyO4R.OFGytf10/qQ87lrmxxY9n6yIWRj6vOtC', 0),
(117, '638-04-18', 'john carlou', 'a', 'rafael', 0, '', 0, 0, 0, 0, '', '', '2018-04-27 01:35:52', '2018-06-06 07:19:53', '$2y$10$BuHjJPKMpoG0.Osgk.DoTOYzari1kDMz3bAj7wY6iAO.kl4X8BPt6', 1),
(118, '646-05-18', 'benjamin', 'c', 'neuda', 0, '', 0, 0, 0, 0, '', 'i', '2018-06-06 07:13:38', '2018-08-15 07:38:05', '$2y$10$0qWej.Bt6tkSbAjP05pBpOr0Q8gkePD8GhIOoN8QNE2x0xtn6EVQG', 1),
(119, '647-05-18', 'jaycel', 'c', 'bautista', 0, '', 0, 0, 0, 0, '', '', '2018-06-06 07:16:14', '2018-06-06 07:17:52', '$2y$10$xc50hk2pgrjinXrL5FqLoefnA2qDT2hw/YVTsfHhOgg1d/6ESFSGe', 1),
(120, '645-05-18', 'khaysielyn', 'r', 'nortez', 0, '', 0, 0, 0, 0, '', '', '2018-06-06 07:16:58', '2018-07-12 06:36:52', '$2y$10$4oAu4oGGG8B6cK/QsAbo/Oqvjr74/SC2wyXLMtPAkQUuc9g2jVjfa', 1),
(121, '652-05-18', 'juniel son', 'a', 'tabilen', 0, '', 0, 0, 0, 0, '', '', '2018-06-06 07:21:01', '2018-07-11 07:27:39', '$2y$10$lnXrtOfDMoyXw9lN9.2/zumfaNLALYRj2Qhq0L937e/xtAn4D3wG6', 1),
(122, '649-05-18', 'meryll joyce', 'c', 'cabinian', 0, '', 0, 0, 0, 0, '', '', '2018-06-06 07:21:52', '2018-07-26 06:55:23', '$2y$10$UUw0G94DVlVE.q5zyddc1u1mZ1.QfYEaaX60S//Pd0E4XPHdYs46e', 1),
(123, '644-05-18', 'jessiebel', 'c', 'perez', 0, '', 0, 0, 0, 0, '', '', '2018-06-06 07:31:32', '2018-07-26 06:55:10', '$2y$10$GAoMCpgn5SJjEGLRmRfxxuEtcOnA15VJ3Jp96f3GxrSY6aohQejvi', 1),
(124, '648-05-18', 'jessa may', 'v', 'santos', 0, '', 0, 0, 0, 0, '', '', '2018-06-06 07:32:07', '2018-07-26 07:01:40', '$2y$10$oqPQ932r2ba/dItv8GrgiOHNiIk8xnF9ddIPpIIAepqwQBpAjgGsG', 1),
(125, '650-05-18', 'rosanna', 'i', 'bueno', 0, '', 0, 0, 0, 0, '', '', '2018-06-06 07:32:54', '2018-06-06 07:33:50', '$2y$10$k8mNf4EJB/jgya58pCYiVuk0ohbOCCT4l4Xl8Z.AcIo1twot6NX7K', 0),
(126, '651-05-18', 'tala', 's', 'molina', 0, '', 0, 0, 0, 0, '', '', '2018-06-06 07:33:27', '2018-07-31 03:40:16', '$2y$10$4BzyggXMrU64t7bsCOa5PeMKRwQZF0alHHLE4j5fdFEmBjmCtEiYS', 1),
(127, '643-05-18', 'edgar', 'p', 'aladeza', 0, '', 0, 0, 0, 0, '', '', '2018-06-06 07:40:05', '2019-03-19 02:08:52', '$2y$10$QfkLiTKzWXxIkKN4SJL/p.qFmPZKAXcvYkf36pZA7DLRsC0bQcZ3S', 0),
(128, '393-11-14', '3m leoner', '', 'MAGHINANG', 0, '', 0, 0, 0, 0, '', '', '2018-07-09 05:05:00', '2019-03-19 02:10:46', '$2y$10$tYsGoG8WuWVcgfRwLo/Dc..JieXb0xj3U8Giv01hcdwXM/4.XFsJW', 0),
(129, '660-07-18', 'Magnolia', 'c', 'Gaviola', 0, '', 0, 0, 0, 0, '', '', '2018-07-09 05:05:34', '2018-07-09 05:05:57', '$2y$10$YwWl5qFJ2c2qVGvm5hUwX.mSFqZ0LyfXI65jrOOnkim/ovUrFOR4a', 1),
(130, '654-06-18', 'Maverick Rovie', 's', 'Lagman', 0, '', 0, 0, 0, 0, '', '', '2018-07-09 05:06:49', '2019-04-02 06:55:03', '$2y$10$TGmSq1PUFEB38N78adrY5.x1lz2MbpdbdxdFUutTQUL3WFr9PhT/i', 1),
(131, '655-06-18', 'John Elton', 'l', 'Mackay', 0, '', 0, 0, 0, 0, '', '', '2018-07-09 05:07:21', '2018-08-22 07:35:39', '$2y$10$.6KltadvP4aBO40bBDwF4ud7PxNuzFj6TKJNhNJb7xFaZMxDY3sWG', 1),
(132, '657-06-18', 'OIC Aldrin', '', 'De Guzman', 0, '', 0, 0, 0, 0, '', '', '2018-07-09 05:07:57', '2018-07-13 00:16:17', '$2y$10$pwqOF2IO7yhrr3uw8SljDOllYdUs1wkTv.iSxLovaoxZeoZwbiuCG', 1),
(133, '658-06-18', 'OIC Jose Romel', '', 'Dela Cruz', 0, '', 0, 0, 0, 0, '', '', '2018-07-09 05:11:26', '2018-08-09 06:13:08', '$2y$10$yAlZ4WGBzepWSYKeTDNrp.c74ZZML3HnFtwvEE6Tk1c5dMoEE2UF.', 1),
(134, '659-07-18', 'jose epimaco', 'r', 'arcega', 0, '', 0, 0, 0, 0, '', '', '2018-07-13 03:32:11', '2018-07-19 07:04:37', '$2y$10$u35Yp/An9E8ZmCg.fdBvEO9wtuTvx/2hgw0.ArvIgD/BsHue2acdy', 0),
(135, '662-07-18', 'reymond', '', 'reyes', 0, '', 0, 0, 0, 0, '', '', '2018-07-13 03:33:06', '2018-08-23 03:03:52', '$2y$10$qDiENJY17OJ1.oYdbCNjTO52CgZouyVD.OCEtEeD8sfsT8/Dps0ry', 1),
(136, '661-07-18', 'ronald', 'e', 'gordolan', 0, '', 0, 0, 0, 0, '', 'jr', '2018-07-13 07:45:45', '2018-10-17 02:38:22', '$2y$10$fXdSS5nfPFJtzbZQEJ5INua2258KyEZtpTDHvlfWjGpetaXYs2rmO', 0),
(137, '663-07-18', 'Franchesca Dianne', 'L', 'Hernandez', 0, '', 0, 0, 0, 0, '', '', '2018-07-24 03:51:29', '2018-07-26 00:56:36', '$2y$10$Syo.kpFtBlEEPKgOOisujuRqX8abA6YeZD/ngJRQCvQ41mOe5EJjS', 1),
(138, '665-08-18', 'john michael', 'gg', 'baruc', 0, '', 0, 0, 0, 0, '', 'jr', '2018-08-13 06:59:08', '2019-05-16 06:15:33', '$2y$10$VLJhqAPpUzjl0rPgnFuV4uj5ei7OyixUphcJ5z80fw6fEyDrgjJWC', 0),
(139, '670-09-18', 'ROSEDOM', '', 'BATANGAN', 0, '', 0, 0, 0, 0, '', '', '2018-09-17 03:26:07', '2018-10-11 23:43:46', '$2y$10$B14nLCVmklJJGnHBXPK63OoE3GU6y0tHKCZ6TTfb4E/hs1ovUXteC', 1),
(140, '669-08-18', 'arwine', '', 'matias', 0, '', 0, 0, 0, 0, '', '', '2018-10-17 02:50:43', '2018-10-26 10:30:02', '$2y$10$t7TGvz4VcKfeW2T/yUL72u.FchuAiJtZjOc8biZf/9zn/2gYnce8a', 0),
(141, '675-10-18', 'JEFFERSON', 'C', 'ORTIZ', 0, '', 0, 0, 0, 0, '', '', '2018-11-28 08:01:31', '2018-11-28 08:01:39', '$2y$10$VmIX9LZB0h0wIM98DKVRT.GLuquA0K6bz8p5aEFQDHw6PrAJENw8e', 0),
(142, '678-11-18', 'JADE CARLO', 'P', 'LAZO', 0, '', 0, 0, 0, 0, '', ' ', '2018-11-28 08:03:16', '2019-05-08 01:03:10', '$2y$10$rpeLlopqDV07MU4CRSomKOFPYL.rUNcAJlzNoXFqBGzbr2TPZIXaC', 1),
(143, '677-11-18', 'JOANNA', 'C', 'MANANGAN', 0, '', 0, 0, 0, 0, '', '', '2018-11-28 08:03:59', '2018-11-28 08:04:06', '$2y$10$qpuvdE9C7i9y0HXJLaLSy.D/smrIXQI0lXH7vC4XMH7BvZxT/nJqa', 0),
(144, '676-10-18', 'EDSON JAY', 'R', 'AQUINO', 0, '', 0, 0, 0, 0, '', '', '2018-11-28 08:04:51', '2018-12-05 12:39:09', '$2y$10$hvsugs4GFwoPIYCjGpKq7uNlnMTkN/B.IqaPdR6WG2TbHWU3gb9wG', 1),
(145, '682-12-18', 'prince ace', '', 'cutaran', 0, '', 0, 0, 0, 0, '', '', '2018-12-06 03:03:18', '2018-12-06 03:03:23', '$2y$10$KJoIQ6WJ97d9tazHYgqbgeF173X2u620omDhZBXklS5VKTO9nPoqW', 1),
(146, '0683-12-18', 'christian john', '', 'joyag', 0, '', 0, 0, 0, 0, '', '', '2018-12-11 01:59:11', '2018-12-11 01:59:53', '$2y$10$SZJ6ZTdtFb1u14hAxpj0y.waZT2tsIhVvee3ma.4Px4sbOcbpyBeq', 1),
(147, '0684-12-18', 'jecson ryan', '', 'gallano', 0, '', 0, 0, 0, 0, '', '', '2018-12-11 01:59:43', '2018-12-11 02:00:00', '$2y$10$YSsCR.2wl28YzrI0PzswPuX2P.vrREkK1ftmXPUK0oiiO3k5n1up2', 0),
(148, '671-09-18', 'JAMIL RAVEN', 'ZERRUDO', 'DELA CRUZ', 0, '', 0, 0, 0, 0, '', '', '2018-12-21 00:58:55', '2018-12-21 00:59:05', '$2y$10$BYbJya29jUeO1E2rmQpCteFRlGvzmC.y4fZex1o6Hqc4HMfdFYcke', 0),
(149, '685-01-19', 'paul laurence', '', 'siwa', 0, '', 0, 0, 0, 0, '', '', '2019-01-09 01:40:51', '2019-01-09 07:17:47', '$2y$10$UZRCXdVNr/KLDrZdBK/54e/5KtpVi2CvKyLetduvD92iIJTUBlm8G', 0),
(150, '686-01-19', 'george', 'e', 'lentic', 0, '', 0, 0, 0, 0, '', '', '2019-01-09 01:41:53', '2019-03-19 02:05:42', '$2y$10$buBVAUnoX4QQY5FJw9Megefky9jvT8cJeoEnj6Ax6WuA0lHsaz/o6', 0),
(151, '689-03-19', 'EDMUND', '', 'BAUTISTA', 0, '', 0, 0, 0, 0, '', '', '2019-03-19 02:07:08', '2019-03-19 02:07:20', '22e022d4cfc4878c8f623fa3e22ce1fa', 0),
(152, '688-02-19', 'FAMELA', '', 'AQUINO', 0, '', 0, 0, 0, 0, '', '', '2019-03-19 02:08:02', '2019-03-27 01:09:06', '3ed33f30f2455768e500e3f585ee665c', 0),
(153, '446-546', 'Avelus', 'wala', 'wala', 0, '', 0, 0, 0, 0, '', 'wala', '2019-05-07 08:09:06', '2019-05-07 08:20:59', 'f6293ccc88c838d12848c24b60e36654', 0),
(154, '689-03-20', 'dsdsd', 'sdsds', 'sdsdsd', 0, '', 0, 0, 0, 0, '', 'sdsdsd', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '54be7ab8387906d2946deee2d094e15d', 1),
(155, '689-03-20', 'dsdsd', 'sdsds', 'sdsdsd', 0, '', 0, 0, 0, 0, '', 'sdsdsd', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '54be7ab8387906d2946deee2d094e15d', 0);

-- --------------------------------------------------------

--
-- Table structure for table `teachers_encoding`
--

CREATE TABLE `teachers_encoding` (
  `id` int(255) NOT NULL,
  `teacher_id` int(255) NOT NULL,
  `status` int(255) NOT NULL,
  `settings` int(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL,
  `employee_no` varchar(1024) NOT NULL,
  `first_name` varchar(1024) NOT NULL,
  `middle_name` varchar(1024) NOT NULL,
  `last_name` varchar(1024) NOT NULL,
  `email` varchar(1024) NOT NULL,
  `password` text NOT NULL,
  `removed` int(255) NOT NULL,
  `remember_token` int(255) NOT NULL,
  `complete_address` varchar(2048) NOT NULL,
  `region_id` int(255) NOT NULL,
  `province_id` int(255) NOT NULL,
  `city_id` int(255) NOT NULL,
  `barangay_id` int(255) NOT NULL,
  `street` varchar(1024) NOT NULL,
  `suffix` varchar(1024) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(255) NOT NULL,
  `employee_type` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `employee_no`, `first_name`, `middle_name`, `last_name`, `email`, `password`, `removed`, `remember_token`, `complete_address`, `region_id`, `province_id`, `city_id`, `barangay_id`, `street`, `suffix`, `created_at`, `updated_at`, `active`, `employee_type`) VALUES
(756, '123-45-67', 'be', 'confirmed', 'to', '', '0a5d1a00675c3966b67f0f426c7f5c2b', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(757, '254-09-01', 'EMMANUEL', 'CARLOS', 'NAVARRO', '', 'cdadad3c01e70117ab47972ed129e2de', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(758, '556-06-16', 'pauline kay', 'sunglao', 'gaya', '', 'ca35ca1bb20bce17ac20421399fe795f', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(759, '555-06-16', 'juan carlo', 'm', 'termulo', '', '20fd73d072a50c98f3a26f68c259c5a0', 0, 1, '', 0, 0, 0, 0, '', '', '2019-07-02 07:46:12', '2019-07-02 01:46:12', 0, '1'),
(760, '523-05-15', 'edward', '', 'rodriguez', '', '437107cddd43761609e66dc53ad95bfe', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(761, '530-06-15', 'sarah joy', 'Gabriel', 'rivera', '', 'e72b9e65b9235a35d23b907bfa7f91ac', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(762, '303-06-05', 'florence', 'alvarez', 'flaviano', '', 'fbd1049b57707f65d22ad5782ee4bc30', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(763, '500-08-14', 'danica', 'evangelista', 'paraton', '', 'ae396d224aa2e8edfb412be70aaabc47', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(764, '547-05-16', 'edrei son', 'c', 'victoria', '', '18f8110d91d01efb3985db40c2032cde', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(765, '572-06-16', 'jassen ka eunice', 'c', 'manalili', '', '6fafe520c4002d239faab976611dec11', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(766, '551-05-16', 'george kevin', 'n', 'tomas', '', '63abac52420b94a98de5c40b9143b7d7', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(767, '549-05-16', 'zyron', 'g', 'tabajonda', '', '0617355eb6a846491d749e9a21d1dffe', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(768, '436-07-13', 'cecile', '', 'alday', '', 'cd5231cb74f524653a9077950d85b547', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(769, '532-06-15', 'magie', '', 'rebicoy', '', '8c9d938549b9f9fb625bceeb63f93ffb', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(770, '570-06-16', 'melvin', 'laluna', 'lacson', '', '2a6ad475c60afe2c4232fa1652383931', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(771, '439-07-13', 'maurine', 'reguyal', 'lazatin', '', 'c59f01ac897e9ab5f4f1634c28d44572', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(772, '566-06-16', 'christine joy', 'Marinas', 'velicaria', '', 'c027e8c42876d5408ac6f1724653fc68', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(773, '563-06-16', 'jamin', 'r', 'asuncion', '', '5e0b2ff46ff0e3b868f5bb07a9247970', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(774, '565-06-16', 'jaynelle', 'Gomez', 'domingo', '', 'b45be0f3540ec9215162f4268a438b85', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(775, '595-11-16', 'ciara mae princes', 'Resilva', 'caba', '', '4e76fca2eaf2377c20628b6f2535e4f3', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(776, '000-00-00', 'nrotc', 'nrotc', 'nrotc', '', '5247be35eca8a7713df5054d49038956', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(777, '531-06-15', 'myko roEve', 'e', 'lagman', '', '20a4e0a61027d3b615907ae7bf65f5f3', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(778, '498-07-14', 'gladys', 'Dueñas', 'dela cruz', '', '3114dde98ccec8d6065f1b86819a2480', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(779, '541-06-15', 'jenelyn', 'D.', 'abrera', '', '9770bdb87744757a08ebbff715b79682', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(780, '321-06-07', 'pedro', 'Catalon', 'DE VERA', '', '0747210df707d949462a6ca2876d560b', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(781, '255-11-01', 'FEDERICO', 'navarro', 'DELIZO', '', '639d4c3ac192f9225d692b44f2dcb36e', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(782, '592-09-16', 'ALFRED', 's', 'DOMA', '', '73406730cc1ee1d101b44fe034bbe821', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(783, '570-09-15', 'ABRAHAM', 'Bero', 'DUQUE', '', '21dc55b56cdefd38a4f9d637649552c8', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(784, '597-11-16', 'rony', 'Ismael', 'DUQUE', '', 'eb9c00b04125bb736ebd68c73ad26455', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(785, '347-06-10', 'NICANOR', 'Wenceslao', 'ESTRERA', '', '1b3127efb49ce837e61c639988b3c87c', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(786, '603-11-19', 'manuel', 'Cruz', 'FERNANDO', '', 'cc61e0a03807890ff0b13986142db528', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(787, '170-06-06', 'VIRGILIO', 'Serrano', 'FERRER', '', '8c5c8c94ff6c7d5a740e4d8613b5361a', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(788, '382-06-12', 'REYNALDO', 'Alvarez', 'FRANCISCO', '', '302d24a2442ef2aa14986a0fb39ca5c3', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(789, '506-11-14', 'ROMMEL', 'Paganagan', 'GARCIA', '', '7a8ecc53866086b705df86ce95e46063', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(790, '572-09-15', 'IRENEO', 'c', 'GERONIMO', '', '3cb141366e553ba96c053da8f1d794cd', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(791, '601-11-16', 'NEIL PATRICK', 'Ricardo', 'GERONIMO', '', '9da0bf9631a290a336740f9897b6f957', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(792, '201-01-97', 'edgardo', 'Ilumin', 'MANUCOT', '', '9aed97b42ac1a2b750223e8f3ab0154e', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(793, '423-06-13', 'evelyn', 'abalos', 'NAVALES', '', '69b624bd31cffa15f005552835811e19', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(794, '540-01-16', 'JEFFREY', 'd', 'NIRO', '', '57bc0e1da02ab56ab25d8c0dca9e07c1', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(795, '469-06-14', 'MICHEIL', 'l', 'NOLASCO', '', '86cb74cabc38539e08df324d10cf3d61', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(796, '392-11-14', 'arnold', 'p', 'nool', '', '7933f2b198484f6d1fe0bcf8845077d5', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(797, '542-06-15', 'celso flor', 'Cayabyab', 'PANTE', '', '75339a393bdab7b94421ace1381565b6', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(798, '596-11-16', 'joyce ann', 'del rosario', 'PARATON', '', '190bc1b82e039e38cf9c6d29bc0d65a0', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(799, '561-06-16', 'ALONA JANE', 'Cabamungan', 'PERALTA', '', '3e27a01168c64e819cba589c073026a8', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(800, '569-06-16', 'RONALD', 'Banzuela', 'polong', '', 'd8318841f98e3f0316585e3f8f0343e8', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(801, '547-06-15', 'ISIDRO ARVIN', 'Santiago', 'ROBERTO', '', '2389f6afa2ec3ccf89ea79a478a67156', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(802, '373-06-11', 'MANOLITO', 'Cayabyab', 'SOLIS', '', '0a6be311d9a98113bdc21a7a982a5234', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(803, '599-11-16', 'EDUARDO', 'Natividad', 'VERGARA', '', '64b12e7a05d848b0f52cebae5cee797f', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(804, '147-06-96', 'CRISPULO', 'Rabida', 'VILLAMAR', '', '4d8c532986fe06f25e53a99bf8c063f9', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(805, '467-06-14', 'JEROME', 'a', 'ABANILLA', '', 'a70338955b76ca72f490186b8e05d577', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(806, '164-11-94', 'RICHARD', 'sawit', 'ANTOLIN', '', 'fafb29037c4305d81e82158b33843ead', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(807, '340-06-09', 'ALEJANDRO', 'Gonzales', 'asana', '', '8ede873642b792c45d3284a92734cd8c', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(808, '567-06-16', 'CHANDRA-JHO', 'Lanuza', 'BALAONG', '', 'f1fd5b73447f58441f434300c0d864d0', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(809, '364-06-11', 'domingo', 'g', 'CABADING', '', '46756faa1e286682ed5bfce2f95e12ea', 0, 0, '', 0, 0, 0, 0, '', 'jr', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(810, '571-06-16', 'CRISTIAN', 'Cortez', 'CABALLERO', '', '0e27e9a0b918b66d53f9cb3d2bfc313a', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(811, '337-04-09', 'DANIEL', 'v', 'CAJUCOM', '', 'f3017d517e15d6a7f462d77626abec37', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(812, '513-01-15', 'LOUIE JANE', 'Agabin', 'CARBONEL', '', 'ea5df1127acaf16d2c838f72e9df1c2c', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(813, '557-08-15', 'JAYSON', 'Alvarez', 'CARINGAL', '', '052448c6897d542a7be9f6f0ac6f3fbb', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(814, '562-06-16', 'ROBIE MAR', 'Aquino', 'DAYTO', '', '4bb174c5277b94908925bdcc72692af1', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(815, '533-06-15', 'PRINCESS JOYCE', 'b', 'CALDERON', '', '3e201353d6ec003d672fcb889eb5e912', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(816, '554-06-15', 'ANJOE MICHAEL', 'm', 'MALLARI', '', '6f72869ab5cc32ded159b5b59fab28ca', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(817, '554-07-15', 'JESUS', 'b', 'DAYRIT', '', '9d4cd90db5ec04a01e9dd3b6143a451c', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(818, '577-06-16', 'JAN LOUIS VINCENT', 'f', 'MIRANDA', '', 'aed1503a40b8566fc52ef67c21319763', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(819, '516-04-15', 'richard', 'l', 'OANDASAN', '', 'b0c568a16b487a0ef06f641fa785aa5f', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(820, '318-11-06', 'citadel', 'b', 'PUNZAL', '', '212235074823855d694c42b4505e5415', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(821, '590-08-16', 'jessa amor', 'Guzman', 'necesito', '', '984a1df98f57208084bd64ca96a8a16c', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(822, '598-11-16', 'pauline kate', 'Fermin', 'catahan', '', 'e01904bdd188f83a1a6b91266598fc62', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(823, '602-11-16', 'venancio simon', 'Sobredilla', 'Sulit', '', 'b8604ad5eb8ff4c51b658b6b43a24d83', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(824, '564-06-16', 'BABY JANE', 'Mirasol', 'CONCEPCION', '', '12ba2a098463b92c6e477ddb1ee9fbe1', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(825, '405-08-14', 'Ariel', 'Canoza', 'Mag Isa', '', '6cd8d14bdca90d4e0f61d59df36aff0a', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(826, '376-11-11', 'Jaxpee', 'Lopez', 'Poblete', '', '7febaaa3e43ae3d82d9b38ac01d24c4e', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(827, '605-02-17', 'IAN CARLO', 'De Luna', 'VillANueva', '', '6a9ebb98925656bbd19b557c4fe1cff8', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(828, '578-07-16', 'RODRIGO', 'JULIAN', 'MARTIN', '', '36f8ff87aefbe9e55b3faca2ddb5f301', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(829, '389-06-12', 'NOWEL', 'URBANO', 'RAMOS', '', '40c562c57a36b43f5e03f6c59d2c666b', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(830, '608-05-17', 'daniel', 'f', 'aguilar', '', '5cfd397ee997d32b5d3c2051d1cca2d4', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(831, '609-05-17', 'gerald', 'dg', 'mabuti', '', '6fe2468eff12f1e11d521948a710406b', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(832, '610-05-17', 'joana mary', 'c', 'salazar', '', 'f21f382d0662ac6120e423ca9a7b6c06', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(833, '611-05-17A', 'janice', 'c', 'ignacio', '', '1c00153a55a4c23e036bbd72c32ea8ef', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(834, '612-05-17', 'esther angela', 't', 'romero', '', '1e47d4b2d17c2978ca260c7a417ed077', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(835, '613-05-17', 'alvin jay', 's', 'ramos', '', 'ac3648f9bf4afe3d55639049e3982ed2', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(836, '614-05-17', 'michelle joie', 'm', 'pascual', '', '352565818d90c35e606d1ee9e90386ec', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(837, '615-05-17', 'junior', 'm', 'pacol', '', '2228ae65efd386797f2b6de62529d883', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(838, '617-06-17', 'bryan jay', 'e', 'dela cruz', '', '8727afb4594f7a4408617baebf120a74', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(839, '618-06-17', 'james michael', 'a', 'dalde', '', 'fdb49981c227ce7c8e750356bbc4d4cf', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(840, '619-06-17', 'nicole', 'dg', 'pascua', '', 'c7690674ced60539b8987dd18c53bb11', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(841, '444-08-13', 'FRANZ RUSSEL', '', 'MURALLA', '', '00c97bbcdb53dacecaaf0e52859f520a', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(842, '574-06-16', 'GLYFORD', '', 'SELENDRE', '', 'c4aa2abc3283af147427abc101664859', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(843, '545-04-16', 'ricardo', '', 'maines', '', 'b8d533a03c926068bef5bc01b0bac5f3', 0, 0, '', 0, 0, 0, 0, '', 'jr', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(844, '620-06-17', 'jessie', 'd', 'pincil', '', '7a05de2e8b00da3b2d7a061a27d6def6', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(845, '616-05-17', 'michel jame', '', 'grospe', '', '89339fef7b7f66be5ee0d2871b694322', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(846, '579-07-16', 'winnie', '', 'matic', '', '9369cf549860eb50b105d46e1d6262c9', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(847, '455-11-13', 'sabas', '', 'mercado', '', 'edd2b0b1c0c8da468d68f76e99fc49ba', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(848, '550-08-15', 'kathleen kay', '', 'antonio', '', 'cead2a7f99cde74088264065d4252fb6', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(849, '565-08-15', 'ervhin Bryan', '', 'de luna', '', '5dfd7e8badcffabb66dba082f49b35ef', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(850, '552-05-16', 'bryan', '', 'dela cruz', '', '774043e6a1e4db38ced2c147905ef15e', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(851, '519-04-15', 'edmar', 'REYES', 'dizon', '', 'd3892a28efc07fe07f89c2c25b1a09ae', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(852, '543-03-16', 'lyn', '', 'gonzales', '', '348b019bec44ef5fa0eb8918405e3dba', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(853, '747-153', 'nstp', '', 'nrotc', '', '5dc8564a5ea8655073b328853520592a', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(854, '489-06-14', 'PHI OMEGA', '', 'MANUBAY', '', '6cb88e8b951eda9e7980d6d0290dcb80', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(855, '526-05-15', 'KEVIN CHRISTIAN', '', 'JUAN', '', '7fc12edb2446b01c842f0cb304174cf5', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(856, '622-08-17', 'chino conrado', 'manalastas', 'pingue', '', '4d166571e4910a3b90492542905c1f34', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(857, '548-06-15', 'ARIES', 'V', 'DAYAO', '', '3936206bca4713367e55b29dc62be216', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(858, '550-05-15', 'alexis', 's', 'villanueva', '', '88b24642c92a10b57aa584453f07d1d2', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(859, '623-11-17', 'c/e jerry', 'a', 'esperon', '', '21ae75b49cc7cecc33044458800b681b', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(860, '624-11-17', 'lexter', 's.', 'rodriguez', '', '55e8bed6d8b9956cf7f75fb710845664', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(861, '625-11-17', 'orlando', 'b', 'andrada', '', '907dfd5a2d406382b2d4e742400d7bb0', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(862, '626-11-17', 'kenneth', '', 'esguerra', '', '2739560c462090663c88f6ec021856d4', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(863, '627-11-17', 'john ephraim', '', 'anday', '', '3b2dde8ea27f472a56452b496e13614e', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(864, '628-11-17', 'christian', '', 'erana', '', 'bb2b564173d25f4b44ad813a568f46df', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(865, '558-06-16', 'eunice gem', '', 'deocares', '', '738049f6106ad978f874cf384f6bf8e0', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(866, '192-07-96', 'maricel', 's', 'esguerra', '', '1596c4030cdf25e3bc38d75904d47419', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(867, '591-08-16', 'sean tristan', 'd', 'sebastian', '', 'fe23db4889291d5bcfb0adb618b2cd25', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(868, '635-01-18', 'Ronolfo', 'a', 'almacen', '', '538c8d883a6fbaf32703fc26b867097a', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(869, '638-04-18', 'john carlou', 'a', 'rafael', '', '74419fd5db852ca2d015636fc5ab2071', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(870, '646-05-18', 'benjamin', 'c', 'neuda', '', 'fc50f15b7db43000040a773ea204e846', 0, 0, '', 0, 0, 0, 0, '', 'i', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(871, '647-05-18', 'jaycel', 'c', 'bautista', '', 'e7884b34785b37790dbfcac6321fcb65', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(872, '645-05-18', 'khaysielyn', 'r', 'nortez', '', 'd3fccf14cd609022c1d2432556e4bfac', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(873, '652-05-18', 'juniel son', 'a', 'tabilen', '', 'e565683d1f7650ec3edd2af1bf2a9d19', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(874, '649-05-18', 'meryll joyce', 'c', 'cabinian', '', 'd9385d2500f1f98ce03ff5f0ba08b83f', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(875, '644-05-18', 'jessiebel', 'c', 'perez', '', '2dcf826ae53436137e03f50c519a064a', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(876, '648-05-18', 'jessa may', 'v', 'santos', '', '657ae4beae133e64c7c30b6d05f58323', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(877, '650-05-18', 'rosanna', 'i', 'bueno', '', 'c279d0411c91d51b713ca48df3f577cd', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(878, '651-05-18', 'tala', 's', 'molina', '', '11f20ea325df4ae2b9ea1a79dad7b427', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(879, '643-05-18', 'edgar', 'p', 'aladeza', '', '08a151a34e39ffe7457092d0a5939d68', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(880, '393-11-14', '3m leoner', '', 'MAGHINANG', '', 'd3bfb932f601e8159fe7eda0c1884c1c', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(881, '660-07-18', 'Magnolia', 'c', 'Gaviola', '', '2788b0c0294c5b6e648a382954c72d7e', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(882, '654-06-18', 'Maverick Rovie', 's', 'Lagman', '', '827215e64dfa320514fb6bf8ae2f3e03', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(883, '655-06-18', 'John Elton', 'l', 'Mackay', '', '1d7eec164bd5b75db9c24943883dbf46', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(884, '657-06-18', 'OIC Aldrin', '', 'De Guzman', '', 'ca32a4540bdc3f9d8ea6918a3b712d50', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(885, '658-06-18', 'OIC Jose Romel', '', 'Dela Cruz', '', 'd6861372f25332732167a6d9d990ba33', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(886, '659-07-18', 'jose epimaco', 'r', 'arcega', '', '38db3f972af10bd10f38a1ee94036b62', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(887, '662-07-18', 'reymond', '', 'reyes', '', '0e64eef562d167cc850b17f5543b761a', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(888, '661-07-18', 'ronald', 'e', 'gordolan', '', '90837900f831a40ada863b2d953c13b2', 0, 0, '', 0, 0, 0, 0, '', 'jr', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(889, '663-07-18', 'Franchesca Dianne', 'L', 'Hernandez', '', 'e57746cf39de881b5ea205190da401ed', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(890, '665-08-18', 'john michael', 'gg', 'baruc', '', '2f8b87d13e27a5f180a0541ab0b9def5', 0, 0, '', 0, 0, 0, 0, '', 'jr', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(891, '670-09-18', 'ROSEDOM', '', 'BATANGAN', '', 'c2c9b0efddd6921a8c561b2e4789dd40', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(892, '669-08-18', 'arwine', '', 'matias', '', 'f7decf9b0fd06a17e6d92c9153887052', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(893, '675-10-18', 'JEFFERSON', 'C', 'ORTIZ', '', '9e12c9768c18d258ba62d147019e3848', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(894, '678-11-18', 'JADE CARLO', 'P', 'LAZO', '', '88fc17f687e6eb02cd377765f3c57abb', 0, 0, '', 0, 0, 0, 0, '', ' ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(895, '677-11-18', 'JOANNA', 'C', 'MANANGAN', '', 'e38264102bf31fa13675c7bf1a9e7595', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(896, '676-10-18', 'EDSON JAY', 'R', 'AQUINO', '', '2b9df52fe04c3854a218a9ac5c00794a', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(897, '682-12-18', 'prince ace', '', 'cutaran', '', '2be80b535a6c5f57c2f8b83598cd91fb', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(898, '0683-12-18', 'christian john', '', 'joyag', '', 'bbbbc8396c504b7d609833e78e39f929', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(899, '0684-12-18', 'jecson ryan', '', 'gallano', '', '5436f3d68881cd7782cdfddc81fca16c', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(900, '671-09-18', 'JAMIL RAVEN', 'ZERRUDO', 'DELA CRUZ', '', 'd97c13078639f8e0fcd5ea14da6d9381', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(901, '685-01-19', 'paul laurence', '', 'siwa', '', '321f9aa055dd9f4c18c17c9be4a945b0', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(902, '686-01-19', 'george', 'e', 'lentic', '', '565572386d78d53143f80c5aa1a8986a', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(903, '689-03-19', 'EDMUND', '', 'BAUTISTA', '', '22e022d4cfc4878c8f623fa3e22ce1fa', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(904, '688-02-19', 'FAMELA', '', 'AQUINO', '', '3ed33f30f2455768e500e3f585ee665c', 0, 0, '', 0, 0, 0, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(905, '446-546', 'Avelus', 'wala', 'wala', '', 'f6293ccc88c838d12848c24b60e36654', 0, 0, '', 0, 0, 0, 0, '', 'wala', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1'),
(906, '689-03-20', 'dsdsd', 'sdsds', 'sdsdsd', '', '54be7ab8387906d2946deee2d094e15d', 0, 0, '', 0, 0, 0, 0, '', 'sdsdsd', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1');

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

CREATE TABLE `vouchers` (
  `id` int(11) NOT NULL,
  `code` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `amount` varchar(15) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `year_levels`
--

CREATE TABLE `year_levels` (
  `id` int(11) NOT NULL,
  `in_number` varchar(50) NOT NULL,
  `in_word` varchar(50) NOT NULL,
  `in_roman` varchar(50) NOT NULL,
  `short_name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enrollment_student`
--
ALTER TABLE `enrollment_student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fees`
--
ALTER TABLE `fees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_structure`
--
ALTER TABLE `fee_structure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labs_subjects`
--
ALTER TABLE `labs_subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payco_receipts`
--
ALTER TABLE `payco_receipts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payco_transactions`
--
ALTER TABLE `payco_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `preregistration_documents_checklist`
--
ALTER TABLE `preregistration_documents_checklist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `preregistration_info`
--
ALTER TABLE `preregistration_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `preregistration_siblings`
--
ALTER TABLE `preregistration_siblings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `preregistration_supporting_info`
--
ALTER TABLE `preregistration_supporting_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schools`
--
ALTER TABLE `schools`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sickbay_doctors`
--
ALTER TABLE `sickbay_doctors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sickbay_inventory`
--
ALTER TABLE `sickbay_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sickbay_ishihara_plates`
--
ALTER TABLE `sickbay_ishihara_plates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sickbay_preregistration_transaction`
--
ALTER TABLE `sickbay_preregistration_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sickbay_transactions`
--
ALTER TABLE `sickbay_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `strands_courses`
--
ALTER TABLE `strands_courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_assigned_subjects`
--
ALTER TABLE `student_assigned_subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_back_accounts`
--
ALTER TABLE `student_back_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_info_fees`
--
ALTER TABLE `student_info_fees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_payment_transactions`
--
ALTER TABLE `student_payment_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_transcript`
--
ALTER TABLE `student_transcript`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_transferee_record`
--
ALTER TABLE `student_transferee_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers_encoding`
--
ALTER TABLE `teachers_encoding`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vouchers`
--
ALTER TABLE `vouchers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `year_levels`
--
ALTER TABLE `year_levels`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=197;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `enrollment_student`
--
ALTER TABLE `enrollment_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fees`
--
ALTER TABLE `fees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fee_structure`
--
ALTER TABLE `fee_structure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `labs_subjects`
--
ALTER TABLE `labs_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payco_receipts`
--
ALTER TABLE `payco_receipts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payco_transactions`
--
ALTER TABLE `payco_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `preregistration_documents_checklist`
--
ALTER TABLE `preregistration_documents_checklist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `preregistration_info`
--
ALTER TABLE `preregistration_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `preregistration_siblings`
--
ALTER TABLE `preregistration_siblings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `preregistration_supporting_info`
--
ALTER TABLE `preregistration_supporting_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `schools`
--
ALTER TABLE `schools`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=652;

--
-- AUTO_INCREMENT for table `sickbay_doctors`
--
ALTER TABLE `sickbay_doctors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sickbay_inventory`
--
ALTER TABLE `sickbay_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sickbay_ishihara_plates`
--
ALTER TABLE `sickbay_ishihara_plates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sickbay_preregistration_transaction`
--
ALTER TABLE `sickbay_preregistration_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sickbay_transactions`
--
ALTER TABLE `sickbay_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `strands_courses`
--
ALTER TABLE `strands_courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_assigned_subjects`
--
ALTER TABLE `student_assigned_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_back_accounts`
--
ALTER TABLE `student_back_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_info_fees`
--
ALTER TABLE `student_info_fees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_payment_transactions`
--
ALTER TABLE `student_payment_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_transcript`
--
ALTER TABLE `student_transcript`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_transferee_record`
--
ALTER TABLE `student_transferee_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;

--
-- AUTO_INCREMENT for table `teachers_encoding`
--
ALTER TABLE `teachers_encoding`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=907;

--
-- AUTO_INCREMENT for table `vouchers`
--
ALTER TABLE `vouchers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `year_levels`
--
ALTER TABLE `year_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
