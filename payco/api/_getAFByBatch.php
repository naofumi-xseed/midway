<?php

include "../../_config/db.php";
$batch_id = $_GET['id'];


$output = array();  
$query = $db->query("SELECT b.name as batch_name,pf.description as fee_name,
batch_details.created_at as payment_date,soc.code,preg.last_name,preg.first_name,preg.middle_name,preg.student_number,es.year_level_id,es.strand_id,preg.birthday 
FROM `batch_details` 
INNER JOIN enrollment_student AS es on es.id = batch_details.enrollment_id
INNER JOIN preregistration_info as preg on preg.id = es.student_id
INNER JOIN strands_courses as soc on soc.id = es.strand_id
INNER JOIN `batches` as b on b.id = batch_details.batch_id
INNER JOIN payco_fees as pf on pf.id = fee_id
WHERE batch_id = $batch_id
");  
 
if($query->num_rows < 1){
     $output[] = ["student_number" => "no results found"];
}else{
  while($row = $query->fetch_assoc())  
     {  
         $output[]= $row;  
     }  
}

$json  = json_encode($output, JSON_INVALID_UTF8_IGNORE). PHP_EOL . PHP_EOL;

     echo $json;
 

?>