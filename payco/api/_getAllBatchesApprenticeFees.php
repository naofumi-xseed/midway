<?php

include "../../_config/db.php";

$output = array();  
$query = $db->query("SELECT fee_id,pf.description FROM `batches` 
LEFT OUTER JOIN payco_fees as pf
on pf.id = batches.fee_id
GROUP BY fee_id
");  
 
if($query->num_rows < 1){
     $output[] = "no results found";
}else{
  while($row = $query->fetch_assoc())  
     {  
         $output[]= $row;  
     }  
}
   
     
$json  = json_encode($output, JSON_INVALID_UTF8_IGNORE). PHP_EOL . PHP_EOL;

     echo $json;
 

?>