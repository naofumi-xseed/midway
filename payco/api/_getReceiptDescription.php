<?php

include "../../_config/db.php";

$output = array();  
$query = $db->query("SELECT pt.cash,pt.subtotal,pt.cash_change,pt.transaction_details,
pt.receipt_no,pt.is_posted,pt.is_voided,s.description,pu.name as cashier,prei.student_number,prei.first_name,
prei.last_name,prei.middle_name,pt.created_at FROM `payco_transactions` as pt
INNER JOIN enrollment_student as es 
on es.id = pt.enrollment_student_id
INNER JOIN preregistration_info as prei
on prei.id = es.student_id
INNER JOIN settings as s 
on s.id = pt.setting_id
INNER JOIN payco_receipts as pr
on pr.receipt_no = pt.receipt_no
INNER JOIN payco_users as pu
on pu.id = pr.payco_user_id
WHERE pt.id = '{$_GET['id']}'");  
 

$json  = json_encode($query->fetch_assoc(), JSON_INVALID_UTF8_IGNORE). PHP_EOL . PHP_EOL;

     echo $json;
 

?>