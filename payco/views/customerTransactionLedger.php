<div class="e-container pt-3">

    <h3 class="poppins d-block" style="font-family: 'Poppins',
          sans-serif !important;">
          
      <?php
      $payload = [];
      $id = $_GET['id'];
     @$q1 = $db->query("SELECT yl.in_word,soc.code,first_name,last_name,middle_name FROM `preregistration_info` 
      INNER JOIN enrollment_student as es ON es.student_id = preregistration_info.id 
      INNER JOIN strands_courses as soc ON soc.id = preregistration_info.strand_course 
      INNER JOIN year_levels as yl on es. year_level_id = yl.id
      WHERE preregistration_info.student_number = $id");

      if($q1->num_rows > 0){
            while($res = $q1->fetch_assoc()){
                  $payload = $res;
            }
      }
            
      // echo $payload->last_name . ', ' . $payload[0].first_name . ' ' . $payload[0].middle_name ;
      echo $payload['last_name']. ', ' . $payload['first_name'] . ' ' . $payload['middle_name'];
      ?>
          Transaction Ledger <span class="e-tag dark static"><?php echo $payload['code']; ?></span>
          <span class="e-tag text-dark static"><?php echo $payload['in_word']; ?></span>
      </h3>

        <nav class="e-tabs">
              <?php
              echo '<ul ng-init="getConsumedSettings('. $_GET['id'] .')">';
              ?>
                    <li ng-repeat='setting in consumedSettings'
                    ng-init="getTransactionBySetting(consumedSettings[0].setting_id,consumedSettings[0].enrollment_id)"
                     ng-class="(buttonClicked == setting.setting_id)? 'active':''" 
                     ng-click="getTransactionBySetting(setting.setting_id,setting.enrollment_id)"><a href="#">{{setting.description}}</a></li>
              </ul>
        </nav>

        <table class="e-table bordered striped hovered" >
              <thead>
                    <tr>
                          <th>OR #</th>
                          <th>Date</th>
                          <th>Cashier</th>
                          <th>is_voided</th>
                          <th>Clearing</th>
                          <th>Cash Received</th>
                          <th>Change</th>
                          <th>Amount Paid</th>
                    </tr>
              </thead>
              <tbody>
                    <tr ng-repeat='transaction in transactions'>
                          <th><a href="" onclick="showReceiptDetails()" ng-click="getReceiptDetails(transaction.id)"  class="d-rel emp">{{transaction.receipt_no}}</a></th>
                          <td>{{transaction.created_at}}</td>
                          <td>{{transaction.name}}</td>
                          <td ng-bind="(transaction.is_voided == 1) ? 'voided': 'not voided'"></td>
                          <td ng-bind="(transaction.is_posted == 1) ? 'posted': 'not posted'"></td>
                          <td>Php. {{transaction.cash | number:2}}</td>
                          <td>Php. {{transaction.cash_change | number:2}}</td>
                          <td>Php. {{transaction.subtotal | number:2}}</td>
                    </tr>
                    <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td style="background:#5e72e4;" class="text-white">Total:</td>
                          <td style="background:#5e72e4;" class="text-white">Php. {{total | number:2}}</td>
                    </tr>
              </tbody>
        </table>

</div>


