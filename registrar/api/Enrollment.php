<?php

require "pdo_db.php";

class EnrollmentRecords {

    protected $con;
    public    $result;

        public function __construct($db){
            $this->con = $db;
        }
        public function select(){

            $output = array();

            $o = $this->con->prepare("SELECT s.description,es.year_level_id,es.setting_id 
                                        FROM enrollment_student as es
                                        LEFT OUTER JOIN settings as s ON es.setting_id = s.id
                                        WHERE student_id = :student
                                    ");

            $o->execute([':student'=> $_GET['student']]);

            $subjects = $this->con->prepare("SELECT ss.*,s.name,s.code 
                                                FROM student_subjects as ss
                                                LEFT OUTER JOIN subjects as s ON s.id=ss.subject_id
                                                WHERE ss.student_id=:student && ss.setting_id = :setting
                                           ");

            while($r=$o->fetch()){

                //result
              

                $counter = count($output);

                $output[$counter] = array(
                                            'description' => $r['description'],
                                            'subjects'    =>array()
                                         );
            
                $subjects->execute(array(
                                            'student'  => $_GET['student'],
                                            ':setting' => $r['setting_id']
                                   ));

                    while($rs=$subjects->fetch()){

                        $output[$counter]['subjects'][] = array(
                                                                'code'  => $rs['code'],
                                                                'name'  => $rs['name'],
                                                                'term1' => $rs['term1'],
                                                                'term2' => $rs['term2'],
                                                                'term3' => $rs['term3'],
                                                                'term4' => $rs['term4'],
                                                                'final' => $rs['final']
                                                              );
                    }

            }
            echo json_encode($output);
        }

        public function history(){

            $output = array();

            $o = $this->con->prepare("SELECT id,short_name FROM year_levels");
            $o->execute();

            $o2 = $this->con->prepare("SELECT st.name as strand,s.description,es.student_id,es.year_level_id,es.setting_id,es.strand_id,es.created_at as enroll_date 
                                        FROM enrollment_student as es
                                        LEFT OUTER JOIN settings as s ON es.setting_id = s.id
                                        LEFT OUTER JOIN strands_courses as st ON es.strand_id = st.id
                                        WHERE es.student_id = :student AND es.year_level_id = :year
                                    ");

            


            while($r=$o->fetch()){
                
                $counter = count($output);

               
                $output[$counter] = array(
                    'short_name'   => $r['short_name'],
                    'transactions' => array()
                );
              
               $o2->execute(array(
                                    ':student'=> $_GET['student'],
                                    ':year'   => $r['id']
                                ));

                while($rs=$o2->fetch()){

                   

                    
                    $output[$counter]['transactions'][] = array(
                        'strand'      => $rs['strand'],
                        'description' => $rs['description'],
                        'enroll_date' => $rs['enroll_date']
                    );
                    
                
                }
            }

            echo json_encode($output);
        }

}

$data = new EnrollmentRecords($db);

if(isset($_GET['e_history'])){
    $data->select();
}
if(isset($_GET['e_tracker'])){
    $data->history();
}

