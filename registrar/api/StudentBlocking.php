<?php
require "pdo_db.php";

class Blockings{

    private $con;
  

        public function __construct($db){
            $this->con = $db;
        }

        public function select()
        {

                $output = array();
                $acads = $this->con->prepare("SELECT id,first_name,last_name,middle_name,student_number FROM preregistration_info");
                $acads->execute();
                                
                while($result = $acads->fetch()){

                     $counter = count($output);

                     $output[$counter] = array(
                        'id'             => $result['id'],
                        'full_name'      => $result['last_name'].", ".$result['first_name']." ".$result['middle_name'],
                        'student_number' => $result['student_number'],
                        'blocking'       => array()

                     );


                     $un = $this->con->prepare("SELECT DISTINCT remarks,is_active from student_blocking WHERE student_id='{$result['id']}' && department_id=8 && is_active=1 group by student_id");
                     $un->execute();

                     while($rs= $un->fetch()){

                        $output[$counter]['blocking'] = array(
                            'remarks'     => $rs['remarks'],
                            'is_active'  => $rs['is_active']
                        );

                     }


                }
                echo json_encode($output);

               
        }
        public function unblock(){

            $up = $this->con->prepare("UPDATE student_blocking SET is_active=0 WHERE student_id='{$_GET['id']}' && department_id=8");
            $up->execute();
            
        }
        public function block($i,$r){


            $in = $this->con->prepare("INSERT into student_blocking 
            (student_id,department_id,user_id,remarks,is_active) 
            VALUES
            (
              '$i',
               8,
               0,
              '$r',
              1

            )
            ");
            $in->execute();


        }

        


}

$data = new Blockings($db);
$dat = json_decode(file_get_contents("php://input"));

if(isset($_GET['find'])){
    $data->select();
}
if(isset($_GET['block'])){
    $data->unblock();
}
if($dat){
    $data->block($dat->id,$dat->remark);
}
