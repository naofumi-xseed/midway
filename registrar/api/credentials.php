<?php
require "pdo_db.php";


class Credential{

    private $con;
        public function __construct($db){
            $this->con = $db; 
        }
        public function create($id,$table){
            
            if($table =='student_training'){
                $tr = $this->con->prepare("INSERT INTO {$table} (`on_the_job_training`, `basic_training`, `engine_watchkeeping`, `deck_watchkeeping`, `sdsd`, `practicum_certificate`, `student_id`, `ship_familiarization`) VALUES ('0', '0', '0', '0', '0', '0', '$id', '0')");
                $tr->execute();
            }
            else{
                $cr = $this->con->prepare("INSERT INTO {$table} (`form_138`, `form_137a`, `certificate_of_junior_high_school_completion`, `good_moral_character_certificate`, `birth_certificate`, `honorable_dismissal`, `certificate_of_grades`, `transcript_of_records`, `qualified_voucher_certificate`, `affidavit_of_discrepancy`, `affidavit_of_lost_form_138`, `als_certificate`, `student_id`, `picture_with_name_tag`, `education_service_contracting`) VALUES ('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '$id', '0', '0')");
                $cr->execute();
            }
        }
        public function select($id,$table){

            $output = array();
            $c    = 0;
            $cred = $this->con->prepare("SELECT * FROM {$table} where student_id = :id");
            $cred->execute([':id' => $id]);

            if($cred->rowCount()<1){
                $this->create($id,$table);
            }
            
            $fields = $this->con->prepare("DESCRIBE {$table}");
            $fields->execute();

            $result = $fields->fetchAll(PDO::FETCH_COLUMN);
            $vals = $cred->fetch();

            foreach($result as $r){
                $c++;
                if($r=='id' || $r=='created_at' || $r=='updated_at' || $r=='student_id'){  
                }
                else{
                    $output[] = array(
                        'id'    =>$c,
                        'field' =>$r,
                        'value' =>$vals[$r]                        
                    );
                 }
            }
            
            echo json_encode($output);
            
        }
        public function update($f,$v,$s,$t){
           
            @$value = $v === 0 ? 1 : 0;
            
            $up = $this->con->prepare("UPDATE {$t} SET {$f} = :v WHERE student_id= :s");
            $up->execute(array(
                ':v' => $value,
                ':s' => $s
            ));
        }
}
$data = new Credential($db);
$dat = json_decode(file_get_contents("php://input"));
if(isset($_GET['fetch'])){
    $data->select($_GET['fetch'],$_GET['table']);
}
if($dat){ 
   $data->update($dat->field,$dat->val,$dat->sid,$dat->tbl);
}
else{
    exit();
}