<?php
class Database{

     private $host = 'localhost';
     private $user = 'root';
     private $pass = '';
     private $dbname   = 'midway_db7';
     private $charset = 'utf8mb4';

  
      private $db;
      
  
  
      public function initConnection(){
  

  
          try {
              $this->db = new mysqli($this->host, $this->user, $this->pass, $this->dbname);
              $this->db->set_charset($this->charset);
             
         } catch (Exception $e) {
              error_log($e->getMessage());
              exit('Database Error');
         }
  
         return $this->db;
  
  
       
      }
  
  
  }
  
  $connect = new Database();
  $db      = $connect->initConnection();
  
