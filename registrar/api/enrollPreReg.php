<?php
include "../../_config/db.php";
$date = new DateTime('now', new DateTimeZone('Asia/Manila'));
$datenow = $date->format('Y-m-d H:i:s');


  @$program                       = $_POST['program'];
  @$setting                       = $_POST['setting'];
  @$first_name                    = $_POST['first_name'];
  @$middle_name                   = $_POST['middle_name'];
  @$last_name                     = $_POST['last_name'];
  @$suffix_name                   = $_POST['suffix_name'];
  @$completeAddress               = $_POST['completeAddress'];
  @$cAddress_Region               = $_POST['cAddress_Region'];
  @$cAddress_Province             = $_POST['cAddress_Province'];
  @$cAddress_CityMunc             = $_POST['cAddress_CityMunc'];
  @$cAddress_Barangay             = $_POST['cAddress_Barangay'];
  @$cAddress_Info                 = $_POST['cAddress_Info'];
  @$email                         = $_POST['email'];
  @$contact                       = $_POST['contact'];
  @$birthdate                     = $_POST['birthdate'];
  @$age                           = $_POST['age'];
  @$sex                           = $_POST['sex'];
  @$PlaceOfBirth                  = $_POST['PlaceOfBirth'];
  @$pAddress_Province             = $_POST['pAddress_Province'];
  @$pAddress_CityMunc             = $_POST['pAddress_CityMunc'];
  @$religion                      = $_POST['religion'];
  @$mother_tongue                 = $_POST['mother_tongue'];
  @$civilStatus                   = $_POST['civilStatus'];
  @$fathers_name                  = $_POST['fathers_name'];
  @$fathers_occupation            = $_POST['fathers_occupation'];
  @$mothers_name                  = $_POST['mothers_name'];
  @$mothers_occupation            = $_POST['mothers_occupation'];
  @$parent_guardian               = $_POST['parent_guardian'];
  @$relationship                  = $_POST['relationship'];
  @$p_guardian_contact            = $_POST['p_guardian_contact'];
  @$LRN                           = $_POST['LRN'];
  @$academicInfo                  = $_POST['academicInfo'];
  @$SHS_elem_school_name          = $_POST['SHS_elem_school_name'];
  @$SHS_elem_school_comp          = $_POST['SHS_elem_school_comp'];
  @$SHS_elem_school_add           = $_POST['SHS_elem_school_add'];
  @$SHS_elem_school_region        = $_POST['SHS_elem_school_region'];
  @$SHS_elem_school_prov          = $_POST['SHS_elem_school_prov'];
  @$SHS_elem_school_citymunc      = $_POST['SHS_elem_school_citymunc'];
  @$SHS_jhs_school_name           = $_POST['SHS_jhs_school_name'];
  @$SHS_jhs_school_comp           = $_POST['SHS_jhs_school_comp'];
  @$SHS_jhs_school_add            = $_POST['SHS_jhs_school_add'];
  @$SHS_jhs_school_region         = $_POST['SHS_jhs_school_region'];
  @$SHS_jhs_school_prov           = $_POST['SHS_jhs_school_prov'];
  @$SHS_jhs_school_citymunc       = $_POST['SHS_jhs_school_citymunc'];
  @$college_elem_school_name      = $_POST['college_elem_school_name'];
  @$college_elem_school_comp      = $_POST['college_elem_school_comp'];
  @$college_elem_school_add       = $_POST['college_elem_school_add'];
  @$college_elem_school_region    = $_POST['college_elem_school_region'];
  @$college_elem_school_prov      = $_POST['college_elem_school_prov'];
  @$college_elem_school_citymunc  = $_POST['college_elem_school_citymunc'];
  @$HSInfo                        = $_POST['HSInfo'];
  @$college_hs_school_name        = $_POST['college_hs_school_name'];
  @$college_hs_school_comp        = $_POST['college_hs_school_comp'];
  @$college_hs_school_add         = $_POST['college_hs_school_add'];
  @$college_hs_school_region      = $_POST['college_hs_school_region'];
  @$college_hs_school_prov        = $_POST['college_hs_school_prov'];
  @$college_hs_school_citymunc    = $_POST['college_hs_school_citymunc'];
  @$college_shs_school_name       = $_POST['college_shs_school_name'];
  @$college_shs_school_comp       = $_POST['college_shs_school_comp'];
  @$college_shs_school_add        = $_POST['college_shs_school_add'];
  @$college_shs_school_region     = $_POST['college_shs_school_region'];
  @$college_shs_school_prov       = $_POST['college_shs_school_prov'];
  @$college_shs_school_citymunc   = $_POST['college_shs_school_citymunc'];
  @$transCollege_school_name      = $_POST['transCollege_school_name'];
  @$transCollege_school_add       = $_POST['transCollege_school_add'];
  @$transCollege_school_region    = $_POST['transCollege_school_region'];
  @$transCollege_school_prov      = $_POST['transCollege_school_prov'];
  @$transCollege_school_citymunc  = $_POST['transCollege_school_citymunc'];
  @$transSHS_elem_school_name     = $_POST['transSHS_elem_school_name'];
  @$transSHS_elem_school_comp     = $_POST['transSHS_elem_school_comp'];
  @$transSHS_elem_school_add      = $_POST['transSHS_elem_school_add'];
  @$transSHS_elem_school_region   = $_POST['transSHS_elem_school_region'];
  @$transSHS_elem_school_prov     = $_POST['transSHS_elem_school_prov'];
  @$transSHS_elem_school_citymunc = $_POST['transSHS_elem_school_citymunc'];
  @$TransSHS_jhs_school_name      = $_POST['TransSHS_jhs_school_name'];
  @$TransSHS_jhs_school_comp      = $_POST['TransSHS_jhs_school_comp'];
  @$TransSHS_jhs_school_add       = $_POST['TransSHS_jhs_school_add'];
  @$TransSHS_jhs_school_region    = $_POST['TransSHS_jhs_school_region'];
  @$TransSHS_jhs_school_prov      = $_POST['TransSHS_jhs_school_prov'];
  @$TransSHS_jhs_school_citymunc  = $_POST['TransSHS_jhs_school_citymunc'];
  @$track                         = $_POST['track'];
  @$strand                        = $_POST['strand'];
  @$student_id                    = $_POST['student_id'];
  @$year_level                    = $_POST['year_level'];



  $query_prereg_info            = "UPDATE `preregistration_info` SET `first_name`='$first_name',`middle_name`= '$middle_name',`last_name`= '$last_name',`suffix`= '$suffix_name',`reference_no`= '$LRN',`email`='$email',`contact_no`='$contact',`sex`='$sex',`birthday`='$birthdate',`complete_address`='$completeAddress',`birth_place`='$PlaceOfBirth',`region`='$cAddress_Region',`province`='$cAddress_Province',`city`='$cAddress_CityMunc',`barangay`='$cAddress_Barangay',`street_info`='$cAddress_Info',`track_id`='$track',`strand_course`='$strand',`program_id`='$program',`setting_id`='$setting',`enrollment_status`='ASSESSED',`updated_at`='$datenow' WHERE id = '$student_id'";
  $result_prereg_info           = $db->query($query_prereg_info);
  $query_prereg_supporting_info ="UPDATE `preregistration_supporting_info` SET `mother_tongue`='$mother_tongue',`civil_status`='$civilStatus',`fathers_name`='$fathers_name',`father_occupation`='$fathers_occupation',`mothers_name`='$mothers_name',`mothers_occupation`='$mothers_occupation',`guardian_name`='$parent_guardian',`guardian_contact`='$p_guardian_contact',`guardian_relationship`='$relationship',`religion`='$religion',`updated_at`='$$datenow' WHERE student_id = '$student_id'";
  $result_prereg_supporting_info=$db->query($query_prereg_supporting_info);

if($academicInfo == "SHS"){
  $query_prereg_academic_info1  = "UPDATE `preregistration_academic_info` SET `school_name`='$SHS_elem_school_name',`date_of_completion`='$SHS_elem_school_comp',`school_address`='$SHS_elem_school_add',`school_region`='$SHS_elem_school_region',`school_province`='$SHS_elem_school_prov',`school_city`='$SHS_elem_school_citymunc',`updated_at`='$datenow' WHERE student_id = '$student_id' AND `acad_type`='$academicInfo' AND `edu_stage`='Elem'";
  $result_prereg_academic_info1 = $db->query($query_prereg_academic_info1);
  $query_prereg_academic_info2  = "UPDATE `preregistration_academic_info` SET `school_name`='$SHS_jhs_school_name',`date_of_completion`='$SHS_jhs_school_comp',`school_address`='$SHS_jhs_school_add',`school_region`='$SHS_jhs_school_region',`school_province`='$SHS_jhs_school_prov',`school_city`='$SHS_jhs_school_citymunc',`updated_at`='$datenow' WHERE student_id = '$student_id' AND `acad_type`='$academicInfo' AND `edu_stage`='JHS'";
  $result_prereg_academic_info2 = $db->query($query_prereg_academic_info2);
}

else if($academicInfo == "College" && $HSInfo=="SHS"){
  $query_prereg_academic_info1  = "UPDATE `preregistration_academic_info` SET `school_name`='$college_elem_school_name',`date_of_completion`='$college_elem_school_comp',`school_address`='$college_elem_school_add',`school_region`='$college_elem_school_region',`school_province`='$college_elem_school_prov',`school_city`='$college_elem_school_citymunc',`updated_at`='$datenow' WHERE student_id = '$student_id' AND `acad_type`='$academicInfo' AND `edu_stage`='Elem'";
  $result_prereg_academic_info1 = $db->query($query_prereg_academic_info1);
  $query_prereg_academic_info2  = "UPDATE `preregistration_academic_info` SET `school_name`='$college_shs_school_name',`date_of_completion`='$college_shs_school_comp',`school_address`='$college_shs_school_add',`school_region`='$college_shs_school_region',`school_province`='$college_shs_school_prov',`school_city`='$college_shs_school_citymunc',`updated_at`='$datenow' WHERE student_id = '$student_id' AND `acad_type`='$academicInfo' AND `edu_stage`='SHS'";
  $result_prereg_academic_info2 = $db->query($query_prereg_academic_info2);
}

else if($academicInfo == "College" && $HSInfo=="HS"){
  $query_prereg_academic_info1  = "UPDATE `preregistration_academic_info` SET `school_name`='$college_elem_school_name',`date_of_completion`='$college_elem_school_comp',`school_address`='$college_elem_school_add',`school_region`='$college_elem_school_region',`school_province`='$college_elem_school_prov',`school_city`='$college_elem_school_citymunc',`updated_at`='$datenow' WHERE student_id = '$student_id' AND `acad_type`='$academicInfo' AND `edu_stage`='Elem'";
  $result_prereg_academic_info1 = $db->query($query_prereg_academic_info1);
  $query_prereg_academic_info2  = "UPDATE `preregistration_academic_info` SET `school_name`='$college_hs_school_name',`date_of_completion`='$college_hs_school_comp',`school_address`='$college_hs_school_add',`school_region`='$college_hs_school_region',`school_province`='$college_hs_school_prov',`school_city`='$college_hs_school_citymunc',`updated_at`='$datenow' WHERE student_id = '$student_id' AND `acad_type`='$academicInfo' AND `edu_stage`='HS'";
  $result_prereg_academic_info2 = $db->query($query_prereg_academic_info2);
}

else if($academicInfo == "TransfereeCollege"){
  $query_prereg_academic_info1  = "UPDATE `preregistration_academic_info` SET `school_name`='$transCollege_school_name',`date_of_completion`='$transCollege_school_comp',`school_address`='$transCollege_school_add',`school_region`='$transCollege_school_region',`school_province`='$transCollege_school_prov',`school_city`='$transCollege_school_citymunc',`updated_at`='$datenow' WHERE student_id = '$student_id' AND `acad_type`='$academicInfo' AND `edu_stage`='College'";
  $result_prereg_academic_info1 =  $db->query($query_prereg_academic_info1);
}
else if($academicInfo == "TransfereeSHS"){
  $query_prereg_academic_info1  = "UPDATE `preregistration_academic_info` SET `school_name`='$transSHS_elem_school_name',`date_of_completion`='$transSHS_elem_school_comp',`school_address`='$transSHS_elem_school_add',`school_region`='$transSHS_elem_school_region',`school_province`='$transSHS_elem_school_prov',`school_city`='$transSHS_elem_school_citymunc',`updated_at`='$datenow' WHERE student_id = '$student_id' AND `acad_type`='$academicInfo' AND `edu_stage`='Elem'";
  $result_prereg_academic_info1 = $db->query($query_prereg_academic_info1);
  $query_prereg_academic_info2  = "UPDATE `preregistration_academic_info` SET `school_name`='$TransSHS_jhs_school_name',`date_of_completion`='$TransSHS_jhs_school_comp',`school_address`='$TransSHS_jhs_school_add',`school_region`='$TransSHS_jhs_school_region',`school_province`='$TransSHS_jhs_school_prov',`school_city`='$TransSHS_jhs_school_citymunc',`updated_at`='$datenow' WHERE student_id = '$student_id' AND `acad_type`='$academicInfo' AND `edu_stage`='JHS'";
  $result_prereg_academic_info2 = $db->query($query_prereg_academic_info2);
}
  


$select_curricula               = $db->query("SELECT * FROM curricula WHERE strand_id='$strand' && is_default=1");
$select_term                    = $db->query("SELECT * FROM settings WHERE  id='$setting'");
$term                           = $select_term->fetch_assoc()['term_id'];
$curricula                      = $select_curricula->fetch_assoc()['id'];


$set_data     = $db->query("SELECT id,setting_id,student_id FROM enrollment_student WHERE setting_id ='$setting' && student_id='$student_id'");


$counter = $set_data->num_rows;

if($counter>0){
echo"Already enrolled";
}
else{
$query_enrollment_student       = "INSERT INTO `enrollment_student`(`student_id`, `year_level_id`, `fees_summary`, `strand_id`, `setting_id`, `created_at`) VALUES ('$student_id','$year_level','','$strand','$setting','$datenow')";
$result_enrollment_student      = $db->query($query_enrollment_student);

    $db->query("INSERT INTO student_curricula
                  (
                      student_id,
                      curricula_id,
                      active
                  )
                  VALUES
                  (
                      '$student_id',
                      '$curricula',
                      '1'
                  )
              ");
}





?>
