<?php

require "pdo_db.php";

class Subjects{

    private $con;

    public function __construct($db){
        $this->con=$db;
    }
    
    public function select()  
    {  
        $schedules = array();
    
        $sd = $this->con->prepare("SELECT sd.*,s.*,s.id as subj_id, ss.schedule_detail_id as sched_id
                                            FROM schedule_details as sd
                                            INNER JOIN subjects as s ON sd.subject_id = s.id
                                            INNER JOIN student_subjects as ss ON ss.schedule_detail_id = sd.id
                                            WHERE ss.setting_id='{$_GET['setting']}' && ss.student_id='{$_GET['sid']}'
                        ");
         $sd->execute();               
        
                while($out_sd = $sd->fetch()){
        
                    $sd_index = count($schedules);
                    $schedules[$sd_index] = array(
             
                        'id'         => $out_sd['sched_id'],
                        'subject_id' => $out_sd['subj_id'],
                        'code'       => $out_sd['code'],
                        'name'       => $out_sd['name'],
                        'hours'      => $out_sd['weeks']*$out_sd['lecture_hours'],
                        'units'      => $out_sd['units'],
                        'schedule_subject' => array()
                    );
        
                        $ssd = $this->con->query("SELECT d.short_name,t.first_name,t.last_name,sd.*,r.name as room,b.name as building
                                                          FROM schedule_subject_details as sd
                                                          LEFT JOIN days as d on d.id = sd.day_id
                                                          INNER JOIN teachers as t on sd.teacher_id = t.id
                                                          INNER JOIN rooms as r on sd.room_id=r.id
                                                          INNER JOIN buildings as b on r.building_id=b.id
                                                          WHERE sd.schedule_detail_id ='{$out_sd['sched_id']}'
                                   ");
                         $ssd->execute();

                                   while($out_ssd = $ssd->fetch()){
                                    
                                    $schedules[$sd_index]['schedule_subject'][] = array(
        
                                            'day'        => $out_ssd['short_name'],
                                            'start_time' => $out_ssd['start_time'],
                                            'end_time'   => $out_ssd['end_time'],
                                            'room'       => $out_ssd['room'],
                                            'building'   => $out_ssd['building'],
                                            'teacher'    => $out_ssd['first_name'].$out_ssd['last_name']
                                    );
        
        
                                   }
        
                }
        
            
        echo json_encode($schedules);
    }  

    public function insert($table_name, $data)  
    {  
         
           $stmt  =$this->con->prepare("INSERT INTO {$table_name} (code,name) VALUES (:code,:name)");           
           
           

           $stmt->execute(array( 
               ':name' => $data['name'], 
               ':code' => $data['code']
           ));
         
           
    }


    public function update($table_name, $data){

    $up =$this->con->prepare("UPDATE {$table_name} SET  name='{$data['name']}', code ='{$data['code']}' WHERE id='{$data['id']}'");
    $up->execute();
    mysqli_query($this->con,$up);

    

    }

    public function remove()
    {
        $del = $this->con->prepare("DELETE FROM student_subjects WHERE student_id='{$_GET['sid']}' && schedule_detail_id ='{$_GET['remove']}' && setting_id='{$_GET['setting']}'");
        $del->execute();
    }

    public function removeall()
    {
        $del = $this->con->prepare("DELETE FROM student_subjects WHERE student_id='{$_GET['sid']}' && setting_id='{$_GET['setting']}'");
        $del->execute();

        echo $_GET['setting'];
        
    }
    
    

}
    

$data= new Subjects($db);
$dat = json_decode(file_get_contents("php://input"));

@$typ = $dat->type;


if(isset($_GET['fetch_enrollment'])){
$data->select();
}


if(isset($_GET['remove'])){
$data->remove();
}

if(isset($_GET['removeall'])){
    $data->removeall();
}

if(isset($_GET['fetch_vouchers'])){
    $data->select('fee_voucher',$_GET['fetch_vouchers']);
    }

if(isset($_GET['fetch_course'])){
$data->select('courses');    
}

else if($typ=='Add School'){
    $name = $dat->name;
    $code = $dat->code;
    $insert_data = array( 
                            'name'     => $name,
                            'code'     => $code
                        );

$data->insert('schools',$insert_data);

}

else if($typ=='Add Course'){
    $name = $dat->name;
    $code = $dat->code;
    $insert_data = array( 
                            'name'     => $name,
                            'code'     => $code
                        );

$data->insert('courses',$insert_data);
}

else if($typ=='Edit School'){
    $name = $dat->name;
    $id   = $dat->id;
    $code = $dat->code;

    $insert_data = array( 
                            'id'       =>$id,
                            'name'     =>$name,
                            'code'     =>$code
                        );

$data->update('schools',$insert_data);

}

else if($typ=='Edit Course'){
    $name = $dat->name;
    $id   = $dat->id;
    $code = $dat->code;

    $insert_data = array( 
                            'id'       =>$id,
                            'name'     =>$name,
                            'code'     =>$code
                        );

$data->update('courses',$insert_data);

}

?>