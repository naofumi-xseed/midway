<?php
$program = $_POST['program'];
$setting = $_POST['setting'];
$first_name = $_POST['first_name'];
$middle_name = $_POST['middle_name'];
$last_name = $_POST['last_name'];
$suffix_name = $_POST['suffix_name'];
$completeAddress = $_POST['completeAddress'];
$cAddress_Region = $_POST['cAddress_Region'];
$cAddress_Province = $_POST['cAddress_Province'];
$cAddress_CityMunc = $_POST['cAddress_CityMunc'];
$cAddress_Barangay = $_POST['cAddress_Barangay'];
$cAddress_Info = $_POST['cAddress_Info'];
$email = $_POST['email'];
$contact = $_POST['contact'];
$birthdate = $_POST['birthdate'];
$age = $_POST['age'];
$sex = $_POST['sex'];
$PlaceOfBirth = $_POST['PlaceOfBirth'];
$pAddress_Province = $_POST['pAddress_Province'];
$pAddress_CityMunc = $_POST['pAddress_CityMunc'];
$religion = $_POST['religion'];
$mother_tongue = $_POST['mother_tongue'];
$civilStatus = $_POST['civilStatus'];
$fathers_name = $_POST['fathers_name'];
$fathers_occupation = $_POST['fathers_occupation'];
$mothers_name = $_POST['mothers_name'];
$mothers_occupation = $_POST['mothers_occupation'];
$parent_guardian = $_POST['parent_guardian'];
$relationship = $_POST['relationship'];
$p_guardian_contact = $_POST['p_guardian_contact'];
$LRN = $_POST['LRN'];
$academicInfo = $_POST['academicInfo'];
$SHS_elem_school_name = $_POST['SHS_elem_school_name'];
$SHS_elem_school_comp = $_POST['SHS_elem_school_comp'];
$SHS_elem_school_add = $_POST['SHS_elem_school_add'];
$SHS_elem_school_region = $_POST['SHS_elem_school_region'];
$SHS_elem_school_prov = $_POST['SHS_elem_school_prov'];
$SHS_elem_school_citymunc = $_POST['SHS_elem_school_citymunc'];
$SHS_jhs_school_name = $_POST['SHS_jhs_school_name'];
$SHS_jhs_school_comp = $_POST['SHS_jhs_school_comp'];
$SHS_jhs_school_add = $_POST['SHS_jhs_school_add'];
$SHS_jhs_school_region = $_POST['SHS_jhs_school_region'];
$SHS_jhs_school_prov = $_POST['SHS_jhs_school_prov'];
$SHS_jhs_school_citymunc = $_POST['SHS_jhs_school_citymunc'];
$college_elem_school_name = $_POST['college_elem_school_name'];
$college_elem_school_comp = $_POST['college_elem_school_comp'];
$college_elem_school_add = $_POST['college_elem_school_add'];
$college_elem_school_region = $_POST['college_elem_school_region'];
$college_elem_school_prov = $_POST['college_elem_school_prov'];
$college_elem_school_citymunc = $_POST['college_elem_school_citymunc'];
$HSInfo = $_POST['HSInfo'];
$college_hs_school_name = $_POST['college_hs_school_name'];
$college_hs_school_comp = $_POST['college_hs_school_comp'];
$college_hs_school_add = $_POST['college_hs_school_add'];
$college_hs_school_region = $_POST['college_hs_school_region'];
$college_hs_school_prov = $_POST['college_hs_school_prov'];
$college_hs_school_citymunc = $_POST['college_hs_school_citymunc'];
$college_shs_school_name = $_POST['college_shs_school_name'];
$college_shs_school_comp = $_POST['college_shs_school_comp'];
$college_shs_school_add = $_POST['college_shs_school_add'];
$college_shs_school_region = $_POST['college_shs_school_region'];
$college_shs_school_prov = $_POST['college_shs_school_prov'];
$college_shs_school_citymunc = $_POST['college_shs_school_citymunc'];
$transCollege_school_name = $_POST['transCollege_school_name'];
$transCollege_school_add = $_POST['transCollege_school_add'];
$transCollege_school_region = $_POST['transCollege_school_region'];
$transCollege_school_prov = $_POST['transCollege_school_prov'];
$transCollege_school_citymunc = $_POST['transCollege_school_citymunc'];
$transSHS_elem_school_name = $_POST['transSHS_elem_school_name'];
$transSHS_elem_school_comp = $_POST['transSHS_elem_school_comp'];
$transSHS_elem_school_add = $_POST['transSHS_elem_school_add'];
$transSHS_elem_school_region = $_POST['transSHS_elem_school_region'];
$transSHS_elem_school_prov = $_POST['transSHS_elem_school_prov'];
$transSHS_elem_school_citymunc = $_POST['transSHS_elem_school_citymunc'];
$TransSHS_jhs_school_name = $_POST['TransSHS_jhs_school_name'];
$TransSHS_jhs_school_comp = $_POST['TransSHS_jhs_school_comp'];
$TransSHS_jhs_school_add = $_POST['TransSHS_jhs_school_add'];
$TransSHS_jhs_school_region = $_POST['TransSHS_jhs_school_region'];
$TransSHS_jhs_school_prov = $_POST['TransSHS_jhs_school_prov'];
$TransSHS_jhs_school_citymunc = $_POST['TransSHS_jhs_school_citymunc'];
$track = $_POST['track'];
$strand = $_POST['strand'];

include "../../_config/db.php";
$date = new DateTime('now', new DateTimeZone('Asia/Manila'));
$datenow = $date->format('Y-m-d H:i:s');
// number generator student
$year_2digs=date('y');
$year_4digs=date('Y');

if($program == 2){
$latest = "SELECT student_number FROM preregistration_info WHERE student_number NOT LIKE '%747-%' order by id DESC limit 0,1";

$college_con =mysqli_query($db,$latest);
$college_no = mysqli_fetch_array($college_con);
$college_number = $college_no['student_number'];
$koleds = substr($college_number,2,6);
$added= $koleds + 1;
$endValue = str_pad($added, 5, '0', STR_PAD_LEFT);
$student_number= $year_2digs.$endValue;
}
else if($program == 1){
$sql = "SELECT student_number FROM preregistration_info WHERE student_number LIKE '%747-%' order by id DESC limit 0,1";
$result = $db ->query($sql);
$shs_no = $result->fetch_assoc()['student_number'];
$koleds = substr($shs_no,8,10);
$added= $koleds + 1;
$endValue = str_pad($added, 3, '0', STR_PAD_LEFT);
$student_number="747-".$year_4digs.$endValue;
}
$student_password = md5($student_number);

$AIData = "SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'midway_db7' AND TABLE_NAME = 'preregistration_info'";
$resultAI = $db ->query($AIData);
$AutoIncrement = $resultAI->fetch_assoc()['AUTO_INCREMENT'];

if($academicInfo == "TransfereeCollege" || $academicInfo == "TransfereeSHS" ){
  $query_prereg_info="INSERT INTO `preregistration_info`(`first_name`, `middle_name`, `last_name`, `suffix`, `email`, `contact_no`, `sex`, `birthday`, `complete_address`, `birth_place`, `region`, `province`, `city`, `barangay`, `street_info`, `track_id`, `strand_course`, `student_number`, `password`, `program_id`, `setting_id`, `is_transferee`, `enrollment_status`, `created_at`) VALUES ('$first_name','$middle_name','$last_name','$suffix_name','$email','$contact','$sex','$birthdate', '$completeAddress','$PlaceOfBirth','$cAddress_Region','$cAddress_Province','$cAddress_CityMunc','$cAddress_Barangay','$cAddress_Info','$track','$strand','$student_number','$student_password','$program','$setting','1','PREREG','$datenow')";

}else{
  $query_prereg_info="INSERT INTO `preregistration_info`(`first_name`, `middle_name`, `last_name`, `suffix`, `email`, `contact_no`, `sex`, `birthday`, `complete_address`, `birth_place`, `region`, `province`, `city`, `barangay`, `street_info`, `track_id`, `strand_course`, `student_number`, `password`, `program_id`, `setting_id`, `is_transferee`, `enrollment_status`, `created_at`) VALUES  ('$first_name','$middle_name','$last_name','$suffix_name','$email','$contact','$sex','$birthdate', '$completeAddress','$PlaceOfBirth','$cAddress_Region','$cAddress_Province','$cAddress_CityMunc','$cAddress_Barangay','$cAddress_Info','$track','$strand','$student_number','$student_password','$program','$setting','0','PREREG','$datenow')";
}
$result_prereg_info=$db->query($query_prereg_info);

$query_prereg_supporting_info="INSERT INTO `preregistration_supporting_info`(`student_id`, `mother_tongue`, `civil_status`, `fathers_name`, `father_occupation`, `mothers_name`, `mothers_occupation`, `guardian_name`, `guardian_contact`, `guardian_relationship`, `religion`, `created_at`) VALUES ('$AutoIncrement','$mother_tongue','$civilStatus','$fathers_name','$fathers_occupation','$mothers_name','$mothers_occupation','$parent_guardian','$p_guardian_contact','$relationship','$religion','$datenow')";
$result_prereg_supporting_info=$db->query($query_prereg_supporting_info);

if($academicInfo == "SHS"){
$query_prereg_academic_info1="INSERT INTO `preregistration_academic_info`(`student_id`,`acad_type`,`edu_stage`, `school_name`, `date_of_completion`, `school_address`, `school_region`, `school_province`, `school_city`, `created_at`) VALUES ('$AutoIncrement', '$academicInfo','Elem','$SHS_elem_school_name','$SHS_elem_school_comp','$SHS_elem_school_add','$SHS_elem_school_region','$SHS_elem_school_prov','$SHS_elem_school_citymunc','$datenow')";
$result_prereg_academic_info1=$db->query($query_prereg_academic_info1);

$query_prereg_academic_info2="INSERT INTO `preregistration_academic_info`(`student_id`,`acad_type`,`edu_stage`, `school_name`, `date_of_completion`, `school_address`, `school_region`, `school_province`, `school_city`, `created_at`) VALUES ('$AutoIncrement','$academicInfo','JHS','$SHS_jhs_school_name','$SHS_jhs_school_comp','$SHS_jhs_school_add','$SHS_jhs_school_region','$SHS_jhs_school_prov','$SHS_jhs_school_citymunc','$datenow')";
$result_prereg_academic_info2=$db->query($query_prereg_academic_info2);
}

else if($academicInfo == "College" && $HSInfo=="SHS"){
$query_prereg_academic_info1="INSERT INTO `preregistration_academic_info`(`student_id`,`acad_type`,`edu_stage`, `school_name`, `date_of_completion`, `school_address`, `school_region`, `school_province`, `school_city`, `created_at`) VALUES ('$AutoIncrement','$academicInfo','Elem','$college_elem_school_name','$college_elem_school_comp','$college_elem_school_add','$college_elem_school_region','$college_elem_school_prov','$college_elem_school_citymunc','$datenow')";
$result_prereg_academic_info1=$db->query($query_prereg_academic_info1);

$query_prereg_academic_info2="INSERT INTO `preregistration_academic_info`(`student_id`,`acad_type`,`edu_stage`, `school_name`, `date_of_completion`, `school_address`, `school_region`, `school_province`, `school_city`, `created_at`) VALUES ('$AutoIncrement','$academicInfo','SHS','$college_shs_school_name','$college_shs_school_comp','$college_shs_school_add','$college_shs_school_region','$college_shs_school_prov','$college_shs_school_citymunc','$datenow')";
$result_prereg_academic_info2=$db->query($query_prereg_academic_info2);
}

else if($academicInfo == "College" && $HSInfo=="HS"){
$query_prereg_academic_info1="INSERT INTO `preregistration_academic_info`(`student_id`,`acad_type`,`edu_stage`, `school_name`, `date_of_completion`, `school_address`, `school_region`, `school_province`, `school_city`, `created_at`) VALUES ('$AutoIncrement','$academicInfo','Elem','$college_elem_school_name','$college_elem_school_comp','$college_elem_school_add','$college_elem_school_region','$college_elem_school_prov','$college_elem_school_citymunc','$datenow')";
$result_prereg_academic_info1=$db->query($query_prereg_academic_info1);

$query_prereg_academic_info2="INSERT INTO `preregistration_academic_info`(`student_id`,`acad_type`,`edu_stage`, `school_name`, `date_of_completion`, `school_address`, `school_region`, `school_province`, `school_city`, `created_at`) VALUES ('$AutoIncrement','$academicInfo','HS','$college_hs_school_name','$college_hs_school_comp','$college_hs_school_add','$college_hs_school_region','$college_hs_school_prov','$college_hs_school_citymunc','$datenow')";
$result_prereg_academic_info2=$db->query($query_prereg_academic_info2);
}

else if($academicInfo == "TransfereeCollege"){
$query_prereg_academic_info1="INSERT INTO `preregistration_academic_info`(`student_id`,`acad_type`,`edu_stage`, `school_name`, `school_address`, `school_region`, `school_province`, `school_city`, `created_at`) VALUES ('$AutoIncrement','$academicInfo','College','$transCollege_school_name','$transCollege_school_add','$transCollege_school_region','$transCollege_school_prov','$transCollege_school_citymunc','$datenow')";
$result_prereg_academic_info1=$db->query($query_prereg_academic_info1);
}
else if($academicInfo == "TransfereeSHS"){
  $query_prereg_academic_info1="INSERT INTO `preregistration_academic_info`(`student_id`,`acad_type`,`edu_stage`, `school_name`, `date_of_completion`, `school_address`, `school_region`, `school_province`, `school_city`, `created_at`) VALUES ('$AutoIncrement','$academicInfo','Elem','$transSHS_elem_school_name','$transSHS_elem_school_comp','$transSHS_elem_school_add','$transSHS_elem_school_region','$transSHS_elem_school_prov','$transSHS_elem_school_citymunc','$datenow')";
  $result_prereg_academic_info1=$db->query($query_prereg_academic_info1);

  $query_prereg_academic_info2="INSERT INTO `preregistration_academic_info`(`student_id`,`acad_type`,`edu_stage`, `school_name`, `date_of_completion`, `school_address`, `school_region`, `school_province`, `school_city`, `created_at`) VALUES ('$AutoIncrement','$academicInfo','JHS','$TransSHS_jhs_school_name','$TransSHS_jhs_school_comp','$TransSHS_jhs_school_add','$TransSHS_jhs_school_region','$TransSHS_jhs_school_prov','$TransSHS_jhs_school_citymunc','$datenow')";
  $result_prereg_academic_info2=$db->query($query_prereg_academic_info2);
}

echo
"done!";
 ?>
