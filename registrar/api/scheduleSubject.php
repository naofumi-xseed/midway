<?php

require "pdo_db.php";


    class Schedule{

      private $con;
      public $dat;
     
        public function __construct($db)
        {
          $this->con = $db;
        
        } 
        public function dataphp()
        {
        $this->dat =  json_decode(file_get_contents("php://input"));
        }
        public function select()
        {
          $output =array();
          $sections = $this->con->prepare("SELECT DISTINCT 
      
                            schedules.*,schedules.id as sched_id,
                            schedules.name as sec_name,schedule_details.*,
                            schedule_details.id as sch_id,subjects.*,
                            subjects.name as sub_d,
                            subjects.code as sub_c,
                            t.*,t.id as t_id,department_strands.*,departments.*,sc.*,sc.name as str_name, sc.id as str_id,yl.*, yl.id as yl_id
    
                            FROM schedules
    
                            INNER JOIN schedule_details ON schedules.id=schedule_details.schedule_id
                            INNER JOIN subjects ON subjects.id=schedule_details.subject_id
                            LEFT JOIN teachers as t ON schedules.teacher_id=t.id
                            INNER JOIN  department_strands ON schedules.strand_id=department_strands.strand_id
                            INNER JOIN  departments ON departments.id=department_strands.department_id
                            INNER JOIN strands_courses as sc ON schedules.strand_id = sc.id
                            INNER JOIN year_levels as yl ON yl.id = schedules.year_level_id
                            WHERE schedules.setting_id='{$_GET['sid']}'  && department_strands.department_id='{$_GET['did']}'
                            group by sec_name
                              ");
            $sections->execute();
      
            while($result=$sections->fetch()){
                $output[]=$result;
            }
    
            echo json_encode($output, JSON_INVALID_UTF8_IGNORE). PHP_EOL . PHP_EOL;  
        }

        public function edit()
        {
         //update script here
          $teacher = $this->dat->teacher;
          $name    = $this->dat->name;
          $sched   = $this->dat->sched_id;

          $up = $this->con->prepare("UPDATE schedules SET teacher_id='$teacher',name='$name' WHERE id='$sched'");
          $up->execute();

          
        }
        


    }

    $data = new Schedule($db);
    $data->dataphp();

    if(isset($_GET['fetch'])){
      $data->select();
    }
    if(isset($_GET['update'])){
      $data->edit();
    }


?>