<?php
include "../../_config/db.php";
$date    = new DateTime('now', new DateTimeZone('Asia/Manila'));
$datenow = $date->format('Y-m-d H:i:s');
//tuitions
//for college
$sqlUnits       = $db->query("SELECT SUM(s.units) as sum_units FROM `student_subjects` as ss
                              LEFT OUTER JOIN subjects as s
                              ON ss.subject_id = s.id 
                              WHERE ss.setting_id='{$_GET['s']}' AND ss.student_id='{$_GET['student']}'
                            ");
//for shs
$sqlLecHours    = $db->query("SELECT SUM(s.lecture_hours * s.weeks) as sum_hours FROM `student_subjects` as ss
                              LEFT OUTER JOIN subjects as s
                              ON ss.subject_id = s.id 
                              WHERE ss.setting_id='{$_GET['s']}' AND ss.student_id='{$_GET['student']}'
                           ");
                           
$sqlLABTotal    = $db->query("SELECT DISTINCT 
                              f.subject_amount as sumlab
                              FROM student_subjects as ss
                              LEFT OUTER JOIN fee_subjects as f 
                              ON ss.subject_id = f.subject_id
                              WHERE f.strand_id = '{$_GET['st']}' AND f.setting_id = '{$_GET['s']}'
                            ");

//enrollment ID
$set_data     = $db->query("SELECT id,setting_id,fees_summary FROM enrollment_student WHERE setting_id ='{$_GET['s']}' && student_id='{$_GET['student']}'");
$enrollment   = $set_data->fetch_assoc();
$rr = json_decode($enrollment['fees_summary']);
//experimental splitter

                            
$pluser= 0;while($lab = $sqlLABTotal->fetch_assoc()['sumlab']){$totlab =  $pluser + $lab;}

if($_GET['y'] == 1 || $_GET['y'] == 2){
  $TotalLecHours  = 0;
  $TotalLecHours  = $sqlLecHours->fetch_assoc()['sum_hours'];
  $TotalTuition   = $TotalLecHours;
}else{
  $TotalUnits     = 0;
  $TotalUnits     = $sqlUnits->fetch_assoc()['sum_units'];
  $TotalTuition   = $TotalUnits;
}
//tuitions

$payment_types = array();

$Total =0;$back  = 0; $adder =0; $discount=0;  $v_discount=0;

$p_type = $db->query("SELECT * FROM fee_type");

while($pt_data = $p_type->fetch_assoc()){

             $pt_index = count($payment_types);
                $payment_types[$pt_index] = array(
                                                   'pt_id' => $pt_data['id'],
                                                   'name'  => $pt_data['name'],
                                                   'fees'  => array(),
                                                   'tot'   => array()
                                                 );

                        $p_amounts = $db->query("SELECT f.id as f_id ,f.name as f_name,f.fee_type_id as ft_id,
                                                   fs.*
                                                   FROM 
                                                   fee_structure as fs 
                                                   INNER JOIN fees as f ON f.id = fs.fee_id
                                                   WHERE f.fee_type_id='{$pt_data['id']}' && fs.strand_id='{$_GET['st']}' && fs.year_level_id ='{$_GET['y']}' && fs.setting_id='{$_GET['s']}'");

              while($fees = $p_amounts->fetch_assoc()){
               
               if($fees['f_name']== 'tuition fee'){

                        $Total = $fees['fee_amount']*$TotalTuition;
                        $tuition = $Total;
                        $payment_types[$pt_index]['tot'] =  number_format($rr[0]->tuition_fee);
               }
               
               else if($pt_data['name'] == 'MISCELLANEOUS FEE'){

                        $Total ='';
                        $adder = $adder+$fees['fee_amount'];
                        $payment_types[$pt_index]['tot'] =  number_format($rr[0]->misc_fee,2);
               }
               
               else if($pt_data['name'] == 'BACK ACCOUNT'){

                        $Total ='';
                        $back = $back+$fees['fee_amount'];
                        $payment_types[$pt_index]['tot'] =  number_format($back,2);
               }
               
               else{
                  $Total ='';
               }
                 
                 $payment_types[$pt_index]['fees'][] = array(
                                                               'fee_name' => $fees['f_name'],
                                                               'amount'   => $fees['fee_amount'],
                                                               'total'    => $Total
                                                             );
                                                            
                                                             
             }

            
           
}




$payment_types[$pt_index]['fees'][] = array(
   'fee_name' => 'Laboratory',
   'amount'   => $totlab,
   'total'    => ''
 );



 $payment_types[$pt_index]['fees'][] = array(
   'fee_name' => 'Subtotal',
   'amount'   => $sub=$totlab+$adder+$tuition,
   'total'    => ''
 );

 if(isset($_GET['am'])){
 $ds = $db->query("SELECT * from discounts where id='{$_GET['am']}'");

      $res_ds = $ds->fetch_assoc();

      if($res_ds['fee_amount_type'] == 2){
        $discount = $sub*$res_ds['amount'];
      }
      else{
        $discount = $res_ds['amount'];
      }




}


foreach($rr[0]->discount as $fd => $key){

  $payment_types[$pt_index]['fees'][] = array(
    'fee_name' => $fd['id'],
    'amount'   => $fd['amount'],
    'total'    => ''
  );



}






if(isset($_GET['voucher'])){
  $ds = $db->query("SELECT fv.*, v.* from fee_voucher as fv
                    INNER JOIN vouchers as v ON fv.voucher_id = v.id 
                    WHERE fv.id='{$_GET['voucher']}'");
 
  $res_ds = $ds->fetch_assoc();
 
    $v_discount = $res_ds['voucher_amount'];
 
   $payment_types[$pt_index]['fees'][] = array(
     'fee_name' => $res_ds['code'],
     'amount'   => $v_discount,
     'total'    => ''
   );
 }


 $payment_types[$pt_index]['fees'][] = array(
   'fee_name' => 'Total',
   'amount'   => $back+$sub-$discount-$v_discount,
   'total'    => ''
 );




if(isset($_GET['voucher'])){
  $rr = json_decode($enrollment['fees_summary']);


  
  $sumtotal                     = $tuition+$adder+$totlab-$v_discount;

  
  $payload[]                    = array('total_tuition_fee'          =>  $sumtotal,
                                        'tuition_fee'                =>  $rr[0]->tuition_fee,
                                        'misc_fee'                   =>  $rr[0]->misc_fee,
                                        'lab_fee'                    =>  $rr[0]->lab_fee,
                                        'discount'                   =>  $rr[0]->discount,
                                        'voucher_discount'           =>  $v_discount,
                                        'tuition_summary'            =>  $rr[0]->tuition_summary,
                                        'tuition_payment_timestamps' =>  $rr[0]->tuition_payment_timestamps
                                       );

}


else if(isset($_GET['am'])){
  $rr = json_decode($enrollment['fees_summary']);

  $sumtotal                     = $tuition+$adder+$totlab-$discount;


  $ds = array(
        'id'      => $_GET['am'],
        'amount'  => $discount
  );
 
  array_push($rr[0]->discount, $ds);

  
  
  $payload[]                    = array('total_tuition_fee'          =>  $sumtotal,
                                        'tuition_fee'                =>  $rr[0]->tuition_fee,
                                        'misc_fee'                   =>  $rr[0]->misc_fee,
                                        'lab_fee'                    =>  $rr[0]->lab_fee,
                                        'discount'                   =>  $rr[0]->discount,
                                        'voucher_discount'           =>  $rr[0]->voucher_discount,
                                        'tuition_summary'            =>  $rr[0]->tuition_summary,
                                        'tuition_payment_timestamps' =>  $rr[0]->tuition_payment_timestamps
                                       );


                                  $db->query("INSERT INTO student_discounts 
                                                            (
                                                            enrollment_student_id,
                                                            discount_id
                                                            )

                                                            VALUES
                                                            (
                                                              '{$enrollment['id']}',
                                                              '{$_GET['am']}'
                                                            )
                                          ");                            
}
else{
$sumtotal                     = $tuition+$adder+$totlab;
$payload[]                    = array(  'total_tuition_fee'          => $sumtotal,
                                        'tuition_fee'                => $tuition,
                                        'misc_fee'                   => $adder,
                                        'lab_fee'                    => $totlab,
                                        'discount'                   => array(),
                                        'voucher_discount'           => array(),
                                        'tuition_summary'            => [[$tuition],[0],[0]],
                                        'tuition_payment_timestamps' => [$datenow]
                                       );
}

$fees_summary                 = json_encode($payload);










//$rs = json_encode($rr);









$db->query("UPDATE enrollment_student SET fees_summary ='$fees_summary' WHERE id='{$enrollment['id']}'");
echo json_encode($payment_types);

?>