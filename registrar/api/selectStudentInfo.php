<?php
require 'pdo_db.php';
class Student{

 private $con;
 public  $dat;
    
    public function __construct($db)
    {
        $this->con = $db;  
    }
    public function select()
    {
        $out = array();
        $s = $this->con->prepare("SELECT p.*,psi.* , sc.*,sc.name as strand_name, p.strand_course as strand_id
                                    FROM preregistration_info as p
                                    INNER JOIN preregistration_supporting_info as psi ON psi.student_id = p.id
                                    INNER JOIN strands_courses as sc ON sc.id = p.strand_course
                                    WHERE p.id = :id
        "); 

        $s->execute([':id'=>$_GET['id']]);

        $r = $s->fetch();
        echo json_encode($r);
    }
    public function upload()
    {

    }
    public function edit($params)
    {
            foreach($params as $p){
                
                        $mail    = $p['email'];
                        $phone   = $p['phone'];
                        $rel     = $p['religion'];
                        $bday    = $p['bday'];
                        $sex     = $p['sex'];
                        $fname   = $p['fname'];
                        $mname   = $p['mname'];
                        $lname   = $p['lname'];
                        $cstatus = $p['cstatus'];
                        $id      = $p['id'];
            }
            $editing = $this->con->prepare("UPDATE preregistration_info as p
            INNER JOIN preregistration_supporting_info as  psi ON p.id = psi.student_id
             SET 

                p.email='$mail',
                p.contact_no='$phone',
                p.birthday='$bday',
                p.sex='$sex',
                p.first_name ='$fname',
                p.middle_name ='$mname',
                p.last_name ='$lname',
                psi.civil_status='$cstatus',
                psi.religion ='$rel'
                
                WHERE p.id = '$id'
            ");
            
            $editing->execute();

            if($editing->rowCount()>0){
                echo "updated";
            }
    }
}
    $data = new Student($db);
    $dat = json_decode(file_get_contents("php://input"));
    if(isset($_GET['id']))
    {
        $data->select();
    }
    if($dat){
        $par[] = array(
            'email' => $dat->email,
            'phone' => $dat->phone,
            'religion' =>$dat->religion,
            'bday' =>$dat->bday,
            'sex' =>$dat->sex,
            'fname'=>$dat->fname,
            'mname'=>$dat->mname,
            'lname'=>$dat->lname,
            'cstatus'=>$dat->cstatus,
            'id'    => $dat->id
        );
    $data->edit($par);
}

