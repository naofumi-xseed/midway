<?php
require 'pdo_db.php';


class Student{

 private $con;
    
    public function __construct($db)
    {
        $this->con = $db;  
    } 
    public function select()
    {
        $out = array();
        $s = $this->con->prepare("SELECT p.*,psi.* , sc.*
                                    FROM preregistration_info as p
                                    INNER JOIN preregistration_supporting_info as psi ON psi.student_id = p.id
                                    INNER JOIN strands_courses as sc ON sc.id = p.strand_course
                                    WHERE p.id = :id
        "); 

        $s->execute([':id'=>$_GET['id']]);

            while($r = $s->fetch())
            {
                $out[] = $r;
            }
            echo json_encode($out);
    }

    

}

$data = new Student($db);

if(isset($_GET['id']))
{
    $data->select();
}


