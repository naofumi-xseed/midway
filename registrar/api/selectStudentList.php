<?php
include "pdo_db.php";

class Student{

     private $con;

          public function __construct($db){
               $this->con = $db;
          }
          public function select(){

               $output = array();  


               if(empty($_GET['year_level_id'])){
                    $year_level_id = '%';
               }else{
                    $year_level_id = $_GET['year_level_id'];     
               }
               if(empty($_GET['setting_id'])){
                    $setting_id = '%';
               }else{
                    $setting_id = $_GET['setting_id'];     
               }

               if(empty($_GET['strand_id'])){
                    $strand_id = '%';
               }else{
                    $strand_id = $_GET['strand_id'];     
               }

               $out = array();
               $students = $this->con->query("SELECT DISTINCT yl.short_name,preg.id as stud_id, yl.id as yl_id, soc.id as st_id,
                                        preg.student_number,preg.first_name,preg.middle_name,preg.last_name,preg.enrollment_status,soc.name,es.setting_id FROM enrollment_student as es 
                                        INNER JOIN year_levels as yl
                                        on yl.id = es.year_level_id
                                        INNER JOIN preregistration_info as preg
                                        on preg.id = es.student_id
                                        INNER JOIN strands_courses as soc
                                        on soc.id = es.strand_id
                                        INNER JOIN settings as s
                                        on s.id = es.setting_id
                                        AND soc.id LIKE '$strand_id'
                                        AND yl.id LIKE '$year_level_id'
                                        AND s.id LIKE '$setting_id'
                                        AND preg.enrollment_status LIKE '%{$_GET['enrollment_status']}%'
                                        GROUP BY es.student_id                     
               ");

               while($result=$students->fetch()){

                    $out[] = $result;

               }
               echo json_encode($out,JSON_INVALID_UTF8_IGNORE). PHP_EOL . PHP_EOL;
          }
          public function withdraw(){

          }
}
$data = new Student($db);
$data->select();

 
?>  
