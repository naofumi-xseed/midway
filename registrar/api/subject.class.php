<?php
require "pdo_db.php";


class Subjects{

    private $con;
    public function __construct($db){
        $this->con=$db;

    }
    
    public function select()  
    {  
         $objects = array();
         $sub = $this->con->prepare("SELECT cd.*, s.*,cd.subject_id as sub_id from curriculum_details as cd
                                     INNER JOIN subjects as s ON cd.subject_id = s.id
                                     WHERE cd.curricula_id='{$_GET['cur']}' 
                                    ");

        //year level id not needed && cd.year_level_id<3 
         $sub->execute();
        
         while($result = $sub->fetch()){

            $counter = count($objects);

            $objects[$counter] = array(
                'sub_id'           =>$result['sub_id'],
                'subject'      =>$result['code'],
                'title'        =>$result['name'],
                'section'      =>array()
            );
            
            $sd=$this->con->prepare("SELECT s.*,s.name as sched_name,sd.*, sd.id as did,s.id as scid FROM schedule_details as sd
                                    INNER JOIN schedules as s ON sd.schedule_id = s.id
                                    WHERE s.setting_id='{$_GET['s']}' && s.strand_id='{$_GET['str']}' && sd.subject_id='{$result['sub_id']}'
                                   ");
            $sd->execute();
            //not needed s.program_id =1 && 
            while($sdr = $sd->fetch()){

                $counter2 = count($objects[$counter]['section']);

                $objects[$counter]['section'][]= array(
                    'id'           => $sdr['scid'],
                    'section_name' => $sdr['sched_name'],
                    'sched'        => array() 
                );

                $sds=$this->con->prepare("SELECT ssd.*,t.*,d.* FROM schedule_subject_details as ssd
                                          INNER JOIN teachers as t ON t.id = ssd.teacher_id
                                          INNER JOIN days as d ON ssd.day_id = d.id 
                                          WHERE ssd.schedule_detail_id='{$sdr['did']}'");
                                          $sds->execute();

                while($sdsr = $sds->fetch()){

                    $objects[$counter]['section'][$counter2]['sched'][] = $sdsr;
                }



            }

            

         }
         

         echo json_encode($objects,JSON_INVALID_UTF8_IGNORE). PHP_EOL . PHP_EOL;  
    }  

    public function insert($table_name, $data)  
    {  
         
           $stmt  =$this->con->prepare("INSERT INTO {$table_name} (code,name) VALUES (:code,:name)");           
           
           

           $stmt->execute(array( 
               ':name' => $data['name'], 
               ':code' => $data['code']
           ));
         
           
    }


    public function update($table_name, $data){

    $up =$this->con->prepare("UPDATE {$table_name} SET  name='{$data['name']}', code ='{$data['code']}' WHERE id='{$data['id']}'");
    $up->execute();
    mysqli_query($this->con,$up);

    

    }
    
}
    

$data= new Subjects($db);
$dat = json_decode(file_get_contents("php://input"));

@$typ = $dat->type;


if(isset($_GET['fetch_subjects'])){
    $data->select();
}

if(isset($_GET['fetch_vouchers'])){
    $data->select('fee_voucher');
    }

if(isset($_GET['fetch_course'])){
    $data->select('courses');    
}



?>