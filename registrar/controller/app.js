

var regApp = angular.module('regApp', ['angularUtils.directives.dirPagination','checklist-model']);


    regApp.controller('mainController', function($scope, $http) {
             //$scope.program=false;
       // $scope.message = 'Everyone come and see how good I look!';
        //$scope.period={};
            $scope.clickedSh=0;
            $scope.clickedCl=0;
//buttons in teachers controller
            $scope.mode='';
            $scope.action='';
//encoding
            $scope.tabnow='';
            $scope.sched_id='';

            $scope.popup   = angular.element("#grades");
            $scope.popup1  = angular.element("#modals");
            $scope.emodal  = angular.element("#add");
            

            $scope.success = angular.element("#finish");

           //student list
            $scope.yr='';
            $scope.stran='';
            $scope.status='';    

            $scope.btn='';
            $scope.mark='';
            
//encoding
        $scope.clickMeSh= function() {
           
            $scope.clickedSh = 1;
            $scope.clickedCl = 0;
            $scope.program=1;
            $scope.getSetting();

                 
           };
           $scope.clickMeCl= function() {
  
            $scope.clickedCl = 1;
            $scope.clickedSh = 0;
            $scope.program=2;
             $scope.getSetting();
           };

           $scope.edit = function(enc,id,from,to){

                  $scope.popup1.addClass('launch');
      
                        $scope.id = id;  
                        $scope.from = from;  
                        $scope.to = to;  
           };

           $scope.cancel = function(){
             var popup = angular.element("#edit");
             popup.modal('hide');
       
            //enc.editable=false;
           };
      //ito ay dun sa summary per place controller
        $scope.data = {};

        $scope.changeInput = function(val){
          $scope.data.tes = val;

        }
         //ito ay dun sa summary per place controller -end
        //dito magsisimula ang json data ng mga lugar  registration controller

        $scope.getReligion = function(){
          
          $http.get('../_public/json/refreligion.json').then(function(resp) {
          $scope.religions = resp.data;
          $scope.sreligion();
        });
      };

      $scope.sreligion = function(){
   
        $scope.belief = $scope.religions.find(rel => rel.id == $scope.students.religion);

      };
      
      $scope.getCivilStatus = function(){
          $http.get('../_public/json/refcivilstatus.json').then(function(resp) {
          $scope.civilstatuses = resp.data;
        });

        
      };

      $scope.getOccupation = function(){
        $http.get('../_public/json/occupation.json').then(function(res){
           $scope.occupations = res.data;
           $scope.foc();
           $scope.moc();
         });
        
       };

       $scope.foc = function(){
        $scope.fo = $scope.occupations.find(oc => oc.id == $scope.students.father_occupation);
       };

       $scope.moc = function(){
        $scope.mo = $scope.occupations.find(oc => oc.id == $scope.students.mothers_occupation);
       };

       $scope.getRelationship = function(){
         $http.get('../_public/json/refrelationship.json').then(function(res){
             $scope.relationships = res.data;
          });
        };


        $scope.TriggerPlaces = function(){
          $scope.getRegion();
          // $scope.getProvince();
          // $scope.getCityMunci();
          // $scope.getBrngys();
        };

        //student tabs
        $scope.tabs = function(i){

          $scope.student_tab = 
          [
            {
              'tab'   : 'Student Information',
              'method' : 'stud',
              'id'     : 1
            },{
              'tab'   : 'ENROLLMENT',
              'method' : 'enroll',
              'id' : 2
            },{
              'tab'   : 'PROSPECTUS',
              'method' : 'prospect',
              'id' : 3
            },{
              'tab'   : 'SHIFT COURSE',
              'method' : 'shift',
              'id' : 4
            },{
              'tab'   : 'CREDENTIALS',
              'method' : 'cred',
              'id' : 5
            },{
              'tab'   : 'ACADEMIC INFORMATION',
              'method' : 'ai',
              'id' : 6
            },{
              'tab'   : 'ADJUSTMENTS',
              'method' : 'adjust',
              'id' : 7
            },{
              'tab'   : 'E-TRACKER',
              'method' : 'tracker',
              'id' : 8
            },{
              'tab'   : 'ENROLLMENT HISTORY',
              'method' : 'ehistory',
              'id' : 9
            },{
              'tab'   : 'PROMOTIONS',
              'method' : 'promote',
              'id' : 11
            },{
              'tab'   : 'REPORT CARD',
              'method' : 'rc',
              'id' : 12
            },{
              'tab'   : 'SCHEDULE HISTORY',
              'method' : 'sh',
              'id' : 13
            },{
              'tab'   : 'FORM 9',
              'method' : 'form9',
              'id' : 14
            },{
              'tab'   : 'TOR',
              'method' : 'tor',
              'id' : 15
            },{
              'tab'   : 'COG',
              'method' : 'promote',
              'id' : 16
            },{
              'tab'   : 'DOCUMENTS',
              'method' : 'documents',
              'id' : 17
            }
            

          ];

          $scope.active = $scope.student_tab.find(stab => stab.id === i);

        };



 //student tabs
    $scope.getRegion = function(id){
        $http.get('../_public/json/refregion.json').then(function(resp) {
        $scope.regions = resp.data;
      return $scope.regions;
      });

    };

    $scope.getProvince = function(){
      $http.get("../_public/json/refprovince.json").then(function (resp) {
        $scope.provinces = resp.data;
      });
    }

    $scope.getCityMunci = function(){
      $http.get('../_public/json/refcitymun.json').then(function (resp) {
        $scope.citymuncis = resp.data;
      });
    }

    $scope.getBrngys = function(){
      $http.get('../_public/json/refbrgy.json').then(function (resp) {
          $scope.brngys = resp.data;
      });
    };

    $scope.sel_type = function (type){
      var s_type= type;
      if(s_type == 'SHS'){
      $scope.s_code =1;

    }
    else{
      $scope.s_code=2;
    }
      $scope.getRegion();
    };
      //registration controller end         
                  //registration controller end
                    $http.get('api/activeSettings.php')
                     .then(function(res){
                       
                       $scope.setting = res.data[0].id;
                       $scope.setdesc = res.data[0].description;
                      });
             //scientific breakthrough , ng-change
              $scope.getSetting = function(setting){

              var val = $scope.setting;
              var prog = $scope.program;
              var perd =$scope.period;
                 $http.get('api/encodingSelect.php?sid='+val+'&prg='+prog+'&prd='+perd)
                   .then(function(res){                     
                      $scope.encodingData = res.data;                                   
                    });
                  $scope.getSubjectEncoding();                              
                 };
      

                    $http.get("api/selectSettings.php")
                    .then(function(res){                          
                       $scope.settings = res.data;                                           
                     });
                   //initialize encoding function
                   $scope.insertData=function(){

                  //var res =  $scope.encodingData;    
                    $http.post("api/initEncoding.php", {
                        'setting':$scope.setting,
                        'program':$scope.program,
                        'period' :$scope.period,
                    }).then(function(response){
                            console.log("Data Inserted Successfully");
                         $scope.encodingData =response.data;

                        },function(error){
                            alert("Sorry! Data Couldn't be inserted!");
                            console.error(error);

                        });
                };

                  $scope.save=function(){ 
                  

                      $http.post("api/updateEncoding.php", {
                          'id':$scope.id,
                          'from':$scope.from,
                          'to' :$scope.to,
                      }).then(function(response){
                              console.log("Data Inserted Successfully");
                              $scope.popup1.removeClass('launch');
                              $scope.getSetting();

                          },function(error){
                              alert("Sorry! Data Couldn't be inserted!");
                              console.error(error);

                          });
                        
                };  

                 $http.get("api/selectStatus.php")
                    .then(function(res){
                          
                       $scope.statuses = res.data;    
                                      
                     });


                    $scope.tab = function(id){

                        $scope.tabnow= id;
                        $scope.getSubjectEncoding();
                        
                        
                   
                    };
                      //requesting data to getSetting as trigger
                       $scope.getSubjectEncoding = function(){

                        var val = $scope.setting;
                        var prog = $scope.program;
                        var perd =$scope.period;
                        var tab = $scope.tabnow;

                      $http.get("api/selectEncodingSubject.php?sid="+val+'&prg='+prog+'&prd='+perd+'&tb='+tab)
                      .then(function(res){
                       

                        $scope.subjects=res.data;
               
                      });
                    };


                

                    $scope.getSched = function(id,lname,fname,scode,sd,sname){

                      $scope.popup.addClass('launch');
                      $scope.sched_id = id;


                      // for info purpose
                      $scope.lname= lname;
                      $scope.fname= fname;
                      $scope.scode= scode;
                      $scope.sd= sd;
                      $scope.sname= sname;


                      var val=$scope.sched_id ;


                      $http.get("api/selectEncodingGrades.php?sid="+val)
                      .then(function(res){
                       

                        $scope.grades=res.data;
               
                      });
                      
                    };


                    $scope.revise = function(id){
                      $scope.id = id;
                      $scope.en_action=6;
                      $scope.upEncodingStatus();
                    };

                    $scope.verify = function(id){
                      $scope.id = id;
                      $scope.en_action=7;
                      $scope.upEncodingStatus();
                    };


                    $scope.open = function(id){
                      $scope.id = id;
                      $scope.en_action=2;
                      $scope.upEncodingStatus();
                    };                   
                    $scope.upEncodingStatus = function(){                  
                      $http.post("api/updateEncodingStatus.php", {
                        'id':$scope.id,
                        'btn':$scope.en_action,
                        'period' :$scope.period,
                    }).then(function(response){
                            console.log("Data Inserted Successfully");                   
                            $scope.getSubjectEncoding();
                        },function(error){
                            alert("Sorry! Data Couldn't be inserted!");
                            console.error(error);
                        });
                    };


              




                    //teachers Controller

                    $scope.getTeachers = function(){
                      $http.get("api/selectTeacher.php")
                      .then(function(res){

                        $scope.teachers=res.data;
               
                      });
                    };

                    $scope.activate= function(id){
                      $scope.id= id;
                      $scope.mode=1;
                       $scope.active();
                    };

                     $scope.deact= function(id){
                      $scope.id= id;
                       $scope.mode=0;
                       $scope.active();
                    };

                    $scope.active=function(){ 
                    $http.post("api/activate.php", {
                        'id':$scope.id,
                        'btn':$scope.mode,
                    }).then(function(response){
                            console.log("Data Inserted Successfully");
                        
                            $scope.getTeachers();

                        },function(error){
                            alert("Sorry! Data Couldn't be inserted!");
                            console.error(error);

                        });
                        
                };  
                 $scope.reset=function(id,employee_no){ 
                    $scope.id=id;
                    $scope.emp_no=employee_no;

                    $http.post("api/password_reset.php", {
                        'id':$scope.id,
                        'emp_no':$scope.emp_no,
                    }).then(function(response){
                            console.log("Data Inserted Successfully");
                        
                            $scope.getTeachers();

                        },function(error){
                            alert("Sorry! Data Couldn't be inserted!");
                            console.error(error);

                        });          
                };  

                $scope.add = function(){
                  
                   $scope.popup1.addClass('launch');
                   $scope.action='insert';

                   $scope.emp_no='';
                   $scope.f_name='';
                   $scope.m_name='';
                   $scope.l_name='';
                   $scope.suffix='';
                };
                
                $scope.insertEmployee=function(){

                  
                  //var res =  $scope.encodingData;    
                    $http.post("api/addEmployee.php", {
                        'emp_no':$scope.emp_no,
                        'f_name':$scope.f_name,
                        'm_name' :$scope.m_name,
                        'l_name' :$scope.l_name,
                        'suffix' :$scope.suffix,
                    }).then(function(response){
                            console.log("Data Inserted Successfully");
                            $scope.popup1.removeClass('launch');
                         $scope.getTeachers();

                        },function(error){
                            alert("Sorry! Data Couldn't be inserted!");
                            console.error(error);

                        });
                };
                         $scope.editEmployee = function(id,emp_no,f_name,m_name,l_name,suffix){

                          $scope.popup1.addClass('launch');
              
                                $scope.id = id;
                                $scope.emp_no = emp_no;    
                                $scope.f_name = f_name;  
                                $scope.m_name = m_name;  
                                $scope.l_name = l_name;  
                                $scope.suffix = suffix;
                                $scope.action='edit';
                   };


                    $scope.editNow=function(){

                
                      $scope.popup1.addClass('launch');
                  //var res =  $scope.encodingData;    
                    $http.post("api/updateEmployee.php", {
                        'id': $scope.id,
                        'emp_no':$scope.emp_no,
                        'f_name':$scope.f_name,
                        'm_name' :$scope.m_name,
                        'l_name' :$scope.l_name,
                        'suffix' :$scope.suffix,
                    }).then(function(response){
                            console.log("Data Inserted Successfully");
                            $scope.popup1.removeClass('launch');
                         $scope.getTeachers();

                        },function(error){
                            alert("Sorry! Data Couldn't be inserted!");
                            console.error(error);

                        });
                };
                
                  //schedule builder controller
                  $scope.initDepartment = function(id){
                    $scope.dep_id=id;
                    $scope.getSchedules();

                  };

                  $scope.getSchedules = function(){
                      var set = $scope.setting;
                  
                      var dep_id = $scope.dep_id;
                    $http.get('api/scheduleSubject.php?sid='+set+'&did='+dep_id+'&fetch')
                    .then(function(resp){

                      $scope.schedules = resp.data;
                      

                    });
                  };

                  $scope.department = function(){

                      $http.get('api/selectDepartment.php')
                      .then(function(res){
                        $scope.departments = res.data;
                      });

                  };


                  //student list controller and student
                  $scope.studentYearlvl = function(){
                    $http.get('api/selectYearlevels.php')
                    .then( function(resp){
                      $scope.years = resp.data;
                    });               
                  };
                  
                  $scope.studentStrand = function(){
                    $http.get('api/selectStrands.php')
                    .then( function(resp){
                      $scope.strands = resp.data;
                    });               
                  };
                  $scope.studentTrack = function(id){
                    $http.get('api/selectTrack.php?prog_id='+id)
                    .then( function(resp){
                      $scope.trackss = resp.data;
                    });
                  };
            
                  $scope.studentList = function(setting){
                    var yr = $scope.yr;
                    var cour = $scope.stran;
                    var stat = $scope.status;                
                    $http.get('api/selectStudents.php?desc='+setting+'&year='+yr+'&course='+cour+'&status='+stat)
                    .then( function(resp){
                      $scope.students = resp.data;
                    });               
                  };

                  //student only controller
                  $scope.resetStud=function(id,stud_no){ 
                    $scope.id=id;
                    $scope.stud_no=stud_no;
                    $http.post("api/reset_student.php", {
                        'id':$scope.id,
                        'stud_no':$scope.stud_no,
                    }).then(function(response){
                            alert("Reset Success");
                        },function(error){
                            alert("Sorry! Data Couldn't be inserted!");
                            console.error(error);
                        });
                };  

                $scope.buildStart = function(sched_id){
                  $scope.builder_id = sched_id;
                  window.location="?builder_sched="+sched_id;
                
                };
                 
                $scope.loadSubjects = function(id){
                  

                  $http.get('api/selectSubjectSchedules.php?sid='+id)
                  .then (function(resp){
                      $scope.details = resp.data;
                    
                  });
                };


                $scope.loadDate = function(id){
                  $scope.schedId=id;
                  console.log(id);
                  $http.get('api/selectScheduleDate.php?sid='+id)
                  .then (function(resp){
                     $scope.subjectdetails = resp.data;                                                          
                  });
                };


                $scope.openEncoding=function(){

                  $scope.btn = '1';
                  $scope.closeData();
                };
                $scope.closeEncoding=function(){

                  $scope.btn = '2';
                  $scope.closeData();
                };
             

                $scope.generate=function(id){
                    $http.post("api/generateEncoding.php?id="+id, {
                        'set_id': $scope.setting,
                        'prog':   $scope.program,
                        'period': $scope.period
                    }).then(function(response){
                      $scope.getSubjectEncoding();
                        },function(error){
                      
                        });

                  alert($scope.setting+$scope.program+$scope.period);
                };

//checklist model
                  $scope.selection = [];

                  $scope.checkAll = function() {
                    $scope.mark = 1;
                    $scope.selection.subjects = $scope.subjects.map(function(item) { return item.sdes_id; });
                  };
                  $scope.uncheckAll = function() {
                    $scope.mark ='';
                    $scope.selection.subjects = [];
                  };
                  $scope.checkFirst = function() {
                    $scope.selection.subjects.splice(0, $scope.selection.subjects.length); 
                    $scope.selection.subjects.push(1);
                  };

////////////                 
               
                    $scope.closeData = function(){
                            
                      $http.post("api/closeSelected.php", {
                          'id':$scope.selection.subjects,
                          'btn': $scope.btn, 
                      }).then(function(response){
                              console.log("Data Closed Successfully");
                            
                          $scope.getSubjectEncoding();
                          },function(error){
                              alert("Sorry! Data Couldn't be inserted!");
                              console.error(error);
                          });
                    };

                    
                    $scope.colorRand = function colorRand(){
                      var colors = ['dark','gray','primary','sky','success','purple'];
                    return 'e-btn '+ colors[Math.floor((Math.random() * colors.length))]+'-gradient';
                    };
                    
                    $scope.addSched= function (){
                      $scope.popup.modal('show');
                    };

                    $scope.getBuildings = function (){

                      $http.get("api/selectBuildings.php")
                      .then (function(resp){
                        $scope.buildings= resp.data;
                      });
                    };

                    $scope.getRooms = function (id){
                      $http.get("api/selectRooms.php?bid="+id)
                      .then (function(resp){
                        $scope.rooms= resp.data;
                      });
                    };

                    $scope.insertSched = function(schedId,build,rm,from,to,alt,day,teacher,setting){
                     $http.post("api/insertSchedule.php", {
                        'id'      : $scope.schedId,
                        'build'   : build,
                        'rm'      : rm,
                        'from'    : from,
                        'to'      : to,
                        'alt'     : alt,
                        'day'     : day,
                        'teacher' : teacher,
                        'setting' : setting,
                    }).then(function(response){
                      var notif = $scope.userNotif = response.data;
                      $scope.cancel();
                      alert(notif);
                      $scope.fetchSubSched($scope.sub_id);
                    
                        },function(error){
                            alert("Sorry! Data Couldn't be inserted!");
                            console.error(error);
                        });
                      // console.log(schedId);
                      // console.log(build);
                      // console.log(rm);
                      // console.log(from);
                      // console.log(to);
                      // console.log(alt);
                      // console.log(day);
                      // console.log(teacher);
                      // console.log(setting);
                    };

            //programs controller
            $scope.getProg = function(){                          
              $http.get("api/selectProgram.php")
              .then(function(resp){
                $scope.progs = resp.data;             
              });
            };


            $scope.getTrack = function(id){
              var p_id= id;
              $scope.program = id;
              $http.get("api/selectTrack.php?pro_id="+p_id)
              .then(function(resp){
                $scope.tracks = resp.data;
              });
            };

            
            $scope.Trace = function(id){

              $scope.track_id = id;

              $http.get("api/selectStrandTrack.php?t_id="+id)
              .then(function(resp){
                $scope.strands = resp.data;
              });
            };

            $scope.getStrandId = function(id){              
              $scope.showadd=id;
              $http.get("api/selectCurricula.php?st_id="+id)
              .then(function(resp){
                $scope.curriculas = resp.data;
              });
            };

            $scope.adding = function(type){
              $scope.schedId=type;
              var popup = angular.element("#add");
              console.log('success');
              popup.toggleClass('launch');
                $scope.mtitle=type;
                if($scope.mtitle=='update'){
                    $scope.scode   = $scope.subject_info[0].code;
                    $scope.sname   = $scope.subject_info[0].sp_name;
                    $scope.subtype = $scope.subject_info[0].subtype_id;       
                    $scope.week    = $scope.subject_info[0].weeks;
                    $scope.lecture = $scope.subject_info[0].lecture_hours;
                    $scope.unit    = $scope.subject_info[0].units;
                }               
            };

            $scope.cancel = function(){
              var popup = angular.element("#add");
              console.log('success');
              popup.removeClass('launch');
            };

            $scope.insert = function (type){

              var tracker = $scope.track_id;
              var prog    =  $scope.programm;
              var stran   = $scope.showadd;
              var popup   = angular.element("#add");

              if($scope.mtitle =='track'){

              $http.post("api/insertTrack.php", {
                'code':$scope.code,
                'track':$scope.name,
                'program':$scope.programm,
                }).then(function(response){
                    console.log("Data Inserted Successfully");
                    popup.removeClass('launch');
                    $scope.getTrack(prog);
                },function(error){
                    alert("Sorry! Data Couldn't be inserted!");
                    console.error(error);
                });
              }
              else if($scope.mtitle=='strand'){
                $http.post("api/insertStrand.php", {
                  'code'     : $scope.code,
                  'strand'   : $scope.name,
                  'track_id' : $scope.track_id,
                  }).then(function(response){
                      console.log("Data Inserted Successfully");
                      popup.removeClass('launch');
                      $scope.Trace(tracker);
                  },function(error){
                      alert("Sorry! Data Couldn't be inserted!");
                      console.error(error);
                  });
              }

              else if($scope.mtitle=='syear'){
                $http.post("api/insertCurricula.php", {
                  'name'      : $scope.code,
                  'syear'     : $scope.sy,
                  'strand_id' : $scope.showadd,
                  }).then(function(response){
                      console.log("Data Inserted Successfully");
                      popup.removeClass('launch');
                      $scope.getStrandId(stran);
                  },function(error){
                      alert("Sorry! Data Couldn't be inserted!");
                      console.error(error);
                  });
              
              }
            };
            
            $scope.getSy = function (){
              $http.get("api/selectSchoolYears.php")
              .then(function(resp){
                $scope.syears = resp.data;
              });
            };

            $scope.activateCur = function(id,status){
              var idd= $scope.showadd;
              
                  $http.post("api/activateCurricula.php", {
                    'id': id,
                    'status': status, // this is the activate value
                    }).then(function(response){    
                      console.log("success");       
                      $scope.getStrandId(idd);        
                    },function(error){                    
                      alert("Sorry! Data Couldn't be inserted!");
                        console.error(error);
                    });                
            };

            $scope.getSubjectParent = function(id,type){
            
              $scope.program_id=id;
              $http.get("api/selectSubjectParent.php?id="+id+"&"+type)
              .then(function(resp){
                  $scope.subject_parents = resp.data;
              });

            
            };

            $scope.getSubjectInfo = function(id,type){
            
              $scope.subject_info_id=id;
              console.log(id);
              $http.get("api/selectSubjectParent.php?id="+id+"&"+type)
              .then(function(resp){
                  $scope.subject_info = resp.data;
              });
            };

            $scope.getPrerequisites = function(id){
              var popup = angular.element("#prereq");
              popup.addClass('launch')
              console.log(id);
              $http.get("api/selectSubjectPrerequisite.php?id="+id)
              .then(function(resp){
                  $scope.subject_prerequisites = resp.data;
              });

              $scope.getPrerequisiteSubject(id);
            };
            
            $scope.getPrerequisiteSubject = function(id){
              $scope.reciever = id;
              console.log('this'+id);
              $http.get("api/selectSubjectFromPrerequisite.php?id="+id)
              .then(function(resp){
                  $scope.subject_from_prerequisite = resp.data;
              });
            };

            $scope.insertPrerequisite = function(){
               
              $http.post("api/insertPrerequisite.php",{
              'p_id'      : $scope.reciever,
              'this_p_id' : $scope.giver,
              }).then(function(resp){
             
                $scope.getPrerequisites($scope.reciever);
                            
              },function(error){

              });
            
            };

            $scope.deletePrerequisite = function(id){
                $http.get("api/insertPrerequisite.php?delete_data="+id)
                .then(function(resp){
                  $scope.getPrerequisites($scope.reciever);
                });[o]
            };

            $scope.cancelPr = function (){
                var popup = angular.element("#prereq");
                popup.removeClass('launch');
            };


            $scope.getSubjectType = function(){
              $http.get("api/selectSubjectType.php")
              .then(function(resp){
                $scope.stypes = resp.data;
              });

            };

            $scope.deleteParentSubject = function(){

            

              $http.post("api/deleteParentSubject.php",{
                'parent_id' : $del_id,
              }).then(function(resp){
                $scope.getSubjectParent($scope.program_id,'subjects');
                $scope.getSubjectInfo($scope.subject_info_id,'subject');
                $scope.popup1.removeClass('launch');
              },function(error){


              });

            };

            
            $scope.deleteValid = function(id){

              $del_id = id;

              $scope.popup1.addClass('launch');

            };

            $scope.cancelValid = function(){

              $scope.popup1.removeClass('launch');
            };


            $scope.insertSubject = function(){
                  
              $http.post("api/insertSubject.php",{
              'id'      : $scope.subject_info_id,
              'code'    : $scope.scode,
              'name'    : $scope.sname,
              'type'    : $scope.subtype,
              'week'    : $scope.week,
              'lecture' : $scope.lecture,
              'unit'    : $scope.unit,
              'program' : $scope.program_id,
              'mode'    : $scope.mtitle,
              }).then(function(resp){
                $scope.emodal.removeClass('launch');
                $scope.getSubjectParent($scope.program_id,'subjects');

                if($scope.mtitle =='update'){
                  $scope.getSubjectInfo($scope.subject_info_id,'subject');
                }else{}
               
              },function(error){

              });
            
            };


            $scope.getTerms = function(){
              $http.get("api/selectTerm.php")
              .then(function(resp){

                $scope.terms = resp.data;

              });
            };

            $scope.curriculumDetails = function(id){      
              
              $scope.curriculum_id = id;

              $http.get("api/selectCurricula.php?cur_id="+id)
              .then(function(resp){
                $scope.curr_header = resp.data;
              });


              // $http.get("api/selectCurriculumDetails.php?cur_id="+id)
              // .then(function(resp){
              // $scope.cur_details= resp.data;
              // });

              // $http.get("api/selectCurriculumTerm.php?cur_id="+id)
              // .then(function(resp){
              // $scope.cur_terms= resp.data;
              // });
               
               
              // $http.get("api/selectCurriculumSubject.php?cur_id="+id)
              // .then(function(resp){
              // $scope.cur_subjects= resp.data;                

              // });

              $http.get("api/prospectus.class.php?cur_id="+id)
              .then(function(resp){
                $scope.curriculums = resp.data;
              });

         };

          $scope.getSubjects = function(id){
            $http.get("api/selectSubject.php?id="+id)
            .then(function(resp){
                $scope.stud_subjects = resp.data;
            });
          };
          
          $scope.insertCurriculumSubject = function(){
            $http.post("api/insertCurriculumSubject.php",{
            'id'              : $scope.cur_sub_id, 
            'subject_id'      : $scope.sub,
            'school_year'     : $scope.curr_header[0].syear_id,
            'year_lvl'        : $scope.sub_year,
            'term_id'         : $scope.sub_term,
            'curricula_id'    : $scope.curriculum_id,
            'mode'            : $scope.mtitle,
            }).then(function(resp){
              $scope.emodal.removeClass('launch');
              $scope.curriculumDetails($scope.curriculum_id);
              if($scope.mtitle =='update'){                
              }else{}
            },function(error){
            });    
          };

          $scope.updateCurriculumSubject = function(id,parent,subject,year,term){
              
          
              $scope.cur_sub_id = id;
              $scope.adding('edit');

              $scope.sub      = subject;
              $scope.sub_year = year;
              $scope.sub_term = term;
              $scope.sub_parent   = parent;

              alert(term);

            $http.get("api/selectSubject.php?parent="+subject)
            .then(function(resp){
              
              $scope.subjectEdit  = resp.data;
              $scope.getSubjects($scope.sub_parent);

              

            });
        
            
           };

                $scope.fetchPreReg = function (id){

                    window.location='?register='+id;
                };


                $scope.getAdequate = function (keyword){

                $http.get("api/fetchAdequate.php?searchPreReg="+keyword)
                .then (function(resp){
                  $scope.adequates = resp.data;
                });

                };


                $scope.getStudent = function(id){
                  $scope.stud_id=id;
                 
                  $http.get("api/selectStudentInfo.php?id="+id)
                  .then (function(resp){
                    $scope.students = resp.data;
                    $scope.getReligion();
                    $scope.getOccupation();
                    
                    
                  });

                  $http.get("api/selectProspectus.php?id="+id)
                  .then (function(resp){
                    $scope.curriculum = resp.data;


                    $scope.curriculumDetails($scope.curriculum.cur_id);

                   
                    
                    
                  });

                  $http.get("api/selectStudentProspectus.php?id="+id)
                  .then (function(resp){
                    $scope.subject_enrolled = resp.data;


                   
                    
                  });
                  

                };

                $scope.studentList = function () {
                  var yr = $scope.yr;
                  var cour = $scope.stran;
                  var stat = $scope.status;
                  if ($scope.setting == null){$scope.setting = ""}
                  $scope.students = [];
                  $http.get('api/selectStudentList.php?setting_id=' + $scope.setting + '&year_level_id=' + yr + '&strand_id=' + cour + '&enrollment_status=' + stat)
                    .then(function (resp) {
                            $scope.students = resp.data;
                            console.log(resp);
                     
                    });
                    
                    
                    console.log($scope.yr);
                };
              

                // $scope.getStudentInfo = function(id){
                //     $http.get("api/selectStudentInfos.php?id="+id)
                //     .then(function(resp){
                //       $scope.infos = resp.data;
            
                //     });
                  
                //   };

                  $scope.initShiftCourse = function(val){
                    console.log(val);
                    $scope.curs = val;
                  };

                  $scope.addStudentCurricula = function(id){
                    // alert($scope.strand);
                    $http.post("api/shiftStudent.php",{
                      'student': id,
                      'cur'    : $scope.curs,
                      'strand' : $scope.strand,
                    }).then(function(resp){
                      alert(resp.data);
                    },function(error){
                    });
                  };

                  $scope.cancelSchedule = function(){
                    var popup = angular.element("#schedule");
                    popup.removeClass('launch');
                    $scope.popup1.removeClass('launch');
                  };

                  $scope.selectSchedules = function(id){
                    // alert($scope.sett +" "+$scope.year+ " "+$scope.students.strand_id);  
                    //alert($scope.curriculum.cur_id);  
                    $scope.this_cur = id;       //insertSubSched  
                    var popup = angular.element("#schedule");
                    popup.addClass('launch');
                    $http.get("api/selectSchedule.php?s_id="+$scope.students.strand_id+"&y_id="+$scope.year+"&set_id="+$scope.sett)
                    .then(function(resp){
                      $scope.schedules = resp.data;      
                    });
                  };
                  
                  $scope.fetchSubSched = function(id){
                    $scope.sub_id = id;
                    $http.get("api/fetchSchedules.php?id="+id)
                    .then(function(resp){

                      $scope.subs = resp.data;
            
                      var popup = angular.element("#schedule");
                      popup.removeClass('launch');
                      //scroll buttom
                      var bottom = angular.element("html, body");
                      bottom.animate({ scrollTop: $(document).height() }, "slow");
                      //scroll bottom
                    });
                    
                  
                  };


                  //push problems
                 
                  $scope.subs =[];
                  $scope.fetchPer = function(sub_id,id){
                    $http.get("api/fetchSchedules.php?id="+id+"&subj="+sub_id)
                    .then(function(resp){

                      $scope.subscheds = resp.data;

                      angular.forEach($scope.subscheds,function(s){
                    
                        $scope.subs.push(
                          {
                              'id' : s.id,
                              details:[]  
                          }
    
                        );
                        $scope.subs[0].details.push(
                        s.details[0]
                        );

                    
                      
                      });
                     
                      var popup = angular.element("#modals");
                      popup.removeClass('launch');
                      //scroll buttom
                      var bottom = angular.element("html, body");
                      bottom.animate({ scrollTop: $(document).height() }, "slow");
                      //scroll bottom
                    });
                    
                   
                  //   $scope.subs =  $scope.subscheds[0].details.map(function(sub){
                  //     return sub;
                  // });  

                 
        
                  };

                  $scope.selectionSubs = function(cur){
                    $scope.popup1.addClass('launch');
                    $http.get("api/subject.class.php?cur="+cur+"&str="+$scope.students.strand_id+"&s="+$scope.sett+"&fetch_subjects")
                    .then(function(resp){

                      $scope.thesubjects = resp.data;
                    });
                  };

                
                  $scope.verify = 0;          
                  $scope.insertSubSched = function(){        
                    // $scope.loader = 1;
                    
                    $scope.selectedsub =  $scope.subs[0].details.map(function(sub){
                      return sub;
                  }); 

                      var popup = angular.element("#load");
                      popup.addClass('launch');
                
                      $http.post("api/insertSubSched.php",{
                        'subjects'  : $scope.selectedsub,
                        'setting'   : $scope.sett,
                        'year'      : $scope.year,
                        'strand'    : $scope.students.strand_id,
                         'student_id': $scope.students.student_id,
                       }).then(function(resp){
                      
                        $scope.subscheds ='';
                        $scope.subs = [];
                        $scope.userMessage = resp.data;                      
                          $scope.createPayment();
                          $scope.enrollmentSubject();                       
                       },function(error){     
                       });                  
                  };
                  
                  $scope.removeSub = function (detail){
                      $scope.subs[0].details.splice(detail, 1);
                  };

                  $scope.removeEnroll = function(id){

                    $http.get("api/enrollmentSubject.php?remove="+id+"&setting="+$scope.sett+"&sid="+$scope.students.student_id)
                    .then(function(resp){
                      $scope.enrollmentSubject();
                      $scope.createPayment();
                     
                    });
                  };
                 

                  $scope.enrollDetails = function(){

                    $scope.enrollmentSubject();
                  $scope.paymentDetails();
                  };
     
                  $scope.enrollmentSubject = function(){
                    
                    
                    $http.get("api/enrollmentSubject.php?fetch_enrollment&setting="+$scope.sett+"&sid="+$scope.students.student_id)
                    .then(function(resp){

                      $scope.enrollmentsubs = resp.data;
                      

                    });

                   
               
                 };

                  
             $scope.paymentDetails = function(){
                $http.get("api/Payments.php?s="+$scope.sett+"&st="+$scope.students.strand_id+"&y="+$scope.year+"&student="+$scope.students.student_id)
                .then(function(resp){
                // $scope.loader = 0;
                 $scope.payments = resp.data;
                
                  if($scope.payments){

                    var popup = angular.element("#load");
                    popup.removeClass('launch');
                    var bottom = angular.element("html, body");
                    bottom.animate({ scrollTop: $(document).height() }, "slow");
                    //bottom.animate({scrollTop:0}, 'slow'); for scroll top
                   $scope.verify = 1;
                  }
                });

             };

             $scope.createPayment = function(){
              $http.get("api/Payments.php?s="+$scope.sett+"&st="+$scope.students.strand_id+"&y="+$scope.year+"&student="+$scope.students.student_id+"&create")
              .then(function(resp){
              // $scope.loader = 0;
              $scope.payments = resp.data;
              $scope.paymentDetails();
                if($scope.payments){

                  var popup = angular.element("#load");
                  popup.removeClass('launch');
                  var bottom = angular.element("html, body");
                  bottom.animate({ scrollTop: $(document).height() }, "slow");
                  //bottom.animate({scrollTop:0}, 'slow'); for scroll top
                 $scope.verify = 1;
                }
              });

           };


             $scope.openDiscounts = function(){

              $http.get("api/Discounts.php?fetch_discounts")
              .then(function(resp){
                $scope.discounts = resp.data;
              });
             };

             $scope.openVoucher = function(){
              $http.get("api/Discounts.php?fetch_vouchers="+$scope.sett)
              .then(function(resp){
                $scope.vdiscounts = resp.data;
              });
             };

             $scope.paymentDiscounts = function(set,strand,year,stud,amount){
              var popup = angular.element("#load");
              popup.addClass('launch');
              $http.get("api/Payments.php?s="+set+"&st="+strand+"&y="+year+"&student="+stud+"&discount="+amount)
              .then(function(resp){
              // $scope.loader = 0;
              $scope.paymentDetails();
              $scope.payments = resp.data;
                if($scope.payments){                
                  popup.removeClass('launch');
                  var bottom = angular.element("html, body");
                  bottom.animate({ scrollTop: $(document).height() }, "slow");
                  //bottom.animate({scrollTop:0}, 'slow'); for scroll top
                 $scope.verify = 1;
                }
              });
           };

           $scope.removeDiscount = function(id){

            $http.get("api/Payments.php?s="+$scope.sett+"&st="+$scope.students.strand_id+"&y="+$scope.year+"&student="+$scope.students.student_id+"&delete="+id)
            .then(function(resp){

              $scope.paymentDetails();
            });

           };


           $scope.paymentVoucher = function(set,strand,year,stud,amount){

            var popup = angular.element("#load");
            popup.addClass('launch');
            $http.get("api/Payments.php?s="+set+"&st="+strand+"&y="+year+"&student="+stud+"&voucher="+amount)
            .then(function(resp){
            // $scope.loader = 0;
            $scope.paymentDetails();
            $scope.payments = resp.data;

              if($scope.payments){

              
                popup.removeClass('launch');
                var bottom = angular.element("html, body");
                bottom.animate({ scrollTop: $(document).height() }, "slow");
                //bottom.animate({scrollTop:0}, 'slow'); for scroll top
               $scope.verify = 1;
              }
            });

         };

         $scope.removeVoucher = function(id){

          $http.get("api/Payments.php?s="+$scope.sett+"&st="+$scope.students.strand_id+"&y="+$scope.year+"&student="+$scope.students.student_id+"&deletev="+id)
          .then(function(resp){

            $scope.paymentDetails();
          });

         };
               
             $scope.fetchBuilding = function(){

              $http.get("api/selectBuild.php?fetch_building")
              .then(function(resp){

                $scope.buildings = resp.data;
              });

             };

             
             $scope.insertBuilding = function(type){ 
              // if(type === 'Add Room'){
              //   $scope.b_id =
              // }
               $scope.name ='';
               $scope.title=type;    
         
              $scope.popup1.addClass('launch');
             };

             $scope.updateBuild = function(type,id){
              $scope.roommm = $scope.rooms.find(room => room.id === id);
              $scope.theId =id;
              if(type==='Edit Building'){$scope.name = $scope.buildinfo.name;}
              else if(type==='Edit Room'){$scope.name = $scope.roommm.name;}
              $scope.title =type;
              $scope.popup1.addClass('launch');

             };

             $scope.cancelModal = function(){
              $scope.popup1.removeClass('launch');
              $scope.popup.removeClass('launch');
             };



             $scope.insertBuild = function(){
          
               if($scope.title==='Add Building'){
                  $http.post("api/selectBuild.php",{
                    'name'    : $scope.name,
                    'type'    : "insert",
                  }).then(function(resp){
                    $scope.popup1.removeClass('launch');
                    $scope.fetchBuilding();                    
                  },function(error){
                  });
              }
              else if($scope.title==='Add Room'){
                $http.post("api/selectBuild.php",{
                  'id'      : $scope.buildinfo.id,
                  'name'    : $scope.name,
                  'type'    : "room",
                }).then(function(resp){
                  $scope.popup1.removeClass('launch');
                  $scope.fetchBuilding();
                  $scope.fetchRoom($scope.buildinfo.id);                    
                },function(error){
                });
              }
             };

             $scope.editBuild = function(){
                      
              if($scope.title==='Edit Building'){
              $http.post("api/selectBuild.php",{
                'id'      : $scope.buildinfo.id,
                'name'    : $scope.name,
                'type'    : "update",
              }).then(function(resp){
                $scope.popup1.removeClass('launch');
                $scope.fetchBuilding();
                $scope.fetchRoom($scope.rid);   
                   
              },function(error){
              });
            }

            else if($scope.title==='Edit Room'){
              $http.post("api/selectBuild.php",{
                'id'      : $scope.theId,
                'name'    : $scope.name,
                'type'    : "updateroom",
              }).then(function(resp){
                $scope.popup1.removeClass('launch');
                $scope.fetchBuilding();
                $scope.fetchRoom($scope.buildinfo.id);   
                                 
              },function(error){
              });
            }
         };


             $scope.fetchRoom = function(id){

              $scope.rid = id;
              $scope.buildinfo = $scope.buildings.find(build => build.id === id);
                
              
              $http.get("api/selectBuild.php?fetch_room="+id)
              .then(function(resp){
                $scope.rooms = resp.data;
                $scope.buildinfo = $scope.buildings.find(build => build.id === $scope.rid);    
              });
             };



             $scope.fetchSchool = function(){

              $http.get("api/schoolCourse.php?fetch_school")
              .then(function(resp){

                $scope.schools = resp.data;
              });

             };


             $scope.fetchCourse = function(){

              $http.get("api/schoolCourse.php?fetch_course")
              .then(function(resp){

                $scope.courses = resp.data;
              });

             };
                
             $scope.insertSchoolnow = function(){
          
          
               $http.post("api/schoolCourse.php",{
                 'name'    : $scope.name,
                 'code'    : $scope.code,
                 'type'    : $scope.title,
               }).then(function(resp){
                 $scope.popup1.removeClass('launch');
                 $scope.fetchSchool();
                 $scope.fetchCourse();
                                   
               },function(error){
               });
             
            };


            $scope.insertSchool = function(type){ 
    
               $scope.name ='';
               $scope.code = '';
               $scope.title=type; 
         
              $scope.popup1.addClass('launch');
             };



             $scope.updateSchool = function(type,id){ 
              
              if(type=='Edit School'){
                $scope.info = $scope.schools.find(school => school.id === id);
               
              }
              else if(type=='Edit Course'){
                $scope.info = $scope.courses.find(course => course.id === id);
              }

              $scope.name =$scope.info.name;
              $scope.code = $scope.info.code;
             
              $scope.title=type; 
        
             $scope.popup1.addClass('launch');
            };

            $scope.editSchool = function(){
              $http.post("api/schoolCourse.php",{
                'id'      : $scope.info.id,
                'code'    : $scope.code,
                'name'    : $scope.name,
                'type'    : $scope.title,
              }).then(function(resp){
                $scope.popup1.removeClass('launch');
                $scope.fetchSchool();
                $scope.fetchCourse();                
              },function(error){
              });
           
        };

        //credentials
        $scope.credential = function(){ 
          $http.get("api/credentials.php?fetch="+$scope.stud_id+"&table=student_requirements")
          .then(function(resp){
            $scope.cred = resp.data;
          });
        };

        $scope.training = function(){
          $http.get("api/credentials.php?fetch="+$scope.stud_id+"&table=student_training")
          .then(function(resp){
            $scope.train = resp.data;
          });
        };

        $scope.updateCred = function(column,vals,tbl){
          $http.post("api/credentials.php",{
            'field'      : column,
            'val'        : vals,
            'sid'        : $scope.stud_id,
            'tbl'        : tbl,
          }).then(function(resp){
            $scope.credential();                     
          },function(error){
          });

        };
        
        $scope.editStudent = function(id){
          $http.post("api/selectStudentInfo.php",{
            'id'         : id,
            'email'      : $scope.students.email,
            'phone'      : $scope.students.contact_no,
            'religion'   : $scope.belief.id,
            'bday'       : $scope.students.birthday,
            'sex'        : $scope.students.sex,
            'fname'      : $scope.students.first_name,
            'mname'      : $scope.students.middle_name,
            'lname'      : $scope.students.last_name,
            'cstatus'    : $scope.students.civil_status,
          }).then(function(resp){
          $scope.getStudent(id);
          
            if(resp.data ==='updated'){
                $scope.success.addClass('launch'); 
                setTimeout(function(){
                  $scope.success.removeClass('launch'); 
                },2000);
            }

          },function(error){
          });
        };
        $scope.date = function(){
          $(function(){
            $('input[name="birthday"]').daterangepicker({
              singleDatePicker: true,
              showDropdowns: true,
              minYear: 1901,
              maxYear: parseInt(moment().format('YYYY'),10)
            }, function(start, end, label) {
              var years = moment().diff(start, 'years');
                $scope.age = years;
                $scope.students.birthday = start.format('YYYY-MM-DD');
           
            });
          });
          $scope.age($scope.students.birthday);
        };
        $scope.age = function(d){
          
          var y = d.substr(0,3);
          var m = d.substr(5,6);
          var d = d.substr(8,9);
  
          var bd  = m+"/"+d+"/"+y;
          var birth = new Date(bd);
          var curr  = new Date();
          var diff  = curr.getTime() - birth.getTime();
          var ag =Math.floor(diff / (1000 * 60 * 60 * 24 * 365.25));
           $scope.age = ag;
         
        };

        $scope.getReportCard = function(){
         
          $http.get("api/AcademicInfo.php?id="+$scope.students.student_id+"&set="+$scope.sett+"&find")
          .then(function(resp){
            $scope.subinfo = resp.data;
            $scope.getAverage();
          });


          $http.get("api/AcademicInfo.php?id="+$scope.students.student_id+"&set="+$scope.sett+"&value")
          .then(function(resp){
            $scope.student_values = resp.data;
           
          });

              
        };
       
        $scope.getAverage = function(){
          var ave=0;
          var counter =0;
          
          angular.forEach($scope.subinfo,function(s){
            if(s.is_academic == 1){
              if(s.final>6){
                ave = ave + Math.round(s.final) * 1;
              }
              else{
              ave = ave + s.final * 1;
              }
              
              counter++;
            }

          });
          return  ave / counter;
          
        };

        $scope.getBlocking = function(){

          $http.get("api/StudentBlocking.php?find")
          .then(function(resp){
            $scope.blockings = resp.data;
          });
        };

        $scope.block = function(id,active){

          $scope.act = active;
          $scope.stid = id;

          if(isNaN(active)){
            $scope.popup1.addClass('launch');
          }

          $http.get("api/StudentBlocking.php?id="+id+"&active="+active+"&block")
          .then(function(resp){

            $scope.getBlocking();
           

          });
        };

        $scope.newblock = function(){
          $http.post("api/StudentBlocking.php",{
            'remark'      : $scope.remark,
            'id'          : $scope.stid,
          }).then(function(resp){
            $scope.popup1.removeClass('launch');
            $scope.getBlocking();              
          },function(error){
          });
        };

        $scope.upload = function(){
 
          var fd = new FormData();
          var files = document.getElementById('file').files[0];
          fd.append('file',files);
        
          // AJAX request
          $http({
           method: 'post',
           url: 'api/upload.php?id='+$scope.students.student_id,
           data: fd,
           headers: {'Content-Type': undefined},
          }).then(function successCallback(resp) { 
            // Store response data
            $scope.response = resp.data;
            $scope.have = 0;
            if(resp.data ==='updated'){
              $scope.success.addClass('launch'); 
              setTimeout(function(){
                $scope.success.removeClass('launch'); 
              },2000);
          }

            $scope.getStudent($scope.students.student_id);
          });

        //   $http.post("api/upload.php",{
        //     'student_id'      : $scope.students.student_id,
        //     'photo'          : fd,
        //   }).then(function(resp){
        //     $scope.popup1.removeClass('launch');
        //     $scope.getBlocking();              
        //   },function(error){
        //   });
          
         };

         $scope.havephoto = function(){
           $scope.have = 1;
         };

         $scope.withdraw = function(id,strand,y,stud){


          $scope.sett= id;
          $scope.students.strand_id=strand;
          $scope.year=y;
          $scope.students.student_id = stud;

          $http.get("api/enrollmentSubject.php?removeall"+"&setting="+id+"&sid="+stud)
          .then(function(resp){
            
            $scope.createPayment();
           
          });



         };
    
        
         $scope.editSched = function(sched,name,strand,yearlvl,teacher){

           $scope.s_id = sched;
           $scope.sname = name;
           $scope.str = strand;
           $scope.ylevel = yearlvl;
           $scope.teacher_id = teacher;

            $scope.studentStrand();
            $scope.studentYearlvl();
            $scope.getTeachers();

          $scope.popup1.addClass('launch');
         };


         $scope.updateSched = function(){

          // $http.get("api/scheduleSubject.php?sched_id="+$scope.s_id+"&update")
          // .then(function(resp){
           
          // });
          alert()

          $http.post("api/scheduleSubject.php?update",{
            'sched_id'         : $scope.s_id,
            'name'             : $scope.sname,
            'strand'           : $scope.str,
            'year'             : $scope.ylevel,
            'teacher'          : $scope.teacher_id,
          }).then(function(resp){
            $scope.popup1.removeClass('launch');
            $scope.getSchedules();
          },function(error){
          });
         };

         $scope.getEnrollmentHistory = function(id){

          $http.get('api/Enrollment.php?e_history&student='+id)
          .then(function(resp){
            $scope.ehistory = resp.data;
          });

         };

         $scope.getEnrollmentTracker = function(id){

          $http.get('api/Enrollment.php?e_tracker&student='+id)
          .then(function(resp){
            $scope.ehistory = resp.data;
          });

         };


    });

