<?php
include "../_config/db.php";
session_start();
?>


<?php date_default_timezone_set('Asia/Singapore');?>
<?php
 $time = date('Y-m-d H:i:s');

?>


<?php

if(empty($_SESSION['username']))
{
    echo "
   <script>
   window.location = '../';
   </script>
    ";
}

else{



//USERS

$users="SELECT first_name , last_name , middle_name from users where employee_no = '{$_SESSION['username']}'";
$connect_user = mysqli_query($db,$users);
$results = mysqli_fetch_array($connect_user);

}



?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Font awesome icons -->
    <link rel="stylesheet" href="../_public/css/all.css">
    <!-- Css -->
    <link rel="stylesheet" href="../_public/css/efrolic.min.css">
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="../_public/css/registrar.css">
    
    <!-- button -->


    <link rel="stylesheet" href="../_public/css/icon.min.css">
    <link rel="stylesheet" href="../_public/css/checkbox.css">
    <link rel="stylesheet" href="../_public/css/datetimepicker.min.css">
 
    
    <!-- <script src="../_public/js/jquery.one.js"></script>
    <script src="../_public/js/datetimepicker.min.js"></script>
    <script src="../_public/js/moment.min.js"></script> -->
    <script src="../_public/js/checkbox.js"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />



</head>

    <body ng-app="regApp" ng-controller="mainController">


    <div class="e-container-fluid">
      <nav class="e-nav maroon">
        <img src="../_public/photos/logo.png" style="width:90px;height:auto;">
        <h6 class="text-white no-m">Midway Colleges
          <p class="text-white no-m">School Information Management Portal</p></h6>
          <i class="e-distribution"></i>
          <span class="px-3">
            <span class="text-center e-title no-p text-white">Registrar</span>
            <br>

            <span class="no-p text-white size-13">
              Hi! <strong> <?php echo $results['first_name']?> </strong>

              <input class="e-control" id="emailUser" type="text" value="<?php echo $results['email']?>" style="display:none;">
              <input class="e-control" id="IDUser" type="text" value="<?php echo $results['id']?>" style="display:none;">
              <a href="?logout" class="pl-3 text-dark text-bold size-11"><i class="fas fa-sign-out-alt"></i>  Logout</a>
            </span>
          </span>

      </nav>

          <nav class="e-container-fluid">
            <div class="align-end">
            <label for="e-menu" class="e-btn circle maroon small no-desktop"><i class="fas fa-bars text-white"></i></label>
          </div>
              <input type="checkbox" id="e-menu">
            <div class="e-menu">
              <!-- More content -->
              <a class="e-menu-item size-11" href="?home"><i class="fa fa-home text-primary"></i> Home</a>
              <a class="e-menu-item size-11" href="?schoolcourse"><i class="fa fa-home text-primary"></i> School & Course</a>
              <a class="e-menu-item size-11" href="?regsubject"><i class="fa fa-home text-primary"></i> Subjects</a>
              <a class="e-menu-item size-11" href="?programs"><i class="fa fa-home text-primary"></i> Programs</a>
              <a class="e-menu-item size-11" href="?teachers"><i class="fa fa-chalkboard-teacher text-primary"></i> Teachers</a>
              <a class="e-menu-item size-11" href="?encoding"><i class="fa fa-key text-primary"></i> Encoding</a>
              <a class="e-menu-item size-11" href="?builder"><i class="fa fa-key text-primary"></i> Schedule Builder</a>
              <a class="e-menu-item size-11" href="?building"><i class="fa fa-building text-primary"></i> Buildings</a>
              <a class="e-menu-item size-11" href="?block"><i class="fa fa-unlock-alt text-primary"></i> Blocking/Unblocking</a>
              <a class="e-menu-item size-11" href="?students"><i class="fa fa-users text-primary"></i> Students</a>
              <a class="e-menu-item size-11" href="?students_list"><i class="fa fa-users text-primary"></i> Students list</a>
              <a class="e-menu-item size-11" href="?strands"><i class="fa fa-chart-bar text-primary"></i> Reports</a>
              <a class="e-menu-item size-11" href="?register"><i class="fa fa-user-plus text-primary"></i> Registration</a>
              <!-- More content -->
            </div>
          </nav>

                <div class="row">

                <!-- route -->
                <?php include "php/route.php"; ?>
                <!-- route -->


                </div>

                <br><br>

                <div class="e-footer-bar maroon">
                  <p class = "text-white">©<a class="link text-white">2019 Midway Colleges Inc.</a> | Created and Developed by IT Department | All rights reserved</p>
                </div>
              </div>
            </body>
            </html>




 <?php


if(isset($_GET['logout'])){

    $up_data = " UPDATE users SET remember_token='0',updated_at=''  where employee_no='{$_SESSION['username']}'";
    mysqli_query($db,$up_data);

    echo "<script>
    window.location='../';
    </script>
    ";
    session_destroy();

}


?>


    <!--Angular-->

    <!--Controllers-->


    <!--
	ACV - Application Layer Interface , controller and view ..
	web system software interface by Dark Xseed 2017

    -->


     <script src="../_public/js/angular.this.js"></script>
     <script src="controller/dirPaginate.js"></script>
     <script src="controller/app.js"></script>
     <script src="controller/checklist-model.js"></script>
     <script src="../_public/js/datetimepicker.min.js"></script>
     <script src="../_public/js/checkbox.js"></script>
     <script src="../_public/js/moment.min.js"></script>
