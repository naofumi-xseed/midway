<?php

if(isset($_GET['strands']) ||  isset($_GET['sos']) || isset($_GET['sop']) || isset($_GET['sog']) || isset($_GET['soc']) || isset($_GET['sc']) || isset($_GET['scd'])|| isset($_GET['gc']) || isset($_GET['soe']) || isset($_GET['es'])|| isset($_GET['sl']) || isset($_GET['sls']))
{
    include "views/reports.php";
}

if(isset($_GET['students_list']))
{
    include "views/students_list.php";
}
else if(isset($_GET['students'])){

    include "views/students.php";
}
else if(isset($_GET['test']))
{
    include "views/testing.php";
}



else if(isset($_GET['stud']))
{
  
    include "views/student_info.php";
}


else if(isset($_GET['sos'])){
    include "views/sos.php";
}

else if(isset($_GET['sog'])){
    include "views/sog.php";
}

else if(isset($_GET['sop'])){
    include "views/sop.php";

}

else if(isset($_GET['soc'])){
    include "views/soc.php";

}

else if(isset($_GET['sc'])){
    include "views/sc.php";

}
else if(isset($_GET['gc'])){
    include "views/gc.php";

}
else if(isset($_GET['scd'])){
    include "views/scd.php";

}

else if(isset($_GET['strands'])){


    include "views/sum_strand.php";
}

else if(isset($_GET['soe'])){


    include "views/soe.php";
}

else if(isset($_GET['es'])){


    include "views/es.php";
}

else if(isset($_GET['sl'])){


    include "views/subject_list.php";
}

else if(isset($_GET['sls'])){


    include "views/subject_list_stud.php";
}

else if(isset($_GET['schoolcourse'])){


    include "views/school_course.php";
}


else if(isset($_GET['register'])){


    include "views/registration.php";
}

else if(isset($_GET['building'])){

    include "views/building.php";
}
else if(isset($_GET['block'])){

    include "views/student_blocking.php";
}

else if(isset($_GET['encoding'])){

    include "views/encoding.php";
}
   else if(isset($_GET['teachers'])){

    include "views/teachers_view.php";
}
else if(isset($_GET['builder'])){

    include "views/schedule_builder.php";
}
else if(isset($_GET['builder_sched'])){

    include "views/builder_schedule.php";
}
else if(isset($_GET['programs'])){

    include "views/programs.php";
}
else if(isset($_GET['regsubject'])){

    include "views/registrar_subjects.php";
}
else if(isset($_GET['curriculum_details'])){

    include "views/curriculum_details.php";
}
else if(isset($_GET['prospect'])){

    include "views/prospectus.php";
}
else if(isset($_GET['enroll'])){

    include "views/enrollment.php";
}
else if(isset($_GET['shift'])){

    include "views/shift.php";
}
else if(isset($_GET['cred']))
{
    include "views/credential.php";
}
else if(isset($_GET['rc'])){
    include "views/reportcard.php";
}
else if(isset($_GET['ehistory'])){
    include "views/enrollment_history.php";
}
else if(isset($_GET['tracker'])){
    include "views/enrollment_tracker.php";
}
else{
    include "views/home.php";
}


?>