
<div ng-init="fetchSubSched(<?php echo $_GET['builder_sched'];?>)" class="mt-3">
<!-- <a  ng-repeat="detail in details"    class="e-btn primary" style="font-size:12px; text-transform:uppercase" >
{{detail.code}}{{detail.sched_id}}
</a> -->
<nav class="e-tabs">
                  <ul>
                        <li><a class="e-btn inverted gray" style="border:none;" ><i class="fas fa-plus" style="margin-left:10px"> </i>Add Subject</a></li>
                        <li ng-repeat="detail in details" style="font-size:12px" ng-model="atab=schedId===detail.sched_id ? 'active' : 'n'"  class="{{atab}}" ><a href="" ng-click="loadDate(detail.sched_id)">{{detail.code}}</a></li>
                  </ul>
</nav>
</div>

<div class="e-col-12">
  <table class="e-table small" >
      <thead>
            <tr>
            <th>Subject Code</th>
            <th>Subject Item</th>
            <th>Hours</th>
            <th>Unit</th>
            <th>Section</th>
            <th>Schedule</th>
            </tr>
      </thead>
      <tbody>
      <tr ng-repeat="sched in subs[0].details">
      <td>{{sched.code}}</td>
      <td>{{sched.name}}</td>
      <td>{{year ==='1' || year ==='2' ? sched.hours : '0'}}</td>
      <td>{{year !=='1'  || year !=='2' ? sched.units : '0'}}</td>
      <td></td>
      <td>
      <table>
        <tr>
            <td>DAY</td><td>TIME</td><td>ROOM</td>
        </tr>
        <tr ng-repeat="detail in sched.schedule_subject">
            <td>{{detail.day}}</td><td>{{detail.start_time}} - {{detail.end_time}}</td><td>{{detail.room}}-{{detail.building}}</td><td>{{detail.teacher}}</td>
        </tr>
        <tr><td><button class="e-btn fa fa-plus inverted danger small"  ng-click="adding(sched.id)"></button></td></tr>
      </table>
      </td>

      <td ng-click="removeSub($index)"><button class="e-btn outlined sky" disabled>X</button></td>
      </tr>

    

      </tbody>
      

  </table>

  </div>


<div id="add" class="e-modal">
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">Add {{mtitle}}</p>
      <button type="button" ng-click="cancel()" class="e-delete">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    
    <div class="e-modal-body">
    <!--Content-->
    <form method="POST">
        <div class="e-cols">
                  <input type="hidden" name="id" ng-model="id">
                    <div class="e-col">
                    <b>BUILDING</b>
                    </div>
                    <div class="e-col">
                    <select class="e-control rounded" name="build" ng-model="build" ng-init="getBuildings()" ng-change="getRooms(build)" required>
                    <option value="">Select Building</option>
                    <option ng-repeat="building in buildings" value="{{building.id}}" style="text-transform:uppercase;">{{building.name}}</option>
                    </select>
                    </div>
                </div><br>
                <div class="e-cols">
                <div class="e-col">
                  <b>ROOM</b>
                    </div>
                    <div class="e-col mb-5">
                    <select class="e-control rounded" name="rm" ng-model="rm"  required>
                    <option value="">Select Room</option>
                    <option ng-repeat="room in rooms" value="{{room.id}}" style="text-transform:uppercase;">{{room.name}}</option>
                    </select>
                    </div>
                </div> <br>

                <div class="e-cols">
                <div class="e-col">
                   <b>TIME</b>
                    </div>
                    <div class="e-col">FROM
                    <!--<input type="number" class="form-control"  name="from" ng-model="from" required>-->
                            <div class='e-form-group date' id='datetimepicker3'>
                            <input type='text' class="e-control rounded" name="from" ng-model="from" placeholder="00:00" required>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                            </div>
                    </div>
                    <div class="e-col">TO
                   
                    <div class='input-group date' id='datetimepicker2'>
                            <input type='text' class="e-control rounded" name="to" placeholder="00:00"  ng-model="to" required>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                            </div>
                    </div>
                </div> <br>
                <div class="e-cols">
                <div class="e-col">
                  <b>Alternating</b>
                    </div>
                    <div class="e-col">
                   <input type="radio"  name="alt" ng-model="alt" value="1">
                   Day 1
                    </div>
                    <div class="e-col">
                   <input type="radio"  name="alt" ng-model="alt" value="2">
                   Day 2
                    </div>
                    <div class="e-col">
                   <input type="radio"  name="alt" ng-model="alt" value="0">
                   Regular
                    </div>
                </div> <br>

                <div class="e-cols">
                <div class="e-col">
                    <b>Day</b>
                    </div>
                          <div class="e-col">
                          <input type="radio"  name="day" ng-model="day" value="2">
                          MON
                            <input type="radio"  name="day" ng-model="day" value="3">
                          TUE                                                    
                            <input type="radio"  name="day" ng-model="day" value="4">
                          WED                         
                            <input type="radio"  name="day" ng-model="day" value="5">
                          THU                        
                            <input type="radio"  name="day" ng-model="day" value="6">
                          FRI                                                   
                            <input type="radio"  name="day" ng-model="day" value="7">
                          SAT                         
                            <input type="radio"  name="day" ng-model="day" value="1">
                          SUN
                          </div>
                    
                </div> <br>

                <div class="e-cols">
                <div class="e-col">
                 <b>TEACHER</b>
                    </div>
                    <div class="col-sm-4">
                    <select class="e-control rounded" name="teacher" ng-model="teach" ng-init="getTeachers()" required>
                    <option value="">Select Teacher</option>
                    <option ng-repeat="teacher in teachers" value="{{teacher.id}}" style="text-transform:uppercase;">{{teacher.last_name}}, {{teacher.first_name}} {{teacher.middle_name}}</option>
                    </select>
                    </div>
                </div> 
                
    

    <!--Content-->  
    </div>
    <footer class="e-modal-footer">
      <button class="e-btn inverted" ng-click="cancel()">Cancel</button>
      <button class="e-btn danger" ng-click="insertSched(schedId,build,rm,from,to,alt,day,teach,'<?php echo $_GET['setting'];?>')">Save changes</button>
    </footer>
  </div>
</div>













                <script type="text/javascript">
            $(function () {
                $('#datetimepicker3').datetimepicker({
                    format: 'HH:mm'
                });
            });
        </script>
    </div>
</div>

<script type="text/javascript">
            $(function () {
                $('#datetimepicker2').datetimepicker({
                    format: 'HH:mm'
                });
            });
        </script>
    </div>
</div>