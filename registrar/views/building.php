

<div ng-init="fetchBuilding()" style="text-transform:uppercase" >



<div class="e-cols">
  <div class="e-col-5 pt-1">
  <button class="e-btn btn primary" ng-click="insertBuilding('Add Building')"> ADD BUILDING</button>
    <ul class="e-list">
    <li class="e-list-item"  ng-repeat="build in buildings" ng-click="fetchRoom(build.id)">{{build.name}}</li>
    </ul>
  </div>

  <div class="e-col-5 pt-1">
  <div class="e-card primary" style="width: 100%;">
  <div class="card-body">
    <h3 class="card-title">{{buildinfo.name}}</h3>
    <p class="card-text"></p>
    <a class="e-btn btn sky gradient" ng-click="insertBuilding('Add Room')">Add Room</a>
    <a class="e-btn transparent" ng-click="updateBuild('Edit Building',buildinfo.id)">Edit</a>
  </div>
</div>
  
    <ul class="e-list">
  
    <li class="e-list-item"  ng-repeat="room in rooms">{{room.name}} <a class="align-end" ng-click="updateBuild('Edit Room',room.id)"><i class="fa fa-pen"></i></a></li>
    </ul>
  </div>



   
</div>


</div>



<div id="modals" class="e-modal" >
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">{{title}}</p>
  
      <button type="button" ng-click="cancelModal()" class="e-delete">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    <div class="e-modal-body">
    <!--Content-->
   <div class="e-cols">
        <div class="e-col-12" style="font-size:12px">
              <input type="text"  ng-model="name" class="e-control">
        </div>
    <!--Content-->  
    </div>
    <footer class="e-modal-footer">
      <button class="e-btn inverted" ng-click="cancelModal()">Cancel</button>
      <button ng-if="title==='Add Building'" class="e-btn danger" ng-click="insertBuild()">Save changes</button>

      <button ng-if="title==='Add Room'" class="e-btn danger" ng-click="insertBuild()">Save Room</button>

      <button ng-if="title==='Edit Building'" class="e-btn danger" ng-click="editBuild()">Update Building</button>

      <button ng-if="title==='Edit Room'" class="e-btn danger" ng-click="editBuild()">Update Room</button>
    </footer>
  </div>
</div>


