
<!-- php scripts if any -->
<?php include "views/student_tabs.php";?>

<h4 class="pl-3">Admission Requirements</h4>
<div ng-init="credential()" class="e-cols">
  <div class="e-col-6">
    <ul class="e-list">
      <li class="e-list-item small" ng-repeat="c in cred  | orderBy: 'id'  | limitTo:'8'">
        <div class="ui toggle checkbox">
          <input type="checkbox" name="public" ng-checked="c.value ===1"  ng-click="updateCred(c.field,c.value,'student_requirements')">
          <label style="text-transform:uppercase">{{c.field }}</label><br>
        </div>
      </li>
    </ul>
  </div>
  <div class="e-col-6">
  <ul class="e-list">
    <li class="e-list-item small" ng-repeat="c in cred  | orderBy: '-id'  | limitTo:'6'">
      <div class="ui toggle checkbox">
        <input type="checkbox" name="public2" ng-checked="c.value ===1"  ng-click="updateCred(c.field,c.value,'student_requirements')">
        <label style="text-transform:uppercase">{{c.field }}</label><br>
      </div>
    </li>
  </ul>
  </div>
</div>
<h4 class="pl-3">Training Requirements</h4>
<div ng-init="training()" class="e-cols">
  <div class="e-col-6">
  <ul class="e-list">
    <li class="e-list-item small" ng-repeat="t in train">
      <div class="ui toggle checkbox">
        <input type="checkbox" name="public" ng-checked="t.value ===1"  ng-click="updateCred(t.field,t.value,'student_training')">
        <label style="text-transform:uppercase">{{t.field }}</label><br>
      </div>
    </li>
  </ul>
  </div>
  <div class="e-col-6">
  </div>
</div>