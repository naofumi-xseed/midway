<?php

?>




<form method="POST">



<input type="hidden" ng-model="program" value="{{program}}" name="prog">

  <div class="e-cols  px-1 py-1">
      <div class="e-col  no-gap">
      <button  type="submit"  ng-click="clickMeSh()" ng-disabled="clickedSh" class="e-btn small" style="width:47%;background-color:gray;color:white" > Senior High
    </button>
    <button type="submit"  ng-click="clickMeCl()"  ng-disabled="clickedCl" class="e-btn small" style="width:47%;background-color:gray;color:white" > College 
    </button>
      </div>
      
      <div class="e-col   ml-1">
      <select name="sets" id="yo" ng-model="setting" class="e-control ml-1 rounded" ng-change="getSetting(setting)" style="width:240px; font-size:14px" >
                              <option value="{{set.id}}" ng-repeat="set in settings">{{set.description}}</option>
                        </select>
      </div>
  </div>
   
   
 
  
                        



<div class="e-cols">



    <div class="e-col-5">

        <table class="e-table style="font-size:11px">
        <thead>
            <tr>
                <th>Period</th>
                <th>FROM</th>
                <th>TO</th>
                <th>TYPE</th>
            </tr>
        </thead>

        <tbody ng-repeat="enc in encodingData">
        <tr>
            
            <td>
                {{ enc.grading_period_id === '1' ? 'Prelim' : ''}}
                {{ enc.grading_period_id === '2' ? 'Midterm' : ''}}
                {{ enc.grading_period_id === '4' ? 'Finals' : ''}}
            </td>

            <td>{{enc.from | limitTo : 10}}</td>
            <td>{{enc.to | limitTo : 10}}</td>
            
            

            <td>{{enc.type}}</td>
            
            <td>
            {{enc.active}}

                <button type="submit" ng-click="edit(enc,enc.id,enc.from,enc.to)" class="e-btn success">edit</button>
                <button  class="e-btn warning" ng-if="enc.from!=='0000-00-00 00:00:00' && enc.type!=='Dean' && enc.active==0" ng-click="generate(enc.id)">Generate</button>
                <!--<button class="btn btn-danger"  ng-if="enc.type!=='Dean' && subjects.length!==0" ng-click="closeEncoding()">Close</button>-->
         
            </td>
          
        </tr>
      
       
        </tbody>
        <tr ng-if="encodingData.length===0">
            <td><button type="submit" class="e-btn" ng-click="insertData()">Initialize Encoding</button></td>
        </tr>
      
       <?php

     
       ?>



        </table>

        </div>

        <div class="e-col-5" >
        
       
                      <table class="e-table  table-striped">
                        <tr>
                        
                      
                        <tr>
                            <td><input type="radio" ng-change="getSetting(setting)" ng-model="period" value="1">Prelim</td> 
                            <td><input type="radio" ng-change="getSetting(setting)" ng-model="period" value="2" ng-disabled="program===1">Midterm</td>
                            <td><input type="radio" ng-change="getSetting(setting)" ng-model="period" value="4">Finals</td>
                        </tr>
                      <tr>
                        <td>
                       
                        </td>
                      </tr>
                     </table>
                        
                      
    
                </form>

        </div>

            <div> 
            
            </div>
            {{tabnow}}
          
           
            <div class="e-col-11" ng-if="period!=null && program!=null">
               <input type="text" ng-model="search" placeholder="search" class="e-control rounded" style="width: 30%">

               <dir-pagination-controls max-size="10" direction-links="true" boundary-links="true" style="font-size: 10px" class="pull-right">
    </dir-pagination-controls>
              <!--Its better to put init than to direct function-->
              <div class="e-col">
              <a ng-repeat="stat in statuses" class="e-btn primary" style="font-size:10px; text-transform:uppercase;margin-top:19px;" ng-click="tab(stat.id)">
                {{stat.name}}
                </a>
           
              </div>
           
            <table class="e-table table-striped" style="text-transform: uppercase;">
              <thead> 
                <tr>  <td>      </td></tr>
                 <tr>
                 <th ng-if="tabnow==2 || tabnow==8">
                 
                 <button ng-if="mark===''" ng-click="checkAll()" class="e-btn">Mark all</button>
                 <button ng-if="mark!==''" ng-click="uncheckAll()" class="e-btn">Deselect</button>
                 <button ng-if="selection.subjects.length>0 && tabnow==2" ng-click="closeEncoding()" class="e-btn danger">Close</button>
                 <button ng-if="selection.subjects.length>0 && tabnow==8" ng-click="openEncoding()" class="e-btn success">open</button>
                 </th>
                    <th>Teacher</th>
                    <th>Subject</th>
                    <th>Section</th>
                    <th>Remarks</th>
                 </tr>    
               </thead>  

        <tbody> 
          <tr dir-paginate="subject in subjects | filter : search | itemsPerPage:10 ">
          <td ng-if="tabnow==2 || tabnow==8">
          <input type="checkbox" checklist-model="selection.subjects" checklist-value="subject.sdes_id" ng-model="checked" checklist-before-change="selection.subjects.length > 1 || checked">
          </td>
            <td><a href=""  ng-click="getSched(subject.schedule_detail_id,subject.last_name,subject.first_name,subject.code,subject.sub_d,subject.sec_name)">{{subject.last_name}}, {{subject.first_name}} {{subject.middle_name}}</a></td>
            <td>{{subject.code}} - {{subject.sub_d}}</td>
            <td>{{subject.sec_name}}</td>
            <td>{{subject.remarks}}</td>
            <td ng-if="tabnow==5">
                  <button class="e-btn" ng-click="verify(subject.schedule_detail_id)">Verify</button>
                  <button class="e-btn" ng-click="revise(subject.schedule_detail_id)">Revised</button>
            </td>
            <td ng-if="tabnow==8">
                  <button class="e-btn" ng-click="open(subject.schedule_detail_id)">Open</button>   
            </td>
            
          </tr>
        <tbody> 

            
              </table>
  </div>


</div>


<!-- 

<div class="modal fade" id="modals" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
    
         <table class="table">
             <tr>
                  <!--<td><input type="hidden" ng-model="editable"></td>-->
                  <!-- <td><input type="hidden" ng-model="id"></td>
                  <td><input type="text" class="form-control" name="from" style="width: 170px" ng-model="from"></td>
                  <td><input type="text" class="form-control" name="to" style="width: 170px" ng-model="to"></td>
             </tr>
         </table> -->
         

  
        <!-- </div>
      <div class="modal-footer">
        <button type="button" ng-click="cancel()" class="btn btn-secondary pull-left" style="background-color:gray;color:white;">Close</a>
      
        <button type='submit' id="rel" class="btn btn-success" value='Login' ng-click="save()"><i class="fa fa-check"></i> save</button>
      </div>
    </div>
  </div>
</div> --> 

<div id="modals" class="e-modal" >
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">Edit Schedule</p>
  
      <button type="button" ng-click="cancelModal()" class="e-delete">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    <div class="e-modal-body">
    <!--Content-->
    <table class="table">
             <tr>
                  <!--<td><input type="hidden" ng-model="editable"></td>-->
                  <td><input type="hidden" ng-model="id"></td>
                  <td><input type="text" class="e-control" name="from" style="width: 170px" ng-model="from"></td>
                  <td><input type="text" class="e-control" name="to" style="width: 170px" ng-model="to"></td>
             </tr>
         </table>

    </div>
    <footer class="e-modal-footer">
      <button class="e-btn inverted" ng-click="cancelModal()">Cancel</button>


      <button type='submit' id="rel" class="e-btn success" ng-click="save()"><i class="fa fa-check"></i> save</button>

    </footer>
  </div>
</div>





<!-- <div class="modal fade" id="grades" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content col-lg-14">
      <div class="modal-header" style="text-transform:uppercase;">
        <center><h5 class="modal-title" id="exampleModalLongTitle"><b>{{scode}} - {{sd}}</b></h5>
        <p>{{sname}} - {{lname}} {{fname}}</p>
        </center>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
    
         <table class="table table-striped" style="text-transform:uppercase;font-size:12px">
            <thead>
              <tr>
                  <th>Stud NO</th>
                  <th>Name</th>
                  <th>Prelim</th>
                  <th>Midterm</th>
                  <th>Finals</th>
                  <th>GRADE</th>
              </tr>
            </thead>


            <tbody>
             <tr ng-repeat="grade in grades">
                  <!--<td><input type="hidden" ng-model="editable"></td>-->
                  <!-- <td>{{grade.student_no}}</td>
                  <td>{{grade.last_name}}, {{grade.first_name}} {{grade.middle_name}}</td>
                  <td>{{grade.term1}}</td>
                  <td>{{grade.term2}}</td>
                  <td>{{grade.term4}}</td>
                  <td>{{grade.final}}</td>

             </tr>
            </tbody>
         </table>
         

  
        </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-secondary pull-left" style="background-color:gray;color:white;">Close</a> -->
      
       <!-- <button type='submit' id="rel" class="btn btn-success" value='Login' ng-click="save()"><i class="fa fa-check"></i> save</button>-->
      <!-- </div>
    </div>
  </div>
</div> -->

<div id="grades" class="e-modal" >
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
    <center><h5 class="e-modal-title" id="exampleModalLongTitle"><b>{{scode}} - {{sd}}</b></h5>
        <p>{{sname}} - {{lname}} {{fname}}</p>
      </center>
  
      <button type="button" ng-click="cancelModal()" class="e-delete">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    <div class="e-modal-body">
  
    <table class="e-table" style="text-transform:uppercase;font-size:12px">
            <thead>
              <tr>
                  <th>Stud NO</th>
                  <th>Name</th>
                  <th>Prelim</th>
                  <th>Midterm</th>
                  <th>Finals</th>
                  <th>GRADE</th>
              </tr>
            </thead>


            <tbody>
             <tr ng-repeat="grade in grades">
                  <!--<td><input type="hidden" ng-model="editable"></td>-->
                  <td>{{grade.student_no}}</td>
                  <td>{{grade.last_name}}, {{grade.first_name}} {{grade.middle_name}}</td>
                  <td>{{grade.term1}}</td>
                  <td>{{grade.term2}}</td>
                  <td>{{grade.term4}}</td>
                  <td>{{grade.final}}</td>

             </tr>
            </tbody>
         </table>
 

    </div>
    <footer class="e-modal-footer">
      <button class="e-btn inverted" ng-click="cancelModal()">Cancel</button>


 

    </footer>
  </div>
</div>
