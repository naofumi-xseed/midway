<div class="e-container" >
      <div class="e-cols mt-3">

              <div class="e-col-2">
              <a class="e-btn anime danger">Create Program</a>
              </div>
        
              <div class="e-col-7">
              </div>

              <div class="e-col-3" ng-init="getProg()">
                 <div class="e-form-group unified width_inherit">
                      <div class="e-control-helper">
                         <i class="far fa-clipboard text-danger"></i>
                       </div>
                          <select class="e-select" ng-model="programm" ng-change="getTrack(programm)">
                              <option value="" selected>Select a Program</option>
                              <option ng-repeat="prog in progs" value="{{prog.id}}">{{prog.name}}</option>
                          </select>
                 </div>
              </div>
</div>


<div class="e-cols">
        <div class="e-col-4">
            <div class="e-card" style="width: 24rem;" ng-if="programm>0">
                 <!--Card image-->
                 <img src="../_public/photos/cover.jpg" />
                 <!--Card body-->

                <div class="card-body" ng-model="title=programm==='1' ? 'Senior High' : 'College'">
                  <h3 class="card-title">{{title}}<a class="e-btn inverted danger" ng-click="adding('track')" style="float:right; border:none;"><i class="fas fa-plus"></i></a></h3>
                  <p class="card-text">List of tracks : </p>
                <button class="e-btn plane inverted danger" ng-repeat="track in tracks" style="text-transform:uppercase" ng-click="Trace(track.id)">{{track.code}} - {{track.name}}  </button>
                </div>
           </div>
        </div>
             
              <div class="e-col-8 mt-3">
              <nav class="e-tabs">
                  <ul>
                        <li ng-if="strands.length>0"><a class="e-btn inverted gray" style="border:none;" ng-click="adding('strand')"><i class="fas fa-plus" style="margin-left:10px"> </i></a></li>
                        <li ng-repeat="strand in strands" style="font-size:12px" ng-model="atab=showadd===strand.id ?  'active' : 'n'" class="{{atab}}"><a href="" ng-click="getStrandId(strand.id)">{{strand.code}}</a></li>
                  </ul>
              </nav>
        
            <br>
           
            <div class="e-cols">
                  <div class="e-card primary e-col-6 mb-1" style="width: 20rem;" ng-repeat="curricula in curriculas">
                        <a href="" style="float:right;" class="e-btn warning"><i class="fa fa-edit"></i></a>
                          <div class="card-body" ng-model="status=curricula.is_active==='0' ? '1' : '0'">
                               <a href="?curriculum_details={{curricula.cur_id}}&prog={{program}}" ng-click="curriculumDetails(curricula.name)" style="text-transform:uppercase; font-size:14px;"><i class="fa fa-scroll"></i><b> {{curricula.name}}</b></a>
                                    <p class="card-text">{{curricula.year}} - {{curricula.year}}</p>
                                      <a id="" class="e-btn  fullwidth {{status === '1' ? 'success' : 'danger'}}"  ng-click="activateCur(curricula.cur_id,status)">{{status === '1' ? 'Activate' : 'Deactivate' }}</a>                                     
                          </div>
                  </div>                                                
                          <div ng-if="showadd" class="e-col-5 centered" ><a class="e-btn circle gray" ng-click="adding('syear')"><i class="fas fa-plus"></i></a></div>
                        
            </div>
         
              </div>
  </div>
</div>


<!--
    danger
    inverted danger
-->


<div id="add" class="e-modal">
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">Add {{mtitle}}</p>
      <button type="button" ng-click="cancel()" class="e-delete">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    <div class="e-modal-body">
    <!--Content-->
    <form class="rounded" ng-model="opac=mtitle!=='syear' ? '1' : '0'"><!-- Here -->
  <div class="e-form-group">
    <label class="e-label">Code</label>
    <input class="e-control" type="text" ng-model="code" placeholder="Department Code">
  </div>

  <div class="e-form-group"  >
    <label class="e-label">Name</label>
    <input class="e-control" type="text" ng-model="name" placeholder="Department Track">
  </div>

  <div class="e-form-group" ng-init="getSy()">
    <label class="e-label">Shool Year</label>
    <select name="" class="e-control rounded" ng-model="sy" ng-disabled="opac==1">
      <option value="" selected>Select School Year</option>
      <option ng-repeat="syear in syears" value="{{syear.id}}">{{syear.year}} - {{syear.year}}</option>
    </select>
  </div>
</form>

    <!--Content-->  
    </div>
    <footer class="e-modal-footer">
      <button class="e-btn inverted" ng-click="cancel()">Cancel</button>
      <button class="e-btn danger" ng-click="insert('track')">Save changes</button>
    </footer>
  </div>
</div>