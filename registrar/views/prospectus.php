
<?php
include "views/student_tabs.php";
?>
<br>
<div>
<center>
<p style="text-transform:uppercase; font-size:14px"><b style="font-size:22px">MIDWAY</b> <br>
BITAS, MAHARLIKHA HIGHWAY, CABANATUAN CITY</p>
<h4 style="text-transform:uppercase; font-size:14px">{{curr_header[0].name}}</h4>
{{curr_header[0].year}}<br>
</center><br><br><br>
</div>

<div class="e-container" ng-repeat="detail in curriculums">
        <center><label style="text-transform:uppercase"><b>{{detail.year_name}}</b></label></center>
        <div class="e-col-12" ng-repeat="term in detail.terms">
                <label style="text-transform:uppercase">{{term.term_name}}</label>                
            <table class="e-table e-x shadow-2">
                <thead> 
                    <tr style="text-transform:uppercase; font-size:13px;">
                        <th>Subject Code</th>
                        <th>Descriptive Title</th>
                        <th>Subject Type</th>
                        <th>Unit</th>
                        <th>LEC Hours</th>
                        <th>LAB</th>
                        <th>Weeks</th>
                        <th>TOTAL LEC HOURS</th>
                        <th>FINAL GRADE</th>
                        <th style="text-decoration:regular;">STATUS</th>
                    </tr>  
                </thead>
                <tbody>
                    <tr ng-repeat="subject in term.subjects" style="text-transform:uppercase; font-size:12px;">
                        <td>{{subject.code}}</td>
                        <td>{{subject.descriptive_title}}</td>
                        <td>{{subject.subtype_name}}</td>
                        <td>{{subject.units}}</td>
                        <td>{{subject.lecture_hours}}</td>
                        <td>{{subject.lab_hours}}</td>
                        <td>{{subject.weeks}}</td>
                        <td>{{subject.weeks * subject.lecture_hours}}</td>
                        <td ng-repeat="sub_e in subject_enrolled" ng-if="sub_e.subject_id===subject.subject_id">{{sub_e.final}}</td>
                        <td ng-repeat="sub_e in subject_enrolled" ng-if="sub_e.subject_id===subject.subject_id">Enrolled</td>
                    </tr>
                </tbody>
            </table>
            </div>
</div>


<div id="add" class="e-modal">

  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title"></p>
      <button type="button" ng-click="cancel()" class="e-delete">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>

       <!--Content add-->
       {{mtitle}}
    <div class="e-modal-body" >
 
    <form method="POST"  ng-init="getSubjectParent('<?php echo $_GET['prog'];?>','prog')">
        

        

        <select name="" class="e-control rounded wt-10" ng-model="sub_parent" id="" ng-change="getSubjects(sub_parent)">
        <option value="">Select Parent Subject</option>
        <option ng-repeat="sparent in subject_parents" value="{{sparent.sub_id}}" >{{sparent.sp_name}}</option>
        </select>

        <select name="" class="e-control rounded wt-10" ng-model="sub" id="">
        <option value="">Select Subject</option>
        <option ng-repeat="subj in stud_subjects" value="{{subj.id}}">{{subj.code}} - {{subj.name}}</option>
        </select>

        <select name="" class="e-control rounded wt-10" ng-model="sub_year" id="" ng-init="studentYearlvl()">
        <option value="">Select Year Level</option>
        <option ng-repeat="year in years" value="{{year.id}}">{{year.in_word}}</option>
        </select>

        <select name="" class="e-control rounded wt-10" ng-model="sub_term" id="" ng-init="getTerms()">
        <option value="">Select Terms</option>
        <option ng-repeat="term in terms" value="{{term.id}}">{{term.in_word}}</option>
        </select>        


    </div>


    <footer class="e-modal-footer">
      <button class="e-btn inverted" ng-click="cancel()">Cancel</button>
      <button class="e-btn danger" ng-click="insertCurriculumSubject()">Save changes</button>
    </footer>
  </div>
</div>


<div id="prereq" class="e-modal">
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">Prerequisites</p>     
      <button class="e-btn danger small rounded">Add</button>
      
      <select type="text" ng-model="giver" class="e-control rounded" style="width:180px;height:auto">
      <option value="">Select Parent Subject</option>
      
      <option ng-repeat="subject_p in subject_parents | filter : searchsub" value="{{subject_p.sub_id}}">{{subject_p.code}}</option>
      </select>
      <br>
      <input type="text" ng-model="searchsub" placeholder="Search" class="e-control " style="width:150px;height:21px">
      <button type="button" ng-click="cancelPr()" class="e-delete">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    
    <div class="e-modal-body">
    <!--Content-->
    <ul>
    <li ng-repeat="prerequisite in subject_prerequisites" style="font-size:11px">
            <b>{{prerequisite.name}}
              <!-- <i class="fa fa-times" ng-click="deletePrerequisite(prerequisite.prerequisite_subject_parent_id)" style="cursor:pointer;"></i> -->
            </b>
        <ol>
            <li ng-repeat="prerequisite_subject in subject_from_prerequisite | filter :  { sub_pid : prerequisite.prerequisite_subject_parent_id}" class="pl-5" style="text-decoration:italic;font-size:10px;">
            - {{prerequisite_subject.sub_name}}
            </li>
        </ol>
    </li>
    </ul>
    </div>
    
  </div>
</div>

