<div class="e-container  e-x shadow-5 pt-1">
  <h3 class="text-bold centered">Registration Form</h3>
  <div class="e-cols pt-3">
    <div class="e-col align-end"></div>
    <div class="e-col-3 align-end">
      <div class="e-form-group unified">

        <input class="e-control" type="text" placeholder="..." ng-model="search" >
        <button type="button" class="e-btn inverted primary" ng-click="getAdequate(search)">Search</button>
      </div>
    </div>
</div>


<div class = "e-cols" >

    <div class="e-card blue-gradient e-col-4 pb-3" style="width: 20rem;"  ng-repeat="ade in adequates">
    <div class="card-body">
    <center>
    <h3 class="card-title text-white">
      {{ade.last_name}}, {{ade.first_name}} {{ade.middle_name}}
    </h3>
    </center>
    <h5 class="centered p-3 text-white">ADEQUATE</h5>
    <div class="centered">
    <a class="e-btn transparent" ng-click="fetchPreReg(ade.id)">Register</a>
    </div>
    </div>
    </div>
</div>

<?php
if ($_GET['register']){
$sql = "SELECT * FROM `preregistration_info` where id = '{$_GET['register']}'";
$result = $db->query($sql);
while($row = $result->fetch_assoc()){
  $first_name             = $row['first_name'];
  $middle_name            = $row['middle_name'];
  $last_name              = $row['last_name'];
  $suffix                 = $row['suffix'];
  $email                  = $row['email'];
  $LRN                    = $row['reference_no'];
  $contact_no             = $row['contact_no'];
  $sex                    = $row['sex'];
  $birthday               = $row['birthday'];
  $complete_address       = $row['complete_address'];
  $birth_place            = $row['birth_place'];
  $region                 = $row['region'];
  $province               = $row['province'];
  $city                   = $row['city'];
  $barangay               = $row['barangay'];
  $street_info            = $row['street_info'];
 $track_id               = $row['track_id'];
  $strand_course          = $row['strand_course'];
  $program_id             = $row['program_id'];
  $setting_id             = $row['setting_id'];
  $is_transferee          = $row['is_transferee'];
  $enrollment_status      = $row['enrollment_status'];
}


$sql2 = "SELECT * FROM `preregistration_supporting_info` where student_id = '{$_GET['register']}'";
$result2 = $db->query($sql2);
while($row = $result2->fetch_assoc()){
  $mother_tongue          = $row['mother_tongue'];
  $civil_status           = $row['civil_status'];
  $fathers_name           = $row['fathers_name'];
  $father_occupation      = $row['father_occupation'];
  $mothers_name           = $row['mothers_name'];
  $mothers_occupation     = $row['mothers_occupation'];
  $guardian_name          = $row['guardian_name'];
  $guardian_contact       = $row['guardian_contact'];
  $guardian_relationship  = $row['guardian_relationship'];
  $religion               = $row['religion'];
}

  $sql3                   = "SELECT * FROM `preregistration_academic_info` where student_id = '{$_GET['register']}'";
  $result3                = $db->query($sql3);
  $sql4                   = "SELECT acad_type FROM `preregistration_academic_info` where student_id = '{$_GET['register']}'";
  $result4                = $db->query($sql4);
  $acadTry                = $result4->fetch_assoc();

echo'<form class="e-cols" id="formRegistration">
    <div class="e-cols">
      <div class="e-col-3">
        <select class="e-select width_inherit" ng-init="getProg()" ng-model="prog"   id="program" ng-click="getTrack(prog)" name="program" onChange="ProgramCheck(this)">
          <option value="" disabled="" class="" selected="selected">Select Program</option>
          <option ng-repeat = "program in progs" value = "{{program.id}}">{{program.name}}</option>
        </select>
      </div>
      <div class="e-col"></div>
      <div class="e-col-3">
      <select class="e-select width_inherit" ng-init="studentYearlvl()" id="year_level" name="year_level">
        <option value="" disabled="" class="" selected="selected">Select Year Level</option>
        <option ng-repeat = "yearlevel in years" value = "{{yearlevel.id}}">{{yearlevel.in_word}}</option>
      </select>
      </div>
      <div class="e-col-3">
        <select class="e-select width_inherit" ng-init="getSelectSetting()" id="setting" name="setting">
          <option value="" disabled="" class="" selected="selected">Select Settings</option>
          <option ng-repeat = "setting in settings" value = "{{setting.id}}">{{setting.description}}</option>
        </select>
      </div>
    </div>
    <div class="e-col-12 e-form-group">
      <h4 class="e-label text-primary">Personal Information</h4>
    </div>
    <input class="e-control" type="text" id="student_id" name="student_id" value="'.$_GET['register'].'" style="display:none;" >


      <input class="e-control" type="text" id="academicInfo" name="academicInfo" value="'.$acadTry['acad_type'].'" style="display:none;">

    <div class="e-col-3 e-form-group">
      <label class="e-label">First Name</label>
      <input class="e-control" type="text" id="first_name" name="first_name" value="'.$first_name.'">
    </div>

    <div class="e-col-3 e-form-group">
      <label class="e-label">Middle Name</label>
      <input class="e-control" type="text" id="middle_name" name="middle_name" value="'.$middle_name.'">
    </div>

    <div class="e-col-3 e-form-group">
      <label class="e-label">Last Name</label>
      <input class="e-control" type="text" id="last_name" name="last_name" value="'.$last_name.'">
    </div>

    <div class="e-col-3 e-form-group">
      <label class="e-label">Suffix Name</label>
      <input class="e-control" type="text" id="suffix_name" name="suffix_name" value="'.$suffix.'">
    </div>

    <div class="e-col-12 e-form-group">
      <label class="e-label">Complete Address</label>
      <input class="e-control unselectable" type="text" readonly id="completeAddress" name="completeAddress">
    </div>

    <div class="e-col e-form-group" >
      <label class="e-label">Region</label>';



    echo  '<select class="e-select width_inherit cAddress_Region" ng-init="getRegion()" ng-model="student.region" id="cAddress_Region" name="cAddress_Region" value="{{student.region.id}}" ng-options="region as region.regDesc for region in regions track by region.id" >
      </select>
    </div>
    <div class="e-col e-form-group">
      <label class="e-label">Province</label>
      <select class="e-select width_inherit" ng-change="getProvince()" ng-model="student.province" value="{{student.province.id}}" ng-options="province as province.provDesc for province in provinces | filter: student.region.regCode track by province.id" id="cAddress_Province" name="cAddress_Province">
      </select>
    </div>

    <div class="e-col e-form-group">
      <label class="e-label">City/Municipality</label>
      <select class="e-select width_inherit" ng-init="getCityMunci()" ng-model="student.citymunc" value="{{student.citymunc.id}}" ng-options="citymunci as citymunci.citymunDesc for citymunci in citymuncis | filter: student.province.provCode track by citymunci.id" id="cAddress_CityMunc" name="cAddress_CityMunc">
      </select>
    </div>

    <div class="e-col e-form-group">
      <label class="e-label">Barangay</label>
      <select class="e-select width_inherit" ng-init="getBrngys()" ng-model="student.brngy" value="student.brngy.id" ng-options="brngy as brngy.brgyDesc for brngy in brngys | filter: student.citymunc.citymunCode track by brngy.id" id="cAddress_Barangay" name="cAddress_Barangay">
      </select>
    </div>

    <div class="e-col e-form-group">
      <label class="e-label">No./Blk/Lot/Flr/Bldg/St.</label>
      <input class="e-control" type="text" id="cAddress_Info" name="cAddress_Info" value="'.$street_info.'">
    </div>

    <div class="e-col-12 e-form-group">
      <h4 class="e-label text-primary">General Information</h4>
    </div>

    <div class="e-col-6 e-form-group">
      <label class="e-label">E-Mail</label>
      <input class="e-control" type="text" id="email" name="email" value="'.$email.'">
    </div>

    <div class="e-col-6 e-form-group">
      <label class="e-label">Contact No.</label>
      <input class="e-control" type="text" id="contact" name="contact" value="'.$contact_no.'">
    </div>

    <div class="e-col-4 e-form-group">
      <label class="e-label">Date of Birth</label>
      <input class="e-control" type="date" id="birthdate" name="birthdate" value="'.$birthday.'">
    </div>

    <div class="e-col-4 e-form-group">
      <label class="e-label">Age</label>
      <input class="e-control" type="text" id="age" name="age" value="">
    </div>

    <div class="e-col-4 e-form-group">
      <label class="e-label">Sex</label>
      <select class="e-select width_inherit" id="sex" name="sex">
        <option value="" disabled="" class="" selected="selected">Select Sex</option>
        <option value ="1">Male</option>
        <option value ="0">Female</option>
      </select>
    </div>

    <div class="e-col-12 e-form-group">
      <label class="e-label">Place of Birth</label>
      <input class="e-control" type="text" readonly id="PlaceOfBirth" name="PlaceOfBirth" value="'.$birth_place.'">
    </div>

    <div class="e-col-6 e-form-group">
      <label class="e-label">Province</label>
      <select class="e-select width_inherit" ng-model="birthdate.province" value="{{birthdate.province.id}}" ng-options="province as province.provDesc for province in provinces track by province.id" id="pAddress_Province" name="pAddress_Province">
      </select>
    </div>

    <div class="e-col-6 e-form-group">
      <label class="e-label">City/Municipality</label>
      <select class="e-select width_inherit" ng-model="birthdate.citymunc" value="{{birthdate.citymunc.id}}" ng-options="citymunci as citymunci.citymunDesc for citymunci in citymuncis | filter: birthdate.province.provCode track by citymunci.id" id="pAddress_CityMunc" name="pAddress_CityMunc" disabled>
      </select>
    </div>

    <div class="e-col-4 e-form-group">
      <label class="e-label">Religion</label>
      <select class="e-select width_inherit" ng-init="getReligion()" ng-model = "rel" id="religion" name="religion">
        <option value="" disabled="" class="" selected="selected">Select Religion</option>
        <option ng-repeat = "religion in religions" value = "{{religion.id}}" >{{religion.description}}</option>
      </select>
    </div>

    <div class="e-col-4 e-form-group">
      <label class="e-label">Mother Tongue</label>
      <input class="e-control" type="text" id="mother_tongue" name="mother_tongue" value="'.@$mother_tongue.'">
    </div>

    <div class="e-col-4 e-form-group">
      <label class="e-label">Civil Status</label>
      <select class="e-select width_inherit"  id="civilStatus" name="civilStatus">
        <option value="" disabled="" class="" selected="selected">Select Civil Status</option>
        <option value = "1">Single</option>
        <option value = "2">Married</option>
      </select>
    </div>

    <div class="e-col-6 e-form-group">
      <label class="e-label">Father\'s Name</label>
      <input class="e-control" type="text" id="fathers_name" name="fathers_name"  value="'.@$fathers_name.'">
    </div>

    <div class="e-col-6 e-form-group">
      <label class="e-label">Father\'s Occupation</label>
      <select class="e-select width_inherit"  ng-init="getOccupation()" id="fathers_occupation" name="fathers_occupation">
        <option value="" disabled="" class="" selected="selected">Select Occupation</option>>
        <option class= "text-uppercase" ng-repeat = "occupation in occupations" value = "{{occupation.id}}">{{occupation.description}}</option>
      </select>
    </div>

    <div class="e-col-6 e-form-group">
      <label class="e-label">Mother\'s Name</label>
      <input class="e-control" type="text" id="mothers_name" name="mothers_name"  value="'.@$mothers_name.'">
    </div>

    <div class="e-col-6 e-form-group">
      <label class="e-label">Mother\'s Occupation</label>
      <select class="e-select width_inherit"  ng-init="getOccupation()" id="mothers_occupation" name="mothers_occupation">
        <option value="" disabled="" class="" selected="selected">Select Occupation</option>>
        <option class= "text-uppercase" ng-repeat = "occupation in occupations" value = "{{occupation.id}}">{{occupation.description}}</option>
      </select>
    </div>

    <div class="e-col-4 e-form-group">
      <label class="e-label">Parent/Guiardian</label>
      <input class="e-control" type="text" id="parent_guardian" name="parent_guardian" value="'.@$guardian_name.'">
    </div>

    <div class="e-col-4 e-form-group">
      <label class="e-label">Relationship</label>
      <select class="e-select width_inherit"  ng-init="getRelationship()" id="relationship" name="relationship">
        <option value="" disabled="" class="" selected="selected">Select Relationship</option>>
        <option class= "text-uppercase" ng-repeat = "relationship in relationships" value = "{{relationship.id}}">{{relationship.description}}</option>
      </select>
    </div>

    <div class="e-col-4 e-form-group">
      <label class="e-label">Parent/Guiardian\'s Contact No.</label>
      <input class="e-control" type="text"  id="p_guardian_contact" name="p_guardian_contact" value="'.@$guardian_contact.'">
    </div>

    <div class="e-col-12 e-form-group" id="LRNdiv" hidden>
      <h4 class="e-label text-primary">Learner"s Reference Number (LRN)</h4>
      <br>
      <label class="e-label">LRN</label>
      <input class="e-control" type="text" id="LRN" name="LRN" value="'.$LRN.'">
    </div>

    <div class="e-col-12 e-form-group">
      <h4 class="e-label text-primary">Academic Information</h4>
    </div>';


    while($row = $result3->fetch_assoc()){
      $acad_type = $row['acad_type'];
      $edu_stage = $row['edu_stage'];
      $school_name = $row['school_name'];
      $date_of_completion = $row['date_of_completion'];
      $school_address= $row['school_address'];
      $school_region = $row['school_region'];
      $school_province= $row['school_province'];
      $school_city = $row['school_city'];
      if($acad_type == "SHS"){
        echo '<!-- SHS -->
         <div id="SHSForm" class="e-cols" >';
        if($edu_stage == "Elem"){
          echo '<script>
          $(function() {
            setTimeout(function(){
              $("#SHS_elem_school_region").val("'.$school_region.'").change();
              $("#SHS_elem_school_prov").val("'.$school_province.'").change();
              $("#SHS_elem_school_citymunc").val("'.$school_city.'").change();
            }, 2000);
          });
          </script>';
      echo '<div class="e-cols">
        <div class="e-col-12 e-form-group">
          <h5 class="e-label">ELEMENTARY</h5>
        </div>
        <div class="e-col-6 e-form-group">
          <label class="e-label">School Name</label>
          <input class="e-control" type="text"  id="SHS_elem_school_name" name="SHS_elem_school_name" value="'.$school_name.'">
        </div>
        <div class="e-col-6 e-form-group">
          <label class="e-label">Month/Yr of Completion</label>
          <input class="e-control" type="date" placeholder="Date" id="SHS_elem_school_comp" name="SHS_elem_school_comp" value="'.$date_of_completion.'">
        </div>
        <div class="e-col-12 e-form-group">
          <label class="e-label">School Address</label>
          <input class="e-control ESchoolAddress" type="text"  readonly id="SHS_elem_school_add" name="SHS_elem_school_add">
        </div>
        <div class="e-col-4 e-form-group" >
          <label class="e-label">Region</label>
          <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Region" ng-model="ES.region" id="SHS_elem_school_region" name="SHS_elem_school_region" value="{{ES.region.id}}" ng-options="region as region.regDesc for region in regions track by region.id" >
          </select>
        </div>

        <div class="e-col-4 e-form-group">
          <label class="e-label">Province</label>
          <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Province" ng-model="ES.province" value="{{ES.province.id}}" ng-options="province as province.provDesc for province in provinces | filter: ES.region.regCode track by province.id" id="SHS_elem_school_prov" name="SHS_elem_school_prov"  disabled>
          </select>
        </div>

        <div class="e-col-4 e-form-group">
          <label class="e-label">City/Municipality</label>
          <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_CityMunc" ng-model="ES.citymunc" value="{{ES.citymunc.id}}" ng-options="citymunci as citymunci.citymunDesc for citymunci in citymuncis | filter: ES.province.provCode track by citymunci.id" id="SHS_elem_school_citymunc" name="SHS_elem_school_citymunc" disabled>
          </select>
        </div>
      </div>';
    }else{
      echo '<script>
      $(function() {
        setTimeout(function(){
          $("#SHS_jhs_school_region").val("'.$school_region.'").change();
          $("#SHS_jhs_school_prov").val("'.$school_province.'").change();
          $("#SHS_jhs_school_citymunc").val("'.$school_city.'").change();
        }, 2000);
      });
      </script>';
      echo '<div class="e-cols">
        <div class="e-col-12 e-form-group">
          <h5 class="e-label">Junior High School</h5>
        </div>
        <div class="e-col-6 e-form-group">
          <label class="e-label">School Name</label>
          <input class="e-control" type="text"  id="SHS_jhs_school_name" name="SHS_jhs_school_name" value="'.$school_name.'">
        </div>
        <div class="e-col-6 e-form-group">
          <label class="e-label">Month/Yr of Completion</label>
          <input class="e-control" type="date" placeholder="Date" id="SHS_jhs_school_comp" name="SHS_jhs_school_comp" value="'.$date_of_completion.'">
        </div>
        <div class="e-col-12 e-form-group">
          <label class="e-label">School Address</label>
          <input class="e-control ESchoolAddress" type="text"  readonly id="SHS_jhs_school_add" name="SHS_jhs_school_add" >
        </div>

        <div class="e-col-4 e-form-group" >
          <label class="e-label">Region</label>
          <select  onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Region" ng-model="HS.region" id="SHS_jhs_school_region" name="SHS_jhs_school_region" value="{{HS.region.id}}" ng-options="region as region.regDesc for region in regions track by region.id" >
          </select>
        </div>

        <div class="e-col-4 e-form-group">
          <label class="e-label">Province</label>
          <select  onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Province" ng-model="HS.province" value="{{HS.province.id}}" ng-options="province as province.provDesc for province in provinces | filter: HS.region.regCode track by province.id" id="SHS_jhs_school_prov" name="SHS_jhs_school_prov"  disabled>
          </select>
        </div>

        <div class="e-col-4 e-form-group">
          <label class="e-label">City/Municipality</label>
          <select  onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_CityMunc" ng-model="HS.citymunc" value="{{HS.citymunc.id}}" ng-options="citymunci as citymunci.citymunDesc for citymunci in citymuncis | filter: HS.province.provCode track by citymunci.id" id="SHS_jhs_school_citymunc" name="SHS_jhs_school_citymunc" disabled>
          </select>
        </div>
      </div>
    </div>
    <!-- SHS -->';
  }
}else if ($acad_type == "College"){
echo '<!-- College -->
<div id="CollegeForm" class="e-cols">';
  if ($edu_stage == "Elem"){
    echo '<script>
    $(function() {
      setTimeout(function(){
        $("#college_elem_school_region").val("'.$school_region.'").change();
        $("#college_elem_school_prov").val("'.$school_province.'").change();
        $("#college_elem_school_citymunc").val("'.$school_city.'").change();
      }, 2000);
    });
    </script>';
echo  '<div class="e-cols">
    <div class="e-col-12 e-form-group">
      <h5 class="e-label">ELEMENTARY</h5>
    </div>
    <div class="e-col-6 e-form-group">
      <label class="e-label">School Name</label>
      <input class="e-control" type="text"  id="college_elem_school_name" name="college_elem_school_name" value="'.$school_name.'">
    </div>
    <div class="e-col-6 e-form-group">
      <label class="e-label">Month/Yr of Completion</label>
      <input class="e-control" type="date" placeholder="Date" id="college_elem_school_comp" name="college_elem_school_comp" value="'.$date_of_completion.'">
    </div>
    <div class="e-col-12 e-form-group">
      <label class="e-label">School Address</label>
      <input class="e-control ESchoolAddress" type="text"  readonly id="college_elem_school_add" name="college_elem_school_add">
    </div>
    <div class="e-col-4 e-form-group" >
      <label class="e-label">Region</label>
      <select  onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Region" ng-model="ES.region" id="college_elem_school_region" name="college_elem_school_region" value="{{ES.region.id}}" ng-options="region as region.regDesc for region in regions track by region.id" >
      </select>
    </div>

    <div class="e-col-4 e-form-group">
      <label class="e-label">Province</label>
      <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Province" ng-model="ES.province" value="{{ES.province.id}}" ng-options="province as province.provDesc for province in provinces | filter: ES.region.regCode track by province.id" id="college_elem_school_prov" name="college_elem_school_prov"  disabled>
      </select>
    </div>

    <div class="e-col-4 e-form-group">
      <label class="e-label">City/Municipality</label>
      <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_CityMunc" ng-model="ES.citymunc" value="{{ES.citymunc.id}}" ng-options="citymunci as citymunci.citymunDesc for citymunci in citymuncis | filter: ES.province.provCode track by citymunci.id" id="college_elem_school_citymunc" name="college_elem_school_citymunc" disabled>
      </select>
    </div>
  </div>';
}

else if ($edu_stage == "HS"){
  echo '<script>
  $(function() {
    setTimeout(function(){
      $("#college_hs_school_region").val("'.$school_region.'").change();
      $("#college_hs_school_prov").val("'.$school_province.'").change();
      $("#college_hs_school_citymunc").val("'.$school_city.'").change();
    }, 2000);
  });
  </script>';

  echo '<!--HS / COLLEGE-->
  <input class="e-control" type="text" id="HSInfo" name="HSInfo" value="'.$edu_stage.'">
  <div id="HSInfoForm" class="e-cols">
    <div class="e-col-12 e-form-group">
      <h5 class="e-label">High School</h5>
    </div>
    <div class="e-col-6 e-form-group">
      <label class="e-label">School
        Name</label>
        <input class="e-control" type="text"  id="college_hs_school_name" name="college_hs_school_name" value="'.$school_name.'">
      </div>
      <div class="e-col-6 e-form-group">
        <label class="e-label">Month/Yr of Completion</label>
        <input class="e-control" type="date" placeholder="Date" id="college_hs_school_comp" name="college_hs_school_comp" value="'.$date_of_completion.'">
      </div>
      <div class="e-col-12 e-form-group">
        <label class="e-label">School Address</label>
        <input class="e-control ESchoolAddress" type="text"  readonly id="college_hs_school_add" name="college_hs_school_add">
      </div>
      <div class="e-col-4 e-form-group" >
        <label class="e-label">Region</label>
        <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Region" ng-model="HS.region" id="college_hs_school_region" name="college_hs_school_region" value="{{HS.region.id}}" ng-options="region as region.regDesc for region in regions track by region.id" >
        </select>
      </div>

      <div class="e-col-4 e-form-group">
        <label class="e-label">Province</label>
        <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Province" ng-model="HS.province" value="{{HS.province.id}}" ng-options="province as province.provDesc for province in provinces | filter: HS.region.regCode track by province.id" id="college_hs_school_prov" name="college_hs_school_prov"  disabled>
        </select>
      </div>

      <div class="e-col-4 e-form-group">
        <label class="e-label">City/Municipality</label>
        <select  onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_CityMunc" ng-model="HS.citymunc" value="{{HS.citymunc.id}}" ng-options="citymunci as citymunci.citymunDesc for citymunci in citymuncis | filter: HS.province.provCode track by citymunci.id" id="college_hs_school_citymunc" name="college_hs_school_citymunc" disabled>
        </select>
      </div>
    </div>
    <!--HS / COLLEGE-->';
}else if ($edu_stage == "SHS"){
  echo '<script>
  $(function() {
    setTimeout(function(){
      $("#college_shs_school_region").val("'.$school_region.'").change();
      $("#college_shs_school_prov").val("'.$school_province.'").change();
      $("#college_shs_school_citymunc").val("'.$school_city.'").change();
    }, 2000);
  });
  </script>';
    echo '<!--SHS / COLLEGE-->
    <input class="e-control" type="text" id="HSInfo" name="HSInfo" value="'.$edu_stage.'">
    <div id="SHSInfoForm" class="e-cols">
      <div class="e-col-12 e-form-group">
        <h5 class="e-label">Senior High School</h5>
      </div>
      <div class="e-col-6 e-form-group">
        <label class="e-label">School
          Name</label>
          <input class="e-control" type="text"  id="college_shs_school_name" name="college_shs_school_name" value="'.$school_name.'">
        </div>
        <div class="e-col-6 e-form-group">
          <label class="e-label">Month/Yr of Completion</label>
          <input class="e-control" type="date" placeholder="Date" id="college_shs_school_comp" name="college_shs_school_comp" value="'.$date_of_completion.'">
        </div>
        <div class="e-col-12 e-form-group">
          <label class="e-label">School Address</label>
          <input class="e-control ESchoolAddress" type="text"  readonly id="college_shs_school_add" name="college_shs_school_add">
        </div>
        <div class="e-col-4 e-form-group" >
          <label class="e-label">Region</label>
          <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Region" ng-model="HS.region" id="college_shs_school_region" name="college_shs_school_region" value="{{HS.region.id}}" ng-options="region as region.regDesc for region in regions track by region.id" >
          </select>
        </div>

        <div class="e-col-4 e-form-group">
          <label class="e-label">Province</label>
          <select onChange = "updateAddress(this)"  class="e-select width_inherit ESAddress_Province" ng-model="HS.province" value="{{HS.province.id}}" ng-options="province as province.provDesc for province in provinces | filter: HS.region.regCode track by province.id" id="college_shs_school_prov" name="college_shs_school_prov"  disabled>
          </select>
        </div>

        <div class="e-col-4 e-form-group">
          <label class="e-label">City/Municipality</label>
          <select onChange = "updateAddress(this)"  class="e-select width_inherit ESAddress_CityMunc" ng-model="HS.citymunc" value="{{HS.citymunc.id}}" ng-options="citymunci as citymunci.citymunDesc for citymunci in citymuncis | filter: HS.province.provCode track by citymunci.id" id="college_shs_school_citymunc" name="college_shs_school_citymunc" disabled>
          </select>
        </div>
      </div>
      <!--SHS / COLLEGE-->
    </div>
    <!-- College -->';
}
}else if ($acad_type == "TransfereeCollege"){
  echo '<script>
  $(function() {
    setTimeout(function(){
      $("#transCollege_school_region").val("'.$school_region.'").change();
      $("#transCollege_school_prov").val("'.$school_province.'").change();
      $("#transCollege_school_citymunc").val("'.$school_city.'").change();
    }, 2000);
  });
  </script>';
    echo '<!-- Transferee College -->
    <div id="TransfereeCollegeForm" class="e-cols">';
      echo '<div class="e-col-12 e-form-group">
        <h5 class="e-label">SCHOOL</h5>
      </div>
      <div class="e-col-12 e-form-group">
        <label class="e-label">School Name</label>
        <input class="e-control" type="text"  id="transCollege_school_name" name="transCollege_school_name" value="'.$school_name.'">
      </div>
      <div class="e-col-12 e-form-group">
        <label class="e-label">School Address</label>
        <input class="e-control ESchoolAddress" type="text"  readonly id="transCollege_school_add" name="transCollege_school_add" value="'.$date_of_completion.'">
      </div>
      <div class="e-col-4 e-form-group" >
        <label class="e-label">Region</label>
        <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Region" ng-model="ES.region" id="transCollege_school_region" name="transCollege_school_region" value="{{ES.region.id}}" ng-options="region as region.regDesc for region in regions track by region.id" >
        </select>
      </div>

      <div class="e-col-4 e-form-group">
        <label class="e-label">Province</label>
        <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Province" ng-model="ES.province" value="{{ES.province.id}}" ng-options="province as province.provDesc for province in provinces | filter: ES.region.regCode track by province.id" id="transCollege_school_prov" name="transCollege_school_prov"  disabled>
        </select>
      </div>

      <div class="e-col-4 e-form-group">
        <label class="e-label">City/Municipality</label>
        <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_CityMunc" ng-model="ES.citymunc" value="{{ES.citymunc.id}}" ng-options="citymunci as citymunci.citymunDesc for citymunci in citymuncis | filter: ES.province.provCode track by citymunci.id" id="transCollege_school_citymunc" name="transCollege_school_citymunc" disabled>
        </select>
      </div>
    </div>
    <!-- Transferee College -->';
  }else if ($acad_type == "TransfereeSHS"){
    echo '<!-- Transferee SHS -->
    <div id="TransfereeSHSForm" class="e-cols">';
    if ($edu_stage == "Elem"){
      echo '<script>
      $(function() {
        setTimeout(function(){
          $("#transSHS_elem_school_region").val("'.$school_region.'").change();
          $("#transSHS_elem_school_prov").val("'.$school_province.'").change();
          $("#transSHS_elem_school_citymunc").val("'.$school_city.'").change();
        }, 2000);
      });
      </script>';
    echo '<div class="e-cols">
        <div class="e-col-12 e-form-group">
          <h5 class="e-label">ELEMENTARY</h5>
        </div>
        <div class="e-col-6 e-form-group">
          <label class="e-label">School Name</label>
          <input class="e-control" type="text"  id="transSHS_elem_school_name" name="transSHS_elem_school_name" value="'.$school_name.'">
        </div>
        <div class="e-col-6 e-form-group">
          <label class="e-label">Month/Yr of Completion</label>
          <input class="e-control" type="date" placeholder="Date" id="transSHS_elem_school_comp" name="transSHS_elem_school_comp" value="'.$date_of_completion.'">
        </div>
        <div class="e-col-12 e-form-group">
          <label class="e-label">School Address</label>
          <input class="e-control ESchoolAddress" type="text"  readonly id="transSHS_elem_school_add" name="transSHS_elem_school_add">
        </div>
        <div class="e-col-4 e-form-group" >
          <label class="e-label">Region</label>
          <select  onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Region" ng-model="ES.region" id="transSHS_elem_school_region" name="transSHS_elem_school_region" value="{{ES.region.id}}" ng-options="region as region.regDesc for region in regions track by region.id" >
          </select>
        </div>

        <div class="e-col-4 e-form-group">
          <label class="e-label">Province</label>
          <select  onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Province" ng-model="ES.province" value="{{ES.province.id}}" ng-options="province as province.provDesc for province in provinces | filter: ES.region.regCode track by province.id" id="transSHS_elem_school_prov" name="transSHS_elem_school_prov"  disabled>
          </select>
        </div>

        <div class="e-col-4 e-form-group">
          <label  onChange = "updateAddress(this)" class="e-label">City/Municipality</label>
          <select class="e-select width_inherit ESAddress_CityMunc" ng-model="ES.citymunc" value="{{ES.citymunc.id}}" ng-options="citymunci as citymunci.citymunDesc for citymunci in citymuncis | filter: ES.province.provCode track by citymunci.id" id="transSHS_elem_school_citymunc" name="transSHS_elem_school_citymunc" disabled>
          </select>
        </div>
      </div>';
    }else{
      echo '<script>
      $(function() {
        setTimeout(function(){
          $("#TransSHS_jhs_school_region").val("'.$school_region.'").change();
          $("#TransSHS_jhs_school_prov").val("'.$school_province.'").change();
          $("#TransSHS_jhs_school_citymunc").val("'.$school_city.'").change();
        }, 2000);
      });
      </script>';
      echo '<div class="e-cols">
        <div class="e-col-12 e-form-group">
          <h5 class="e-label">Junior High School</h5>
        </div>
        <div class="e-col-6 e-form-group">
          <label class="e-label">School Name</label>
          <input class="e-control" type="text"  id="TransSHS_jhs_school_name" name="TransSHS_jhs_school_name" value="'.$school_name.'">
        </div>
        <div class="e-col-6 e-form-group">
          <label class="e-label">Month/Yr of Completion</label>
          <input class="e-control" type="date" placeholder="Date" id="TransSHS_jhs_school_comp" name="TransSHS_jhs_school_comp" value="'.$date_of_completion.'">
        </div>
        <div class="e-col-12 e-form-group">
          <label class="e-label">School Address</label>
          <input class="e-control ESchoolAddress" type="text"  readonly id="TransSHS_jhs_school_add" name="TransSHS_jhs_school_add">
        </div>
        <div class="e-col-4 e-form-group" >
          <label class="e-label">Region</label>
          <select  onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Region" ng-model="HS.region" id="TransSHS_jhs_school_region" name="TransSHS_jhs_school_region" value="{{HS.region.id}}" ng-options="region as region.regDesc for region in regions track by region.id" >
          </select>
        </div>

        <div class="e-col-4 e-form-group">
          <label class="e-label">Province</label>
          <select  onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Province" ng-model="HS.province" value="{{HS.province.id}}" ng-options="province as province.provDesc for province in provinces | filter: HS.region.regCode track by province.id" id="TransSHS_jhs_school_prov" name="TransSHS_jhs_school_prov"  disabled>
          </select>
        </div>

        <div class="e-col-4 e-form-group">
          <label class="e-label">City/Municipality</label>
          <select  onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_CityMunc" ng-model="HS.citymunc" value="{{HS.citymunc.id}}" ng-options="citymunci as citymunci.citymunDesc for citymunci in citymuncis | filter: HS.province.provCode track by citymunci.id" id="TransSHS_jhs_school_citymunc" name="TransSHS_jhs_school_citymunc" disabled>
          </select>
        </div>
      </div>
    </div>
    <!-- Transferee SHS -->';
  }
  }
  }
      echo  '<div class="e-col-12 e-form-group">
          <h4 class="e-label text-primary">Choose the Track and Strand</h4>
        </div>

        <div class="e-col-12 e-form-group">
          <h5 class="e-label">Specialization</h5>
          <label class="e-label">Track</label>
          <select class="e-select width_inherit"  ng-model="tra"  id="track" name="track">
            <option value="" disabled="" class="" selected="selected">Select Track</option>
            <option ng-repeat ="track in tracks" value = "{{track.id}}">{{track.name}}</option>
          </select>
        </div>

        <div class="e-col-12 e-form-group">
          <label class="e-label">Strand</label>
          <select class="e-select width_inherit" ng-init="studentStrand()" id="strand" name="strand">
            <option value="'.$track_id.'" disabled="" class="" selected="selected">Select Strand</option>
            <option class="text-uppercase" ng-repeat = "strand in strands" value = "{{strand.id}}">{{strand.name}}</option>
          </select>
        </div>

        <div class="e-col-12 e-form-group centered">
          <button type="submit" class="e-btn primary">Enroll now</a>
          </div>
        </form>';

        echo '<script>
        $(function() {
          setTimeout(function(){
             $("#program").val("'.$program_id.'").change();
             $("#setting").val("'.$setting_id.'").change();
             $("#cAddress_Region").val("'.$region.'").change();
             $("#cAddress_Province").val("'.$province.'").change();
             $("#cAddress_CityMunc").val("'.$city.'").change();
             $("#cAddress_Barangay").val("'.$barangay.'").change();
             $("#cAddress_Info").val("'.$street_info.'").blur();
             var cRegion = $("option:selected", "#cAddress_Region").text();
             var cProvince = $("option:selected", "#cAddress_Province").text();
             var cCityMunc = $("option:selected", "#cAddress_CityMunc").text();
             var cBarangay = $("option:selected", "#cAddress_Barangay").text();
             var cInfo = $("#cAddress_Info").val();
             $("#completeAddress").val(cInfo+", "+cBarangay+", "+cCityMunc+", "+cProvince+", "+cRegion);
             $("#sex").val("'.$sex.'");
             $("#religion").val("'.$religion.'");
             $("#civilStatus").val("'.$civil_status.'");
             $("#fathers_occupation").val("'.$father_occupation.'");
             $("#mothers_occupation").val("'.$mothers_occupation.'");
             $("#relationship").val("'.$guardian_relationship.'");
        }, 500);
          setTimeout(function(){
            $("#track").val("'.$track_id.'").change();
            $("#strand").val("'.$strand_course.'").change();
          }, 3000);
        $("#age").val(getAge("'.$birthday.'"));
        });

        </script>';
        }
        ?>



<div class="e-container">
        <h3 class="text-bold centered pt-3"></h3>

        <form class="e-cols" id="formRegistration0">
          <div class="e-cols no-pt">
            <div class="e-col-12">
              <p><b>TO THE STUDENT: </b>Please fill up information below:</p>
            </div>
          </div>
          <div class="e-cols">
            <div class="e-col-3">
              <select class="e-select width_inherit" ng-init="getProg()" ng-model="prog"  ng-change="getTrack(prog)" id="program" name="program" onChange="ProgramCheck(this)">
                <option value="" disabled="" class="" selected="selected">Select Program</option>
                <option ng-repeat = "program in progs" value = "{{program.id}}">{{program.name}}</option>
              </select>
            </div>
            <div class="e-col"></div>
            <div class="e-col-3">
              <select class="e-select width_inherit" ng-init="getSetting()" id="setting" name="setting">
                <option value="" disabled="" class="" selected="selected">Select Settings</option>
                <option ng-repeat = "setting in settings" value = "{{setting.id}}">{{setting.description}}</option>
              </select>
            </div>
          </div>
          <div class="e-col-12 e-form-group">
            <h4 class="e-label text-primary">Personal Information</h4>
          </div>

          <div class="e-col-3 e-form-group">
            <label class="e-label">First Name</label>
            <input class="e-control" type="text" id="first_name" name="first_name">
          </div>

          <div class="e-col-3 e-form-group">
            <label class="e-label">Middle Name</label>
            <input class="e-control" type="text" id="middle_name" name="middle_name">
          </div>

          <div class="e-col-3 e-form-group">
            <label class="e-label">Last Name</label>
            <input class="e-control" type="text" id="last_name" name="last_name">
          </div>

          <div class="e-col-3 e-form-group">
            <label class="e-label">Suffix Name</label>
            <input class="e-control" type="text" id="suffix_name" name="suffix_name">
          </div>

          <div class="e-col-12 e-form-group">
            <label class="e-label">Complete Address</label>
            <input class="e-control unselectable" type="text" readonly id="completeAddress" name="completeAddress" >
          </div>

          <div class="e-col e-form-group" >
            <label class="e-label">Region</label>
            <select class="e-select width_inherit" ng-init="getRegion()" ng-model="student.region" id="cAddress_Region" name="cAddress_Region" value='{{student.region.id}}' ng-options='region as region.regDesc for region in regions track by region.id' >
            </select>
          </div>

          <div class="e-col e-form-group">
            <label class="e-label">Province</label>
            <select class="e-select width_inherit" ng-init = "getProvince()" ng-model="student.province" value='{{student.province.id}}' ng-options='province as province.provDesc for province in provinces | filter: student.region.regCode track by province.id' id="cAddress_Province" name="cAddress_Province" disabled>
            </select>
          </div>

          <div class="e-col e-form-group">
            <label class="e-label">City/Municipality</label>
            <select class="e-select width_inherit" ng-init="getCityMunci()" ng-model="student.citymunc" value='{{student.citymunc.id}}' ng-options='citymunci as citymunci.citymunDesc for citymunci in citymuncis | filter: student.province.provCode track by citymunci.id' id="cAddress_CityMunc" name="cAddress_CityMunc" disabled>
            </select>
          </div>

          <div class="e-col e-form-group">
            <label class="e-label">Barangay</label>
            <select class="e-select width_inherit" ng-init="getBrngys()" ng-model="student.brngy" value='student.brngy.id' ng-options='brngy as brngy.brgyDesc for brngy in brngys | filter: student.citymunc.citymunCode track by brngy.id' id="cAddress_Barangay" name="cAddress_Barangay" disabled>
            </select>
          </div>

          <div class="e-col e-form-group">
            <label class="e-label">No./Blk/Lot/Flr/Bldg/St.</label>
            <input class="e-control" type="text" id="cAddress_Info" name="cAddress_Info" disabled>
          </div>

          <div class="e-col-12 e-form-group">
            <h4 class="e-label text-primary">General Information</h4>
          </div>

          <div class="e-col-6 e-form-group">
            <label class="e-label">E-Mail</label>
            <input class="e-control" type="text" id="email" name="email">
          </div>

          <div class="e-col-6 e-form-group">
            <label class="e-label">Contact No.</label>
            <input class="e-control" type="text" id="contact" name="contact">
          </div>

          <div class="e-col-4 e-form-group">
            <label class="e-label">Date of Birth</label>
            <input class="e-control" type="date" id="birthdate" name="birthdate">
          </div>

          <div class="e-col-4 e-form-group">
            <label class="e-label">Age</label>
            <input class="e-control" type="text" id="age" name="age">
          </div>

          <div class="e-col-4 e-form-group">
            <label class="e-label">Sex</label>
            <select class="e-select width_inherit" id="sex" name="sex">
              <option value="" disabled="" class="" selected="selected">Select Sex</option>
              <option value ="1">Male</option>
              <option value ="0">Female</option>
            </select>
          </div>

          <div class="e-col-12 e-form-group">
            <label class="e-label">Place of Birth</label>
            <input class="e-control" type="text" readonly id="PlaceOfBirth" name="PlaceOfBirth">
          </div>

          <div class="e-col-6 e-form-group">
            <label class="e-label">Province</label>
            <select class="e-select width_inherit" ng-model="birthdate.province" value='{{birthdate.province.id}}' ng-options='province as province.provDesc for province in provinces track by province.id' id="pAddress_Province" name="pAddress_Province">
            </select>
          </div>

          <div class="e-col-6 e-form-group">
            <label class="e-label">City/Municipality</label>
            <select class="e-select width_inherit" ng-model="birthdate.citymunc" value='{{birthdate.citymunc.id}}' ng-options='citymunci as citymunci.citymunDesc for citymunci in citymuncis | filter: birthdate.province.provCode track by citymunci.id' id="pAddress_CityMunc" name="pAddress_CityMunc" disabled>
            </select>
          </div>

          <div class="e-col-4 e-form-group">
            <label class="e-label">Religion</label>
            <select class="e-select width_inherit" ng-init="getReligion()" ng-model = "rel" id="religion" name="religion">
              <option value="" disabled="" class="" selected="selected">Select Religion</option>
              <option ng-repeat = "religion in religions" value = "{{religion.id}}">{{religion.description}}</option>
            </select>
          </div>

          <div class="e-col-4 e-form-group">
            <label class="e-label">Mother Tongue</label>
            <input class="e-control" type="text" id="mother_tongue" name="mother_tongue">
          </div>

          <div class="e-col-4 e-form-group">
            <label class="e-label">Civil Status</label>
            <select class="e-select width_inherit"  id="civilStatus" name="civilStatus">
              <option value="" disabled="" class="" selected="selected">Select Civil Status</option>
              <option value = "1">Single</option>
              <option value = "2">Married</option>
            </select>
          </div>

          <div class="e-col-6 e-form-group">
            <label class="e-label">Father's Name</label>
            <input class="e-control" type="text" id="fathers_name" name="fathers_name">
          </div>

          <div class="e-col-6 e-form-group">
            <label class="e-label">Father's Occupation</label>
            <select class="e-select width_inherit"  ng-init="getOccupation()" id="fathers_occupation" name="fathers_occupation">
              <option value="" disabled="" class="" selected="selected">Select Occupation</option>>
              <option class= "text-uppercase" ng-repeat = "occupation in occupations" value = "{{occupation.id}}">{{occupation.description}}</option>
            </select>
          </div>

          <div class="e-col-6 e-form-group">
            <label class="e-label">Mother's Name</label>
            <input class="e-control" type="text" id="mothers_name" name="mothers_name">
          </div>

          <div class="e-col-6 e-form-group">
            <label class="e-label">Mother's Occupation</label>
            <select class="e-select width_inherit"  ng-init="getOccupation()" id="mothers_occupation" name="mothers_occupation">
              <option value="" disabled="" class="" selected="selected">Select Occupation</option>>
              <option class= "text-uppercase" ng-repeat = "occupation in occupations" value = "{{occupation.id}}">{{occupation.description}}</option>
            </select>
          </div>

          <div class="e-col-4 e-form-group">
            <label class="e-label">Parent/Guiardian</label>
            <input class="e-control" type="text" id="parent_guardian" name="parent_guardian">
          </div>

          <div class="e-col-4 e-form-group">
            <label class="e-label">Relationship</label>
            <select class="e-select width_inherit"  ng-init="getRelationship()" id="relationship" name="relationship">
              <option value="" disabled="" class="" selected="selected">Select Relationship</option>>
              <option class= "text-uppercase" ng-repeat = "relationship in relationships" value = "{{relationship.id}}">{{relationship.description}}</option>
            </select>
          </div>

          <div class="e-col-4 e-form-group">
            <label class="e-label">Parent/Guiardian's Contact No.</label>
            <input class="e-control" type="text"  id="p_guardian_contact" name="p_guardian_contact">
          </div>

          <div class="e-col-12 e-form-group" id="LRNdiv" hidden>
            <h4 class="e-label text-primary">Learner's Reference Number (LRN)</h4>
            <br>
            <label class="e-label">LRN</label>
            <input class="e-control" type="text" id="LRN" name="LRN">
          </div>

          <div class="e-col-12 e-form-group">
            <h4 class="e-label text-primary">Academic Information</h4>
          </div>

          <div class="e-col-6 e-form-group centered" id="radioSHS" style="display:none;">
            <label>
              <input type="radio" name="academicInfo" id="academicInfo" value= "SHS" OnClick = "AcademicForm(this)">
              SHS
            </label>
          </div>
          <div class="e-col-6 e-form-group centered" id="radioCollege" style="display:none;">
            <label>
              <input type="radio" name="academicInfo" id="academicInfo" value= "College" OnClick = "AcademicForm(this)">
              College
            </label>
          </div>
          <div class="e-col-6 e-form-group centered" id="radioTransfereeCollege" style="display:none;">
            <label>
              <input type="radio" name="academicInfo" id="academicInfo" value= "TransfereeCollege" OnClick = "AcademicForm(this)">
              Transferee (College)
            </label>
          </div>
          <div class="e-col-6 e-form-group radioButtons centered" id="radioTransfereeSHS" style="display:none;">
            <label>
              <input type="radio" name="academicInfo" id="academicInfo" value= "TransfereeSHS" OnClick = "AcademicForm(this)">
              Transferee (SHS)
            </label>
          </div>

          <!-- SHS -->
          <div id="SHSForm" class="e-cols" style="display:none;">
            <div class="e-cols">
              <div class="e-col-12 e-form-group">
                <br>
                <h5 class="e-label">ELEMENTARY</h5>
              </div>
              <div class="e-col-6 e-form-group">
                <label class="e-label">School Name</label>
                <input class="e-control" type="text"  id="SHS_elem_school_name" name="SHS_elem_school_name">
              </div>
              <div class="e-col-6 e-form-group">
                <label class="e-label">Month/Yr of Completion</label>
                <input class="e-control" type="date" placeholder="Date" id="SHS_elem_school_comp" name="SHS_elem_school_comp">
              </div>
              <div class="e-col-12 e-form-group">
                <label class="e-label">School Address</label>
                <input class="e-control ESchoolAddress" type="text"  readonly id="SHS_elem_school_add" name="SHS_elem_school_add">
              </div>
              <div class="e-col-4 e-form-group" >
                <label class="e-label">Region</label>
                <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Region" ng-model="ES.region" id="SHS_elem_school_region" name="SHS_elem_school_region" value='{{ES.region.id}}' ng-options='region as region.regDesc for region in regions track by region.id' >
                </select>
              </div>

              <div class="e-col-4 e-form-group">
                <label class="e-label">Province</label>
                <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Province" ng-model="ES.province" value='{{ES.province.id}}' ng-options='province as province.provDesc for province in provinces | filter: ES.region.regCode track by province.id' id="SHS_elem_school_prov" name="SHS_elem_school_prov"  disabled>
                </select>
              </div>

              <div class="e-col-4 e-form-group">
                <label class="e-label">City/Municipality</label>
                <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_CityMunc" ng-model="ES.citymunc" value='{{ES.citymunc.id}}' ng-options='citymunci as citymunci.citymunDesc for citymunci in citymuncis | filter: ES.province.provCode track by citymunci.id' id="SHS_elem_school_citymunc" name="SHS_elem_school_citymunc" disabled>
                </select>
              </div>
            </div>
            <div class="e-cols">
              <div class="e-col-12 e-form-group">
                <br>
                <h5 class="e-label">Junior High School</h5>
              </div>
              <div class="e-col-6 e-form-group">
                <label class="e-label">School Name</label>
                <input class="e-control" type="text"  id="SHS_jhs_school_name" name="SHS_jhs_school_name">
              </div>
              <div class="e-col-6 e-form-group">
                <label class="e-label">Month/Yr of Completion</label>
                <input class="e-control" type="date" placeholder="Date" id="SHS_jhs_school_comp" name="SHS_jhs_school_comp">
              </div>
              <div class="e-col-12 e-form-group">
                <label class="e-label">School Address</label>
                <input class="e-control ESchoolAddress" type="text"  readonly id="SHS_jhs_school_add" name="SHS_jhs_school_add">
              </div>

              <div class="e-col-4 e-form-group" >
                <label class="e-label">Region</label>
                <select  onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Region" ng-model="HS.region" id="SHS_jhs_school_region" name="SHS_jhs_school_region" value='{{HS.region.id}}' ng-options='region as region.regDesc for region in regions track by region.id' >
                </select>
              </div>

              <div class="e-col-4 e-form-group">
                <label class="e-label">Province</label>
                <select  onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Province" ng-model="HS.province" value='{{HS.province.id}}' ng-options='province as province.provDesc for province in provinces | filter: HS.region.regCode track by province.id' id="SHS_jhs_school_prov" name="SHS_jhs_school_prov"  disabled>
                </select>
              </div>

              <div class="e-col-4 e-form-group">
                <label class="e-label">City/Municipality</label>
                <select  onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_CityMunc" ng-model="HS.citymunc" value='{{HS.citymunc.id}}' ng-options='citymunci as citymunci.citymunDesc for citymunci in citymuncis | filter: HS.province.provCode track by citymunci.id' id="SHS_jhs_school_citymunc" name="SHS_jhs_school_citymunc" disabled>
                </select>
              </div>
            </div>
          </div>
          <!-- SHS -->

          <!-- College -->
          <div id="CollegeForm" class="e-cols" style="display:none;">
            <div class="e-cols">
              <div class="e-col-12 e-form-group">
                <br>
                <h5 class="e-label">ELEMENTARY</h5>
              </div>
              <div class="e-col-6 e-form-group">
                <label class="e-label">School Name</label>
                <input class="e-control" type="text"  id="college_elem_school_name" name="college_elem_school_name">
              </div>
              <div class="e-col-6 e-form-group">
                <label class="e-label">Month/Yr of Completion</label>
                <input class="e-control" type="date" placeholder="Date" id="college_elem_school_comp" name="college_elem_school_comp">
              </div>
              <div class="e-col-12 e-form-group">
                <label class="e-label">School Address</label>
                <input class="e-control ESchoolAddress" type="text"  readonly id="college_elem_school_add" name="college_elem_school_add">
              </div>
              <div class="e-col-4 e-form-group" >
                <label class="e-label">Region</label>
                <select  onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Region" ng-model="ES.region" id="college_elem_school_region" name="college_elem_school_region" value='{{ES.region.id}}' ng-options='region as region.regDesc for region in regions track by region.id' >
                </select>
              </div>

              <div class="e-col-4 e-form-group">
                <label class="e-label">Province</label>
                <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Province" ng-model="ES.province" value='{{ES.province.id}}' ng-options='province as province.provDesc for province in provinces | filter: ES.region.regCode track by province.id' id="college_elem_school_prov" name="college_elem_school_prov"  disabled>
                </select>
              </div>

              <div class="e-col-4 e-form-group">
                <label class="e-label">City/Municipality</label>
                <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_CityMunc" ng-model="ES.citymunc" value='{{ES.citymunc.id}}' ng-options='citymunci as citymunci.citymunDesc for citymunci in citymuncis | filter: ES.province.provCode track by citymunci.id' id="college_elem_school_citymunc" name="college_elem_school_citymunc" disabled>
                </select>
              </div>
            </div>
            <div class="e-col-3 e-form-group">
              <label>
                <input type="radio" name="HSInfo"  id="HSInfo" value= "SHS" onClick="HSForm(this)">
                Senior High School
              </label>
            </div>
            <div class="e-col-3 e-form-group">
              <label>
                <input type="radio" name="HSInfo" id="HSInfo" value= "HS" onClick="HSForm(this)">
                High School
              </label>
            </div>
            <!--HS / COLLEGE-->
            <div id="HSInfoForm" class="e-cols" style="display:none;">
              <div class="e-col-12 e-form-group">
                <br>
                <h5 class="e-label">High School</h5>
              </div>
              <div class="e-col-6 e-form-group">
                <label class="e-label">School
                  Name</label>
                  <input class="e-control" type="text"  id="college_hs_school_name" name="college_hs_school_name">
                </div>
                <div class="e-col-6 e-form-group">
                  <label class="e-label">Month/Yr of Completion</label>
                  <input class="e-control" type="date" placeholder="Date" id="college_hs_school_comp" name="college_hs_school_comp">
                </div>
                <div class="e-col-12 e-form-group">
                  <label class="e-label">School Address</label>
                  <input class="e-control ESchoolAddress" type="text"  readonly id="college_hs_school_add" name="college_hs_school_add">
                </div>
                <div class="e-col-4 e-form-group" >
                  <label class="e-label">Region</label>
                  <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Region" ng-model="HS.region" id="college_hs_school_region" name="college_hs_school_region" value='{{HS.region.id}}' ng-options='region as region.regDesc for region in regions track by region.id' >
                  </select>
                </div>

                <div class="e-col-4 e-form-group">
                  <label class="e-label">Province</label>
                  <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Province" ng-model="HS.province" value='{{HS.province.id}}' ng-options='province as province.provDesc for province in provinces | filter: HS.region.regCode track by province.id' id="college_hs_school_prov" name="college_hs_school_prov"  disabled>
                  </select>
                </div>

                <div class="e-col-4 e-form-group">
                  <label class="e-label">City/Municipality</label>
                  <select  onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_CityMunc" ng-model="HS.citymunc" value='{{HS.citymunc.id}}' ng-options='citymunci as citymunci.citymunDesc for citymunci in citymuncis | filter: HS.province.provCode track by citymunci.id' id="college_hs_school_citymunc" name="college_hs_school_citymunc" disabled>
                  </select>
                </div>
              </div>
              <!--HS / COLLEGE-->

              <!--SHS / COLLEGE-->
              <div id="SHSInfoForm" class="e-cols" style="display:none;">
                <div class="e-col-12 e-form-group">
                  <br>
                  <h5 class="e-label">Senior High School</h5>
                </div>
                <div class="e-col-6 e-form-group">
                  <label class="e-label">School
                    Name</label>
                    <input class="e-control" type="text"  id="college_shs_school_name" name="college_shs_school_name">
                  </div>
                  <div class="e-col-6 e-form-group">
                    <label class="e-label">Month/Yr of Completion</label>
                    <input class="e-control" type="date" placeholder="Date" id="college_shs_school_comp" name="college_shs_school_comp">
                  </div>
                  <div class="e-col-12 e-form-group">
                    <label class="e-label">School Address</label>
                    <input class="e-control ESchoolAddress" type="text"  readonly id="college_shs_school_add" name="college_shs_school_add">
                  </div>
                  <div class="e-col-4 e-form-group" >
                    <label class="e-label">Region</label>
                    <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Region" ng-model="HS.region" id="college_shs_school_region" name="college_shs_school_region" value='{{HS.region.id}}' ng-options='region as region.regDesc for region in regions track by region.id' >
                    </select>
                  </div>

                  <div class="e-col-4 e-form-group">
                    <label class="e-label">Province</label>
                    <select onChange = "updateAddress(this)"  class="e-select width_inherit ESAddress_Province" ng-model="HS.province" value='{{HS.province.id}}' ng-options='province as province.provDesc for province in provinces | filter: HS.region.regCode track by province.id' id="college_shs_school_prov" name="college_shs_school_prov"  disabled>
                    </select>
                  </div>

                  <div class="e-col-4 e-form-group">
                    <label class="e-label">City/Municipality</label>
                    <select onChange = "updateAddress(this)"  class="e-select width_inherit ESAddress_CityMunc" ng-model="HS.citymunc" value='{{HS.citymunc.id}}' ng-options='citymunci as citymunci.citymunDesc for citymunci in citymuncis | filter: HS.province.provCode track by citymunci.id' id="college_shs_school_citymunc" name="college_shs_school_citymunc" disabled>
                    </select>
                  </div>
                </div>
                <!--SHS / COLLEGE-->
              </div>
              <!-- College -->

              <!-- Transferee College -->
              <div id="TransfereeCollegeForm" class="e-cols" style="display:none;">
                <div class="e-col-12 e-form-group">
                  <br>
                  <h5 class="e-label">SCHOOL</h5>
                </div>
                <div class="e-col-12 e-form-group">
                  <label class="e-label">School Name</label>
                  <input class="e-control" type="text"  id="transCollege_school_name" name="transCollege_school_name">
                </div>
                <div class="e-col-12 e-form-group">
                  <label class="e-label">School Address</label>
                  <input class="e-control ESchoolAddress" type="text"  readonly id="transCollege_school_add" name="transCollege_school_add">
                </div>
                <div class="e-col-4 e-form-group" >
                  <label class="e-label">Region</label>
                  <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Region" ng-model="ES.region" id="transCollege_school_region" name="transCollege_school_region" value='{{ES.region.id}}' ng-options='region as region.regDesc for region in regions track by region.id' >
                  </select>
                </div>

                <div class="e-col-4 e-form-group">
                  <label class="e-label">Province</label>
                  <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Province" ng-model="ES.province" value='{{ES.province.id}}' ng-options='province as province.provDesc for province in provinces | filter: ES.region.regCode track by province.id' id="transCollege_school_prov" name="transCollege_school_prov"  disabled>
                  </select>
                </div>

                <div class="e-col-4 e-form-group">
                  <label class="e-label">City/Municipality</label>
                  <select onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_CityMunc" ng-model="ES.citymunc" value='{{ES.citymunc.id}}' ng-options='citymunci as citymunci.citymunDesc for citymunci in citymuncis | filter: ES.province.provCode track by citymunci.id' id="transCollege_school_citymunc" name="transCollege_school_citymunc" disabled>
                  </select>
                </div>
              </div>
              <!-- Transferee College -->

              <!-- Transferee SHS -->
              <div id="TransfereeSHSForm" class="e-cols" style="display:none;">
                <div class="e-cols">
                  <div class="e-col-12 e-form-group">
                    <br>
                    <h5 class="e-label">ELEMENTARY</h5>
                  </div>
                  <div class="e-col-6 e-form-group">
                    <label class="e-label">School Name</label>
                    <input class="e-control" type="text"  id="transSHS_elem_school_name" name="transSHS_elem_school_name">
                  </div>
                  <div class="e-col-6 e-form-group">
                    <label class="e-label">Month/Yr of Completion</label>
                    <input class="e-control" type="date" placeholder="Date" id="transSHS_elem_school_comp" name="transSHS_elem_school_comp">
                  </div>
                  <div class="e-col-12 e-form-group">
                    <label class="e-label">School Address</label>
                    <input class="e-control ESchoolAddress" type="text"  readonly id="transSHS_elem_school_add" name="transSHS_elem_school_add">
                  </div>
                  <div class="e-col-4 e-form-group" >
                    <label class="e-label">Region</label>
                    <select  onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Region" ng-model="ES.region" id="transSHS_elem_school_region" name="transSHS_elem_school_region" value='{{ES.region.id}}' ng-options='region as region.regDesc for region in regions track by region.id' >
                    </select>
                  </div>

                  <div class="e-col-4 e-form-group">
                    <label class="e-label">Province</label>
                    <select  onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Province" ng-model="ES.province" value='{{ES.province.id}}' ng-options='province as province.provDesc for province in provinces | filter: ES.region.regCode track by province.id' id="transSHS_elem_school_prov" name="transSHS_elem_school_prov"  disabled>
                    </select>
                  </div>

                  <div class="e-col-4 e-form-group">
                    <label  onChange = "updateAddress(this)" class="e-label">City/Municipality</label>
                    <select class="e-select width_inherit ESAddress_CityMunc" ng-model="ES.citymunc" value='{{ES.citymunc.id}}' ng-options='citymunci as citymunci.citymunDesc for citymunci in citymuncis | filter: ES.province.provCode track by citymunci.id' id="transSHS_elem_school_citymunc" name="transSHS_elem_school_citymunc" disabled>
                    </select>
                  </div>
                </div>
                <div class="e-cols">
                  <div class="e-col-12 e-form-group">
                    <br>
                    <h5 class="e-label">Junior High School</h5>
                  </div>
                  <div class="e-col-6 e-form-group">
                    <label class="e-label">School Name</label>
                    <input class="e-control" type="text"  id="TransSHS_jhs_school_name" name="TransSHS_jhs_school_name">
                  </div>
                  <div class="e-col-6 e-form-group">
                    <label class="e-label">Month/Yr of Completion</label>
                    <input class="e-control" type="date" placeholder="Date" id="TransSHS_jhs_school_comp" name="TransSHS_jhs_school_comp">
                  </div>
                  <div class="e-col-12 e-form-group">
                    <label class="e-label">School Address</label>
                    <input class="e-control ESchoolAddress" type="text"  readonly id="TransSHS_jhs_school_add" name="TransSHS_jhs_school_add">
                  </div>
                  <div class="e-col-4 e-form-group" >
                    <label class="e-label">Region</label>
                    <select  onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Region" ng-model="HS.region" id="TransSHS_jhs_school_region" name="TransSHS_jhs_school_region" value='{{HS.region.id}}' ng-options='region as region.regDesc for region in regions track by region.id' >
                    </select>
                  </div>

                  <div class="e-col-4 e-form-group">
                    <label class="e-label">Province</label>
                    <select  onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_Province" ng-model="HS.province" value='{{HS.province.id}}' ng-options='province as province.provDesc for province in provinces | filter: HS.region.regCode track by province.id' id="TransSHS_jhs_school_prov" name="TransSHS_jhs_school_prov"  disabled>
                    </select>
                  </div>

                  <div class="e-col-4 e-form-group">
                    <label class="e-label">City/Municipality</label>
                    <select  onChange = "updateAddress(this)" class="e-select width_inherit ESAddress_CityMunc" ng-model="HS.citymunc" value='{{HS.citymunc.id}}' ng-options='citymunci as citymunci.citymunDesc for citymunci in citymuncis | filter: HS.province.provCode track by citymunci.id' id="TransSHS_jhs_school_citymunc" name="TransSHS_jhs_school_citymunc" disabled>
                    </select>
                  </div>
                </div>
              </div>
              <!-- Transferee SHS -->
              <div class="e-col-12 e-form-group">
                <h4 class="e-label text-primary">Choose the Track and Strand</h4>
              </div>

              <div class="e-col-12 e-form-group">
                <h5 class="e-label">Specialization</h5>
                <label class="e-label">Track</label>
                <select class="e-select width_inherit"  ng-model="tra"  id="track" name="track">
                  <option value="" disabled="" class="" selected="selected">Select Track</option>
                  <option ng-repeat = "track in tracks" value = "{{track.id}}">{{track.name}}</option>
                </select>
              </div>

              <div class="e-col-12 e-form-group">
                <label class="e-label">Strand</label>
                <select class="e-select width_inherit" ng-init="studentStrand()" id="strand" name="strand">
                  <option value="" disabled="" class="" selected="selected">Select Strand</option>
                  <option class="text-uppercase" ng-repeat = "strand in strands" value = "{{strand.id}}">{{strand.name}}</option>
                </select>
              </div>

              <div class="e-col-12 e-form-group centered">
                <button type="submit" class="e-btn primary">Submit</a>
                </div>
              </form>
            </div>
  <script>
  $(document).on('change', '#cAddress_Region', function() {
    var cRegion = $("option:selected", '#cAddress_Region').text();

    $('#cAddress_Province').prop("disabled", false);
    $('#cAddress_CityMunc').prop("disabled", true);
    $('#cAddress_Barangay').prop("disabled", true);
    $('#cAddress_Info').prop("disabled", true);

    $('#cAddress_Province').prop('selectedIndex',0);
    $('#cAddress_CityMunc').prop('selectedIndex',0);
    $('#cAddress_Barangay').prop('selectedIndex',0);
    $('#cAddress_Info').val('');
    $("#completeAddress").val(cRegion);
  });

  $(document).on('change', '#cAddress_Province', function() {
    var cRegion = $("option:selected", '#cAddress_Region').text();
    var cProvince = $("option:selected", '#cAddress_Province').text();
    $('#cAddress_CityMunc').prop("disabled", false);
    $('#cAddress_Barangay').prop("disabled", true);
    $('#cAddress_Info').prop("disabled", true);

    $('#cAddress_CityMunc').prop('selectedIndex',0);
    $('#cAddress_Barangay').prop('selectedIndex',0);
    $('#cAddress_Info').val('');
    $("#completeAddress").val(cProvince+", "+cRegion);
  });

  $(document).on('change', '#cAddress_CityMunc', function() {
    var cRegion = $("option:selected", '#cAddress_Region').text();
    var cProvince = $("option:selected", '#cAddress_Province').text();
    var cCityMunc = $("option:selected", '#cAddress_CityMunc').text();
    $('#cAddress_Barangay').prop("disabled", false);
    $('#cAddress_Info').prop("disabled", true);

    $('#cAddress_Barangay').prop('selectedIndex',0);
    $('#cAddress_Info').val('');
    $("#completeAddress").val(cCityMunc+", "+cProvince+", "+cRegion);
  });

  $(document).on('change', '#cAddress_Barangay', function() {
    var cRegion = $("option:selected", '#cAddress_Region').text();
    var cProvince = $("option:selected", '#cAddress_Province').text();
    var cCityMunc = $("option:selected", '#cAddress_CityMunc').text();
    var cBarangay = $("option:selected", '#cAddress_Barangay').text();
    $('#cAddress_Info').prop("disabled", false);
    $('#cAddress_Info').val('');

    $("#completeAddress").val(cBarangay+", "+cCityMunc+", "+cProvince+", "+cRegion);
  });

  $(document).on('blur', '#cAddress_Info', function() {
    var cRegion = $("option:selected", '#cAddress_Region').text();
    var cProvince = $("option:selected", '#cAddress_Province').text();
    var cCityMunc = $("option:selected", '#cAddress_CityMunc').text();
    var cBarangay = $("option:selected", '#cAddress_Barangay').text();
    var cInfo = $('#cAddress_Info').val();
    $("#completeAddress").val(cInfo +", "+cBarangay+", "+cCityMunc+", "+cProvince+", "+cRegion);
  });

  $(document).on('change', '#pAddress_Province', function() {
    var pProvince = $("option:selected", '#pAddress_Province').text();;
    $('#pAddress_CityMunc').prop("disabled", false);
    $('#pAddress_CityMunc').prop('selectedIndex',0);

    $("#PlaceOfBirth").val(pProvince);
  });

  $(document).on('change', '#pAddress_CityMunc', function() {
    var pProvince = $("option:selected", '#pAddress_Province').text();
    var pCityMunc = $("option:selected", '#pAddress_CityMunc').text();

    $("#PlaceOfBirth").val(pCityMunc + ", " +pProvince);
  });

  function ProgramCheck(obj){
    var Program = $("option:selected", obj).val();
    if(Program == 2){
      $("#LRNdiv").hide();
      $("#radioSHS").hide();
      $("#radioTransfereeSHS").hide();
      $("#radioCollege").show();
      $("#radioTransfereeCollege").show();
    }else{
      $("#radioSHS").show();
      $("#radioTransfereeSHS").show();
      $("#radioCollege").hide();
      $("#radioTransfereeCollege").hide();

    }
  }

  function AcademicForm(obj){
    if ($(obj).val() == "SHS"){
      $('#SHSForm').show();
      $('#CollegeForm').hide();
      $('#TransfereeCollegeForm').hide();
      $('#TransfereeSHSForm').hide();
    }else if ($(obj).val() == "College"){
      $('#CollegeForm').show();
      $('#SHSForm').hide();
      $('#TransfereeCollegeForm').hide();
      $('#TransfereeSHSForm').hide();
    }else if ($(obj).val() == "TransfereeCollege"){
      $('#TransfereeCollegeForm').show();
      $('#SHSForm').hide();
      $('#CollegeForm').hide();
      $('#TransfereeSHSForm').hide();
    }
    else if ($(obj).val() == "TransfereeSHS"){
      $('#TransfereeSHSForm').show();
      $('#SHSForm').hide();
      $('#CollegeForm').hide();
      $('#TransfereeCollegeForm').hide();
    }
  }

  function HSForm(obj){
    if ($(obj).val() == "HS"){
        $('#HSInfoForm').show();
        $('#SHSInfoForm').hide();
    }else if ($(obj).val() == "SHS"){
        $('#SHSInfoForm').show();
        $('#HSInfoForm').hide();
    }
  }

  function updateAddress(obj){
    if ($(obj).hasClass( "ESAddress_Region" )){
      var ESRegion = $("option:selected", obj).text();

      $(obj).parent().parent().find('.ESAddress_Province').prop("disabled", false);
      $(obj).parent().parent().find('.ESAddress_CityMunc').prop("disabled", true);

      $(obj).parent().parent().find('.ESAddress_Province').prop('selectedIndex',0);
      $(obj).parent().parent().find('.ESAddress_CityMunc').prop('selectedIndex',0);
      $(obj).parent().parent().find(".ESchoolAddress").val(ESRegion);
    }
    else if ($(obj).hasClass( "ESAddress_Province" )){
      var ESRegion = $("option:selected", $(obj).parent().parent().find('.ESAddress_Region')).text();
      var ESProvince = $("option:selected", obj).text();
      $(obj).parent().parent().find('.ESAddress_CityMunc').prop("disabled", false);
      $(obj).parent().parent().find('.ESAddress_CityMunc').prop('selectedIndex',0);
      $(obj).parent().parent().find(".ESchoolAddress").val(ESProvince+", "+ESRegion);
    }
    else if ($(obj).hasClass( "ESAddress_CityMunc")){
      var ESRegion = $("option:selected", $(obj).parent().parent().find('.ESAddress_Region')).text();
      var ESProvince = $("option:selected", $(obj).parent().parent().find('.ESAddress_Province')).text();
      var ESCityMunc = $("option:selected", obj).text();
      $(obj).parent().parent().find(".ESchoolAddress").val(ESCityMunc+", "+ESProvince+", "+ESRegion);
    }
  }
  $(document).ready(function(){
    $('#formRegistration').on('submit',(function(e) {
      e.preventDefault();
      var formData = new FormData(this);

      $.ajax({
        type:'POST',
        url: "api/enrollPreReg.php",
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success:function(data){
          console.log("success");
          console.log(data);
        },
        error: function(data){
          console.log("error");
          console.log(data);
        }
      });
    }));
  });

  $(document).ready(function(){
          $('#formRegistration0').on('submit',(function(e) {
            e.preventDefault();
            var formData = new FormData(this);

            $.ajax({
              type:'POST',
              url: "api/insertPreRegStudent.php",
              data:formData,
              cache:false,
              contentType: false,
              processData: false,
              success:function(data){
                console.log("success");
                console.log(data);
              },
              error: function(data){
                console.log("error");
                console.log(data);
              }
            });
          }));
        });

  function fetchPreReg(id){
    $.ajax({
      type:'POST',
      url: "api/fetchReg.php",
      data:"student_id="+ id,
      success:function(data){
        console.log(data);
        $("#ifAdequate").html(data);
      }
    });
  }

  function getAge(DOB) {
    var today = new Date();
    var birthDate = new Date(DOB);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age = age - 1;
    }

    return age;
}

  </script>
