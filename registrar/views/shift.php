
<?php

include "views/student_tabs.php";


?>

<div class="e-cols" ng-init="studentTrack(0)">
    <select name="" ng-model="track" class="e-control rounded" id="">
    <option value="">Select Track</option>
    <option ng-repeat="t in trackss" value="{{t.id}}">{{t.name}}</option>
    </select>
</div>

<div class="e-cols" ng-init="studentStrand()">
    <select name="" ng-model="strand" class="e-control rounded" id="" ng-change="getStrandId(strand)">
    <option value="" >Select Strand</option>
    <option ng-repeat="s in strands | filter : {track_id : track}" value="{{s.id}}">{{s.name}}</option>
    </select>
</div>

<div class="e-cols pt-2">
<ul class="e-list">
  <li ng-repeat="c in curriculas" class="e-list-item"><input type="radio" ng-model="curric" name="curr" value="{{c.cur_id}}" ng-change="initShiftCourse(curric)"> {{c.name}} - {{c.year}}</li>
</ul>
</div>

<div class="e-cols align-end">
<button class="e-btn sky rounded" ng-click="addStudentCurricula(<?php echo $_GET['student'];?>)">Shift</button>
</div>