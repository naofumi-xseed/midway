
<?php
include "views/student_tabs.php";
?>


<div class="e-cols">
<div class="e-col-6">
    <h4>Contact Information</h4>
    <div class="pl-3">
    <table class="e-table">
            <tr>
                <td><i class="fa fa-envelope"></i> Email Add</td>
                <td><input type="text" class="e-control rounded" ng-model="students.email"></td>
            </tr>
            <tr>
                <td><i class="fa fa-phone"></i> Phone no</td>
                <td> <input type="text" class="e-control rounded"  ng-model="students.contact_no"></td>
            </tr>
        </table>
     </div>
    
    <h4>General Information</h4>
        <div class="pl-3">
        <table class="e-table"  ng-init="date()">
            <tr>
                <td><i class="{{students.sex === 1 ? 'fa fa-male' : 'fa fa-female'}}"></i> Gender</td>
                <td>
                    <select name="" id="" class="e-control rounded" ng-model="students.sex">
                        <option value="1">Male</option>
                        <option value="0">Female</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td ><i class="fa fa-birthday-cake"></i> Birthday</td>
                <td><input type="text" class="e-control rounded" name="birthday" ng-model="students.birthday | date:'MM/dd/yyyy'"></td>
            </tr>
            <tr>
                <td><i class="fa fa-birthday-cake"></i> Age</td>
                <td>{{age}}</td>
            </tr>
            <tr>
                <td><i class="fa fa-globe"></i> Birth Place</td>
                <td>{{students.birth_place}}</td>
            </tr>
            <tr >
                <td><i class="fa fa-mosque"></i> Religion</td>
                <td>
                    <select name="" id="" ng-model="belief.id" class="e-control rounded">
                    <option ng-repeat="r in religions" ng-value="{{r.id}}">{{r.description}}</option>
                    </select>
                </td>
            </tr>
            <tr >
                <td><i class="fa fa-language"></i> Mother Toungue</td>
                <td>{{students.mother_tongue}}</td>
            </tr>
            <tr >
                <td><i class="fa fa-atom"></i> Civil Status</td>
                <td>
                <select name="" id="" class="e-control rounded" ng-model="students.civil_status">
                        <option value="1">Single</option>
                        <option value="0">Married</option>
                    </select>
                </td>
            </tr>
        </table>
        </div>
    </div>
    
    <div class="e-col-6">
    <h4>Family Information</h4>
    <div class="pl-3">
        <table class="e-table">
            <tr>
                <td><i class="fa fa-male"></i> Father</td>
                <td>{{students.fathers_name}}</td>
            </tr>
            <tr>
                <td><i class="fa fa-people-carry"></i> Father's Occupation</td>
                <td>{{fo.description}}</td>
            </tr>
            <tr>
                <td><i class="fa fa-female"></i> Mother</td>
                <td>{{students.mothers_name}}</td>
            </tr>
            <tr>
                <td><i class="fa fa-people-carry"></i> Mother's Occupation</td>
                <td>{{mo.description}}</td>
            </tr>
            <tr >
                <td><i class="fa fa-language"></i> Parent/Guardian</td>
                <td>{{students.guardian_name}}</td>
            </tr>
            <tr >
                <td><i class="fa fa-phone"></i> Guardian Conctact</td>
                <td>{{students.guardian_contact}}</td>
            </tr>
            
        </table>
        </div>
    </div>

</div>

<center>
<div class="e-cols">
    <div class="e-col-6">
    <h4>Health Information</h4>
    <i class="fa fa-first-aid"></i>  <br>
    </div>
</div>
</center>



<div id="finish" class="e-modal" >
  <div class="e-modal-content eUp" style="background:transparent">
    <div class="e-modal-body" id="loader-7">
    <center><img  src="../_public/photos/checkmark.gif" alt="" style="width:100px; height:100px" ng-cloak></center>
    <center style="color:white"><b>Successfully Updated!</b></center>
    </div>
  </div>
</div>
