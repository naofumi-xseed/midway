<center>
<!-- student tab -->
<nav class="e-tabs  pb-2">
  <ul ng-init="tabs(<?php echo $_GET['t'];?>)">
    <li ng-repeat="t in student_tab"  class="{{active.id === t.id ? 'active' : 'none'}} small"  style="color:gray;font-size:12px"><a href="?student=<?php echo$_GET['student'];?>&{{t.method}}&t={{t.id}}">{{t.tab}}</a></li>

  </ul>
</nav>

</center>

<div ng-show="active.id ==1">
<button  class="e-btn primary small rounded pl-3" ng-click="editStudent(<?php echo $_GET['student'];?>)"><i class="fa fa-edit"></i> Edit information</button>

</div>
<h4 class="pl-3">{{active.tab === 'Student Information' ? 'Student Information' : active.tab}}</h4>

<div ng-init="getStudent(<?php echo $_GET['student'];?>)"  class="e-cols pt-2 e-x shadow">

<?php if(1) { ?>


<div class="e-col">
    <table class="e-table" style="text-transform:uppercase">
        <thead>
        </thead>
        <tbody>
        <tr>
            <td>FULL NAME</td> <td><input type="text" ng-model="students.first_name" class="e-control rounded" style="width:35%">
            <input type="text" ng-model="students.middle_name" class="e-control rounded" style="width:30%">
            <input type="text" ng-model="students.last_name" class="e-control rounded" style="width:30%">   
            </td>
        </tr>
        <tr>
            <td>Complete Address</td><td> {{students.complete_address}}</td>     
        </tr>
        <tr>
            <td>Curriculum</td><td> {{curriculum.year}}</td>     
        </tr>
        <tr>
            <td>Strand</td><td> {{students.strand_name}}</td>     
        </tr>
       
            
        </tbody>

    </table>
</div>

    <div class="e-col" ng-model="page">
    <table class="e-table">
        <thead>
        </thead>
        <tbody>
        <tr>
            <td>Date Enrolled</td><td> {{students.created_at}}</td>     
        </tr>
        <tr>
            <td>Student No</td> <td>{{students.student_number}}</td>
        </tr>
        <tr>
            <td>LRN</td><td> {{students.reference_no}}</td>     
        </tr>
            
        </tbody>
    </table>  
    <?php if(isset($_GET['enroll'])) { ?>
    <table class="e-table ">
        <thead>
    
        </thead>
        <tbody>
        <tr>
            <td>Setting</td>
             <td>
                <select  class="e-control rounded" name="" ng-model="sett" id="" ng-change="enrollDetails()">
                    <option value="">Select Setting</option>
                    <option ng-repeat="setting in settings" value="{{setting.id}}" >
                    {{setting.description}}
                    </option>
                </select>
             </td>
        </tr>
        <tr>
            <td>Year Level</td>
            <td> 
                <select class="e-control rounded" name="" ng-model="year" id="" ng-init="studentYearlvl()" ng-change="enrollDetails()">
                    <option value="">Select Year Level</option>
                    <option ng-repeat="syear in years" value="{{syear.id}}">
                        {{syear.short_name}}
                    </option>
                </select>
            </td>     
        </tr>
            
        </tbody>

    </table>

         <?php } else { ?>
        
        
    
         <?php }  ?>

    <?php } ?>

    <?php if(isset($_GET['rc'])) { ?>
    <table class="e-table ">
        <thead>
    
        </thead>
        <tbody>
        <tr>
            <td>Setting</td>
             <td>
                <select  class="e-control rounded" name="" ng-model="sett" id="" ng-change="getReportCard()">
                    <option value="">Select Setting</option>
                    <option ng-repeat="setting in settings" value="{{setting.id}}" >
                    {{setting.description}}
                    </option>
                </select>
             </td>
        </tr>
       
            
        </tbody>

    </table>
    <?php }?>
    </div>

    <?php if(isset($_GET['stud'])){ ?>
    <div class="e-col-2">

    <center>
    <img  src="{{students.photo !== '' ? students.photo : '../_public/profile/user.png' }}" alt="" style="width:180px;height:180px;">
    <!-- <button class="e-btn primary small rounded"  style="width:80%"><i class="fa fa-upload"></i> upload photo</button> -->
    <input ng-click="havephoto()" type="file" name="file" id="file" class="e-btn primary small rounded" style="width:80%" >
    <input ng-if="have" type='button' value='Upload' class="e-btn primary small rounded" id='upload' ng-click='upload()' style="width:80%"></center>
    </div>
    
    <?php } else {}?>

</div>

