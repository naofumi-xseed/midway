
<h4 class="pt-3 pl-3">Student List</h4>
<div class="e-cols p-3 e-cols pl-3">

<div class="e-col-8 e-form-group-unified   pt-3">


<select name="sets" id="yo" ng-model="setting" class="e-control" style="max-width:300px;" ng-change="studentList()">
<option value="" >Filter by settings</option>
<option value="{{set.id}}" ng-repeat="set in settings">{{set.description}}</option>
</select>

<select  ng-init="studentYearlvl()" ng-model="yr" style="max-width:300px;" class="e-control" ng-change="studentList()">
    <option value="" >Filter by Year LvL</option>
<option ng-repeat="year in years" value="{{year.id}}">{{year.short_name}}</option>
</select>

<select  ng-init="studentStrand()" name="course" style="max-width:300px;" id="search" class="e-control" ng-model="stran" ng-change="studentList()">
<option value="">Filter by Strand</option>
<option ng-repeat="str in strands" value="{{str.id}}" >{{str.code}}</option>
</select>
</div>

<div class="e-col-4 e-form-group-unified pt-3">
<select name="status" id="" ng-model="status" style="max-width:200px;" class="e-control" ng-change="studentList()">
<option value="">Filter by Status</option>
<option value="Registered">Registered</option>
<option value="Assessed">Assessed</option>
<option value="Withdraw">Withdraw</option>
<option value="Enrolled">Enrolled</option>
</select>


<input id='search' class="e-control" type='text' style="max-width:200px;" placeholder="@Search..." ng-model='search'>
</div>

</div>

{{setting}}

<div class="pl-2 pb-3">
  <dir-pagination-controls max-size="10" direction-links="true" boundary-links="true">
    </dir-pagination-controls>
</div>


<div class="e-cols p-3 e-cols pl-3">
<table class="e-table  e-x shadow-5 ">
  <thead>
    <tr>
      <th>NO</th>
      <th>Student ID</th>
      <th>Student Name</th>
      <th>Strand</th>
      <th>Year</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody ng-init="studentList()">
    <tr dir-paginate="s in students | filter : search  | itemsPerPage:10">
      <td>{{$index+1}}</td>
      <td>{{s.student_number}}</td>
      <td><a href="?student={{s.stud_id}}&stud&t=1">{{s.first_name}} {{s.middle_name}} {{s.last_name}}</a></td>
      <td>{{s.name}}</td>
      <td>{{s.short_name}}</td>
      <td>{{s.enrollment_status}}</td>
      <td><button ng-show="s.enrollment_status=='ASSESSED'" class="e-btn  small rounded blue-gradient" ng-click="withdraw(s.setting_id,s.st_id,s.yl_id,s.stud_id)">Widthraw</button></td>
      </td>  
    </tr>
  </tbody>
</table>
</div>

