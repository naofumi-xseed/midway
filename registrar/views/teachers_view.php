
<div class="e-cols  px-1 py-2">

<div class="e-col-12 pl-3">
<div >
       <input class="e-control rounded" type="text" ng-model="search" placeholder="Search" style="width: 500px;">
      <button ng-click="add()" class="e-btn  btn primary fa fa-plus pull-left rounded" ><span class=""></span></button>
    <dir-pagination-controls max-size="10" direction-links="true" boundary-links="true" >
    </dir-pagination-controls>
   </div>

</div>


<div class="e-cols px-2">



<div class="e-col-12">
<table class="e-table table-striped" style="text-transform: uppercase;">
  <thead style="color:#72bcd4">
    <th >Employee No</th>
    <th>Employee Name</th>
    <th>Actions</th>

  </thead>
<tbody id="list" ng-init="getTeachers()">
                  
<tr dir-paginate="teacher in teachers | filter : search  | itemsPerPage:10 ">
                             <td><b>{{teacher.employee_no}}</b></td> 
                             <td>{{teacher.last_name}}, {{teacher.first_name}} {{teacher.middle_name}}</td>
                             <td ng-model="status=teacher.active==='0' ? 'Activate' : 'Deactivate' ">
                                <button  class='e-btn btn-xs btn primary small' style='color' ng-click="reset(teacher.id,teacher.employee_no)">
                                   Reset
                                </button>
                              <button class='e-btn btn-xs btn gray small' ng-click="editEmployee(teacher.id,teacher.employee_no,
                              teacher.first_name,teacher.middle_name,teacher.last_name,teacher.suffix)" ><i class='fa fa-pen'></i>
                              </button>
                              <button ng-if="teacher.active==0" class='e-btn btn-xs btn danger small' style='color' ng-click="activate(teacher.id)">
                                 {{status}}
                              </button>

                                 <button ng-if="teacher.active==1" class='e-btn btn-xs btn success small' style='color' ng-click="deact(teacher.id)">
                                 {{status}}
                              </button>
                            
                             </td>
</tr>
                        

   

</tbody>

</table>
</div>

</div>
</div>

       





<div id="modals" class="e-modal" >
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">{{mode}} Teacher</p>
  
      <button type="button" ng-click="cancelModal()" class="e-delete">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    <div class="e-modal-body">
    <!--Content-->
    <div class="e-cols">
                  <input type="hidden" name="id" ng-model="id">
                    <div class="e-col-3">
                    Employee NO.
                    </div>
                    <div class="e-col-9">
                    <input type="text" class="e-control" name="emp_no" ng-model="emp_no" required>
                    </div>
                </div><br>

                

                <div class="e-cols">
                <div class="e-col-3">
                    First Name
                    </div>
                    <div class="e-col-9">
                    <input type="text" class="e-control"  name="f_name" ng-model="f_name" required>
                    </div>
                </div> <br>

                <div class="e-cols">
                <div class="e-col-3">
                   Middle Name
                    </div>
                    <div class="e-col-9">
                    <input type="text" class="e-control" name="m_name" ng-model="m_name" required>
                    </div>
                </div> <br>


                <div class="e-cols">
                <div class="e-col-3">
                   Last Name
                    </div>
                    <div class="e-col-9">
                    <input type="text" class="e-control" name="l_name" ng-model="l_name" required>
                    </div>
                </div> <br>

                <div class="e-cols">
                <div class="e-col-3">
                    Suffix
                    </div>
                    <div class="e-col-9">
                    <input type="text" class="e-control" name="suffix" ng-model="suffix" required>
                    </div>
                </div> 
    <!--Content-->  
    </div>
    <footer class="e-modal-footer">
      <button class="e-btn inverted" ng-click="cancelModal()">Cancel</button>


      <button type='submit' id="rel" class="e-btn btn-success" ng-if="action==='insert'" ng-click="insertEmployee()"><i class="fa fa-check"></i> save</button>
        <button type='submit' id="rel" class="e-btn btn-success" ng-if="action==='edit'" ng-click="editNow()"><i class="fa fa-check"></i> save</button>
    </footer>
  </div>
</div>
